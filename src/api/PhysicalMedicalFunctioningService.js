import axios from 'axios';
import environment from './../environments/environment';

const PHYSICAL_MEDICAL_FUNCTIONING_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/physicalmedicalfunctioning";

class PhysicalMedicalFunctioningService {

    getPhysicalMedicalFunctionings(authHeader) {
        return axios.get(PHYSICAL_MEDICAL_FUNCTIONING_API_BASE_URL, authHeader);
    }

    createPhysicalMedicalFunctioning(physicalMedicalFunctioning, authHeader) {
        return axios.post(PHYSICAL_MEDICAL_FUNCTIONING_API_BASE_URL, physicalMedicalFunctioning, authHeader);
    }

    getPhysicalMedicalFunctioningById(patientId, authHeader) {
        console.log(patientId)
        return axios.get(PHYSICAL_MEDICAL_FUNCTIONING_API_BASE_URL + '/' + patientId, authHeader);
    }

    updatePhysicalMedicalFunctioning(physicalMedicalFunctioningId, physicalMedicalFunctioning, authHeader) {
        console.log(physicalMedicalFunctioningId)
        return axios.put(PHYSICAL_MEDICAL_FUNCTIONING_API_BASE_URL + '/' + physicalMedicalFunctioningId, physicalMedicalFunctioning, authHeader);
    }

    deletePhysicalMedicalFunctioning(patientId, authHeader) {
        return axios.delete(PHYSICAL_MEDICAL_FUNCTIONING_API_BASE_URL + '/' + patientId, authHeader);
    }

    getAllPhysicalMedicalFunctionings(patientId, visitId, authHeader) {
        console.log(patientId, visitId)
        return axios.get(PHYSICAL_MEDICAL_FUNCTIONING_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);

    }
}

export default new PhysicalMedicalFunctioningService();