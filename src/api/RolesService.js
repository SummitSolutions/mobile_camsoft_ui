import axios from 'axios';
import environment from '../environments/environment';

const ROLES_API_BASE_URL = environment.apiUrlForUserManagementMoudle;

class RolesService {

    getRoles(authHeader){
        return axios.get(ROLES_API_BASE_URL + '/roles', authHeader);
    }

    createRole(role, authHeader){
        return axios.post(ROLES_API_BASE_URL + '/createRole', role, authHeader);
    }

    getRoleById(roleId, authHeader){
        return axios.get(ROLES_API_BASE_URL + '/roles/' + roleId, authHeader);
    }

    updateRole(role, roleId, authHeader){
        return axios.put(ROLES_API_BASE_URL + '/updateRole' + roleId, role, authHeader);
    }

    deleteRoles(roleId, authHeader){
        return axios.delete(ROLES_API_BASE_URL + '/deleteRole' + roleId, authHeader);
    }

    assignRoles(userId, role, authHeader)
    {
        return axios.get(ROLES_API_BASE_URL + '/assignRoles/' + userId + '/' + role, authHeader)
    }

    removeRoles(userId, role, authHeader)
    {
        return axios.get(ROLES_API_BASE_URL + '/removeRoles/' + userId + '/' + role, authHeader)
    }

    addPermissionsToRole(roleName, permissions, authHeader)
    {
        return axios.get(ROLES_API_BASE_URL + '/addPermissionsToRole/' + roleName + '/' + permissions, authHeader)
    }

    removePermissionsFromRole(roleName, permissions, authHeader)
    {
        return axios.get(ROLES_API_BASE_URL + '/removePermissionsFromRole/' + roleName + '/' + permissions, authHeader)
    }
}

export default new RolesService();