import axios from 'axios';
import environment from './../environments/environment';

const EMOTIONAL_BEHAVIOUR_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/emotionalbehaviour";

class EmotionalBehaviourService {

    getEmotionalBehaviours(authHeader) {
        return axios.get(EMOTIONAL_BEHAVIOUR_API_BASE_URL, authHeader);
    }

    createEmotionalBehaviour(emotionalBehaviour, authHeader) {
        return axios.post(EMOTIONAL_BEHAVIOUR_API_BASE_URL, emotionalBehaviour, authHeader);
    }

    getEmotionalBehaviourById(patientId, authHeader) {
        console.log(patientId)
        return axios.get(EMOTIONAL_BEHAVIOUR_API_BASE_URL + '/' + patientId, authHeader);
    }

    updateEmotionalBehaviour(emotionalBehaviourId, emotionalBehaviour, authHeader) {
        console.log(emotionalBehaviourId)
        return axios.put(EMOTIONAL_BEHAVIOUR_API_BASE_URL + '/' + emotionalBehaviourId, emotionalBehaviour, authHeader);
    }

    deleteEmotionalBehaviour(patientId, authHeader) {
        return axios.delete(EMOTIONAL_BEHAVIOUR_API_BASE_URL + '/' + patientId, authHeader);
    }

    getAllEmotionalBehaviours(patientId, visitId, authHeader) {
        console.log(patientId, visitId)
        return axios.get(EMOTIONAL_BEHAVIOUR_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);

    }
}

export default new EmotionalBehaviourService();