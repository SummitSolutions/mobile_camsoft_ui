import axios from 'axios';
import environment from './../environments/environment';

const DIAGNOSIS_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/diagnosis";

class DiagnosisService {

    getDiagnosis(authHeader) {
        return axios.get(DIAGNOSIS_API_BASE_URL, authHeader);
    }

    getDiagnosisById(patientId, authHeader) {
        return axios.get(DIAGNOSIS_API_BASE_URL + '/' + patientId, authHeader);
    }

    getDiagnosisDataById(patientId, visitId, authHeader) {
        console.log(patientId, visitId)
        return axios.get(DIAGNOSIS_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);
    }

    createDiagnosis(diagnosis, authHeader) {
        return axios.post(DIAGNOSIS_API_BASE_URL, diagnosis, authHeader);
    }

    updateDiagnosis(diagnosisId, diagnosis, authHeader) {
        return axios.put(DIAGNOSIS_API_BASE_URL + '/' + diagnosisId, diagnosis, authHeader);
    }

    deleteDiagnosis(diagnosisId, authHeader) {
        return axios.delete(DIAGNOSIS_API_BASE_URL + '/' + diagnosisId, authHeader);
    }

    getDiagnosisByDiagnosisId(patientId, visitId, diagnosisId, authHeader) {
        return axios.get(DIAGNOSIS_API_BASE_URL + '/' + patientId + '/' + visitId + '/' + diagnosisId, authHeader);
    }

}

export default new DiagnosisService()