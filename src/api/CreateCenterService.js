import axios from 'axios';
import environment from './../environments/environment';

const CENTER_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/centers";

class CreateCenterService {

    getCenters(authHeader) {
        return axios.get(CENTER_API_BASE_URL, authHeader);
    }

    createCenter(center, authHeader) {
        return axios.post(CENTER_API_BASE_URL, center, authHeader);
    }

    getCenterById(centerId, authHeader) {
        console.log(centerId)
        return axios.get(CENTER_API_BASE_URL + '/' + centerId, authHeader);
    }

    updateCenter(centerId, center, authHeader) {
        console.log(centerId)
        return axios.put(CENTER_API_BASE_URL + '/' + centerId, center, authHeader);
    }

    deleteCenter(centerId, authHeader) {
        return axios.delete(CENTER_API_BASE_URL + '/' + centerId, authHeader);
    }

}

export default new CreateCenterService();