import axios from 'axios';
import environment from './../environments/environment';

const LOCATION_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/locations";

class LocationService {

    getLocations(authHeader){
        return axios.get(LOCATION_API_BASE_URL, authHeader);
    }

    createLocation(location, authHeader)
    {
        return axios.post(LOCATION_API_BASE_URL , location, authHeader);
    }

    getLocationById(locationId, authHeader){
        console.log(locationId)
        return axios.get(LOCATION_API_BASE_URL + '/' + locationId, authHeader);
    }

    updateLocation(location, locationId, authHeader){
        return axios.put(LOCATION_API_BASE_URL + '/' + locationId, location, authHeader);
    }

    deleteLocation(locationId, authHeader){
        return axios.delete(LOCATION_API_BASE_URL + '/' + locationId, authHeader);
    }
}

export default new LocationService();