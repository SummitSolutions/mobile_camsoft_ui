import axios from 'axios';
import environment from './../environments/environment';

const MEDICATION_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/medication";

class MedicationService {

    getMedication(authHeader){
        return axios.get(MEDICATION_API_BASE_URL, authHeader);
    }

    getMedicationById(patientId, authHeader){
        return axios.get(MEDICATION_API_BASE_URL + '/' + patientId, authHeader);
    }

    getMedicationDataById(patientId,visitId, authHeader){
        return axios.get(MEDICATION_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);
    }

    createMedication(medication, authHeader){
        return axios.post(MEDICATION_API_BASE_URL, medication, authHeader);
    }

    updateMedication(medicationId, medication,  authHeader){
        return axios.put(MEDICATION_API_BASE_URL + '/' + medicationId, medication, authHeader);
    }

    deleteMedication(medicationId, authHeader){
        return axios.delete(MEDICATION_API_BASE_URL + '/' + medicationId, authHeader);
    }

}

export default new MedicationService()