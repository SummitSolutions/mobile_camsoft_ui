import axios from 'axios';
import environment from './../environments/environment';

const USERS_API_BASE_URL = environment.apiUrlForUserManagementMoudle;

class UsersService {

    getUsers(authHeader){
        return axios.get(USERS_API_BASE_URL+ '/users', authHeader);
    }

    createUser(user, authHeader){
        return axios.post(USERS_API_BASE_URL + '/createUser', user, authHeader);
    }

    getUserById(userId, authHeader){
        return axios.get(USERS_API_BASE_URL + '/users/' + userId, authHeader);
    }

    updateUsers(user, userId, authHeader){
        return axios.put(USERS_API_BASE_URL + '/updateUser' + userId, user, authHeader);
    }

    deleteUsers(userId, authHeader){
        return axios.delete(USERS_API_BASE_URL + '/' + userId, authHeader);
    }
}

export default new UsersService();