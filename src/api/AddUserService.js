import axios from 'axios';
import environment from './../environments/environment';

const USERS_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/adduser";

class AddUserService {

    getUsers(authHeader) {
        return axios.get(USERS_API_BASE_URL, authHeader);
    }

    createUser(user, authHeader) {
        return axios.post(USERS_API_BASE_URL, user, authHeader);
    }

    getUserById(userId, authHeader) {
        console.log(userId)
        return axios.get(USERS_API_BASE_URL + '/' + userId, authHeader);
    }

    updateUser(userId, user, authHeader) {
        console.log(userId)
        return axios.put(USERS_API_BASE_URL + '/' + userId, user, authHeader);
    }

    deleteUser(userId, authHeader) {
        return axios.delete(USERS_API_BASE_URL + '/' + userId, authHeader);
    }

    getUserByCenterId(centerId, authHeader) {
        console.log(centerId)
        return axios.get(USERS_API_BASE_URL + '/getBy-userId/' + centerId, authHeader);
    }

}

export default new AddUserService();