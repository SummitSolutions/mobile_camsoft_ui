import axios from 'axios';
import environment from './../environments/environment';

const SUBSTANCE_DETAIL_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/substancedetail";

class SubstanceDetailService {

    getSubstanceDetails(authHeader) {
        return axios.get(SUBSTANCE_DETAIL_API_BASE_URL, authHeader);
    }

    getSubstanceDetailById(patientId, authHeader) {
        return axios.get(SUBSTANCE_DETAIL_API_BASE_URL + '/' + patientId, authHeader);
    }

    createSubstanceDetail(substanceDetail, authHeader) {
        return axios.post(SUBSTANCE_DETAIL_API_BASE_URL, substanceDetail, authHeader);
    }

    updateSubstanceDetail(sustanceDetailId, substanceDetail, authHeader) {
        return axios.put(SUBSTANCE_DETAIL_API_BASE_URL + '/' + sustanceDetailId, substanceDetail, authHeader);
    }

    deleteSubstanceDetail(patientId, authHeader) {
        return axios.delete(SUBSTANCE_DETAIL_API_BASE_URL + '/' + patientId, authHeader);
    }

    getAllSubstanceDetail(patientId, visitId, authHeader) {
        return axios.get(SUBSTANCE_DETAIL_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);
    }

    getSubstanceDetailForSubstance(patientId, visitId, substanceUseId, authHeader) {
        return axios.get(SUBSTANCE_DETAIL_API_BASE_URL + '/' + patientId + '/' + visitId + '/' + substanceUseId, authHeader);
    }
}

export default new SubstanceDetailService();