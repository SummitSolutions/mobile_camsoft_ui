import axios from 'axios';
import environment from './../environments/environment';

const INVESTIGATION_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/investigation";     

class InvestigationService {

    getInvestigations(authHeader){
        console.log(INVESTIGATION_API_BASE_URL)
        return axios.get(INVESTIGATION_API_BASE_URL, authHeader);
    }

    createInvestigation(investigation, authHeader){
        return axios.post(INVESTIGATION_API_BASE_URL, investigation, authHeader);
    }

    getInvestigationById(patientId, authHeader){
        console.log(patientId)
        return axios.get(INVESTIGATION_API_BASE_URL + '/' + patientId, authHeader);
    }

    updateInvestigation(investigationId, investigation, authHeader){
        console.log(investigationId)
        return axios.put(INVESTIGATION_API_BASE_URL + '/' + investigationId, investigation, authHeader);
    }

    deleteInvestigation(patientId, authHeader){
        return axios.delete(INVESTIGATION_API_BASE_URL + '/' + patientId, authHeader);
    }

    getAllInvestigations(patientId, visitId, authHeader){
        console.log(patientId, visitId)
        return axios.get(INVESTIGATION_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);

    }
}

export default new InvestigationService();