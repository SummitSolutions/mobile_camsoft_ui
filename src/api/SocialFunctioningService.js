import axios from 'axios';
import environment from './../environments/environment';

const SOCIAL_FUNCTIONING_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/socialfunctioning";

class SocialFunctioningService {

    getSocialFunctionings(authHeader) {
        return axios.get(SOCIAL_FUNCTIONING_API_BASE_URL, authHeader);
    }

    createSocialFunctioning(socialFunctioning, authHeader) {
        return axios.post(SOCIAL_FUNCTIONING_API_BASE_URL, socialFunctioning, authHeader);
    }

    getSocialFunctioningById(patientId, authHeader) {
        console.log(patientId)
        return axios.get(SOCIAL_FUNCTIONING_API_BASE_URL + '/' + patientId, authHeader);
    }

    updateSocialFunctioning(socialFunctioningId, socialFunctioning, authHeader) {
        console.log(socialFunctioningId)
        return axios.put(SOCIAL_FUNCTIONING_API_BASE_URL + '/' + socialFunctioningId, socialFunctioning, authHeader);
    }

    deleteSocialFunctioning(patientId, authHeader) {
        return axios.delete(SOCIAL_FUNCTIONING_API_BASE_URL + '/' + patientId, authHeader);
    }

    getAllSocialFunctionings(patientId, visitId, authHeader) {
        console.log(patientId, visitId)
        return axios.get(SOCIAL_FUNCTIONING_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);

    }
}

export default new SocialFunctioningService();