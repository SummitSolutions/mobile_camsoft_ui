import axios from 'axios';
import environment from './../environments/environment';

const PSYCHOTHERAPY_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/psychotherapy";     

class PsychotherapyService {

    getPsychotherapys(authHeader){
        return axios.get(PSYCHOTHERAPY_API_BASE_URL, authHeader);
    }

    createPsychotherapy(psychotherapy, authHeader){
        return axios.post(PSYCHOTHERAPY_API_BASE_URL, psychotherapy, authHeader);
    }

    getPsychotherapyById(patientId, authHeader){
        console.log(patientId)
        return axios.get(PSYCHOTHERAPY_API_BASE_URL + '/' + patientId, authHeader);
    }

    updatePsychotherapy(psychotherapyId, psychotherapy, authHeader){
        console.log(psychotherapyId)
        return axios.put(PSYCHOTHERAPY_API_BASE_URL + '/' + psychotherapyId, psychotherapy, authHeader);
    }

    deletePsychotherapy(patientId, authHeader){
        return axios.delete(PSYCHOTHERAPY_API_BASE_URL + '/' + patientId, authHeader);
    }

    getAllPsychotherapys(patientId, visitId, authHeader){
        console.log(patientId, visitId)
        return axios.get(PSYCHOTHERAPY_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);

    }
}

export default new PsychotherapyService();