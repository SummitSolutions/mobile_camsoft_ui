import axios from 'axios';
import environment from './../environments/environment';

const MEDICATION_DATA_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/medicationdata";

class MedicationDataService {

    getMedicationData(authHeader){
        return axios.get(MEDICATION_DATA_API_BASE_URL, authHeader);
    }
}

export default new MedicationDataService()