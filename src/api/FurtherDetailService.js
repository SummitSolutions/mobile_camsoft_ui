import axios from 'axios';
import environment from './../environments/environment';

const FURTHER_DETAIL_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/furtherdetail";     

class FurtherDetailService {

    getFurtherDetails(authHeader){
        console.log(FURTHER_DETAIL_API_BASE_URL)
        return axios.get(FURTHER_DETAIL_API_BASE_URL, authHeader);
    }

    createFurtherDetail(furtherDetail, authHeader){
        return axios.post(FURTHER_DETAIL_API_BASE_URL, furtherDetail, authHeader);
    }

    getFurtherDetailById(patientId, authHeader){
        console.log(patientId)
        return axios.get(FURTHER_DETAIL_API_BASE_URL + '/' + patientId, authHeader);
    }

    updateFurtherDetail(furtherDetailId, furtherDetail, authHeader){
        console.log(furtherDetailId)
        return axios.put(FURTHER_DETAIL_API_BASE_URL + '/' + furtherDetailId, furtherDetail, authHeader);
    }

    deleteFurtherDetail(patientId, authHeader){
        return axios.delete(FURTHER_DETAIL_API_BASE_URL + '/' + patientId, authHeader);
    }

    getAllFurtherDetails(patientId, visitId, authHeader){
        console.log(patientId, visitId)
        return axios.get(FURTHER_DETAIL_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);

    }
}

export default new FurtherDetailService();