import axios from 'axios';
import environment from './../environments/environment';

const CURRENT_FUNCTIONING_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/currentfunctioning";

class CurrentFunctioningService {

    getCurrentFunctionings(authHeader) {
        return axios.get(CURRENT_FUNCTIONING_API_BASE_URL, authHeader);
    }

    createCurrentFunctioning(currentFunctioning, authHeader) {
        return axios.post(CURRENT_FUNCTIONING_API_BASE_URL, currentFunctioning, authHeader);
    }

    getCurrentFunctioningById(patientId, authHeader) {
        console.log(patientId)
        return axios.get(CURRENT_FUNCTIONING_API_BASE_URL + '/' + patientId, authHeader);
    }

    updateCurrentFunctioning(currentFunctioningId, currentFunctioning, authHeader) {
        console.log(currentFunctioningId)
        return axios.put(CURRENT_FUNCTIONING_API_BASE_URL + '/' + currentFunctioningId, currentFunctioning, authHeader);
    }

    deleteCurrentFunctioning(patientId, authHeader) {
        return axios.delete(CURRENT_FUNCTIONING_API_BASE_URL + '/' + patientId, authHeader);
    }

    getAllCurrentFunctionings(patientId, visitId, authHeader) {
        console.log(patientId, visitId)
        return axios.get(CURRENT_FUNCTIONING_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);

    }
}

export default new CurrentFunctioningService();