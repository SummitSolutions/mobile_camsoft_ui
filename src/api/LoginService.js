import axios from 'axios';
import environment from './../environments/environment';
const LOGIN_API_BASE_URL = environment.apiUrlForUserManagementMoudle;

class LoginService {

    login(user){
        return axios.post(LOGIN_API_BASE_URL + '/login', user);
    }

    loggedUser(authHeader)
    {
        return axios.get(LOGIN_API_BASE_URL + '/loggedUser', authHeader);
    }

    logout(authHeader)
    {
        return axios.get(LOGIN_API_BASE_URL + '/logout', authHeader);
    }
}

export default new LoginService();