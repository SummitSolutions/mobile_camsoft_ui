import axios from 'axios';
import environment from './../environments/environment';

const SCREENING_PROFORMA_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/screeningproforma";     

class ScreeningProformaService {

    getScreeningProformas(authHeader){
        console.log(SCREENING_PROFORMA_API_BASE_URL)
        return axios.get(SCREENING_PROFORMA_API_BASE_URL, authHeader);
    }

    createScreeningProforma(screeningProforma, authHeader){
        return axios.post(SCREENING_PROFORMA_API_BASE_URL, screeningProforma, authHeader);
    }

    getScreeningProformaById(patientId, authHeader){
        console.log(patientId)
        return axios.get(SCREENING_PROFORMA_API_BASE_URL + '/' + patientId, authHeader);
    }

    updateScreeningProforma(screeningProformaId, screeningProforma, authHeader){
        console.log(screeningProformaId)
        return axios.put(SCREENING_PROFORMA_API_BASE_URL + '/' + screeningProformaId, screeningProforma, authHeader);
    }

    deleteScreeningProforma(patientId, authHeader){
        return axios.delete(SCREENING_PROFORMA_API_BASE_URL + '/' + patientId, authHeader);
    }

    getAllScreeningProformas(patientId, visitId, authHeader){
        console.log(patientId, visitId)
        return axios.get(SCREENING_PROFORMA_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);

    }
}

export default new ScreeningProformaService();