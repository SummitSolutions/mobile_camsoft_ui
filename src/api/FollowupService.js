import axios from 'axios';
import environment from '../environments/environment';

const FOLLOWUP_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/followup";

class FollowupService {

    get(authHeader) {
        return axios.get(FOLLOWUP_API_BASE_URL, authHeader);
    }

    createFollowup(followup, authHeader) {
        return axios.post(FOLLOWUP_API_BASE_URL, followup, authHeader);
    }

    getFollowupById(followupId, authHeader) {
        return axios.get(FOLLOWUP_API_BASE_URL + '/' + followupId, authHeader);
    }

    updateFollowup(followup, followupId, authHeader) {
        return axios.put(FOLLOWUP_API_BASE_URL + '/' + followupId, followup, authHeader);
    }

    deleteFollowup(followupId, authHeader) {
        return axios.delete(FOLLOWUP_API_BASE_URL + '/' + followupId, authHeader);
    }

    getAllFollowup(patientId, visitId, authHeader) {
        console.log(patientId, visitId)
        return axios.get(FOLLOWUP_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);

    }
}

export default new FollowupService();