import axios from 'axios';
import environment from './../environments/environment';

const SUBSTANCE_USE_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/substanceuse";     

class SubstanceUseService {

    getSubstanceUses(authHeader){
        console.log(SUBSTANCE_USE_API_BASE_URL)
        return axios.get(SUBSTANCE_USE_API_BASE_URL, authHeader);
    }

    createSubstanceUse(substanceUse, authHeader){
        return axios.post(SUBSTANCE_USE_API_BASE_URL, substanceUse, authHeader);
    }

    getSubstanceUseById(patientId, authHeader){
        console.log(patientId)
        return axios.get(SUBSTANCE_USE_API_BASE_URL + '/' + patientId, authHeader);
    }

    updateSubstanceUse(sustanceUseId, substanceUse, authHeader){
        return axios.put(SUBSTANCE_USE_API_BASE_URL + '/' + sustanceUseId, substanceUse, authHeader);
    }

    deleteSubstanceUse(patientId, authHeader){
        return axios.delete(SUBSTANCE_USE_API_BASE_URL + '/' + patientId, authHeader);
    }

    getAllSubstanceUse(patientId, visitId, authHeader){
        console.log(patientId, visitId)
        return axios.get(SUBSTANCE_USE_API_BASE_URL + '/' + patientId + '/' + visitId, authHeader);

    }
}

export default new SubstanceUseService();