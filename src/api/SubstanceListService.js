import axios from 'axios';
import environment from './../environments/environment';

const SUBSTANCE_LIST_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/substancelist";

class SubstanceListService {

    getSubstanceList(authHeader){
        return axios.get(SUBSTANCE_LIST_API_BASE_URL, authHeader);
    }

    createSubstanceList(substanceList, authHeader){
        return axios.post(SUBSTANCE_LIST_API_BASE_URL, substanceList, authHeader);
    }

    getSubstanceListById(id, authHeader){
        return axios.get(SUBSTANCE_LIST_API_BASE_URL + '/' + id, authHeader);
    }

    updateSubstanceList(id, substanceList, authHeader){
        return axios.put(SUBSTANCE_LIST_API_BASE_URL + '/' + id, substanceList, authHeader);
    }

    deleteSubstanceList(id, authHeader){
        return axios.delete(SUBSTANCE_LIST_API_BASE_URL + '/' + id, authHeader);
    }
}

export default new SubstanceListService();