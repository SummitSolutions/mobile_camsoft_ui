import axios from 'axios';
import environment from './../environments/environment';

const RESEARCH_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/research";

class ResearchService {

    getResearch(authHeader){
        return axios.get(RESEARCH_API_BASE_URL, authHeader);
    }
}

export default new ResearchService()