import axios from 'axios';
import environment from './../environments/environment';

const PATIENT_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/patients";

class PatientService {

    getPatients(authHeader) {
        return axios.get(PATIENT_API_BASE_URL, authHeader);
    }

    createPatient(patient, authHeader) {
        return axios.post(PATIENT_API_BASE_URL, patient, authHeader);
    }
    getPateintAndVisits(patientId, authHeader) {
        return axios.get(PATIENT_API_BASE_URL + '/patient-and-visits/' + patientId, authHeader);
    }

    getPatientById(patientId, authHeader) {
        return axios.get(PATIENT_API_BASE_URL + '/' + patientId, authHeader);
    }

    updatePatient(patient, patientId, authHeader) {
        return axios.put(PATIENT_API_BASE_URL + '/' + patientId, patient, authHeader);
    }

    deletePatient(patientId, authHeader) {
        return axios.delete(PATIENT_API_BASE_URL + '/' + patientId, authHeader);
    }
}

export default new PatientService();