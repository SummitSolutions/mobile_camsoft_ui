import axios from 'axios';
import environment from './../environments/environment';

const AFTERCARE_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/aftercare";

class AftercareService {

    getAftercares(authHeader){
        console.log(AFTERCARE_API_BASE_URL)
        return axios.get(AFTERCARE_API_BASE_URL, authHeader);
    }

    createAftercare(aftercare, authHeader){
        return axios.post(AFTERCARE_API_BASE_URL, aftercare, authHeader);
    }

    getAftercareById(patientId, authHeader){
        return axios.get(AFTERCARE_API_BASE_URL + '/' + patientId, authHeader);
    }

    updateAftercare(aftercare, patientId, authHeader){
        return axios.put(AFTERCARE_API_BASE_URL + '/' + patientId, aftercare, authHeader);
    }

    deleteAftercare(patientId, authHeader){
        return axios.delete(AFTERCARE_API_BASE_URL + '/' + patientId, authHeader);
    }
}

export default new AftercareService();