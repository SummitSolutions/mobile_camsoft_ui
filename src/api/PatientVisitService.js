import axios from 'axios';
import environment from './../environments/environment';

const PATIENT_VISIT_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/patientvisit";

class PatientVisitService {

    getPatientVisits(authHeader) {
        return axios.get(PATIENT_VISIT_API_BASE_URL, authHeader);
    }

    createPatientVisit(patientVisit, authHeader) {
        return axios.post(PATIENT_VISIT_API_BASE_URL, patientVisit, authHeader);
    }

    getPatientVisitById(patientId, authHeader) {
        return axios.get(PATIENT_VISIT_API_BASE_URL + '/' + patientId, authHeader);
    }

    getVisitsAndFollowups(visitId, authHeader) {
        return axios.get(PATIENT_VISIT_API_BASE_URL + '/visits-and-followups/' + visitId, authHeader);
    }

    updatePatientVisit(patientVisit, patientId, authHeader) {
        return axios.put(PATIENT_VISIT_API_BASE_URL + '/' + patientId, patientVisit, authHeader);
    }

    deletePatientVisit(patientId, authHeader) {
        return axios.delete(PATIENT_VISIT_API_BASE_URL + '/' + patientId, authHeader);
    }
}

export default new PatientVisitService();