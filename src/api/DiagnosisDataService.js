import axios from 'axios';
import environment from './../environments/environment';

const DIAGNOSIS_DATA_API_BASE_URL = environment.apiUrlForUserManagementMoudle + "/diagnosisdata";

class DiagnosisDataService {

    getDiagnosisData(authHeader){
        return axios.get(DIAGNOSIS_DATA_API_BASE_URL, authHeader);
    }
}

export default new DiagnosisDataService()