const envMode = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') ? false : true;
const apiUrlForPatientMoudle = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') ? 'http://localhost:5000/patient/api/v1' : 'http://ec2-52-66-159-56.ap-south-1.compute.amazonaws.com/api/v1/';
const apiUrlForUserManagementMoudle = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') ? 'http://localhost:5000/api/v1' : 'http://ec2-52-66-159-56.ap-south-1.compute.amazonaws.com/api/v1/';

const environment = {
    production: envMode,
    appUrl: 'http://localhost:3000/',
    apiUrlForPatientMoudle: apiUrlForPatientMoudle,
    apiUrlForUserManagementMoudle: apiUrlForUserManagementMoudle,
    siteKey: '',
    secretKey: ''
};

export default environment;