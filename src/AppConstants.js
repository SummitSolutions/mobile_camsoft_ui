import { faMale } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// Outlined = {'id' : 'outlined-basic', 'variant' : 'outlined'}
// Filled = {'id' : 'filled-basic', 'variant' : 'filled'}
// standard {'id' : 'standard-basic', 'variant' : 'standard'}
export const INPUT_FLOAT_LABEL = { id: 'standard-basic', variant: 'standard' };
export const buttonOutlined = 'outline';
export const buttonShape = 'pill';

export const GENDER_OPTIONS = [
    { 'id': 'Male', 'label': 'Male', 'value': 'Male' },
    { 'id': 'Female', 'label': 'Female', 'value': 'Female' },
    { 'id': 'Other', 'label': 'Other', 'value': 'Other' }
];
export const LANGUAGES = ['Kannada', 'English', 'Hindi'];
export const LANGUAGESKNOWN = ['Kannada', 'English', 'Hindi', 'Telugu', 'Others'];
export const MARITAL_STATUS = ['Married', 'Un-married', 'Seperated due to drug abuse', 'Divorced/Seperated', 'Widow/Widower', 'Live in relationship'];
export const PICKER_DEFAULT_DATE = "2000-01-01";
export const ITEMS_PER_PAGE = 10;
export const DELETE_BTN_COLOR = "danger";
export const EDIT_BTN_COLOR = "secondary";
export const SUBMIT_BTN_COLOR = "info";
export const VIEW_BTN_COLOR = "info";
export const buttonVariant = 'outline';
export const shape = 'pill';

export const PROFORMA_TYPE = {
    first_visit: 'First Visit',
    follow_up: "Follow Up"
}

export const VISIT_MODES = {
    op: "OP",
    ip: "IP"
}

export const SUBSTANCE_USE_TABS = [
    "Substance Use",
    "Lifetime Pattern",
    "Withdrawal Symptoms",
    "Substance Specific Compilation",
    "Previous Attempts of Abstinence"
]

// export const RECORD_TYPE_FULL_NAME = {
//     1: 'Screening',
//     2: 'OP-Detailed Workup',
//     3: 'Follow up',
//     4: 'OP-Brief Workup',
//     5: 'IP-Breif Workup',
//     6: 'IP-Detailed Workup',
//     7: 'Telephonic Followup',
//     8: 'Video Followup'
// };

// export const PROFORMA_TYPE = [
//     { 'id': RECORD_TYPE_FULL_NAME[1], 'label': RECORD_TYPE_FULL_NAME[1], 'value': RECORD_TYPE_FULL_NAME[1] },
//     { 'id': RECORD_TYPE_FULL_NAME[2], 'label': RECORD_TYPE_FULL_NAME[2], 'value': RECORD_TYPE_FULL_NAME[2] },
//     { 'id': RECORD_TYPE_FULL_NAME[3], 'label': RECORD_TYPE_FULL_NAME[3], 'value': RECORD_TYPE_FULL_NAME[3] },
//     { 'id': RECORD_TYPE_FULL_NAME[4], 'label': RECORD_TYPE_FULL_NAME[4], 'value': RECORD_TYPE_FULL_NAME[4] },
//     { 'id': RECORD_TYPE_FULL_NAME[5], 'label': RECORD_TYPE_FULL_NAME[5], 'value': RECORD_TYPE_FULL_NAME[5] },
//     { 'id': RECORD_TYPE_FULL_NAME[6], 'label': RECORD_TYPE_FULL_NAME[6], 'value': RECORD_TYPE_FULL_NAME[6] },
//     { 'id': RECORD_TYPE_FULL_NAME[7], 'label': RECORD_TYPE_FULL_NAME[7], 'value': RECORD_TYPE_FULL_NAME[7] },
//     { 'id': RECORD_TYPE_FULL_NAME[8], 'label': RECORD_TYPE_FULL_NAME[8], 'value': RECORD_TYPE_FULL_NAME[8] }
// ];

export const RECORD_TYPE_SHORT_NAME = {
    1: 'S',
    2: 'OP-D',
    3: 'F',
    4: 'OP-B',
    5: 'IP-B',
    6: 'IP-D',
    7: 'TF',
    8: 'VF'
};
export const Male = <FontAwesomeIcon icon={faMale} />
export const GENDER_SHORT_NAMES = {
    'Male': Male,
    'Female': 'F',
    "Other": 'O'
}

export const COUNTRIES = ['India', 'USA'];
export const STATES = {
    'India': ["Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu",
        "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala",
        "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Puducherry", "Punjab",
        "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal",
        "Andaman and Nicobar islands"],
    'USA': []
};

export const DEPARTMENTS = [
    { 'id': 'Department1', 'label': 'Department1', 'value': 'Department1' },
    { 'id': 'Department2', 'label': 'Department2', 'value': 'Department2' },
    { 'id': 'Department3', 'label': 'Department3', 'value': 'Department3' },
    { 'id': 'Department4', 'label': 'Department4', 'value': 'Department4' },
    { 'id': 'Department5', 'label': 'Department5', 'value': 'Department5' },
    { 'id': 'Department6', 'label': 'Department6', 'value': 'Department6' }
];

export const DOCTORS = ['Doctor1', 'Doctor2', 'Doctor3', 'Doctor4'];

export const UNITS = ['Units1', 'Units2', 'Units3', 'Units4', 'Units5'];

export const SAVE_BTN_COLOR = "primary";
export const CANCEL_BTN_COLOR = "warning";
export const ALTERNATECOUNTRIES = ['India', 'USA'];
export const ALTERNATESTATES = {
    'India': ["Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu",
        "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala",
        "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Puducherry", "Punjab",
        "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal",
        "Andaman and Nicobar islands"],
    'USA': []
};
export const ECONOMIC_STATUS = ['APL', 'BPL'];

export const EDUCATIONAL_STATUS = ['Nil/literate', 'Literate/No formal education', 'Primary(1-4 years)', 'Middle school (5-8 years)',
    'Secondary (8-10 years)', 'Higher Secondary(11-12)', 'Graduate', 'Post graduate and above', 'No information',
    'Vocational/Tech(ITI/Diploma)'];

export const OCCUPATION = ['Unemployed', 'Farmer', 'Unskilled', 'semiskilled(peon,domestic servant etc)', 'Skilled(plumber,welder)',
    'Clerical', 'Proffesional', 'Business', 'Student', 'Home maker', 'Doctor', 'Nurse', 'Manegerial'
    , 'health professional', 'retired', 'Pharmacist'];
export const REFERRAL = ['Self', 'Family', 'Physician', 'Psychiatrist', 'Employer', 'Police', 'Ex-patient', 'Religious Org',
    'CAM community team', 'Friend', 'Educational institute', 'TCC'];
export const EMPLOYMENT_STATUS = ['Never employed', 'Presently unemployed', 'part time employed', 'Full time employed', 'Self employed',
    'student', 'Home maker', 'Retired'];
export const LIVING_ARRANGEMENT = ['Joint family', 'Nuclear family', 'Alone', 'With friends', 'NGO', 'Prison'];
export const RELIGION = ['Hindu', 'Muslim', 'Jainism', 'Buddhism', 'Christian', 'Sikhism', 'Parsi', 'Not affiliated', 'Others'];
export const VALID_DATE = "2020-01-01";
export const REMINDERS = ['SMS', 'Email', 'Phone call'];
export const TYPEOFCENTRE = ['Public health care', 'Community hospital', 'District hospital'];


export const LOCATIONCOUNTRIES = ['India', 'USA'];
export const LOCATIONSTATES = {
    'India': ["Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu",
        "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala",
        "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Puducherry", "Punjab",
        "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal",
        "Andaman and Nicobar islands"],
    'USA': []
};
export const TELNO = ['Self Mobile Number', 'Alternate Mobile Number', 'Informant Mobile Number', 'Emergency Mobile Number'];
export const ADDRESS = ['Self Address', 'Alternate Address', 'Informant Address', 'Emergency Address'];
export const MODE = ['OP', 'SSW', 'IP', 'Call', 'Home Visit'];
export const CGI = ['Very much improved', 'Much improved', 'Minimally improved', 'No change', 'Minimally worse', 'Much worse', 'Very much worse']
export const RESEARCH = [''];
export const FOLLOWUPMODE = ['Followup At NIMHANS', 'Call Again', 'Home Visit'];
export const FOLLOWUP_DATE = "2020-01-01";
export const FOLLOWUP_HOLD = ['Followup on hold'];
export const REASON = ['Not interested', 'Expired', 'Recovered, No need of further treatment', 'Under treatment from other agency', 'Others'];
export const EXPIRY_DATE = "2020-01-01";
export const COMPLIANCE = [
    { 'id': 'Regular', 'label': 'Regular', 'value': 'Regular' },
    { 'id': 'Irregular', 'label': 'Irregular', 'value': 'Irregular' },
];
export const ADDSUBSTANCE = ['Alcohol', 'Caffeine', 'Opioids', 'Cannabinioids', 'Sedative/hypnotics', 'Cocaine',
    'Other stimulants', 'Hallucinogens', 'Inhalants', 'Tobacco', 'Any other substance',
    'Behavioral addiction'];
export const FREQUENCY = ['Daily/Almost daily', '3-4 times/week', '1-2 times/week', '1-3 times a month',
    'Less than once', 'Never in the last six months', 'Recreational use'];
export const PREDOMINANT = [];
//export const ALCOHOLTYPE =  ['']; 
export const CURRENT_PATTERN = [
    { 'id': 'Regular', 'label': 'Regular', 'value': 'Regular' },
    { 'id': 'Episodic', 'label': 'Episodic', 'value': 'Episodic' },
    { 'id': 'Infrequent', 'label': 'Infrequent', 'value': 'Infrequent' }
];
export const SYMPTOMS = ['Feel anxious', 'Irritable', 'Sweating', 'Headaches', 'Fidgety/ Restless', 'Feel physically weak'];
export const SYMPTOMS_ONE = ['Unable to Sleep', 'Feel depressed', 'Fast heart beat', 'Hands trembling', 'Nausea/ Vomiting',]
export const SEIZURE_TYPE = ['Gtcs', 'Cps', 'Cps Sec', 'Other'];
//export const SEIZURE_TYPE_ONE = ['Cps Sec', 'Other'];
export const PREVIOUS_TREATMENT = ['Yes', 'No', 'Not applicable'];
export const PREVIOUS_ATTEMPTS = ['General Medical', 'General Psychiatric', 'Self help groups',];
export const PREVIOUS_ATTEMPTS_ONE = ['Self Abstinence', 'Traditional healers'];
export const PREVIOUS_MEDICATION = ['Disulfiram', 'Naltraxone', 'SSRI', 'Methadone (Opioid)', 'NAC (Cannabis)'];
export const PREVIOUS_MEDICATION_ONE = ['Baclofen', 'Acamprosaate', 'Ondensetron', 'Buprenorphine (Opioid)', 'No medication'];
export const MEDICATION_ABSTINENCE = ['Disulfiram', 'Naltraxone', 'SSRI', 'Methadone (Opioid)', 'NAC (Cannabis)'];
export const MEDICATION_ABSTINENCE_ONE = ['Baclofen', 'Acamprosaate', 'Ondensetron', 'Buprenorphine (Opioid)', 'No medication'];
export const ABSTINENCE_TIME = ['Days', 'Weeks', 'Months', 'Years'];
export const SEVERITY = [
    { 'id': 'Usual', 'label': 'Usual', 'value': 'Usual' },
    { 'id': 'Significant', 'label': 'Significant', 'value': 'Significant' },
    { 'id': 'Severe', 'label': 'Severe', 'value': 'Severe' }];
export const PHYSICAL_FREQUENCY =
    [{ 'id': 'None', 'label': 'None', 'value': 'None' },
    { 'id': 'Frequent', 'label': 'Frequent', 'value': 'Frequent' },
    { 'id': 'Infrequent', 'label': 'Infrequent', 'value': 'Infrequent' }];
export const HEAD_INJURY = [{ 'id': 'Required Hospital Admission', 'label': 'Required Hospital Admission', 'value': 'Required Hospital Admission' },
{ 'id': 'Required Outpatient Care', 'label': 'Required Outpatient Care', 'value': 'Required Outpatient Care' },
{ 'id': 'No Help Sought', 'label': 'No Help Sought', 'value': 'No Help Sought' }];
export const ATTEMPTS = [{ 'id': 'None', 'label': 'None', 'value': 'None' },
{ 'id': 'Single', 'label': 'Single', 'value': 'Single' },
{ 'id': 'Multiple', 'label': 'Multiple', 'value': 'Multiple' }];
export const LAST_ATTEMPT = [{ 'id': 'Low', 'label': 'Low', 'value': 'Low' },
{ 'id': 'High', 'label': 'High', 'value': 'High' }];
export const DRUG_ABSUSE = [{ 'id': 'Yes', 'label': 'Yes', 'value': 'Yes' },
{ 'id': 'No', 'label': 'No', 'value': 'No' },
{ 'id': 'Not applicable', 'label': 'Not applicable', 'value': 'Not applicable' }];
export const DURATION = ['Hours', 'Days', 'Weeks', 'Months', 'Years'];
export const ROUTE_OF_ADMINISTRATION =
    [{ 'id': 'IV', 'label': 'IV', 'value': 'IV' },
    { 'id': 'IM', 'label': 'IM', 'value': 'IM' },
    { 'id': 'SC', 'label': 'SC', 'value': 'SC' },
    { 'id': 'Not known ', 'label': 'Not known ', 'value': 'Not known ' },
    { 'id': 'Not applicable', 'label': 'Not applicable', 'value': 'Not applicable' }
    ];
export const CIRCUMSTANCE_USE =
    [{ 'id': 'Usually Alone', 'label': 'Usually Alone', 'value': 'Usually Alone' },
    { 'id': ' Usually In Company', 'label': ' Usually In Company', 'value': ' Usually In Company' },
    { 'id': 'No Fixed Pattern', 'label': 'No Fixed Pattern', 'value': 'No Fixed Pattern' }];
export const SHARING_SYRINGE = [{ 'id': 'Yes', 'label': 'Yes', 'value': 'Yes' },
{ 'id': 'No', 'label': 'No', 'value': 'No' },
{ 'id': 'Not applicable', 'label': 'Not applicable', 'value': 'Not applicable' }];
export const CURRENT_USE = [{ 'id': 'Yes', 'label': 'Yes', 'value': 'Yes' },
{ 'id': 'No', 'label': 'No', 'value': 'No' },
{ 'id': 'Not applicable', 'label': 'Not applicable', 'value': 'Not applicable' }];
export const PARAPHEMALIA = [{ 'id': 'Yes', 'label': 'Yes', 'value': 'Yes' },
{ 'id': 'No', 'label': 'No', 'value': 'No' },
{ 'id': 'Not applicable', 'label': 'Not applicable', 'value': 'Not applicable' }];
export const DEFINITIVE = ['Definitive', 'Tentative', 'Provisional'];
export const UOM = ['hours', 'days', 'weeks', 'months', 'years'];
export const FURTHER_ACTION = [{ 'id': 'Admission', 'label': 'Admission', 'value': 'Admission' },
{ 'id': 'Follow Up', 'label': 'Follow Up', 'value': 'Follow Up' },
{ 'id': 'Tele Follow Up', 'label': 'Tele Follow Up', 'value': 'Tele Follow Up' }];
export const WARD = [{ 'id': 'SSW', 'label': 'SSW', 'value': 'SSW' },
{ 'id': 'Ward', 'label': 'Ward', 'value': 'Ward' }];
export const NA = [{ 'id': 'Yes', 'label': 'Yes', 'value': 'Yes' },
{ 'id': 'No', 'label': 'No', 'value': 'No' },
{ 'id': 'NA', 'label': 'NA', 'value': 'NA' }];
export const USUAL = [{ 'id': 'Not more than usual', 'label': 'Not more than usual', 'value': 'Not more than usual' },
{ 'id': 'More than usual', 'label': 'More than usual', 'value': 'More than usual' },
{ 'id': 'Much more than usual', 'label': 'Much more than usual', 'value': 'Much more than usual' }];
export const DEFICTS = [{ 'id': 'NIL', 'label': 'NIL', 'value': 'NIL' },
{ 'id': 'SIGNIFICANT', 'label': 'SIGNIFICANT', 'value': 'SIGNIFICANT' },
{ 'id': 'SEVERE', 'label': 'SEVERE', 'value': 'SEVERE' }];
export const COGNITIVE_EMOTIONAL = [{ 'id': 'None', 'label': 'None', 'value': 'None' },
{ 'id': 'Loss of esteem/dysphoria', 'label': 'Loss of esteem/dysphoria', 'value': 'Loss of esteem/dysphoria' },
{ 'id': 'Loss of motivation', 'label': 'Loss of motivation', 'value': 'Loss of motivation' },
{ 'id': 'Despair', 'label': 'Despair', 'value': 'Despair' }];
export const ECONOMIC = [{ 'id': 'No impact', 'label': 'No impact', 'value': 'No impact' },
{ 'id': 'Significant impact on earnings', 'label': 'Significant impact on earnings', 'value': 'Significant impact on earnings' },
{ 'id': 'Severe impact', 'label': 'Severe impact', 'value': 'Severe impact' }];
export const OCCUPATIONAL = [{ 'id': 'None', 'label': 'None', 'value': 'None' },
{ 'id': 'Poor performance', 'label': 'Poor performance', 'value': 'Poor performance' },
{ 'id': 'Absenteeism', 'label': 'Absenteeism', 'value': 'Absenteeism' },
{ 'id': 'Job loss/discontinued studiesr', 'label': 'Job loss/discontinued studies', 'value': 'Job loss/discontinued studies' }];
export const SIGNIFICANT = [{ 'id': 'Not more than usual', 'label': 'Not more than usual', 'value': 'Not more than usual' },
{ 'id': ' Significant', 'label': ' Significant', 'value': ' Significant' },
{ 'id': 'Severe', 'label': 'Severe', 'value': 'Severe' }];
export const FREQUENT = [{ 'id': 'None', 'label': 'None', 'value': 'None' },
{ 'id': ' Frequent', 'label': ' Frequent', 'value': ' Frequent' },
{ 'id': 'Infrequent', 'label': 'Infrequent', 'value': 'Infrequent' }];
export const APPEARANCE = [{ 'id': 'No Abnormality noted', 'label': 'No Abnormality noted', 'value': 'No Abnormality noted' },
{ 'id': '  Abnormal', 'label': '  Abnormal', 'value': '  Abnormal' }];
export const PSYCOMOTOR = [{ 'id': 'Agitation', 'label': 'Agitation', 'value': 'Agitation' },
{ 'id': ' Retardation', 'label': ' Retardation', 'value': ' Retardation' },
{ 'id': 'Increased', 'label': 'Increased', 'value': 'Increased' },
{ 'id': 'Decreased', 'label': 'Decreased', 'value': 'Decreased' },
{ 'id': 'No Abnormality noted', 'label': 'No Abnormality noted', 'value': 'No Abnormality noted' }];
export const SPEECH = [{ 'id': 'Increased', 'label': 'Increased', 'value': 'Increased' },
{ 'id': 'Decreased', 'label': '  Decreased', 'value': '  Decreased' },
{ 'id': 'Irrelevant', 'label': 'Irrelevant', 'value': 'Irrelevant' },
{ 'id': 'Incoherent', 'label': 'Incoherent', 'value': 'Incoherent' },
{ 'id': ' No Abnormality noted', 'label': ' No Abnormality noted', 'value': ' No Abnormality noted' }];
export const MOOD = [{ 'id': 'Depressed', 'label': 'Depressed', 'value': 'Depressed' },
{ 'id': ' Euphoric', 'label': ' Euphoric', 'value': ' Euphoric' },
{ 'id': 'Perplexed', 'label': 'Perplexed', 'value': 'Perplexed' },
{ 'id': 'Irritable', 'label': 'Irritable', 'value': 'Irritable' },
{ 'id': ' Anxious', 'label': ' Anxious', 'value': ' Anxious' },
{ 'id': 'Fearful', 'label': 'Fearful', 'value': 'Fearful' },
{ 'id': 'No Abnormality noted', 'label': 'No Abnormality noted', 'value': 'No Abnormality noted' }];
export const MOTIVATION = [{ 'id': 'Pre-contemplation', 'label': 'Pre-contemplation', 'value': 'Pre-contemplation' },
{ 'id': 'Contemplation', 'label': 'Contemplation', 'value': 'Contemplation' },
{ 'id': 'Preparation', 'label': 'Preparation', 'value': 'Preparation' },
{ 'id': 'Action', 'label': 'Action', 'value': 'Action' },
{ 'id': 'Maintenance', 'label': 'Maintenance', 'value': 'Maintenance' },
{ 'id': ' No Abnormality noted', 'label': ' No Abnormality noted', 'value': ' No Abnormality noted' },];
export const OPIOIDS_TYPE = ['Prescription', 'Street', 'Brown sugar', 'Both', 'Buprenorphine', 'Codeine', 'Opium',
    'Pentazocine', 'Tramadol', 'tr', 'Morphin', 'Corex dx', 'pe', 'Heroin', 'Her', 'Tapentadaol'];
export const CANNABINIOIDS_TYPE = ['Natural', 'Synthetic', 'Both', 'Weed and Charas', 'Edible'];
export const SEDATIVE_TYPE = ['Benzodiazepine', 'Z drugs', 'Barbiturates', 'NITRAZEPAM 10 MG', 'Pentazocine', 'Diazepam', 'Lorazepam',
    'Alprazolam', 'Corex,fencidyl syp', 'Dicyclamine', 'Spasmo-Proxyvon', 'Quetiapine'];
export const COCAINE_TYPE = ['Cocaine', 'Amphetamine'];
export const STIMULANTS_TYPE = ['Cocaine', 'Amphetamine', 'Protvaine', 'c', 'LSD', 'MDMA'];
export const TOBACCO_TYPE = ['Chewable', 'Smoking', 'Both'];
export const ALCOHOL_UOM = ['ml', 'units'];
export const BEHAVIORAL_UOM = ['minutes', 'hours', 'No of times'];
export const CAFFEINE_UOM = ['cups'];
export const CANNABINIOIDS_UOM = ['Grams', 'Cigaretts', 'Small joints', 'Big joints', 'Chillums', 'Bong shots'];
export const COCAINE_UOM = ['Grams'];
export const OPIOIDS_UOM = ['Milligrams'];
export const STIMULANTS_UOM = ['grams', 'No of tablets'];
export const SEDATIVE_UOM = ['Milligrams'];
export const TOBACCO_UOM = ['Cigaretts', 'Bidis', 'Packets'];
export const FURTHER_REFERRAL = [{ 'id': 'Pre-General Hospital', 'label': 'General Hospital', 'value': 'General Hospital' },
{ 'id': 'N/Ns', 'label': 'N/Ns', 'value': 'N/Ns' },
{ 'id': 'NIMHANS General Psychiatry', 'label': 'NIMHANS General Psychiatry', 'value': 'NIMHANS General Psychiatry' },
{ 'id': 'Outside Agency', 'label': 'Outside Agency', 'value': 'Outside Agency' },
{ 'id': 'Other Psychiatry', 'label': 'Other Psychiatry', 'value': 'Other Psychiatry' }];
export const CGISCORE = ['Normal, not at all ill', 'Borderline Mentally ill', 'Mildly ill', 'Moderately ill',
    'Markedly ill', 'Severely ill', 'Extremly ill'];
export const HALLUCINATION_TYPE = ['Auditory', 'Visual', 'Tactile', 'Multimodal'];
export const CURRENT_FUNCTIONING_OPTIONS = [
    { 'id': 'Abstinent', 'label': 'Abstinent', 'value': 'Abstinent' },
    { 'id': 'Abstinent with Lapses', 'label': 'Abstinent with Lapses', 'value': 'Abstinent with Lapses' },
    { 'id': 'Reduced Use of Substance', 'label': 'Reduced Use of Substance', 'value': 'Reduced Use of Substance' },
    { 'id': 'Relapsed', 'label': 'Relapsed', 'value': 'Relapsed' },
    { 'id': 'No change since previous visit', 'label': 'No change since previous visit', 'value': 'No change since previous visit' },
];
export const RELAPSED_OPTIONS = ['Interpersonal', 'Intrapersonal', 'External'];
export const RELAPSED_DURATION = ['Days', 'Weeks', 'Months', 'Years'];
// export const HALLUCINATION_TYPE = ['Auditory','Visual','Tactile','Multimodal']; 
export const EMOTIONAL_BEHAVIOUR = [
    { 'id': 'Continue', 'label': 'Continue', 'value': 'Continue' },
    { 'id': 'Emergent', 'label': 'Emergent', 'value': 'Emergent' },
    { 'id': 'Improved', 'label': 'Improved', 'value': 'Improved' },
    { 'id': 'NA', 'label': 'NA', 'value': 'NA' },];
export const CURRENT_USE_FREQUENCY = ['Daily/Almost daily', '3-4 times/week', '1-2 times/week', '1-3 times a month',
    'Less than once', 'Never in the last six months', 'Never in last use'];