import * as pdfMakeLayoutConfigs from '../configs/PdfMakeLayoutConfig';
import * as pdfmakePrintService from '../pdfMakePrintService';
import * as logo from '../base-64-images/Logo';
import * as momentService from './../MomentService';
import { element } from 'prop-types';
import _ from 'lodash';

export const printSummary = (list) => {
    let data = getSummaryContent(list);
    pdfmakePrintService.printNow(data);

    // let data2 = getSocialSummaryContent(list);
    // pdfmakePrintService.printNow(data2);

}


export const getSummaryContent = (list) => {
    let content = [];

    content.push(pdfmakePrintService.getHeaderWithLogo());
    content.push(pdfmakePrintService.getPatientInformation(list));

    if (list.work) {
        content.push({ text: 'Followup Details', alignment: 'center', bold: true, fontSize: 13, decoration: 'underline', marginTop: 10 })
        content.push(getFollowupDetails(list));
    }
    return {
        header: function (currentPage, pageCount, pageSize) {
            return [
            ]
        },
        footer: function (currentPage, pageCount, pageSize) {
            return [{
                margin: [0, 20, 0, 0],
                canvas: [{ type: 'line', x1: 0, y1: 0, x2: pageSize.width, y2: 0, dash: { length: 5, space: 5 } }],
            },
            {
                columns:
                    [
                        { text: list.name + '_' + momentService.currentDate(), alignment: 'left', margin: [40, 3, 3, 40] },
                        { text: currentPage + ' of ' + pageCount, alignment: 'right', margin: [40, 3, 3, 40] },
                    ]
            }]
        },
        pageSize: pdfMakeLayoutConfigs.pageSize,
        pageOrientation: pdfMakeLayoutConfigs.pageOrientation,
        info: pdfMakeLayoutConfigs.getMetaData('PrescriptionPrint'),
        pageMargins: pdfMakeLayoutConfigs.pageMargins,
        content: content
    };
}
export const getValues = (data, list, numOfCols) => {
    let body = [];

    data.forEach(element => {
        if (list[element.fieldName]) {
            body.push({ text: element.title + ' : ', border: pdfMakeLayoutConfigs.getBorder('') });
            body.push({ text: list[element.fieldName], border: pdfMakeLayoutConfigs.getBorder(''), bold: true });
        }
    });

    console.log(data, body)
    let chuncks = _.chunk(body, numOfCols);
    let lenOfList = 0;
    if (chuncks.length > 0) {
        lenOfList = chuncks[chuncks.length - 1].length;
        if (lenOfList <= numOfCols) {
            for (let i = 0; i < (numOfCols - lenOfList); i++) {
                chuncks[chuncks.length - 1].push({ text: '', border: pdfMakeLayoutConfigs.getBorder('') });
            }
        }
    }
    return chuncks;
}


// export const getSocialSummaryContent = (list) => {
//     let content = [];

//     content.push(pdfmakePrintService.getHeaderWithLogo());
//     content.push(pdfmakePrintService.getPatientInformation(list));

//     if (list.work) {
//         content.push({ text: 'Current social functioning', alignment: 'center', bold: true, fontSize: 13, decoration: 'underline', marginTop: 10 })
//         content.push(getSocialDetails(list));
//     }


//     return {
//         header: function (currentPage, pageCount, pageSize) {
//             return [
//             ]
//         },
//         footer: function (currentPage, pageCount, pageSize) {
//             return [{
//                 margin: [0, 20, 0, 0],
//                 canvas: [{ type: 'line', x1: 0, y1: 0, x2: pageSize.width, y2: 0, dash: { length: 5, space: 5 } }],
//             },
//             {
//                 columns:
//                     [
//                         { text: list.name + '_' + momentService.currentDate(), alignment: 'left', margin: [40, 3, 3, 40] },
//                         { text: currentPage + ' of ' + pageCount, alignment: 'right', margin: [40, 3, 3, 40] },
//                     ]
//             }]
//         },
//         pageSize: pdfMakeLayoutConfigs.pageSize,
//         pageOrientation: pdfMakeLayoutConfigs.pageOrientation,
//         info: pdfMakeLayoutConfigs.getMetaData('PrescriptionPrint'),
//         pageMargins: pdfMakeLayoutConfigs.pageMargins,
//         content: content
//     };
// }

// export const getSocialValues = (data2, list, numOfCols) => {
//     let body = [];

//     data2.forEach(element => {
//         if (socialFunctionList[element.fieldName]) {
//             body.push({ text: element.title + ' : ', border: pdfMakeLayoutConfigs.getBorder('') });
//             body.push({ text: socialFunctionList[element.fieldName], border: pdfMakeLayoutConfigs.getBorder(''), bold: true });
//         }
//     });

//     console.log(2, body)
//     let chuncks = _.chunk(body, numOfCols);
//     let lenOfList = 0;
//     if (chuncks.length > 0) {
//         lenOfList = chuncks[chuncks.length - 1].length;
//         if (lenOfList <= numOfCols) {
//             for (let i = 0; i < (numOfCols - lenOfList); i++) {
//                 chuncks[chuncks.length - 1].push({ text: '', border: pdfMakeLayoutConfigs.getBorder('') });
//             }
//         }
//     }
//     return chuncks;
// }



export const getFollowupDetails = (list) => {

    let followupList = [
        { 'title': 'Work', 'fieldName': 'work' },
        { 'title': 'Marital', 'fieldName': 'marital' },
        { 'title': 'Family', 'fieldName': 'family' },
        { 'title': 'Friends', 'fieldName': 'friends' },
        { 'title': 'Neighbour', 'fieldName': 'neighbour' },
        { 'title': 'Na', 'fieldName': 'na_interpersonal' },
        { 'title': 'Divorce', 'fieldName': 'divorce' },
        { 'title': 'Criminal Case', 'fieldName': 'criminal_case' },
        { 'title': 'Na legal', 'fieldName': 'na_legal_issues' },
        { 'title': 'Notes', 'fieldName': 'notes' },
    ];

    return {
        style: 'tableExample header',
        table: {
            widths: ['auto', '*', 'auto', '*'],
            body: getValues(followupList, list, 4)
        }
    };
}

// export const getSocialDetails = (list) => {

//     let socialFunctionList = [
//         { 'title': 'Work', 'fieldName': 'work' },
//         { 'title': 'Marital', 'fieldName': 'marital' },
//         { 'title': 'Family', 'fieldName': 'family' },
//         { 'title': 'Friends', 'fieldName': 'friends' },
//         { 'title': 'Neighbour', 'fieldName': 'neighbour' },
//         { 'title': 'Na', 'fieldName': 'na_interpersonal' },
//         { 'title': 'Divorce', 'fieldName': 'divorce' },
//         { 'title': 'Criminal Case', 'fieldName': 'criminal_case' },
//         { 'title': 'Na legal', 'fieldName': 'na_legal_issues' },
//         { 'title': 'Notes', 'fieldName': 'notes' },
//     ];

//     return {
//         style: 'tableExample header',
//         table: {
//             widths: ['auto', '*', 'auto', '*'],
//             body: getSocialValues(socialFunctionList, list, 4)
//         }
//     };
// }
