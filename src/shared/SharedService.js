import React, { useEffect, useState, useContext } from 'react';
import authHeaders from './../shared/GlobalState';



export const getAuthHeaders = (props) => {
    return { 'headers': { 'Authentication-Token': props } };
}

