export const START_DAY_HOUR = 9;
export const END_DAY_HOUR = 17;

export const SLOTS = [
    {"slot":"09:00", "max_no_appts":100}, 
    {"slot":"09:30", "max_no_appts":10}, 
    {"slot":"10:00", "max_no_appts":100}, 
    {"slot":"10:30", "max_no_appts":1}, 
    {"slot":"11:00", "max_no_appts":50}, 
    {"slot":"11:30", "max_no_appts":100}, 
    {"slot":"12:00", "max_no_appts":100}, 
    {"slot":"12:30", "max_no_appts":100}, 
    {"slot":"13:00", "max_no_appts":100}, 
    {"slot":"13:30", "max_no_appts":100}, 
    {"slot":"14:00", "max_no_appts":100}, 
    {"slot":"14:30", "max_no_appts":100}, 
    {"slot":"15:00", "max_no_appts":100}, 
    {"slot":"15:30", "max_no_appts":100}, 
    {"slot":"16:00", "max_no_appts":100}, 
    {"slot":"16:30", "max_no_appts":100}, 
    {"slot":"17:00", "max_no_appts":100}
];

export const APPOINTMENT_SELECTORS = [
                                        {'id':'MyAppointments', 'label':'My Appointments', 'value':'My Appointments'},
                                        {'id':'AllAppointments', 'label':'All Appointments', 'value':'All Appointments'}
                                    ];
export const APPOINTMENT_VIEW_SELECTORS = [    
    {'id':'Day', 'label':'Day', 'value':'Day'},
    {'id':'Week', 'label':'Week', 'value':'Week'},
    {'id':'Month', 'label':'Month', 'value':'Month'}
];

export const APPOINTMENTS = {
    '09:00':[
        {'id':1, 'first_name':'Patient 1', 'record_type':1, 'doctor_name':'Doctor 1'},
        {'id':5, 'first_name':'Patient 5', 'record_type':1, 'doctor_name':'Doctor 2'},
        {'id':6, 'first_name':'Patient 6', 'record_type':5, 'doctor_name':'Doctor 7'},
        {'id':7, 'first_name':'Patient 7', 'record_type':7, 'doctor_name':'Doctor 5'},
    ],
    '09:30':[
        {'id':2, 'first_name':'Patient 2', 'record_type':2, 'doctor_name':'Doctor 3'},
        {'id':8, 'first_name':'Patient 8', 'record_type':1, 'doctor_name':'Doctor 1'},
        {'id':9, 'first_name':'Patient 9', 'record_type':1, 'doctor_name':'Doctor 2'},
        {'id':10, 'first_name':'Patient 10', 'record_type':5, 'doctor_name':'Doctor 7'},
        {'id':11, 'first_name':'Patient 11', 'record_type':7, 'doctor_name':'Doctor 5'},
    ],
    '10:00':[
        {'id':3, 'first_name':'Patient 3', 'record_type':3, 'doctor_name':'Doctor 4'},
        {'id':13, 'first_name':'Patient 13', 'record_type':1, 'doctor_name':'Doctor 1'}
    ],
    '10:30':[
        {'id':4, 'first_name':'Patient 4', 'record_type':1, 'doctor_name':'Doctor 2'}
    ],
    '11:00':[],
    '11:30':[],
    '12:00':[],
    '12:30':[],
    '13:00':[],
    '13:30':[],
    '14:00':[],
    '14:30':[],
    '15:00':[],
    '15:30':[],
    '16:00':[],
    '16:30':[]
};

export const APPOINTMENT_STEPS = [
    {'title': 'Patient Info'},
    {'title': 'Available slots'},
    {'title': 'Preview'}
]