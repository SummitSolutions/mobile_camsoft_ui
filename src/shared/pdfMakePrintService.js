import * as pdfMakeLayoutConfigs from './configs/PdfMakeLayoutConfig';
import * as logo from './base-64-images/Logo';

// PDF make libraries
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export const printNow = (data) => {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    // Opens on new tab
    pdfMake.createPdf(data).open();
}

export const getHeaderWithLogo = () => {
    return {
        style: 'tableExample subheader',
        table: {
            headerRows: 2,
            widths: [60, '*'],
            body: [
                [
                    {
                        rowSpan: 2, image: logo.nimhansLogo, width: 50,
                        border: pdfMakeLayoutConfigs.getBorder('B'), marginBottom: 2
                    },
                    {
                        text: 'National Institute of Mental Health and Neurosciences(NIMHANS) Bangalore',
                        alignment: 'center', marginTop: 5, border: pdfMakeLayoutConfigs.getBorder('')
                    }
                ],
                [
                    '',
                    {
                        text: 'CENTRE FOR ADDICTION MEDICINE',
                        alignment: 'center', border: pdfMakeLayoutConfigs.getBorder('B')
                    }
                ]
            ]
        }
    };
}

export const getPatientInformation = (patientData) => {
    return {
        style: 'tableExample header',
        table: {
            widths: ['auto', '*', 'auto', '*'],
            body: [
                [
                    { text: 'Patient Name : ', border: pdfMakeLayoutConfigs.getBorder('') },
                    { text: patientData?.first_name, border: pdfMakeLayoutConfigs.getBorder(''), bold: true },
                    { text: 'Gender : ', border: pdfMakeLayoutConfigs.getBorder('') },
                    { text: patientData?.gender, border: pdfMakeLayoutConfigs.getBorder(''), bold: true }
                ],
                [

                    { text: 'Age : ', border: pdfMakeLayoutConfigs.getBorder('') },
                    { text: patientData?.age, border: pdfMakeLayoutConfigs.getBorder(''), bold: true },
                    { text: 'DOB : ', border: pdfMakeLayoutConfigs.getBorder('') },
                    { text: patientData?.dob, border: pdfMakeLayoutConfigs.getBorder(''), bold: true },
                ],
                [
                    { text: 'Referred by : ', border: pdfMakeLayoutConfigs.getBorder('') },
                    { text: patientData?.referred_by, border: pdfMakeLayoutConfigs.getBorder(''), bold: true },
                    { text: '', border: pdfMakeLayoutConfigs.getBorder('') },
                    { text: '', border: pdfMakeLayoutConfigs.getBorder(''), bold: true },
                ],
            ]
        }
    };
}
