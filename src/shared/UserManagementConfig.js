export const STATUS = ['Active', 'Inactive', 'Banned'];

export const LANGUAGES = [
    'Kannada', 'Hindi', 'Tamil', 'Telugu', 'Bengali', 
    'Gujarathi', 'Malayalam', 'English', 'Urdu', 'Bangla', 'Oriya', 
    'Konkani', 'Assamese', 'Tulu', 'Kashmir', 'Lambani', 'Marathi', 'Other Language'
];

export const DESIGNATION = [
    { 'label':'Admin', 'value':'Admin' },
    { 'label':'DNS', 'value':'DNS' },
    { 'label':'ANS', 'value':'ANS' },
];