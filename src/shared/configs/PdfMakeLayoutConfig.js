export const prescPrintTitle = 'Summary Print';
export const author = 'Summit Solutions Dev Team';
export const prescPrintSubject = 'Summary Print';
export const metaData = {
    title: 'Print Title',
    author: author,
    subject: 'Print Subject',
};

export const getMetaData = (purpose) => {
    switch (purpose) {
        case 'SummaryPrint':
            metaData.title = prescPrintTitle;
            metaData.subject = prescPrintSubject;
            break;
        default: return metaData;
    }
    return metaData;
}

export const pageSize = 'A4';
/*‘4A0’, ‘2A0’, ‘A0’, ‘A1’, ‘A2’, ‘A3’, ‘A4’, ‘A5’, ‘A6’, ‘A7’, ‘A8’, ‘A9’, ‘A10’,
‘B0’, ‘B1’, ‘B2’, ‘B3’, ‘B4’, ‘B5’, ‘B6’, ‘B7’, ‘B8’, ‘B9’, ‘B10’,
‘C0’, ‘C1’, ‘C2’, ‘C3’, ‘C4’, ‘C5’, ‘C6’, ‘C7’, ‘C8’, ‘C9’, ‘C10’,
‘RA0’, ‘RA1’, ‘RA2’, ‘RA3’, ‘RA4’,
‘SRA0’, ‘SRA1’, ‘SRA2’, ‘SRA3’, ‘SRA4’,
‘EXECUTIVE’, ‘FOLIO’, ‘LEGAL’, ‘LETTER’, ‘TABLOID’*/

// [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
export const pageMargins = [40, 30, 40, 40];

// by default we use portrait, you can change it to landscape if you wish
export const pageOrientation = 'portrait';

//Full Border => LRTB Left,Right,Top,Bottom
export const getBorder = (border) => {
    let left = (border.indexOf('L') != -1) ? true : false;
    let right = (border.indexOf('R') != -1) ? true : false;
    let top = (border.indexOf('T') != -1) ? true : false;
    let bottom = (border.indexOf('B') != -1) ? true : false;
    return [left, top, right, bottom];
}

