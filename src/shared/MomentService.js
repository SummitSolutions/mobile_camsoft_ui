
import moment from 'moment';
import * as DateConfigs from './configs/DateConfigs';

export const subtract = props => {
    return moment(props.date).subtract(props.count, props.term)
}

export const add = props => {
    return moment(props.date).add(props.count, props.term)
}
export const currentDateTime = () => {
    return moment();
}
export const currentDate = (format) => {
    return moment().format(format);
}

export const currentDateTimeCustomFormat = (format) => {
    return moment().format(format);
}
export const formatDate = (date, format) => {
    if (date)
        return moment(date).format(format ? format : DateConfigs.DEFAULT_DATE_FORMAT_UI);
    else
        return;
}
export const getDifferenceBetweenDates = (fromDate, toDate, diffType = 'months') => {
    let diff = 0;
    if (fromDate && toDate) {
        diff = moment(toDate).diff(moment(fromDate), diffType);
    }
    return diff;
}
export const getAgeFromDOB = (date, duration) => {
    if (date) {
        return getDifferenceBetweenDates(date, moment(), duration);
    } else {
        return 0;
    }
}
export const getDOBFromAge = (age, duration) => {
    if (age > 0) {
        return formatDate(subtract({ 'date': moment(), 'count': age, 'term': duration }), DateConfigs.DEFAULT_DATE_FORMAT_BACKEND);
    } else {
        return formatDate(moment(), DateConfigs.DEFAULT_DATE_FORMAT_BACKEND);
    }
}