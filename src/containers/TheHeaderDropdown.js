import React, { useContext, useState } from 'react';
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CDropdownDivider,
} from '@coreui/react'

import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faKey, faCogs, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import LoginService from './../api/LoginService';
import GlobalState from './../shared/GlobalState';
import { useHistory } from 'react-router-dom';
import { List, X } from 'react-bootstrap-icons';
import './index.css';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));

const TheHeaderDropdown = () => {
  const classes = useStyles();
  const [state, setState] = useContext(GlobalState);
  const history = useHistory();

  const logout = () => {
    console.log('logout is calling')
    LoginService.logout(state.authHeader).then(res => {
      console.log(res)
      setState(state => ({ ...state, authHeader: {}, patient: {}, loggedUser: {}, isAuthenticated: false }));
      history.replace('/')
      console.log(state)
      console.log('logged out')
    });
  }

  const centre = () => {
    history.push('/centre/centrelist');
  }

  const patient = () => {
    history.push('/patient/list');
  }

  return (
    <CDropdown
      inNav
      className="c-header-nav-items"
      direction="down"
    >
      <CDropdownToggle caret={false}>
        <div className={classes.root}>
          <List style={{ fontSize: '30px' }} />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem
          tag="div"
          style={{ fontSize: '1rem' }}
        >
          <strong>User Name Here</strong>
        </CDropdownItem>
        <CDropdownDivider />
        <CDropdownItem>
          <FontAwesomeIcon icon={faUser} />
          &nbsp; Profile
        </CDropdownItem>
        <CDropdownItem>
          <FontAwesomeIcon icon={faKey} />
          &nbsp; Change Password
        </CDropdownItem>
        <CDropdownItem>
          <FontAwesomeIcon icon={faCogs} />
          &nbsp; Preferences
        </CDropdownItem>
        <CDropdownItem
          tag="div"
          onClick={patient}
        >
          <FontAwesomeIcon icon={faUser} style={{ fontSize: '1.2rem' }} /> &nbsp;&nbsp;&nbsp;
          <strong>Patient</strong>
        </CDropdownItem>
        <CDropdownItem
          tag="div"
          onClick={centre}
        >
          <FontAwesomeIcon icon={faUser} style={{ fontSize: '1.2rem' }} /> &nbsp;&nbsp;&nbsp;
          <strong>Admin</strong>
        </CDropdownItem>
        <CDropdownDivider />
        <CDropdownItem
          tag="div"
          onClick={logout}
        >
          <FontAwesomeIcon icon={faSignOutAlt} style={{ fontSize: '1.2rem' }} /> &nbsp;&nbsp;&nbsp;
          <strong>Signout</strong>
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdown
