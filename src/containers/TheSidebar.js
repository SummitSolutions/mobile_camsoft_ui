import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
  CLabel
} from '@coreui/react'

import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';

// sidebar nav config
import navigation from './_nav'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  large: {
    width: theme.spacing(5),
    height: theme.spacing(5)
  },
}));

const TheSidebar = () => {
  const classes = useStyles();
  const dispatch = useDispatch()
  const show = useSelector(state => state.sidebarShow)

  return (
    <CSidebar
      show={show}
      minimize={true}
      onShowChange={(val) => dispatch({type: 'set', sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        <div className={'c-sidebar-brand-minimized '+classes.root}>
          <Avatar alt="CAMSOFT, NIMHANS" src={'avatars/Nimhans_logo.png'} className={classes.large} />
        </div>
        <div className={'c-sidebar-brand-full '+classes.root}>
          <Avatar alt="CAMSOFT, NIMHANS" src={'avatars/Nimhans_logo.png'} className={classes.large} /><CLabel style={{marginTop: '15px', fontWeight: '500', fontSize: '1.2rem'}}>CAMSOFT, NIMHANS</CLabel>
        </div>
      </CSidebarBrand>
      <CSidebarNav>

        <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none"/>
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
