import React from 'react'
import {
  CButton,
  CCard,
  CCardHeader,
  CCardBody,
  CRow,
  CCol
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import {  } from '@fortawesome/free-regular-svg-icons';
import SingleTextInput from './../views/form-components/SingleTextInput';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import * as momentService from './../shared/MomentService';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import * as appConstants from './../AppConstants';


const useStyles = makeStyles({
    list: {
        width: 300,
    },
    fullList: {
        width: 'auto',
    },
});

const TheHeaderDropdownPatientNotes = () => {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });
    let noteList = [
        {'doctorName': 'Dr. Arjun', 'patient_notes': 'Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.', 'dateTime': '15-Jan-2021 10:15'},
        {'doctorName': 'Dr. Aravind', 'patient_notes': 'Velit aute mollit ipsum ad dolor consectetur nulla officia culpa adipisicing exercitation fugiat tempor.', 'dateTime': '10-Jan-2021 09:45'},
        {'doctorName': 'Dr. Preethi', 'patient_notes': 'Cupidatat quis ad sint excepteur laborum in esse qui. Et excepteur consectetur ex nisi eu do cillum ad laborum.', 'dateTime': '01-Dec-2020 03:30'},
        {'doctorName': 'Dr. Preethi', 'patient_notes': 'Cupidatat quis ad sint excepteur laborum in esse qui. Et excepteur consectetur ex nisi eu do cillum ad laborum.', 'dateTime': '01-Dec-2020 03:30'},
        {'doctorName': 'Dr. Preethi', 'patient_notes': 'Cupidatat quis ad sint excepteur laborum in esse qui. Et excepteur consectetur ex nisi eu do cillum ad laborum.', 'dateTime': '01-Dec-2020 03:30'}
    ];
    const [notesList, setNotesList] = React.useState([]);
    const [notes, setNotes] = React.useState({
        patient_notes: '',
        dateTime: '',
        doctorName: ''
    });

    React.useEffect(() => {
        setNotesList(noteList)
    }, []);
  
    const toggleDrawer = (anchor, open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
    
        setState({ ...state, [anchor]: open });
        setNotes({})
        setNotesList(noteList)
    };
    const saveNotes = () => {
        notesList.push(notes);
        setNotesList(notesList);
    }
    const handleChange = (e) => {
        setNotes({patient_notes: e.target.value, doctorName: 'Dr. Ram', dateTime: momentService.formatDate(momentService.currentDateTime()._d, 'DD-MMM-YYYY H:MM')})
    }

    const handleAccordianChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const list = (anchor) => (
        <div
            className={clsx(classes.list, {
                [classes.fullList]: anchor === 'top' || anchor === 'bottom',
            })}
            role="presentation"
            //onClick={toggleDrawer(anchor, false)}
            //onKeyDown={toggleDrawer(anchor, false)}
        >
            <CCard accentColor="primary" style={{margin: '10px'}}>
                <CCardHeader>
                    <CRow>
                        <CCol>Patient Daily Notes</CCol>
                        <CCol xs="2" sm="2" md="2" lg="2">
                            <FontAwesomeIcon icon={faTimesCircle} onClick={toggleDrawer(anchor, false)} />
                        </CCol>
                    </CRow>
                </CCardHeader>
                <CCardBody>
                    <CRow>
                        <CCol>
                            <SingleTextInput name="patient_notes" 
                                title="Notes" 
                                inputType="text" 
                                content={(notes.patient_notes)?(notes.patient_notes):''}
                                placeholder="Patient Notes" 
                                manadatorySymbol={true}
                                controlFunc={handleChange}
                                multiline={true}
                                rows={6}/>
                        </CCol>
                        <CCol xs="2" sm="2" md="2" lg="2"><br/><br/><br/><br/>
                            <CButton color={appConstants.CANCEL_BTN_COLOR} style={{marginLeft: '-10px'}} onClick={saveNotes} disabled={notes.patient_notes?false:true}>
                                <FontAwesomeIcon icon={faSave}/>
                            </CButton>
                        </CCol>
                    </CRow>
                    
                    {notesList.map((obj, index) => {
                        return (
                            <Accordion key={index} expanded={expanded === index} onChange={handleAccordianChange(index)}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1bh-content"
                                    id="panel1bh-header"
                                >
                                    {obj.doctorName}/{obj.dateTime}
                                </AccordionSummary>
                                <AccordionDetails>
                                    <CRow style={{width:'100%'}}>
                                        <CCol>
                                            {obj.patient_notes}
                                        </CCol>
                                    </CRow>                                                
                                </AccordionDetails>
                            </Accordion>
                        )})
                    }
                </CCardBody>
            </CCard>
        </div>
      );

    return (
        <React.Fragment key='right'>
            <CButton onClick={toggleDrawer('right', true)} variant="outline" color={appConstants.SAVE_BTN_COLOR}>
                <CIcon name="cil-notes" alt="Notes" />&nbsp;
                <span className="d-md-down-none">Notes</span>
            </CButton>
            <SwipeableDrawer
                anchor='right'
                open={state['right']}
                onClose={toggleDrawer('right', false)}
                onOpen={toggleDrawer('right', true)}
            >
                {list('right')}
            </SwipeableDrawer>
        </React.Fragment>
    )
}

export default TheHeaderDropdownPatientNotes
