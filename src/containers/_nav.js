import React from 'react'
import CIcon from '@coreui/icons-react'
import environment from './../environments/environment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarCheck } from '@fortawesome/free-solid-svg-icons';

const _navList = [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon" />,
    value: 'Dashboard'
    /*badge: {
      color: 'info',
      text: 'NEW',
    }*/
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Patient Management',
    route: '/patient',
    icon: 'cil-list',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Patient List',
        to: '/patient/list',
        value: 'PatientList'
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Add New Patient',
        to: '/patient/create',
        value: 'PatientCreate'
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Appointments',
        to: '/appointment',
        value: 'Appointment'
      },

    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'After Care',
    route: '/aftercare/after-care',
    icon: 'cil-list',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'After Care',
        to: '/aftercare/after-care',
        value: 'AfterCare'
      },
    ],
  },

  {
    _tag: 'CSidebarNavDropdown',
    name: 'Center Management',
    route: '/location',
    icon: 'cil-list',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Create center',
        to: '/center/create-center',
        value: 'CreateCenter'
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Center List',
        to: '/center/center-list',
        value: 'CenterList'
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Test',
    route: '/test',
    icon: 'cil-list',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Accordian',
        to: '/test/accordian',
        value: 'Accordian'
      },
    ],
  },

  {
    _tag: 'CSidebarNavTitle',
    _children: ['App Administration']
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'User Management',
    route: '/user',
    icon: 'cil-people',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Users List',
        to: '/user/list',
        value: 'UserList'
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Create New User',
        to: '/user/create',
        value: 'UserCreate'
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Roles Management',
    route: '/roles',
    icon: 'cil-user',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Roles List',
        to: '/role/list',
        value: 'RolesList'
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Create New Role',
        to: '/role/create',
        value: 'RoleCreate'
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Data Management',
    route: '/data',
    icon: 'cil-spreadsheet',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Queries',
        to: '/data/queries',
        value: 'Queries'

      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Export',
        to: '/data/export',
        value: 'Export'
      },
    ],
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Reports',
    to: '/reports',
    icon: 'cil-notes',
    value: 'Reports'
  }
];

let additionalFeatuers = [];
if (!environment.production) {
  additionalFeatuers = [
    {
      _tag: 'CSidebarNavTitle',
      _children: ['Course Administration']
    },

  ]
}
let _nav = [];
/*if(additionalFeatuers.length > 0)
  _nav = _navList.concat(additionalFeatuers);
else*/
_nav = _navList;

export default _nav
