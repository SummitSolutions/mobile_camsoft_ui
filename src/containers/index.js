import TheContent from './TheContent'
import TheFooter from './TheFooter'
import TheHeader from './TheHeader'
import TheHeaderDropdown from './TheHeaderDropdown'
import TheHeaderDropdownMssg from './TheHeaderDropdownMssg'
import TheHeaderDropdownNotif from './TheHeaderDropdownNotif'
import TheHeaderDropdownTasks from './TheHeaderDropdownTasks'
import TheHeaderDropdownSupportCenter from './TheHeaderDropdownSupportCenter'
import TheHeaderDropdownWaitingRoom from'./TheHeaderDropdownWaitingRoom'
import TheHeaderDropdownVisitLog from './TheHeaderDropdownVisitLog'
import TheHeaderDropdownAppointmentLog from './TheHeaderDropdownAppointmentLog'
import TheHeaderDropdownPatientNotes from './TheHeaderDropdownPatientNotes'
import TheHeaderDropdownPatientInfo from './TheHeaderDropdownPatientInfo'
import TheLayout from './TheLayout'
import TheSidebar from './TheSidebar'


export {
  TheContent,
  TheFooter,
  TheHeader,
  TheHeaderDropdown,
  TheHeaderDropdownMssg,
  TheHeaderDropdownNotif,
  TheHeaderDropdownTasks,
  TheHeaderDropdownSupportCenter,
  TheHeaderDropdownWaitingRoom,
  TheHeaderDropdownVisitLog,
  TheHeaderDropdownAppointmentLog,
  TheHeaderDropdownPatientNotes,
  TheHeaderDropdownPatientInfo,
  TheLayout,
  TheSidebar,
}
