import React from 'react'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CButton,
  CBadge
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import { makeStyles } from '@material-ui/core/styles';
import * as appConstants from './../AppConstants';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));

const data = [
    {'appointment_date':'16-Feb-2021', 'record_type':'OP-Brief Workup', 'status':'In-progress', 'doctor_name':'Dr. Arjun'},
    {'appointment_date':'29-Jan-2021', 'record_type':'Follow up', 'status':'Consulted', 'doctor_name':'Dr. Arun Prasad'},
    {'appointment_date':'19-Jan-2021', 'record_type':'OP-Detailed Workup', 'status':'Consulted', 'doctor_name':'Dr. Aadi Narayana'},
    {'appointment_date':'06-Dec-2020', 'record_type':'Screening', 'status':'Consulted', 'doctor_name':'Dr. Shobha'}
];
const getBadgeColor = (status) => {
    switch(status) {
        case 'Consulted' : return 'success';
                            break;
        case 'In-progress' : return 'primary';
                            break;
        case 'Pending' : return 'warning';
                            break;
    }
}

const TheHeaderDropdownAppointmentLog = () => {
  const classes = useStyles();
  return (
    <CDropdown
      inNav
      className="c-header-nav-items"
      direction="down"
      style={{listStyleType:'none'}}
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <CButton variant="outline" color={appConstants.SAVE_BTN_COLOR}>
            <CIcon name="cil-calendar" alt="Visit Log" />&nbsp;
            <span className="d-md-down-none">Appointment Log</span>
        </CButton>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong><CIcon name="cil-calendar" alt="Visit Log" />&nbsp;Appointment Log</strong>
        </CDropdownItem>
        {
            data.map((item, i) => {
                return(
                <CDropdownItem key={i}>
                  <div className="message">
                    <div className="pt-3 mr-3 float-left font-weight-bold">
                    </div>
                    <div>
                      <small></small>
                      <small className="text-muted float-right mt-1">
                        
                      </small>
                    </div>
                    <div className="text-truncate">
                      <span className="font-weight-bold">{item.appointment_date}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      {item.record_type} - {item.doctor_name}
                      <small className="float-right">
                        <CBadge color={getBadgeColor(item.status)}>{item.status}</CBadge>
                      </small>
                    </div>
                    <div className="small text-muted text-truncate">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                  </div>
                </CDropdownItem>                
                )
            })
        }
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdownAppointmentLog
