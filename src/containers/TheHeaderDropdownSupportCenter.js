import React from 'react'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react'

import { makeStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHandsHelping, faQuestionCircle, faInfoCircle, faBug } from '@fortawesome/free-solid-svg-icons';
import { } from '@fortawesome/free-regular-svg-icons';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));

const TheHeaderDropdownSupportCenter = () => {
  const classes = useStyles();
  return (
    <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down"
    >
      {/* <CDropdownToggle className="c-header-nav-link" caret={false}>
        <FontAwesomeIcon icon={faHandsHelping} style={{ fontSize: "20px" }} />
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
         <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong>Support Center</strong>
        </CDropdownItem>
      <CDropdownItem>
        <FontAwesomeIcon icon={faBug} />
        &nbsp; Report Issue
      </CDropdownItem>
      <CDropdownItem>
        <FontAwesomeIcon icon={faQuestionCircle} />
        &nbsp; FAQ
      </CDropdownItem>
      <CDropdownItem>
        <FontAwesomeIcon icon={faInfoCircle} />
        &nbsp; Help Docs
      </CDropdownItem>
      </CDropdownMenu>  */}
    </CDropdown >
  )
}

export default TheHeaderDropdownSupportCenter
