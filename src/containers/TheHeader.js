import React, { useState, useContext } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {
  CHeader,
  CToggler,
  CHeaderBrand,
  CHeaderNav,
  CLabel
} from '@coreui/react'
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import GlobalState from './../shared/GlobalState';

import {
  TheHeaderDropdown
} from './index'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  large: {
    width: theme.spacing(4),
    height: theme.spacing(4)
  },
  headerLogo: {
    alignContent: 'left'
  }
}));

const TheHeader = () => {

  const classes = useStyles();
  const dispatch = useDispatch()
  const sidebarShow = useSelector(state => state.sidebarShow)
  const [state, setState] = useContext(GlobalState);

  // const toggleSidebar = () => {
  //   const val = [true, 'responsive'].includes(sidebarShow) ? false : 'responsive'
  //   dispatch({ type: 'set', sidebarShow: val })
  // }

  // const toggleSidebarMobile = () => {
  //   const val = [false, 'responsive'].includes(sidebarShow) ? true : 'responsive'
  //   dispatch({ type: 'set', sidebarShow: val })
  // }

  return (
    <CHeader withSubheader style={{ backgroundColor: 'white' }}>
      <CHeaderNav>
        <TheHeaderDropdown />
      </CHeaderNav>
      <CHeaderBrand>
        <Avatar alt="Center for addiction medicine , NIMHANS" src={'avatars/Nimhans_logo.png'} className={classes.headerLogo} />
        <CLabel style={{ marginLeft: '10px' }}>Center for Addiction Medicine, NIMHANS</CLabel>
      </CHeaderBrand>
    </CHeader >

  )
}

export default TheHeader
