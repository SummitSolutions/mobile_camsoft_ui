import React from 'react'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CButton
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import { makeStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWalking, faCalendar } from '@fortawesome/free-solid-svg-icons';
import * as appConstants from './../AppConstants';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));

const data = [
    {'visit_date':'16-Feb-2021', 'record_type':'OP-Brief Workup', 'token_type':'A'},
    {'visit_date':'29-Jan-2021', 'record_type':'Follow up', 'token_type':'W'},
    {'visit_date':'19-Jan-2021', 'record_type':'OP-Detailed Workup', 'token_type':'A'},
    {'visit_date':'06-Dec-2020', 'record_type':'Screening', 'token_type':'W'}
];

const TheHeaderDropdownVisitLog = () => {
  const classes = useStyles();
  return (
    <CDropdown
      inNav
      className="c-header-nav-items"
      direction="down"
      style={{listStyleType:'none'}}
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <CButton variant="outline" color={appConstants.SAVE_BTN_COLOR}><CIcon name="cil-graph" alt="Visit Log" />&nbsp;<span className="d-md-down-none">Visit Log</span></CButton>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong><CIcon name="cil-graph" alt="Visit Log" />&nbsp;Visit Log</strong>
        </CDropdownItem>
        {
            data.map((item, i) => {
                return(
                <CDropdownItem key={i}>
                  <div className="message">
                    <div className="pt-3 mr-3 float-left font-weight-bold">
                    </div>
                    <div>
                      <small></small>
                      <small className="text-muted float-right mt-1">
                        
                      </small>
                    </div>
                    <div className="text-truncate">
                      <span className="font-weight-bold">{item.visit_date}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                      {item.record_type}
                      <small className="float-right mt-1">
                        <FontAwesomeIcon icon={(item.token_type == 'A')? faCalendar :faWalking}/>
                      </small>
                    </div>
                    <div className="small text-muted text-truncate">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                  </div>
                </CDropdownItem>                
                )
            })
        }
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdownVisitLog
