import React from 'react'
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CProgress
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

const TheHeaderDropdownWaitingRoom = () => {
  return (
    <CDropdown
      inNav
      className="c-header-nav-item mx-2"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <CIcon name="cil-list" />
      </CDropdownToggle>
      <CDropdownMenu placement="bottom-end" className="pt-0">
      <CDropdownItem
          header
          tag="div"
          color="light"
        >
          <strong>Waiting Room</strong>
        </CDropdownItem>
        <CDropdownItem className="d-block">
          <div className="text-uppercase mb-1">
            <small><b>My Waiting List</b></small>
          </div>
          <CProgress size="xs" color="info" value={55} />
          <small className="text-muted">24 Consulted 16 Pending.</small>
        </CDropdownItem>
        <CDropdownItem className="d-block">
          <div className="text-uppercase mb-1">
            <small><b>Units Waiting List</b></small>
          </div>
          <CProgress size="xs" color="warning" value={70} />
          <small className="text-muted">168 Consulted 84 Pending</small>
        </CDropdownItem>
        <CDropdownItem className="d-block">
          <div className="text-uppercase mb-1">
            <small><b>Pending Proformas to finish</b></small>
          </div>
          <CProgress size="xs" color="danger" value={90} />
          <small className="text-muted">6 Completed 2 Pending</small>
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdownWaitingRoom