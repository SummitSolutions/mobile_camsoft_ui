import React from 'react'
import Chip from '@material-ui/core/Chip';
import Badge from '@material-ui/core/Badge';
import Avatar from '@material-ui/core/Avatar';

const TheHeaderDropdownPatientInfo = (props) => {
    let patientName = props.patientData.first_name+' '; 
    patientName += (props.patientData.last_name)?(props.patientData.last_name):'';
    patientName += (props.patientData.uhid) ? ' | '+(props.patientData.uhid) : '';
    let gender = (props.patientData.gender === 'Male')? 'M' : (props.patientData.gender === 'Female') ? 'F' : 'O';
    
    const defaultProps = {
      color: 'secondary',
      children: <Chip label={patientName} avatar={<Avatar>{gender}</Avatar>}/>,
    };
    
    return (
      <div style={{padding: '10px', textTransform: 'capitalize', marginTop: '5px', fontWeight: '800', fontSize: '20'}}>
          <Badge badgeContent={props.patientData.age} {...defaultProps} />          
      </div>
    )
}

export default TheHeaderDropdownPatientInfo
