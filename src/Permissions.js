import routes from './routes';

/*export const PERMISSIONS = [
    {
        'module_name' : 'Patient', 'list' : [
            {'name': 'Patient List', 'value' : 'PatientList', 'path' : 'patient/list', 'description' : 'List of Patients will be shown'},
            {'name' : 'Patient Create', 'value' : 'PatientCreate', 'path' : 'patient/create', 'description' : 'New Patient details entry page'},
            {'name' : 'Patient Edit', 'value' : 'PatientEdit', 'path' : 'patient/update/:patient_id', 'description' : 'Update existing Patient details'},
            {'name' : 'Patient Delete', 'value' : 'PatientDelete', 'path' : 'patient/list', 'description' : 'In Patient List each patient record will have a button to delete patient record'}
        ]
    },
    {
        'module_name' : 'PatientVisit', 'list' : [            
            {'name' : 'Patient Visit List', 'value' : 'PatientVisitList', 'path' : 'patient-visit/list', 'description' : 'List of visits willbe shown based on the PatientId'},
            {'name' : 'Add Visit', 'value' : 'AddVisit', 'path' : 'patient-visit/add-visit', 'description' : 'Can add visit to Patient'}
        ]   
    },
    {
        'module_name' : 'Appointment', 'list' : [
            {'name' : 'Appointment Create', 'value' : 'AppointmentCreate', 'path' : 'appointment/create', 'description' : 'New Appointment create page'},
            {'name' : 'Appointment List', 'value' : 'AppointmentList', 'path' : 'appointment/list', 'description' : 'List of Appointments'},
            {'name' : 'Appointment Edit', 'value' : 'AppointmentEdit', 'path' : 'appointment/update/:appointment_id', 'description' : 'Rescheduling Appointment'},
            {'name' : 'Appointment Cancel', 'value' : 'AppointmentCancel', 'path' : 'appointment/:appointment_id', 'description' : 'Cancel Appointments'}
        ]   
    },
    {
        'module_name' : 'UserManagement', 'list' : [
            {'name' : 'User List', 'value' : 'UserList', 'path' : 'users/list', 'description' : 'List of user will be shown'},
            {'name' : 'User Create', 'value' : 'UserCreate', 'path' : 'users/create', 'description' : 'Creates New User'},
            {'name' : 'User Edit', 'value' : 'UserEdit', 'path' : 'users/update/:user_id', 'description' : 'Eidt and Update Existing User details'},
            {'name' : 'User Delete', 'value' : 'UserDelete', 'path' : 'users/:user_id', 'description' : 'Delete a User'},
            {'name' : 'Assign/Remove Roles', 'value' : 'AssignRemoveRoles', 'path' : 'users/assign-roles/:user_id/:role', 'description' : 'Add Roles to Users and remove Roles from User'}
        ]
    },
    {
        'module_name' : 'RoleManagement', 'list' : [
            {'name' : 'Role List', 'value' : 'RoleList', 'path' : 'roles/list', 'description' : 'List of user will be shown'},
            {'name' : 'Role Create', 'value' : 'RoleCreate', 'path' : 'roles/create', 'description' : 'Creates New Role'},
            {'name' : 'Role Edit', 'value' : 'RoleEdit', 'path' : 'roles/update/:role_id', 'description' : 'Eidt and Update Existing Role details'},
            {'name' : 'Role Delete', 'value' : 'RoleDelete', 'path' : 'roles/:role_id', 'description' : 'Delete a Role'},
            {'name' : 'Assign/Remove Permission', 'value' : 'AssignRemovePermissions', 'path' : 'roles/assign-permissions/:role_id/:permission', 'description' : 'Add Permissions to Roles and remove Permissions from Role'}
        ]
    }
];*/

export const getAllPermissions = () => {
    let permissions = [];
    routes.forEach(route => {
        if(route.moduleName != 'Theme' && !permissions.find(perm => (perm.moduleName === route.moduleName)))
        {
            permissions.push({'moduleName' : route.moduleName, list: []});
        }
    });  
    routes.forEach(route => {
        permissions.forEach(element => {
            if(element['moduleName'] == route.moduleName)
                element.list.push(route);
        });
    });
    console.log(permissions)
    return permissions;
}

export const getRoutesByRole = (navList, loggedUserRoles) => {
    let permissionList = [];
    loggedUserRoles.forEach(role => {
        if(role.name == 'admin')
        {
            permissionList = navList;
            return;
        }
        let perms = role.permissions.split(',');
        navList.forEach(element => {
            if(!element.name)
            {
                permissionList.push(element);
                return;
            }
            if(!element._children)
            {
                if(perms.includes(element.value))
                    permissionList.push(element);
                return;
            }
            else
            {
                let childs = [];
                element._children.forEach(child => {
                    if(perms.includes(child.value))
                    {
                        childs.push(child);
                    }
                });
                if(childs.length > 0)
                {
                    element._children = childs;
                    permissionList.push(element);
                }
            }
        });
    });
    console.log('Setting Left Navbar menu items')
    console.log(permissionList)
    return permissionList;
}