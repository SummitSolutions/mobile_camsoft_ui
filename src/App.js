import React, { useState } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './scss/style.scss';
import environment from './environments/environment';
import GlobalState from './shared/GlobalState';
import routes from './routes';

const loading = (
	<div className="pt-3 text-center">
		<div className="sk-spinner sk-spinner-pulse"></div>
	</div>
)

// Containers
const TheLayout = React.lazy(() => import('./containers/TheLayout'));

// Pages
const Login = React.lazy(() => import('./Login'));

function App() {
	const [state, setState] = useState({});

	return (
		<GlobalState.Provider value={[state, setState]}>
			<HashRouter>
				<React.Suspense fallback={loading}>
					<Switch>
						<Route exact path="/" name="Login Page" render={props => <Login {...props} />} />
						{!state.isAuthenticated &&
							<Route path="/" name="Login Page" render={props => <Login {...props} />} />
						}
						{state.isAuthenticated &&
							routes.map((item, index) => (
								<Route key={item.name} path={item.path} name={item.name} render={props => <TheLayout {...props} />} />
							))
						}
					</Switch>
				</React.Suspense>
			</HashRouter>
		</GlobalState.Provider>
	);
}

export default App;
