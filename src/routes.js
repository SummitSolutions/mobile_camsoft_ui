import React from 'react';


const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const CreatePatientComponent = React.lazy(() => import('./views/patient/Create.js'));
const FileUpload = React.lazy(() => import('./views/patient/FileUpload.js'));
const PatientList = React.lazy(() => import('./views/patient/List.js'));
const PatientVisitList = React.lazy(() => import('./views/patient-visit/List.js'));
const AppointmentComponent = React.lazy(() => import('./views/appointment/Appointment.js'));
const CreateFollowupComponent = React.lazy(() => import('./views/followup/Create.js'));
const Summary = React.lazy(() => import('./views/followup/Summary.js'));
const CreateLocationComponent = React.lazy(() => import('./views/location/Create.js'));
const LocationList = React.lazy(() => import('./views/location/List.js'));
const CreateTabsComponent = React.lazy(() => import('./views/location/tabs.js'));
const CreateSubstanceComponent = React.lazy(() => import('./views/patient/Substance.js'));
const SubstanceHeader = React.lazy(() => import('./views/substance-pages/SubstanceHeaderAlcohol.js'));
const AddNewSubstanceModal = React.lazy(() => import('./views/substance-pages/AddNewSubstanceModal.js'));
const AlcoholSubstanceDetails = React.lazy(() => import('./views/substance-details/AlcoholSubstanceDetails.js'));
const Mainpage = React.lazy(() => import('./views/substance-pages/Mainpage.js'));
const FollowupPages = React.lazy(() => import('./views/followup-pages/FollowupPages.js'));
const UserList = React.lazy(() => import('./views/user-management/users/List.js'));
const CreateUserComponent = React.lazy(() => import('./views/user-management/users/Create.js'));
const AssignRolesComponent = React.lazy(() => import('./views/user-management/users/AssignRoles.js'));
const RolesList = React.lazy(() => import('./views/user-management/roles/List.js'));
const CreateRoleComponent = React.lazy(() => import('./views/user-management/roles/Create.js'));
const AddRemovePermissionsComponent = React.lazy(() => import('./views/user-management/roles/AddRemovePermissions.js'));
const NewPatientComponent = React.lazy(() => import('./views/patient/Patient'));
const StepOne = React.lazy(() => import('./views/patient/StepOne'));
const PatientDetails = React.lazy(() => import('./views/form-components/PatientDetails'));
const CenterList = React.lazy(() => import('./views/Center/CenterList'));
const CreateCenter = React.lazy(() => import('./views/Center/CreateCenter'));
const AddUser = React.lazy(() => import('./views/users/AddUser'));
const UsersList = React.lazy(() => import('./views/users/List'));

const routes = [
  { path: '/', exact: true, moduleName: 'Home', name: 'Home', value: 'Home' },
  { path: '/dashboard', moduleName: 'Dashboard', name: 'Dashboard', value: 'Dashboard', component: Dashboard },
  { path: '/patient/create', moduleName: 'Patient', name: 'Patient Create', value: 'PatientCreate', component: CreatePatientComponent, },
  { path: '/patient/file-upload', moduleName: 'Patient', name: 'File Upload', value: 'FileUpload', component: FileUpload },
  { path: '/patient/update/:patient_id', moduleName: 'Patient', name: 'Patient Update', value: 'PatientUpdate', component: CreatePatientComponent },
  { path: '/patient/list', moduleName: 'Patient', name: 'Patient List', value: 'PatientList', component: PatientList },
  { path: '/patient-visit/list/:patient_id', moduleName: 'Patient', name: 'Patient Visit List', value: 'PatientVisitList', component: PatientVisitList },
  { path: '/patient/substance/:patient_id/:visit_id', moduleName: 'Patient', name: 'Add Substance', value: 'AddSubstance', component: CreateSubstanceComponent },
  { path: '/appointment', moduleName: 'Appointment', name: 'Appointment', value: 'Appointment', component: AppointmentComponent },
  { path: '/location/create', moduleName: 'Location', name: 'Location Create', value: 'LocationCreate', component: CreateLocationComponent },
  { path: '/location/list', moduleName: 'Location', name: 'Location List', value: 'LocationList', component: LocationList },
  { path: '/location/update/:location_id', moduleName: 'Location', name: 'Location Update', value: 'LocationUpdate', component: CreateLocationComponent },
  { path: '/location/tabs', moduleName: 'Location', name: 'Tabs Create', value: 'TabsCreate', component: CreateTabsComponent },
  { path: '/followup/create', moduleName: 'Followup', name: 'Followup Create', value: 'FollowupCreate', component: CreateFollowupComponent },
  { path: '/followup/summary/:patient_id/:visit_id', moduleName: 'Followup', name: 'Summary Sheet', value: 'SummarySheet', component: Summary },
  { path: '/substance-pages/substance-header/:patient_id/:visit_id', moduleName: 'Substance', name: 'Substance Header', value: 'SubstaceHeader', component: SubstanceHeader },
  { path: '/substance-pages/patient-substance-use/:patient_id/:visit_id', moduleName: 'SubstanceUse', name: 'Patient Substance Use', value: 'PatientSubstanceUse', component: AddNewSubstanceModal },
  { path: '/substance-details/alcohol-substance-details/:patient_id/:visit_id', moduleName: 'SubstanceDetails', name: 'Alcohol Substance Details', value: 'AlcoholSubstanceDetails', component: AlcoholSubstanceDetails },
  { path: '/substance-pages/mainpage/:patient_id/:visit_id', moduleName: 'Mainpage', name: 'Mainpage', value: 'Mainpage', component: Mainpage },
  { path: '/followup-pages/followup-page/:patient_id/:visit_id', moduleName: 'Followup', name: 'Followup', value: 'Followup', component: FollowupPages },
  { path: '/patient/newpatient', moduleName: 'Patient', name: 'New Patient Create', value: 'NewPatientCreate', component: NewPatientComponent },
  { path: '/patient/stepOne', moduleName: 'Patient', name: 'Step one', value: 'StepOne', component: StepOne },
  { path: '/form-components/patientdetails', moduleName: 'Patient', name: 'Patient Details', value: 'PatientDetails', component: PatientDetails },
  { path: '/centre/centrelist', moduleName: 'Centre', name: 'Centre', value: 'Centre', component: CenterList },
  { path: '/center/update/:center_id', moduleName: 'Center', name: 'Center Update', value: 'CenterUpdate', component: CreateCenter },
  { path: '/users/add-user/:center_id', moduleName: 'User', name: 'Add User', value: 'AddUser', component: AddUser },
  { path: '/users/list/:center_id', moduleName: 'User', name: 'User List', value: 'UsersList', component: UsersList },
  { path: '/users/update/:center_id/:user_id', moduleName: 'User', name: 'User Update', value: 'UserUpdate', component: AddUser },
  { path: '/center/create-center', moduleName: 'Center', name: 'Create center', value: 'CreateCenter', component: CreateCenter },

  { path: '/user/list', moduleName: 'UserManagement', name: 'User List', value: 'UserList', component: UserList },
  { path: '/user/create', moduleName: 'UserManagement', name: 'User Create', value: 'UserCreate', component: CreateUserComponent },
  { path: '/user/update/:user_id', moduleName: 'UserManagement', name: 'User Update', value: 'UserUpdate', component: CreateUserComponent },
  { path: '/user/assign-roles/:user_id', moduleName: 'UserManagement', name: 'Add/Remove Roles', value: 'AddRemoveRoles', component: AssignRolesComponent },

  { path: '/role/list', moduleName: 'RolesManagement', name: 'Roles List', value: 'RolesList', component: RolesList },
  { path: '/role/create', moduleName: 'RolesManagement', name: 'Role Create', value: 'RoleCreate', component: CreateRoleComponent },
  { path: '/role/update/:role_id', moduleName: 'RolesManagement', name: 'Role Update', value: 'RoleUpdate', component: CreateRoleComponent },
  { path: '/role/add-remove-permissions/:role_id', moduleName: 'RolesManagement', name: 'Add/Remove Permissions', value: 'AddRemovePermissions', component: AddRemovePermissionsComponent },

];

export default routes;