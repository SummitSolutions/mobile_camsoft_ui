import React, { useContext, useEffect, useState } from 'react';
import RolesService from '../../../api/RolesService';
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CButtonGroup,
    CLink,
    CButton
} from '@coreui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLayerGroup, faPencilAlt, faPlus } from '@fortawesome/free-solid-svg-icons';
import * as appConstants from './../../../AppConstants';
import GlobalState from './../../../shared/GlobalState';
import { useHistory } from 'react-router-dom';
import { css } from "@emotion/core";

const fields = [
    "name",
    {
        key: 'description',
        label: 'Description'
    },
    {
        key: 'edit_role',
        label: 'Action'
    }
];

const RolesList = () => {

    const [state, setState] = useContext(GlobalState);
    const [rolesList, setRolesList] = useState({data : null});
    let [loading, setLoading] = useState(true);
    let [color, setColor] = useState("#ffffff");
    const history = useHistory();
    
    useEffect(() => {
        console.log("Roles List page loading")
        RolesService.getRoles(state.authHeader).then((res) => {
            setRolesList({ data: res.data});
            setLoading(false)
        });
    }, []);

    const editRoles = (role) => {
        history.push('/role/update/'+role.id);
    }

    const AddRemovePermissions = (role) => {
        history.push('/role/add-remove-permissions/'+role.id);
    }

    const Loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

    return (
        <> 
            <button onClick={() => setLoading(!loading)}>Toggle Loader</button>
            
            <CCard accentColor="primary">
                <CCardHeader>
                    Roles List
                    <CLink 
                        className="c-subheader-nav-link" 
                        aria-current="page" 
                        to="/role/create"
                        style={{float:'right'}}>
                        <FontAwesomeIcon icon={faPlus} />&nbsp;New Roles
                    </CLink>
                </CCardHeader>
                <CCardBody>
                    <CDataTable
                        items={rolesList.data}
                        fields={fields}
                        itemsPerPage={appConstants.ITEMS_PER_PAGE}
                        pagination
                        tableFilter
                        itemsPerPageSelect
                        hover
                        sorter                    
                        scopedSlots = {{
                            'description':
                            (item, index)=>{
                                return (
                                    <td>
                                        {item.description}
                                    </td>
                                )
                            },
                            'edit_role':
                                (item, index)=>{
                                return (
                                    <td className="py-2">
                                        <CButtonGroup>
                                            <CButton
                                                color={appConstants.SAVE_BTN_COLOR}
                                                size="sm"
                                                title="Edit Roles"
                                                onClick={()=>{editRoles(item)}}
                                            >
                                                <FontAwesomeIcon icon={faPencilAlt} />
                                            </CButton>
                                            <CButton
                                                color={appConstants.CANCEL_BTN_COLOR}
                                                size="sm"
                                                title="Add/Remove Permissions"
                                                onClick={()=>{AddRemovePermissions(item)}}
                                            >
                                                <FontAwesomeIcon icon={faLayerGroup} />
                                            </CButton>
                                        </CButtonGroup>
                                    </td>
                                    )
                                }
                            }
                        }
                    />
                </CCardBody>
            </CCard>
        </>
    );
}

export default RolesList;