import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow, CDataTable, CLabel, CBadge} from '@coreui/react';
import { useHistory } from 'react-router-dom';
import { useParams} from "react-router";
import * as appConstants from './../../../AppConstants';
import RolesService from './../../../api/RolesService';
import GlobalState from './../../../shared/GlobalState';
import * as Permissions from './../../../Permissions';
import {Table, TableBody,TableCell,TableContainer, TableHead, TableRow, Paper, } from '@material-ui/core';

const fields = [
    {
        key: '#',
        label: '#'
    },
    {
        key: 'name',
        label: 'Name'
    },
    {
        key: 'assign_roles',
        label: 'Choose Permissions'
    }
];

const AddRemovePermissionsComponent = () => {
    const [state, setState] = useContext(GlobalState);
    const history = useHistory();
    const params = useParams();
    const [role, setRole] = useState({});
    const [pModule, setPModule] = useState({
        modules : []
    })

    useEffect(() => {
        console.log("Permission assignment page loaded. Msg. from UseEffect")
        
        getRoleById();
    }, []);

    const checkPermissionModules = (data) => {
        let permissions = [];
        Permissions.getAllPermissions().forEach(element => {
            let flag = true;
            for(let i = 0; i < (element.list.length); i++)
            {
                if(!(data.permissions?.includes(element.list[i].value)))
                {
                    flag = false;
                    break;
                }
            }
            if(flag)
            {
                permissions.push(element.moduleName);
            }
        });
        return permissions;
    }
    
    const getRoleById = () => {
        if(params.role_id > 0)
        {
            RolesService.getRoleById(params.role_id, state.authHeader).then(resp => {
                if(resp.data.id)
                {             
                    resp.data.perm_modules = checkPermissionModules(resp.data);          
                    console.log("Setting Role to local state variable")
                    setRole(resp.data);
                    console.log("Role Data retrieved Successfully")
                }
            });
        }
    }

    const goToRoleList = () => {
        history.replace('/role/list');
    }

    const addPermissionsToRole = (role, permission) => {
        RolesService.addPermissionsToRole(role.name, permission, state.authHeader).then(resp => {
            console.log(permission +" Permission assigned to " + role.name + " Role.");
        });
    }

    const removePermissionsFromRole = (role, permission) => {
        RolesService.removePermissionsFromRole(role.name, permission, state.authHeader).then(resp => {
            console.log(permission +" Permission removed from " + role.name + " Role.");
        });
    }

    const savePermissionModule = (e, permModule) => {
        if(e.target.checked)
        {
            let permissions= [];
            permModule.list.forEach(perm => {
                permissions.push(perm.value);
            });
            console.log(permissions)
            addPermissionsToRole(role, permissions.join(','));
            getRoleById(role.id)
        }
        else
        {
            permModule.list.forEach(perm => {
                removePermissionsFromRole(role, perm.value);
            });
            getRoleById(role.id)
        }        
    }

    const savePermissions = (e, permission) => {
        if(e.target.checked)
        {
            addPermissionsToRole(role, permission.value);
        }
        else
        {
            removePermissionsFromRole(role, permission.value);
        }
    }

    return(
        <>
        <CCard accentColor="primary">
            <CCardHeader>
                <h3> Add/Remove Permissions to/from <CBadge shape="pill" color="info">{role.name}</CBadge> Role</h3> 
            </CCardHeader>
            <CCardBody>
                <TableContainer>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>
                                    Module
                                </TableCell>
                                <TableCell>
                                    Permission
                                </TableCell>
                                <TableCell>
                                    Path
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        {Permissions.getAllPermissions()?.map((permModule, index) => (
                            <TableBody key={index}>
                                { permModule.list.map((perm, permIndex) => ( 
                                    <TableRow key={index+permIndex}>
                                        { permIndex == 0 && 
                                            <TableCell rowSpan={permModule.list?.length}>
                                                <CLabel>
                                                    <input type="checkbox" name="permission_module" 
                                                        onClick={e => {savePermissionModule(e, permModule)}} 
                                                        defaultChecked={role.perm_modules?.includes(permModule.moduleName)}/>
                                                    &nbsp;<b>{permModule.moduleName}</b>
                                                </CLabel>
                                            </TableCell> 
                                        }
                                        <TableCell>
                                            <CLabel>
                                                <input type="checkbox" name="permisssions" 
                                                    onClick={e => {savePermissions(e, perm)}} 
                                                    defaultChecked={role.permissions?.includes(perm.value)}/>
                                                &nbsp;{perm.name}
                                            </CLabel>
                                        </TableCell>
                                        <TableCell>{perm.path}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        ))}
                    </Table>
                </TableContainer>
            </CCardBody>
            <CCardFooter>
                <CButton color={appConstants.CANCEL_BTN_COLOR} onClick={goToRoleList} style={{float: 'right'}}>Go Back</CButton>
            </CCardFooter>
        </CCard>
        </>
    );
}
export default AddRemovePermissionsComponent;