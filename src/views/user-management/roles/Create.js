import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import { useHistory } from 'react-router-dom';
import { useParams} from "react-router";
import * as appConstants from './../../../AppConstants';
import RolesService from './../../../api/RolesService';
import GlobalState from './../../../shared/GlobalState';
import SimpleReactValidator from 'simple-react-validator';
import SingleTextInput from './../../form-components/SingleTextInput';

const CreateRoleComponent = () => {
    const [state, setState] = useContext(GlobalState);
    const history = useHistory();
    const params = useParams();
    const [role, setRole] = useState({
        name: null, description: null, permissions: null
    });
    
    const validator = useRef(new SimpleReactValidator());
    const [, forceUpdate] = useState();

    useEffect(() => {
        console.log("Role Create page loaded. Msg. from UseEffect")
        
        getRoleById();
    }, []);
    
    const getRoleById = () => {
        if(params.role_id > 0)
        {
            RolesService.getRoleById(params.role_id, state.authHeader).then(resp => {
                if(resp.data.id)
                {                        
                    console.log("Setting Role Id to state")
                    setRole(resp.data)
                    console.log("Role Data retrieved Successfully")
                }
            });
        }
    }

    const saveOrUpdateRole = (data, e) => {
        if(params.role_id){
            console.log('Role data updating..: ', data)
            e.preventDefault();
            RolesService.updateRole(data, params.role_id, state.authHeader).then(res =>{
                console.log(res)
                setRole({});
                history.replace('/role/list');
            });
        }
        else{                
            console.log('Role data saving')
            e.preventDefault();
            RolesService.createRole(data, state.authHeader).then(res =>{
                console.log(res)
                setRole({});
                history.replace('/role/list');
            });
        }
    }

    const validateForm = (e) => {
        console.log(role)
        e.preventDefault();
        if (validator.current.allValid()) {
            console.log('Valid')
            saveOrUpdateRole(role, e)
        } else {
            console.log('Not Valid')
            validator.current.showMessages();
            forceUpdate(1)
        }
    }

    const handleChange = (e) => {
        console.log(e.target.name, e.target.value)
            setRole({ ...role, [e.target.name]: e.target.value });
    }

    const resetRole = () => {
        setRole({});
    }

    const gotoRoleList = () => {
        resetRole();
        history.replace('/role/list');
    }

    return(
        <>
            <form  onSubmit={validateForm} className="form-horizontal">
                <CCard accentColor="primary">
                    <CCardHeader>
                        <h3> Role { params.role_id && 'Update' } { !params.role_id && 'Create' } Form</h3> 
                    </CCardHeader>
                    <CCardBody> 
                        <CRow className="formMargins">
                            <CCol xs="12" sm="12" md="4" lg="4" xl="4">
                                <SingleTextInput name="name" 
                                    title="Role Name" 
                                    inputType="text" 
                                    content={(role.name)?(role.name):''} 
                                    placeholder="Role Name" 
                                    manadatorySymbol={true} 
                                    controlFunc={handleChange}/>
                                {validator.current.message('name', role.name, 'required')}
                            </CCol>
                            <CCol xs="12" sm="12" md="4" lg="4" xl="4">
                                <SingleTextInput name="description" 
                                    title="Description" 
                                    inputType="text" 
                                    content={(role.description)?(role.description):''} 
                                    placeholder="Description" 
                                    manadatorySymbol={false} 
                                    controlFunc={handleChange}/>
                            </CCol>
                        </CRow>
                    </CCardBody>
                    <CCardFooter className="text-right">
                        <CButton type="submit" color={appConstants.SAVE_BTN_COLOR} style={{float: 'right'}}>Submit</CButton>
                        <CButton color={appConstants.CANCEL_BTN_COLOR} style={{float: 'right', marginRight: '5px'}} onClick={gotoRoleList}>Cancel</CButton>
                    </CCardFooter>
                </CCard>
            </form>
        </>
    );
}
export default CreateRoleComponent;