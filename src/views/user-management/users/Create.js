import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import { useHistory } from 'react-router-dom';
import { useParams} from "react-router";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus } from '@fortawesome/free-solid-svg-icons';
import * as UserConfigs from './../../../shared/UserManagementConfig';
import * as appConstants from './../../../AppConstants';
import UsersService from './../../../api/UsersService';
import GlobalState from './../../../shared/GlobalState';
import SimpleReactValidator from 'simple-react-validator';
import SingleTextInput from './../../form-components/SingleTextInput';
import NameSection from './../../form-components/section-components/NameSection';
import SelectBox from './../../form-components/SelectBox';
import CheckboxList from './../../form-components/CheckboxList';
import RadioButtonList from './../../form-components/RadioButtonList';
import SingleCheckbox from './../../form-components/SingleCheckbox';

const CreateUserComponent = () => {
    const [state, setState] = useContext(GlobalState);
    const history = useHistory();
    const params = useParams();
    const [user, setUser] = useState({
        first_name: null, middle_name: null, last_name:null, status: null,
        languages_known: null, designation: null, department: null, email: null, salutation: null, unit: null, 
        email_2: null, phone_1: null, phone_2: null, cug_extn: null, med_reg_num: null, qualification: null,
        gender: null, dob: null, manager: null
    });
    const [userParams, setUserParams] = useState({
        selectedLanguages : [],
        confirmPass: null,
        managerList: []
    });
    
    const validator = useRef(new SimpleReactValidator());
    const [, forceUpdate] = useState();

    useEffect(() => {
        console.log("User Create page loaded. Msg. from UseEffect")
        
        getUserById();
     
    }, []);
    
    const getUserById = () => {
        if(params.user_id > 0)
        {
            UsersService.getUserById(params.user_id, state.authHeader).then(resp => {
                if(resp.data.id)
                {                        
                    console.log("Setting User Id to local state variable")
                    if(resp.data.languages_known)
                        userParams.selectedLanguages = resp.data.languages_known.split(", ");
                    setUserParams({...userParams, confirmPass: resp.data.password});
                    setUser(resp.data)
                    console.log("User Data retrieved Successfully")
                }
            });
        }
        return;
    }

    

    const saveOrUpdateUser = (data, e) => {
        if(data.password !== userParams.confirmPass)
        {
            alert("password not matching, please re-enter again");
            return false;
        }
        if(params.user_id){
            console.log('User data updating..: ', data)
            e.preventDefault();
            UsersService.updateUser(data, params.user_id, state.authHeader).then(res =>{
                console.log(res)
                setUser({});
                history.replace('/user/list');
            });
        }
        else{                
            console.log('User data saving')
            e.preventDefault();
            UsersService.createUser(data, state.authHeader).then(res =>{
                console.log(res)
                setUser({});
                history.replace('/user/list');
            });
        }
    }

    const validateForm = (e) => {
        console.log(user)
        e.preventDefault();
        if (validator.current.allValid()) {
            console.log('Valid')
            saveOrUpdateUser(user, e)
        } else {
            console.log('Not Valid')
            validator.current.showMessages();
            forceUpdate(1)
        }
    }

    const handleChange = (e) => {
        console.log(e.target.name, e.target.value)
        if(e.target.name === 'confirmPass')
            setUserParams({...userParams, [e.target.name]: e.target.value});
        else if(e.target.name == 'is_manager')
            setUser({ ...user, [e.target.name]: !user.is_manager });
        else
            setUser({ ...user, [e.target.name]: e.target.value });
        console.log(user)
    }    

    const checkboxChangeHandler = (e) => {
        const newSelection = e.target.value;
        let newSelectionArray;
        switch(e.target.name)
        {
            case 'languages_known' :    if(userParams.selectedLanguages.indexOf(newSelection) > -1) {
                                            newSelectionArray = userParams.selectedLanguages.filter(s => s !== newSelection)
                                        } else {
                                            newSelectionArray = [...userParams.selectedLanguages, newSelection];
                                        }
                                        userParams.selectedLanguages = newSelectionArray;
                                        setUser({ ...user, [e.target.name]: userParams.selectedLanguages.join(", ")});
                                        break;

        }        
    }

    const resetUser = () => {
        setUser({});
    }

    const gotoUserList = () => {
        resetUser();
        history.replace('/user/list');
    }

    return(
        <>
            <form  onSubmit={validateForm} className="form-horizontal">
                <CCard accentColor="primary">
                    <CCardHeader>
                        <h3> User { params.user_id && 'Update' } { !params.user_id && 'Create' } Form</h3> 
                    </CCardHeader>
                    <CCardBody> 
                        <NameSection obj={user} validators={validator} controlFunc={handleChange} />
                        { !params.user_id && <CRow className="formMargins">
                            <CCol xs="12" sm="12" md="4" lg="4" xl="4">
                                <SingleTextInput name="username" 
                                    title="Employee Id" 
                                    inputType="text" 
                                    content={(user.username)?(user.username):''} 
                                    placeholder="Employee Id" 
                                    manadatorySymbol={true} 
                                    controlFunc={handleChange}/>
                                {validator.current.message('username', user.username, 'required')}
                            </CCol>
                            <CCol xs="12" sm="12" md="4" lg="4" xl="4">
                                <SingleTextInput name="password" 
                                    title="Password" 
                                    inputType="password" 
                                    content={(user.password)?(user.password):''} 
                                    placeholder="Password" 
                                    manadatorySymbol={true} 
                                    controlFunc={handleChange}/>
                                {validator.current.message('password', user.password, 'required')}
                            </CCol>
                            <CCol xs="12" sm="12" md="4" lg="4" xl="4">
                                <SingleTextInput name="confirmPass" 
                                    title="Confirm Password" 
                                    inputType="password" 
                                    content={(userParams.confirmPass)?(userParams.confirmPass):''} 
                                    placeholder="Confirm Password" 
                                    manadatorySymbol={true} 
                                    controlFunc={handleChange}/>
                                    {validator.current.message('confirmPass', userParams.confirmPass, 'required')}
                            </CCol>
                        </CRow>}
                        <CRow className="formMargins">
                            <CCol xs="12" sm="12" md="8" lg="8" xl="8">
                                <CRow>
                                    <CCol xs="12" sm="12" md="6" lg="6" xl="6">
                                        <SingleTextInput name="dob" 
                                            title="DOB" 
                                            inputType="date" 
                                            content={(user.dob)?(user.dob):appConstants.PICKER_DEFAULT_DATE} 
                                            placeholder="DOB" 
                                            manadatorySymbol={true} 
                                            controlFunc={handleChange}/>
                                        {validator.current.message('dob', user.dob, 'required')}
                                    </CCol>
                                    <CCol xs="12" sm="12" md="6" lg="6" xl="6">
                                        <RadioButtonList 
                                            name="gender"
                                            title="Gender"
                                            controlFunc={ handleChange }
                                            options={ appConstants.GENDER_OPTIONS }
                                            content={ user.gender }
                                            manadatorySymbol={true} />
                                        {validator.current.message('gender', user.gender, 'required')}
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12" sm="12" md="6" lg="6" xl="6">
                                        <SingleTextInput name="email" 
                                            title="Email" 
                                            inputType="email" 
                                            content={(user.email)?(user.email):''} 
                                            placeholder="Email" 
                                            manadatorySymbol={true} 
                                            controlFunc={handleChange}/>
                                            {validator.current.message('email', user.email, 'email|required')}
                                    </CCol>
                                    <CCol xs="12" sm="12" md="6" lg="6" xl="6">
                                        <SingleTextInput name="email_2" 
                                            title="Alternative Email" 
                                            inputType="email_2" 
                                            content={(user.email_2)?(user.email_2):''} 
                                            placeholder="Alternative Email" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}/>
                                            {validator.current.message('email_2', user.email_2, 'email')}
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12" sm="12" md="6" lg="6" xl="6">
                                        <SingleTextInput name="phone_1" 
                                            title="Phone Number" 
                                            inputType="text" 
                                            content={(user.phone_1)?(user.phone_1):''} 
                                            placeholder="Phone Number" 
                                            manadatorySymbol={true} 
                                            controlFunc={handleChange}/>
                                            {validator.current.message('phone_1', user.phone_1, 'required|numeric|phone')}
                                    </CCol>
                                    <CCol xs="12" sm="12" md="6" lg="6" xl="6">
                                        <SingleTextInput name="phone_2" 
                                            title="Alternative Phone Number" 
                                            inputType="text" 
                                            content={(user.phone_2)?(user.phone_2):''} 
                                            placeholder="Alternative Phone Number" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}/>
                                            {validator.current.message('phone_2', user.phone_2, 'numeric|phone')}
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12" sm="12" md="6" lg="6" xl="6">
                                        <SingleTextInput name="department" 
                                            title="Department" 
                                            inputType="text" 
                                            content={(user.department)?(user.department):''} 
                                            placeholder="Department" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}/>
                                    </CCol>
                                    <CCol xs="12" sm="12" md="6" lg="6" xl="6">
                                        <SingleTextInput name="unit" 
                                            title="Unit" 
                                            inputType="text" 
                                            content={(user.unit)?(user.unit):''} 
                                            placeholder="Unit" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}/>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12" sm="12" md="6" lg="6" xl="6">
                                        <SingleTextInput name="designation" 
                                            title="Designation" 
                                            inputType="text" 
                                            content={(user.designation)?(user.designation):''} 
                                            placeholder="Designation" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}/>
                                    </CCol>   
                                    <CCol xs="12" sm="12" md="6" lg="6" xl="6">
                                        <SelectBox
                                            name="status"
                                            title="Status"
                                            placeholder='Status'
                                            controlFunc={handleChange}
                                            options={UserConfigs.STATUS}
                                            selectedOption={user.status?user.status:'Active'}
                                            manadatorySymbol={true} 
                                            label = {'label'}
                                            value={'value'}/>
                                    </CCol>
                                </CRow>
                            </CCol>
                            <CCol xs="12" sm="12" md="4" lg="4" xl="4">
                                <CRow>
                                    <CCol xs="12" sm="12" md="12" lg="12" xl="12">
                                        <SingleTextInput name="salutation" 
                                            title="Salutation" 
                                            inputType="text" 
                                            content={(user.salutation)?(user.salutation):''} 
                                            placeholder="Salutation" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}/>
                                    </CCol>
                                    <CCol xs="12" sm="12" md="12" lg="12" xl="12">
                                        <SingleTextInput name="qualification" 
                                            title="Qualification" 
                                            inputType="text" 
                                            content={(user.qualification)?(user.qualification):''} 
                                            placeholder="Qualification" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}/>
                                    </CCol>
                                    <CCol xs="12" sm="12" md="12" lg="12" xl="12">
                                        <SingleTextInput name="cug_extn" 
                                            title="CUG/Extn" 
                                            inputType="text" 
                                            content={(user.cug_extn)?(user.cug_extn):''} 
                                            placeholder="CUG/Extn" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}/>
                                    </CCol>
                                    <CCol xs="12" sm="12" md="12" lg="12" xl="12">
                                        <SingleTextInput name="med_reg_num" 
                                            title="Medical Register Number" 
                                            inputType="text" 
                                            content={(user.med_reg_num)?(user.med_reg_num):''} 
                                            placeholder="Medical Register Number" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}/>
                                    </CCol>
                                    <CCol xs="12" sm="12" md="12" lg="12" xl="12">
                                        <CRow>
                                            <CCol xs="6" sm="6" md="6" lg="6" xl="6">
                                                <SingleCheckbox 
                                                    controlFunc={handleChange} 
                                                    name='is_manager'
                                                    title='Is Manager?'
                                                    content={user.is_manager?user.is_manager:false} />
                                            </CCol>
                                            <CCol xs="6" sm="6" md="6" lg="6" xl="6">
                                                <SelectBox
                                                    name="manager"
                                                    title="Manager"
                                                    placeholder='Manager'
                                                    controlFunc={handleChange}
                                                    options={userParams?.managerList}
                                                    selectedOption={user.manager?user.manager:''}
                                                    manadatorySymbol={false}
                                                    label = {'first_name'}
                                                    value={'id'}/>
                                            </CCol>
                                        </CRow>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow>
                        <CRow className="formMargins">
                            <CCol xs="12" sm="12" md="12" lg="12" xl="12">                              
                                <CheckboxList 
                                    name="languages_known"
                                    title="Languages Known"
                                    controlFunc={checkboxChangeHandler}
                                    options={ UserConfigs.LANGUAGES }
                                    selectedOptions={ userParams.selectedLanguages }
                                    manadatorySymbol={false} />
                            </CCol>
                        </CRow>
                    </CCardBody>
                    <CCardFooter className="text-right">
                        <CButton type="submit" color={appConstants.SAVE_BTN_COLOR} style={{float: 'right'}}>Submit</CButton>
                        <CButton color={appConstants.CANCEL_BTN_COLOR} style={{float: 'right', marginRight: '5px'}} onClick={gotoUserList}>Cancel</CButton>
                    </CCardFooter>
                </CCard>
            </form>
        </>
    );
}
export default CreateUserComponent;