import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow, CDataTable, CLabel} from '@coreui/react';
import { useHistory } from 'react-router-dom';
import { useParams} from "react-router";
import * as appConstants from './../../../AppConstants';
import RolesService from './../../../api/RolesService';
import UsersService from './../../../api/UsersService';
import GlobalState from './../../../shared/GlobalState';
import SimpleReactValidator from 'simple-react-validator';
import SingleTextInput from './../../form-components/SingleTextInput';

const fields = [
    {
        key: '#',
        label: '#'
    },
    {
        key: 'name',
        label: 'Name'
    },
    {
        key: 'assign_roles',
        label: 'Choose Roles'
    }
];

const AssignRolesComponent = () => {
    const [state, setState] = useContext(GlobalState);
    const history = useHistory();
    const params = useParams();
    const [roleList, setRoleList] = useState([]);
    const [user, setUser] = useState({});

    const validator = useRef(new SimpleReactValidator());
    const [, forceUpdate] = useState();

    useEffect(() => {
        console.log("Role assignment page loaded. Msg. from UseEffect")
        
        getUserById();
        getAllRoles();
    }, []);
    
    const getUserById = () => {
        if(params.user_id > 0)
        {
            UsersService.getUserById(params.user_id, state.authHeader).then(resp => {
                if(resp.data.id)
                {                        
                    console.log("Setting User Id to local state variable")
                    setUser(resp.data)
                    console.log("User Data retrieved Successfully")
                }
            });
        }
    }

    const getAllRoles = () => {
        RolesService.getRoles(state.authHeader).then(resp => {
            console.log(resp)
            setRoleList(resp)
        });
    }

    const validateForm = (e) => {
        //console.log(role)
        e.preventDefault();
        if (validator.current.allValid()) {
            console.log('Valid')
            //saveOrUpdateRole(role, e)
        } else {
            console.log('Not Valid')
            validator.current.showMessages();
            forceUpdate(1)
        }
    }

    const gotoUserList = () => {
        history.replace('/user/list');
    }

    const assignRoles = (user, roleName) => {
        RolesService.assignRoles(user.id, roleName, state.authHeader).then(resp => {
            console.log(roleName +" Role assigned to " + user.first_name + ' ' + user.last_name + " successfully.");
            console.log(resp)
        });
    }

    const removeRoles = (user, roleName) => {
        RolesService.removeRoles(user.id, roleName, state.authHeader).then(resp => {
            console.log(roleName +" Role removed from " + user.first_name + ' ' + user.last_name + " successfully.");
            console.log(resp)
        });
    }

    const saveRoles = (e, role) => {
        console.log(e.target.checked, role)
        if(e.target.checked)
        {
            assignRoles(user, role.name);
        }
        else{
            removeRoles(user, role.name);
        }
    }

    return(
        <>
            <form  onSubmit={validateForm} className="form-horizontal">
                <CCard accentColor="primary">
                    <CCardHeader>
                        <h3> Assign Roles to {user.first_name} {user.last_name}</h3> 
                    </CCardHeader>
                    <CCardBody>
                        <CDataTable
                            items={roleList.data}
                            fields={fields}
                            itemsPerPage={appConstants.ITEMS_PER_PAGE}
                            pagination
                            tableFilter
                            itemsPerPageSelect
                            hover
                            sorter                    
                            scopedSlots = {
                                {
                                    '#': (item, index)=>{
                                        return (
                                            <td className="py-2">
                                                <CLabel>{index+1}</CLabel>
                                            </td>
                                        )
                                    },
                                    'name': (item, index)=>{
                                        return (
                                            <td className="py-2">
                                                <CLabel>{item.name}</CLabel>
                                            </td>
                                        )
                                    },
                                    'assign_roles':(item, index)=>{
                                        return (
                                            <td className="py-2">
                                                <input type="checkbox" name="roles" onChange={e => {saveRoles(e, item)}} defaultChecked={user.roles?.find(role => role.id === item.id)}/>
                                            </td>
                                        )
                                    },
                                }
                            }
                        />                        
                    </CCardBody>
                    <CCardFooter className="text-right">
                        <CButton color={appConstants.CANCEL_BTN_COLOR} style={{float: 'right', marginRight: '5px'}} onClick={gotoUserList}>Go Back</CButton>
                    </CCardFooter>
                </CCard>
            </form>
        </>
    );
}
export default AssignRolesComponent;