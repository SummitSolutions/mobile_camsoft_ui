import React, { useContext, useEffect, useState } from 'react';
import UsersService from './../../../api/UsersService';
import RolesService from './../../../api/RolesService';
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CButtonGroup,
    CLink,
    CButton,
    CLabel
} from '@coreui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus, faUserTag } from '@fortawesome/free-solid-svg-icons';
import * as momentService from './../../../shared/MomentService';
import * as appConstants from './../../../AppConstants';
import GlobalState from './../../../shared/GlobalState';
import { useHistory } from 'react-router-dom';

const fields = [
    {
        key: '#',
        label: '#'
    },
    {
        key: 'name',
        label: 'Name'
    },
    {
        key: 'username',
        label: 'Employee Id'
    },
    {
        key: 'edit_user',
        label: 'Action'
    }
];

const UserList = () => {

    const [state, setState] = useContext(GlobalState);
    const [userList, setUserList] = useState({data : null});
    const history = useHistory();
    
    useEffect(() => {
        console.log("User List page loading")
        console.log(state.authHeader)
        UsersService.getUsers(state.authHeader).then((res) => {
            setUserList({ data: res.data});
        });
    }, []);

    const editUser = (user) => {
        history.push('/user/update/'+user.id);
    }

    const AssignRoles = (user) => {
        history.push('/user/assign-roles/'+user.id);
    }

    const loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard accentColor="primary">
                    <CCardHeader>
                        User List
                        <CLink 
                            className="c-subheader-nav-link" 
                            aria-current="page" 
                            to="/user/create"
                            style={{float:'right'}}>
                            <FontAwesomeIcon icon={faPlus} />&nbsp;New User
                        </CLink>
                    </CCardHeader>
                    <CCardBody>
                    <CDataTable
                        items={userList.data}
                        fields={fields}
                        itemsPerPage={appConstants.ITEMS_PER_PAGE}
                        pagination
                        tableFilter
                        itemsPerPageSelect
                        hover
                        sorter                    
                        scopedSlots = {
                            {
                                '#': (item, index)=>{
                                    return (
                                        <td className="py-2">
                                            <CLabel>{index+1}</CLabel>
                                        </td>
                                    )
                                },
                                'name': (item, index)=>{
                                    return (
                                        <td className="py-2">
                                            <CLabel>{item.first_name} &nbsp; {item.last_name}</CLabel>
                                        </td>
                                    )
                                },
                                'username': (item, index)=>{
                                    return (
                                        <td className="py-2">
                                            <CLabel>{item.username}</CLabel>
                                        </td>
                                    )
                                },
                                'edit_user':(item, index)=>{
                                    return (
                                        <td className="py-2">
                                            <CButtonGroup>
                                                <CButton
                                                    color={appConstants.CANCEL_BTN_COLOR}
                                                    size="sm"
                                                    title="Edit User"
                                                    onClick={()=>{editUser(item)}}
                                                >
                                                    <FontAwesomeIcon icon={faPencilAlt} />
                                                </CButton>
                                                <CButton
                                                    color={appConstants.SAVE_BTN_COLOR}
                                                    size="sm"
                                                    title="Assign Roles"
                                                    onClick={()=>{AssignRoles(item)}}
                                                >
                                                    <FontAwesomeIcon icon={faUserTag} />
                                                </CButton>
                                            </CButtonGroup>
                                        </td>
                                    )
                                },
                            }
                        }
                    />
                    </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
}

export default UserList;