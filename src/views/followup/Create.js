import React, { useEffect, useState, useRef, Fragment, useContext } from 'react'
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CFormText, CLabel, CRow, CFormGroup, CButton } from '@coreui/react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../form-components/SingleTextInput';
import CheckboxList from '../form-components/CheckboxList';
import RadioButtonList from '../form-components/RadioButtonList';
//import SelectBox from '../form-components/SelectBoxNew';
import SimpleReactValidator from 'simple-react-validator';
import FollowupService from '../../api/FollowupService';
import * as appConstants from './../../AppConstants';
import moment from 'moment';
import GlobalState from './../../shared/GlobalState';
import ResearchService from '../../api/ResearchService';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import SingleCheckbox from '../form-components/SingleCheckbox';
import SelectBox from '../form-components/SelectBox';
import * as momentServices from './../../shared/MomentService';
import * as DateConfigs from './../../shared/configs/DateConfigs';
import KeyboardDatePickers from '../form-components/KeyboardDatePicker';

export default function CreateFollowupComponent() {
    const { register, handleSubmit, errors, getValues, watch, formState } = useForm();
    const [state, setState] = useContext(GlobalState);
    const history = useHistory();
    const params = useParams();
    const [followup, setFollowup] = useState({
    });
    const [researchList, setResearchList] = useState([]);
    const validator = useRef(new SimpleReactValidator());
    const [, forceUpdate] = useState();
    const animatedComponents = makeAnimated();
    const alcoholOptions = [
        { value: 'beer', label: 'Beer' },
        { value: 'wine', label: 'Wine' },
        { value: 'spirit', label: 'spirit' },
        { value: 'arrack', label: 'Arrack' },
        { value: 'country_liquor', label: 'Country liquor' },
        { value: 'vodka', label: 'Vodka' },
        { value: 'brandy', label: 'Brandy' },
        { value: 'rum', label: 'Rum' },
        { value: 'gin', label: 'Gin' },
        { value: 'toddy', label: 'Toddy' },
    ];

    useEffect(() => {
        console.log("Patient Create page loaded. Msg. from UseEffect")
        getResearch();
        getAllFollowups();
    }, []);

    const getResearch = () => {
        ResearchService.getResearch(state.authHeader).then(resp => {
            setResearchList(resp.data);
            console.log(resp.data)
        });
    }

    const getFollowupById = () => {
        if (params.id > 0) {
            FollowupService.getFollowupById(params.id, state.authHeader).then(resp => {
                if (resp.data.data.id) {
                    if (resp.data.data.follow_up_date) {
                        resp.data.data.follow_up_date = moment(resp.data.data.follow_up_date).locale('en').format('YYYY-MM-DD');
                    }
                    setFollowup(resp.data.data)
                    console.log("Patient Data retrieved Successfully")
                }
            });
        }
    }
    const getAllFollowups = () => {
        FollowupService.getAllFollowup(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setFollowup(resp.data);
        });
    }

    const saveOrUpdateFollowup = (data, event) => {
        console.log(data.id)
        if (data.id) {
            console.log('Patient data updating..: ', data)
            FollowupService.updateFollowup(data, data.id, state.authHeader).then(res => {
                console.log(res)
                setFollowup(res.data);

            });
        }
        else {
            console.log('Patient data saving')
            data.patient_id = parseInt(params.patient_id);
            data.visit_id = parseInt(params.visit_id);
            FollowupService.createFollowup(data, state.authHeader).then(res => {
                console.log(res)
                setFollowup(res.data);

            });
        }

    }


    const handleChange = (event) => {
        const { name, value } = event.target;
        console.log('auto saving function')
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setFollowup({ ...followup, [event.target.name]: (event.target.checked) ? (value) : '' });
            followup[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setFollowup({ ...followup, [event.target.name]: value });
            followup[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdateFollowup(followup, event);

    };
    const handleBlurChange = (event) => {
        console.log("blur is calling")
        saveOrUpdateFollowup(followup, event);
    }
    const handleKeyboardPickerChange = (date) => {
        setFollowup({ ...followup, 'follow_up_date': date });
    }
    const handleKeyboardPicker = (date) => {
        setFollowup({ ...followup, 'expiry_date': date });
    }


    return (
        <>
            <CCard accentColor="primary">
                <CCardHeader>
                    <h3>Followup Summary</h3>
                </CCardHeader>
                <CCardBody>
                    <CRow>
                        <CCol xs="12" md="6" lg="6">
                            <CCard borderColor="primary">
                                <CCardBody>
                                    <CRow>
                                        <CCol xs="12" md="4" lg="4">
                                            <SelectBox
                                                name="cgi_improvement"
                                                title="CGI improvement"
                                                placeholder='CGI improvement'
                                                controlBlurFunc={handleBlurChange}
                                                controlFunc={handleChange}
                                                options={appConstants.CGI}
                                                selectedOption={followup.cgi_improvement ? followup.cgi_improvement : ''}
                                            />
                                        </CCol>

                                        <CCol xs="4" md="3" lg="3">
                                            <SingleTextInput name="pulse"
                                                title="Pulse"
                                                inputType="text"
                                                content={(followup.pulse) ? (followup.pulse) : ''}
                                                placeholder="Pulse"
                                                manadatorySymbol={false}
                                                controlBlurFunc={handleBlurChange}
                                                controlFunc={handleChange} />
                                        </CCol>
                                        <CCol xs="4" md="2" lg="2">
                                            <SingleTextInput name="bp"
                                                title="BP"
                                                inputType="text"
                                                content={(followup.bp) ? (followup.bp) : ''}
                                                placeholder="BP"
                                                manadatorySymbol={false}
                                                controlBlurFunc={handleBlurChange}
                                                controlFunc={handleChange} />
                                        </CCol>
                                        <CCol xs="4" md="3" lg="3">
                                            <SingleTextInput name="weight"
                                                title="Weight"
                                                inputType="text"
                                                content={(followup.weight) ? (followup.weight) : ''}
                                                placeholder="Weight"
                                                manadatorySymbol={false}
                                                controlBlurFunc={handleBlurChange}
                                                controlFunc={handleChange} />
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="12" md="6" lg="6">
                                            <RadioButtonList
                                                name="compliance"
                                                title="Compliance"
                                                controlBlurFunc={handleBlurChange}
                                                controlFunc={handleChange}
                                                options={appConstants.COMPLIANCE}
                                                content={followup.compliance}
                                                manadatorySymbol={false} />
                                            {/* {validator.current.message('compliance', patient.compliance, 'required')} */}
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="12" md="10" lg="10">
                                            <SingleTextInput name="complication"
                                                title="Complication"
                                                inputType="text"
                                                content={(followup.complication) ? (followup.complication) : ''}
                                                placeholder="Complication"
                                                manadatorySymbol={false}
                                                controlFunc={handleChange}
                                                controlBlurFunc={handleBlurChange}
                                                multiline={true}
                                                rows={3}
                                                variant="outlined" />
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="12" md="10" lg="10">
                                            <SingleTextInput name="notes"
                                                title="Notes"
                                                inputType="text"
                                                content={(followup.notes) ? (followup.notes) : ''}
                                                placeholder="Notes"
                                                manadatorySymbol={false}
                                                controlFunc={handleChange}
                                                controlBlurFunc={handleBlurChange}
                                                multiline={true}
                                                rows={3}
                                                variant="outlined" />
                                        </CCol>
                                    </CRow>
                                </CCardBody>
                            </CCard>
                        </CCol>
                        <CCol xs="12" md="6" lg="6">
                            <CCard borderColor="primary">
                                <CCardBody>
                                    <CRow>
                                        <CCol xs="12" md="8" lg="8">
                                            <SelectBox
                                                name="followup_mode"
                                                title="Next followup Mode"
                                                placeholder='Next followup Mode'
                                                controlBlurFunc={handleBlurChange}
                                                controlFunc={handleChange}
                                                options={appConstants.FOLLOWUPMODE}
                                                selectedOption={followup.followup_mode ? followup.followup_mode : ''}
                                            />
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="8" md="8" lg="6">
                                            {/* <SingleTextInput name="follow_up_date"
                                                title="Next followup date"
                                                inputType="date"
                                                content={(followup.follow_up_date) ? (followup.follow_up_date) : appConstants.FOLLOWUP_DATE}
                                                placeholder="Next followup date"
                                                manadatorySymbol={true}
                                                controlBlurFunc={handleBlurChange}
                                                controlFunc={handleChange} /> */}
                                            <KeyboardDatePickers
                                                name="follow_up_date"
                                                disableToolbar
                                                format="DD/MM/yyyy"
                                                content={(followup.follow_up_date) ? (followup.follow_up_date) : momentServices.currentDate(DateConfigs.DEFAULT_DATE_FORMAT_BACKEND)}
                                                manadatorySymbol={false}
                                                controlFunc={handleKeyboardPickerChange}
                                            />
                                            {validator.current.message('follow_up_date', followup.follow_up_date, 'required')}
                                        </CCol>
                                        <CCol xs="8" md="8" lg="6">
                                            <SingleCheckbox
                                                controlFunc={handleChange}
                                                name='followup_hold'
                                                title='Followup on hold'
                                                type="checkbox"
                                                value='followup_hold'
                                                content={(followup.followup_hold) ? (followup.followup_hold) : ''}
                                            />
                                        </CCol>
                                    </CRow>
                                    &nbsp;
                                    <CRow>
                                        {followup.followup_hold &&
                                            <CCol xs="12" md="12" lg="12">
                                                <SelectBox
                                                    name="reason"
                                                    title="Reason"
                                                    placeholder='Reason'
                                                    controlBlurFunc={handleBlurChange}
                                                    controlFunc={handleChange}
                                                    options={appConstants.REASON}
                                                    selectedOption={followup.reason ? followup.reason : ''}
                                                />
                                            </CCol>}
                                        <CCol xs="12" md="6" lg="6">
                                            {followup.reason == 'Expired' && followup.followup_hold &&
                                                // <SingleTextInput name="expiry_date"
                                                //     title="Expiry date"
                                                //     inputType="date"
                                                //     content={(followup.expiry_date) ? (followup.expiry_date) : appConstants.EXPIRY_DATE}
                                                //     placeholder="Expiry date"
                                                //     manadatorySymbol={false}
                                                //     controlBlurFunc={handleBlurChange}
                                                //     controlFunc={handleChange} />}
                                                <KeyboardDatePickers
                                                    name="expiry_date"
                                                    disableToolbar
                                                    format="DD/MM/yyyy"
                                                    content={(followup.expiry_date) ? (followup.expiry_date) : momentServices.currentDate(DateConfigs.DEFAULT_DATE_FORMAT_BACKEND)}
                                                    manadatorySymbol={false}
                                                    controlFunc={handleKeyboardPicker}
                                                />}
                                        </CCol>
                                        <CCol xs="12" md="10" lg="10">
                                            {followup.reason == 'Expired' && followup.followup_hold &&
                                                <SingleTextInput name="expiry_reason"
                                                    title="Expiry Reason"
                                                    inputType="text"
                                                    content={(followup.expiry_reason) ? (followup.expiry_reason) : ''}
                                                    placeholder="Expiry Reason"
                                                    manadatorySymbol={false}
                                                    controlBlurFunc={handleBlurChange}
                                                    controlFunc={handleChange}
                                                    multiline={true}
                                                    rows={3}
                                                    variant="outlined" />}
                                        </CCol>
                                        <CCol xs="12" md="12" lg="12">
                                            {followup.reason == 'Under treatment from other agency' && followup.followup_hold &&
                                                <SingleTextInput name="agency"
                                                    title="Agency"
                                                    inputType="text"
                                                    content={(followup.agency) ? (followup.agency) : ''}
                                                    placeholder="Agency"
                                                    manadatorySymbol={false}
                                                    controlBlurFunc={handleBlurChange}
                                                    controlFunc={handleChange}
                                                    multiline={true}
                                                    rows={6}
                                                    variant="outlined" />}
                                        </CCol>
                                        <CCol xs="12" md="12" lg="12">
                                            {followup.reason == 'Others' && followup.followup_hold &&
                                                <SingleTextInput name="description"
                                                    title="Description"
                                                    inputType="text"
                                                    content={(followup.description) ? (followup.description) : ''}
                                                    placeholder="Description"
                                                    manadatorySymbol={false}
                                                    controlBlurFunc={handleBlurChange}
                                                    controlFunc={handleChange}
                                                    multiline={true}
                                                    rows={6}
                                                    variant="outlined" />}
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="12" md="12" lg="12">
                                            <SingleTextInput name="informant_name"
                                                title="Informant Name"
                                                inputType="text"
                                                content={(followup.informant_name) ? (followup.informant_name) : ''}
                                                placeholder="Informant Name"
                                                manadatorySymbol={false}
                                                controlBlurFunc={handleBlurChange}
                                                controlFunc={handleChange} />
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="12" md="12" lg="12">
                                            <SingleTextInput name="informant_relation"
                                                title="Informant Relation"
                                                inputType="text"
                                                content={(followup.informant_relation) ? (followup.informant_relation) : ''}
                                                placeholder="Informant Relation"
                                                manadatorySymbol={false}
                                                controlBlurFunc={handleBlurChange}
                                                controlFunc={handleChange} />
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    {/* <CRow>
                                        <CCol xs="12" md="6" lg="6">
                                            <SelectBox
                                                name='research_thesis'
                                                //ref={register}
                                                closeMenuOnSelect={false}
                                                components={followup.animatedComponents}
                                                isMulti
                                                options={alcoholOptions}
                                            //onChange={handleChangeMulti}
                                            />
                                        </CCol>
                                    </CRow> */}
                                    <br></br>
                                </CCardBody>
                            </CCard>
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>

        </>
    )
}




