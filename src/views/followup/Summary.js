import React, { useEffect, useState, useRef, Fragment, useContext } from 'react'
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CFormText, CLabel, CRow, CFormGroup, CButton } from '@coreui/react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import FollowupService from '../../api/FollowupService';
import PatientService from '../../api/PatientService';
import PatientVisitService from '../../api/PatientVisitService';
import EmotionalBehaviourService from '../../api/EmotionalBehaviourService';
import SocialFunctioningService from '../../api/SocialFunctioningService';
import PhysicalMedicalFunctioningService from '../../api/PhysicalMedicalFunctioningService';
import DiagnosisService from '../../api/DiagnosisService';
import PsychotherapyService from '../../api/PsychotherapyService';
import FurtherDetailService from '../../api/FurtherDetailService';
import CurrentFunctioningService from '../../api/CurrentFunctioningService';
import * as appConstants from '../../AppConstants'
import Button from '../form-components/Button';
import moment from 'moment';
import GlobalState from '../../shared/GlobalState';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import * as SummaryPrintService from '../../shared/print-services/SummaryPrintService';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    large: {
        width: theme.spacing(5),
        height: theme.spacing(5),
        marginLeft: '-50px'
    },
}));
export default function Summary() {
    const classes = useStyles();
    const [list, setList] = useState([]);
    const [patientList, setPatientList] = useState([]);
    const [visitList, setVisitList] = useState([]);
    const [emotionalList, setEmotionalList] = useState([]);
    const [socialList, setSocialList] = useState([]);
    const [physicalList, setPhysicalList] = useState([]);
    const [diaList, setDiaList] = useState([]);
    const [psychList, setPsychList] = useState([]);
    const [furtherList, setFurtherList] = useState([]);
    const [currentList, setCurrentList] = useState([]);
    const [state, setState] = useContext(GlobalState);
    const history = useHistory();

    const params = useParams();

    useEffect(() => {
        getFollowupById();
        getPatientById();
        getPatientVisitById();
        getAllEmotionalBehaviours();
        getAllSocialFunctionings();
        getAllPhysicalMedicalFunctionings();
        getDiagnosisDataById();
        getAllPsychotherapys();
        getAllFurtherDetails();
        getAllCurrentFunctionings();

    }, []);

    const getFollowupById = () => {
        FollowupService.getAllFollowup(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setList(resp.data);
        });
    }
    const getPatientById = () => {
        PatientService.getPatientById(params.patient_id, state.authHeader).then(resp => {
            setPatientList(resp.data);
        });
    }
    const getPatientVisitById = () => {
        PatientVisitService.getPatientVisitById(params.patient_id, state.authHeader).then(resp => {
            setVisitList(resp.data);
        });
    }
    const getAllEmotionalBehaviours = () => {
        EmotionalBehaviourService.getAllEmotionalBehaviours(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setEmotionalList(resp.data);
        });
    }
    const getAllSocialFunctionings = () => {
        SocialFunctioningService.getAllSocialFunctionings(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setSocialList(resp.data);
        });
    }
    const getAllPhysicalMedicalFunctionings = () => {
        PhysicalMedicalFunctioningService.getAllPhysicalMedicalFunctionings(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setPhysicalList(resp.data);
        });
    }
    const getDiagnosisDataById = () => {
        DiagnosisService.getDiagnosisDataById(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setDiaList(resp.data);
        });
    }

    const getAllPsychotherapys = () => {
        PsychotherapyService.getAllPsychotherapys(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setPsychList(resp.data);
        });
    }
    const getAllFurtherDetails = () => {
        FurtherDetailService.getAllFurtherDetails(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setFurtherList(resp.data);
        });
    }
    const getAllCurrentFunctionings = () => {
        CurrentFunctioningService.getAllCurrentFunctionings(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setCurrentList(resp.data);
        });
    }


    return (
        <form id="printSummary">
            <CCard>
                <CCardHeader>
                    <CRow>
                        <CCol>
                            <h4>Summary Sheet</h4>
                        </CCol>
                        <CCol className="text-right">
                            <Button label="Print"
                                color={appConstants.EDIT_BTN_COLOR}
                                handleClick={() => { SummaryPrintService.printSummary(list) }}
                            />
                        </CCol>
                    </CRow>
                </CCardHeader>
                <CCardBody>
                    <CRow>
                        <CCol xs="4" sm="4" md="3" lg="3" xl="3" align="right">
                            <Avatar alt="CAMSOFT, NIMHANS" src={'avatars/Nimhans_logo.png'} />
                        </CCol>
                        <CCol align="center">
                            <h4>CENTRE FOR ADDICTION MEDICINE, NIMHANS</h4>
                        </CCol>
                    </CRow>
                    <fieldset className="scheduler-border">
                        <legend className="scheduler-border"><h3 className="subHeading">Followup Details</h3></legend>
                        <CRow id='DivIdToPrint'>
                            {(patientList.first_name) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="first_name">Name:<b>{patientList.first_name} {patientList.middle_name} {patientList.last_name} </b></CLabel>
                            </CCol>) : ''}
                            {(patientList.age) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="age">Age/Gender:<b>{patientList.age}/{patientList.gender}</b></CLabel>
                            </CCol>) : ''}
                            {(visitList.visit_date) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="visit_date">Visit Date:<b>{visitList.visit_date}</b></CLabel>
                            </CCol>) : ''}
                            {(list.pulse) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="pulse">PULSE:<b>{list.pulse}</b></CLabel>
                            </CCol>) : ''}
                            {(list.mode) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="mode">MODE:<b>{list.mode}</b></CLabel>
                            </CCol>) : ''}
                            {(list.bp) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="bp">BP: <b>{list.bp}</b></CLabel>
                            </CCol>) : ''}
                            {(list.cgi_improvement) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="cgi_improvement">CGI: <b>{list.cgi_improvement}</b></CLabel>
                            </CCol>) : ''}
                            {(list.weight) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="weight">WEIGHT: <b>{list.weight}</b></CLabel>
                            </CCol>) : ''}
                            {(list.compliance) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="compliance">COMPLIANCE: <b>{list.compliance}</b></CLabel>
                            </CCol>) : ''}
                            {(list.informant_name) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="informant_name">Informant name: <b>{list.informant_name}</b></CLabel>
                            </CCol>) : ''}
                            {(list.informant_relation) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="informant_relation">Informant relation: <b>{list.informant_relation}</b></CLabel>
                            </CCol>) : ''}
                            {(list.complication) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="complication">Complication: <b>{list.complication}</b></CLabel>
                            </CCol>) : ''}
                            {(list.notes) ? (<CCol xs="12" md="12" lg="6">
                                <CLabel htmlFor="notes">Followup Notes: <b>{list.notes}</b></CLabel>
                            </CCol>) : ''}
                        </CRow>
                    </fieldset>
                    <fieldset className="scheduler-border">
                        <legend className="scheduler-border"><h3 className="subHeading"><u>Current Functioning</u></h3></legend>
                        <CRow id='DivIdToPrint'>
                            <CCol xs="12" md="12" lg="12">
                                {currentList.current_functioning && <label> Alcohol:</label>}
                                {(currentList.abstinent_duration) ?
                                    <CLabel htmlFor="abstinent_duration"><b>{currentList.abstinent_duration},</b></CLabel>
                                    : ''}
                                {(currentList.abstinent_duration_uom) ?
                                    <CLabel htmlFor="abstinent_duration_uom"><b>{currentList.abstinent_duration_uom},</b></CLabel>
                                    : ''}
                                {(currentList.abstinent_laps) ?
                                    <CLabel htmlFor="abstinent_laps"><b>{currentList.abstinent_laps},</b></CLabel>
                                    : ''}
                                {(currentList.abstinent_lap_mgmt) ?
                                    <CLabel htmlFor="abstinent_lap_mgmt"><b>{currentList.abstinent_lap_mgmt},</b></CLabel>
                                    : ''}
                                {(currentList.relapsed_options) ?
                                    <CLabel htmlFor="relapsed_options"><b>{currentList.relapsed_options},</b></CLabel>
                                    : ''}
                                {(currentList.relapsed_duration) ?
                                    <CLabel htmlFor="relapsed_duration"><b>{currentList.relapsed_duration},</b></CLabel>
                                    : ''}
                                {(currentList.duration) ?
                                    <CLabel htmlFor="duration"><b>{currentList.duration},</b></CLabel>
                                    : ''}
                                {(currentList.notes) ?
                                    <CLabel htmlFor="notes"><b>{currentList.notes}</b></CLabel>
                                    : ''}
                                <legend className="scheduler-border"><h3 className="subHeading"><u>Current Status</u></h3></legend>

                                {(currentList.current_quantity) ?
                                    <CLabel htmlFor="current_quantity"><b>{currentList.current_quantity},</b></CLabel>
                                    : ''}
                                {(currentList.current_quantity_duration) ?
                                    <CLabel htmlFor="current_quantity_duration"><b>{currentList.current_quantity_duration},</b></CLabel>
                                    : ''}
                                {(currentList.last_use) ?
                                    <CLabel htmlFor="last_use"><b>{currentList.last_use},</b></CLabel>
                                    : ''}
                                {(currentList.current_status_notes) ?
                                    <CLabel htmlFor="current_status_notes"><b>{currentList.current_status_notes}</b></CLabel>
                                    : ''}
                            </CCol>
                        </CRow>
                    </fieldset>
                    <fieldset className="scheduler-border">
                        <legend className="scheduler-border"><h3 className="subHeading"><u>Current Social Functioning</u></h3></legend>
                        <CRow id='DivIdToPrint'>
                            <CCol xs="12" md="2" lg="2">
                                {(socialList.work) ?
                                    <CLabel htmlFor="work">Work:<b>{socialList.work}</b></CLabel>
                                    : ''}
                            </CCol>
                            <CCol xs="12" md="10" lg="10">
                                {(socialList.marital || socialList.family || socialList.friends || socialList.neighbour || socialList.na_interpersonal) && <label> Interpersonal:</label>}
                                {(socialList.marital) ?
                                    <CLabel htmlFor="marital"><b>{socialList.marital},</b></CLabel>
                                    : ''}
                                {(socialList.family) ?
                                    <CLabel htmlFor="family"><b>{socialList.family},</b></CLabel>
                                    : ''}
                                {(socialList.friends) ?
                                    <CLabel htmlFor="friends"><b>{socialList.friends},</b></CLabel>
                                    : ''}
                                {(socialList.neighbour) ?
                                    <CLabel htmlFor="neighbour"><b>{socialList.neighbour},</b></CLabel>
                                    : ''}
                                {(socialList.na_interpersonal) ?
                                    <CLabel htmlFor="na_interpersonal"><b>{socialList.na_interpersonal}</b></CLabel>
                                    : ''}
                            </CCol>
                            <CCol xs="12" md="10" lg="10">
                                {(socialList.divorce || socialList.criminal_case || socialList.na_legal_issues) && <label> Legal Issues:</label>}
                                {(socialList.divorce) ?
                                    <CLabel htmlFor="divorce"><b>{socialList.divorce},</b></CLabel>
                                    : ''}

                                {(socialList.criminal_case) ?
                                    <CLabel htmlFor="criminal_case"><b>{socialList.criminal_case},</b></CLabel>
                                    : ''}
                                {(socialList.na_legal_issues) ?
                                    <CLabel htmlFor="na_legal_issues"><b>{socialList.na_legal_issues}</b></CLabel>
                                    : ''}
                            </CCol>
                            <CCol xs="12" md="12" lg="12">
                                {(socialList.notes) ?
                                    <CLabel htmlFor="notes">Notes:<b>{socialList.notes}</b></CLabel>
                                    : ''}
                            </CCol>
                        </CRow>
                    </fieldset>
                    <fieldset className="scheduler-border">
                        {((emotionalList.externalizing_syndrome) || (emotionalList.mood_disoder) || (emotionalList.psychotic_disoder) ||
                            (emotionalList.addictive_behavior) || (emotionalList.anxiety_spec) || (emotionalList.anxiety_spec) ||
                            (emotionalList.other_spec) || (emotionalList.notes)) &&
                            <legend className="scheduler-border"><h3 className="subHeading"><u>Current Functional Emotional Behaviour</u></h3></legend>}
                        <CRow id='DivIdToPrint'>
                            <CCol xs="12" md="12" lg="12">
                                {(emotionalList.externalizing_syndrome) ?
                                    <CLabel htmlFor="externalizing_syndrome"><b>{emotionalList.externalizing_syndrome}</b></CLabel>
                                    : ''}
                                {(emotionalList.mood_disoder) ?
                                    <CLabel htmlFor="mood_disoder">,<b>{emotionalList.mood_disoder}</b></CLabel>
                                    : ''}

                                {(emotionalList.psychotic_disoder) ?
                                    <CLabel htmlFor="psychotic_disoder">,<b>{emotionalList.psychotic_disoder}</b></CLabel>
                                    : ''}
                                {(emotionalList.addictive_behavior) ?
                                    <CLabel htmlFor="addictive_behavior">,<b>{emotionalList.addictive_behavior}</b></CLabel>
                                    : ''}
                                {(emotionalList.anxiety_spec) ?
                                    <CLabel htmlFor="anxiety_spec">,<b>{emotionalList.anxiety_spec}</b></CLabel>
                                    : ''}
                                {(emotionalList.other_spec) ?
                                    <CLabel htmlFor="other_spec">,<b>{emotionalList.other_spec}</b></CLabel>
                                    : ''}
                                {(emotionalList.notes) ?
                                    <CLabel htmlFor="notes"><b>{emotionalList.notes}</b></CLabel>
                                    : ''}
                            </CCol>
                        </CRow>
                    </fieldset>
                    <fieldset className="scheduler-border">
                        {((physicalList.hypertension) || (physicalList.infections) || (physicalList.tuberculosis) ||
                            (physicalList.hiv) || (physicalList.fungal) || (physicalList.syphilis) || (physicalList.gastritis) ||
                            (physicalList.peripheral_neuritis) || (physicalList.pancreatitis) || (physicalList.alcholic_liver_disease) ||
                            (physicalList.notes)) &&
                            <legend className="scheduler-border"><h3 className="subHeading"><u>Physical/Medical Functioning</u></h3></legend>}
                        <CRow id='DivIdToPrint'>
                            <CCol xs="12" md="12" lg="12">
                                {(physicalList.hypertension) ?
                                    <CLabel htmlFor="hypertension"><b>{physicalList.hypertension}</b></CLabel>
                                    : ''}
                                {(physicalList.infections) ?
                                    <CLabel htmlFor="infections">,<b>{physicalList.infections}</b></CLabel>
                                    : ''}
                                {(physicalList.tuberculosis) ?
                                    <CLabel htmlFor="tuberculosis">,<b>{physicalList.tuberculosis}</b></CLabel>
                                    : ''}
                                {(physicalList.hiv) ?
                                    <CLabel htmlFor="hiv">,<b>{physicalList.hiv}</b></CLabel>
                                    : ''}
                                {(physicalList.fungal) ?
                                    <CLabel htmlFor="fungal">,<b>{physicalList.fungal}</b></CLabel>
                                    : ''}
                                {(physicalList.syphilis) ?
                                    <CLabel htmlFor="syphilis">,<b>{physicalList.syphilis}</b></CLabel>
                                    : ''}
                                {(physicalList.gastritis) ?
                                    <CLabel htmlFor="gastritis">,<b>{physicalList.gastritis}</b></CLabel>
                                    : ''}
                                {(physicalList.peripheral_neuritis) ?
                                    <CLabel htmlFor="peripheral_neuritis">,<b>{physicalList.peripheral_neuritis}</b></CLabel>
                                    : ''}
                                {(physicalList.pancreatitis) ?
                                    <CLabel htmlFor="pancreatitis">,<b>{physicalList.pancreatitis}</b></CLabel>
                                    : ''}
                                {(physicalList.alcholic_liver_disease) ?
                                    <CLabel htmlFor="alcholic_liver_disease">,<b>{physicalList.alcholic_liver_disease}</b></CLabel>
                                    : ''}
                                {(physicalList.notes) ?
                                    <CLabel htmlFor="notes"><b>{physicalList.notes}</b></CLabel>
                                    : ''}
                            </CCol>
                        </CRow>
                    </fieldset>
                    {(diaList.diagnosis_name || diaList.icd_code || diaList.types) &&
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Diagnosis Name</th>
                                    <th>ICD Code</th>
                                    <th>Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><fieldset> {diaList.diagnosis_name} </fieldset></td>
                                    <td><fieldset> {diaList.icd_code} </fieldset></td>
                                    <td><fieldset> {diaList.types} </fieldset></td>
                                </tr>
                            </tbody>
                        </table>}
                    <fieldset className="scheduler-border">
                        <legend className="scheduler-border"><h3 className="subHeading"><u></u></h3></legend>
                        <CRow id='DivIdToPrint'>
                            <CCol xs="12" md="12" lg="12">
                                {(diaList.notes) ?
                                    <CLabel htmlFor="notes"><b>{diaList.notes}</b></CLabel>
                                    : ''}

                            </CCol>
                        </CRow>
                    </fieldset>
                    <fieldset className="scheduler-border">
                        {((psychList.counselling) || (psychList.psychoeducation) || (psychList.family_therapy) ||
                            (psychList.individual_psychotherapy) || (psychList.tobacco_cessation_counselling)) &&
                            <legend className="scheduler-border"><h3 className="subHeading"><u>Psychotheraphy</u></h3></legend>}
                        <CRow id='DivIdToPrint'>
                            <CCol xs="12" md="12" lg="12">
                                {(psychList.counselling) ?
                                    <CLabel htmlFor="counselling"><b>{psychList.counselling},</b></CLabel>
                                    : ''}
                                {(psychList.psychoeducation) ?
                                    <CLabel htmlFor="psychoeducation"><b>{psychList.psychoeducation},</b></CLabel>
                                    : ''}
                                {(psychList.family_therapy) ?
                                    <CLabel htmlFor="family_therapy"><b>{psychList.family_therapy},</b></CLabel>
                                    : ''}
                                {(psychList.individual_psychotherapy) ?
                                    <CLabel htmlFor="individual_psychotherapy"><b>{psychList.individual_psychotherapy},</b></CLabel>
                                    : ''}
                                {(psychList.tobacco_cessation_counselling) ?
                                    <CLabel htmlFor="tobacco_cessation_counselling"><b>{psychList.tobacco_cessation_counselling}</b></CLabel>
                                    : ''}
                            </CCol>
                        </CRow>
                    </fieldset>

                    <fieldset className="scheduler-border">
                        {((furtherList.further_action) || (furtherList.admission) || (furtherList.followup_date) ||
                            (furtherList.referral)) &&
                            <legend className="scheduler-border"><h3 className="subHeading"><u>Further Details</u></h3></legend>}
                        <CRow id='DivIdToPrint'>
                            <CCol xs="12" md="12" lg="12">
                                {(furtherList.further_action) ?
                                    <CLabel htmlFor="further_action"><b>{furtherList.further_action},</b></CLabel>
                                    : ''}
                                {(furtherList.admission) ?
                                    <CLabel htmlFor="admission"><b>{furtherList.admission},</b></CLabel>
                                    : ''}
                                {(furtherList.followup_date) ?
                                    <CLabel htmlFor="followup_date"><b>{furtherList.followup_date},</b></CLabel>
                                    : ''}
                                {(furtherList.referral) ?
                                    <CLabel htmlFor="referral"><b>{furtherList.referral},</b></CLabel>
                                    : ''}
                            </CCol>
                        </CRow>
                    </fieldset>
                </CCardBody >
            </CCard >


        </form >
    )
}