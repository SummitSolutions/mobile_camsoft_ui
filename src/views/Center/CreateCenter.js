import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../form-components/SingleTextInput';
import SelectBox from '../form-components/SelectBox';
import SimpleReactValidator from 'simple-react-validator';
import CreateCenterService from '../../api/CreateCenterService';
import * as appConstants from './../../AppConstants';
import GlobalState from './../../shared/GlobalState';
import SingleCheckbox from '../form-components/SingleCheckbox';

export default function CreateCenter() {

    const { handleSubmit, errors, getValues, watch, formState } = useForm();
    const history = useHistory();
    const params = useParams();
    const [state, setState] = useContext(GlobalState);
    const [center, setCenter] = useState({
    });
    const validator = useRef(new SimpleReactValidator());
    useEffect(() => {
        getCentreById();
        console.log("Center Create page loaded. Msg. from UseEffect")
    }, []);

    const handleChange = (event) => {
        const { name, value } = event.target;
        console.log('auto saving function')
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setCenter({ ...center, [event.target.name]: (event.target.checked) ? (value) : '' });
            center[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setCenter({ ...center, [event.target.name]: value });
            center[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdatecenter(center, event);

    };

    const handleBlurChange = (event) => {
        console.log("Blur is calling")
        saveOrUpdatecenter(center, event);
    }

    // const gotoCenterList = () => {
    //     resetCenter();
    //     history.replace('/center/center-list');

    // }
    const saveOrUpdatecenter = (data, event) => {
        console.log(data)
        if (data.id) {
            console.log('center data updating..: ',)
            //let id = (params.patient_id) ? params.id : data.id
            CreateCenterService.updateCenter(data.id, data, state.authHeader).then(res => {
                console.log(res)
                setCenter(res.data);
            });
        }
        else {
            console.log('center saving')
            console.log(data)
            CreateCenterService.createCenter(data, state.authHeader).then(res => {
                console.log("here")
                console.log(res.data)
                setCenter(res.data);
            });
        }
    }

    const getCentreById = () => {
        if (params.center_id > 0) {
            CreateCenterService.getCenterById(params.center_id, state.authHeader).then(resp => {
                console.log("Setting Patient Id to state")
                setState(state => ({ ...state, center: resp.data }))
                setCenter(resp.data)
                console.log("Center Data retrieved Successfully")

            });
        }
    }

    return (
        <>

            <div className="body-container" >
                <CRow className="formMargins">
                    <CCol xs="12" md="6" lg="6">
                        <h3>Add new centre</h3>
                    </CCol>
                </CRow>
                &nbsp;
                <CRow className="formMargins">
                    <CCol xs="12" md="3" lg="6">
                        <SingleTextInput name="center_name"
                            title="Center Name"
                            inputType="text"
                            content={(center.center_name) ? (center.center_name) : ''}
                            placeholder="Center name"
                            manadatorySymbol={true}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                        {validator.current.message('center', center.center_name, 'required')}
                    </CCol>
                    <CCol xs="12" md="4" lg="6">
                        <SingleTextInput
                            name="center_code"
                            title="Centre code"
                            placeholder='Center code'
                            content={(center.center_code) ? (center.center_code) : ''}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange}
                            manadatorySymbol={true} />
                    </CCol>
                </CRow>
                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="6">
                        <SingleTextInput name="address"
                            title="Address"
                            inputType="text"
                            content={(center.address) ? (center.address) : ''}
                            placeholder="Address"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange}
                        />
                    </CCol>
                    <CCol xs="12" md="6" lg="6">
                        <SingleTextInput name="town_or_city"
                            title="Town/City"
                            inputType="text"
                            content={(center.town_or_city) ? (center.town_or_city) : ''}
                            placeholder="City / Village"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                </CRow>
                <CRow className="formMargins">
                    <CCol xs="12" md="6" lg="6">
                        <SelectBox
                            name="country"
                            title="Country"
                            placeholder='Country'
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange}
                            options={appConstants.LOCATIONCOUNTRIES}
                            selectedOption={center.country ? center.country : ''}
                            manadatorySymbol={false} />
                    </CCol>
                    <CCol xs="12" md="6" lg="6">
                        {center.country == 'India' && <SelectBox
                            name="state"
                            title="State"
                            placeholder='State'
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange}
                            options={(center.country) ? appConstants.LOCATIONSTATES[center.country] : appConstants.STATES['India']}
                            selectedOption={center.state ? center.state : ''}
                            manadatorySymbol={false} />}
                        {center.country != 'India' &&
                            <SingleTextInput name="state"
                                title="State"
                                inputType="text"
                                content={(center.state) ? (center.state) : ''}
                                placeholder="State"
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange} />}
                    </CCol>
                </CRow>
                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="4">
                        <SingleTextInput name="pincode"
                            title="Pincode"
                            inputType="pincode"
                            content={(center.pincode) ? (center.pincode) : ''}
                            placeholder="Pincode"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SingleTextInput name="contact_number"
                            title="Contact Number"
                            inputType="text"
                            content={(center.contact_number) ? (center.contact_number) : ''}
                            placeholder="Contact Number"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                        {validator.current.message('contact_number', center.contact_number, 'numeric|phone')}
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SingleTextInput name="alternate_number"
                            title="Alternate Number"
                            inputType="text"
                            content={(center.alternate_number) ? (center.alternate_number) : ''}
                            placeholder="Contact Number"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                </CRow>
                <CRow className="formMargins">
                    <CCol xs="8" md="4" lg="4">
                        <SingleCheckbox
                            controlFunc={handleChange}
                            name='is_active'
                            title='Is Active'
                            type="checkbox"
                            value='is_active'
                            content={center.is_active}
                        />
                    </CCol>
                </CRow>
            </div>
        </>
    )
}