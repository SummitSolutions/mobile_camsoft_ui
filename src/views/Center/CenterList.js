import React, { useContext, useEffect, useState } from 'react';
import CreateCenterService from '../../api/CreateCenterService';
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CButtonGroup,
    CLink,
    CButton
} from '@coreui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus, faUsers } from '@fortawesome/free-solid-svg-icons';
import * as momentService from './../../shared/MomentService';
import * as appConstants from './../../AppConstants';
import GlobalState from './../../shared/GlobalState';
import { useHistory } from 'react-router-dom';
import { AddButtonWithTitle } from './../form-components/AddButtonWithTitle';
import 'semantic-ui-css/semantic.min.css';

const fields = [
    "center_name",
    "center_code",
    "town_or_city",
    {
        key: 'edit_patient',
        label: 'Action',
        _style: { width: '18%' },
        sorter: false,
        filter: false
    }
];

const CenterList = () => {

    const [state, setState] = useContext(GlobalState);
    const [centerList, setCenterList] = useState([]);
    const history = useHistory();

    useEffect(() => {
        console.log("Patient List page loading")
        CreateCenterService.getCenters(state.authHeader).then((res) => {
            setCenterList(res.data);
        });
    }, []);

    const updateCenter = (center) => {
        console.log(center)
        history.push('/center/update/' + center.id);
    }
    const addUser = (item) => {
        history.push('/users/add-user/' + item.id);
    }

    const viewUsers = (user) => {
        console.log(user)
        history.push('/users/list/' + user.id);
        console.log(user.id)
    }

    const gotoCreateCentre = () => {
        history.push('/center/create-center');
    }



    const loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

    return (
        <>
            <AddButtonWithTitle
                title='ADD NEW CENTER'
                addNewEntityOnClick={gotoCreateCentre}
            />
            {/* <div className="body-container">
                <CDataTable
                    items={centerList.data}
                    fields={fields}
                    //itemsPerPage={appConstants.ITEMS_PER_PAGE}
                    pagination
                    tableFilter
                    //itemsPerPageSelect
                    hover
                    sorter
                    scopedSlots={{
                        'edit_patient':
                            (item, index) => {
                                return (
                                    <td className="py-2">
                                        <CButtonGroup>
                                            <CButton
                                                color={appConstants.SAVE_BTN_COLOR}
                                                size="sm"
                                                title="Edit Patient"
                                                onClick={() => { editPatient(item) }}
                                            >
                                                <FontAwesomeIcon icon={faPencilAlt} />
                                            </CButton>
                                            <CButton
                                                color={appConstants.VIEW_BTN_COLOR}
                                                size="sm"
                                                title="View Users"
                                                onClick={() => { viewUsers(item) }}
                                            >
                                                User List
                                            </CButton>
                                        </CButtonGroup>
                                    </td>
                                )
                            },
                    }
                    }
                />
            </div> */}

            {centerList?.map((element, index) => (
                <CCol xs="12" md="3" lg="3" xl="3" key={index}>
                    <div className="ui cards">
                        <div className="card">
                            <div className="content">
                                <div className="description">
                                    <span>{element.center_name} [{element.center_code}]  </span><br></br>
                                    <span>{element.town_or_city}, {element.state}  </span><br></br>
                                    <span>{element.contact_number}</span><br></br>
                                </div>
                                <div className="extra content">
                                    <div className="ui two buttons">
                                        <div className="ui basic green button" onClick={() => { updateCenter(element) }}> <FontAwesomeIcon icon={faPencilAlt} /></div>
                                        <div className="ui basic red button" onClick={() => { viewUsers(element) }}><FontAwesomeIcon icon={faUsers} style={{ color: '' }} /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </CCol>))
            }
        </>
    );
}

export default CenterList;
