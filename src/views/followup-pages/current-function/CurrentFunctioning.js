import React, { useContext, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EmotionalBehaviour from '../../followup-pages/current-function/EmotionalBehaviour';
import SocialFunctioning from '../../followup-pages/current-function/SocialFunctioning';
import PhysicalMedicalFunctioning from '../../followup-pages/current-function/PhysicalMedicalFunctioning';
import SubstanceCurrentFunctioning from '../../followup-pages/current-function/SubstanceCurrentFunctioning';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

export default function CurrentFunctioning(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <h1 component={'div'}>Current Functioning</h1>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading} component={'div'}>Substance</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <SubstanceCurrentFunctioning />
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header">
          <Typography className={classes.heading} component={'div'}>Emotional Behaviour</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography component={'div'}>
            <EmotionalBehaviour />
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header">
          <Typography className={classes.heading} component={'div'}>Social Functioning</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography component={'div'}>
            <SocialFunctioning />
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header">
          <Typography className={classes.heading} component={'div'}>Physical/Medical Functioning</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography component={'div'}>
            <PhysicalMedicalFunctioning />
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}