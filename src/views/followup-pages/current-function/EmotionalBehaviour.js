import React, { useEffect, useState, useRef, Fragment, useContext } from 'react'
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CFormText, CLabel, CRow, CFormGroup, CButton } from '@coreui/react';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../../form-components/SingleTextInput';
import RadioButtonList from '../../form-components/RadioButtonList';
import SelectBox from '../../form-components/SelectBox';
import * as appConstants from './.././../../AppConstants';
import GlobalState from 'src/shared/GlobalState';
import EmotionalBehaviourService from 'src/api/EmotionalBehaviourService';

const EmotionalBehaviour = () => {

    const [emotionalBehaviour, setEmotionalBehaviour] = useState({});
    const params = useParams();
    const [state, setState] = useContext(GlobalState);

    useEffect(() => {

        getEmotionalBehaviourByPatientIdVisitId();

    }, []);

    const getEmotionalBehaviourByPatientIdVisitId = () => {
        EmotionalBehaviourService.getAllEmotionalBehaviours(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setEmotionalBehaviour(resp.data);
        });
    }

    const handleChange = (event) => {
        const { name, value } = event.target;
        if (event.target.type == 'checkbox') {
            setEmotionalBehaviour({ ...emotionalBehaviour, [event.target.name]: (event.target.checked) ? (value) : '' });
            emotionalBehaviour[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            setEmotionalBehaviour({ ...emotionalBehaviour, [event.target.name]: value });
            emotionalBehaviour[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdateEmotionalBehaviour(emotionalBehaviour, event);
    };

    const handleBlurChange = (event) => {
        console.log("Blur function calling")
        saveOrUpdateEmotionalBehaviour(emotionalBehaviour, event);
    }

    const saveOrUpdateEmotionalBehaviour = (data, event) => {
        if (data.id) {
            console.log(' Emotional behaviour data updating..: ')
            let id = (params.patient_id) ? params.id : data.id
            EmotionalBehaviourService.updateEmotionalBehaviour(data.id, data, state.authHeader).then(res => {
                console.log(res)
                setEmotionalBehaviour(res.data);
            });
        }
        else {
            console.log('Emotional behaviour saving')
            console.log(data)
            data.patient_id = parseInt(params.patient_id);
            data.visit_id = parseInt(params.visit_id);
            EmotionalBehaviourService.createEmotionalBehaviour(data, state.authHeader).then(res => {
                console.log("here")
                console.log(res.data)
                setEmotionalBehaviour(res.data);
            });
        }

    }

    return (
        <>
            <CCard borderColor="primary">
                <CCardBody>
                    <CRow className="formMargins">
                        <CCol xs="12" md="12" lg="12">
                            <label><b>Externalizing Syndrome</b></label>
                            <RadioButtonList
                                name="externalizing_syndrome"
                                title=" "
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                options={appConstants.EMOTIONAL_BEHAVIOUR}
                                content={emotionalBehaviour.externalizing_syndrome}
                                manadatorySymbol={false} />
                        </CCol>
                        <CCol xs="12" md="12" lg="12">
                            <label><b>Mood Disoder</b></label>
                            <RadioButtonList
                                name="mood_disoder"
                                title=" "
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                options={appConstants.EMOTIONAL_BEHAVIOUR}
                                content={emotionalBehaviour.mood_disoder}
                                manadatorySymbol={false} />
                        </CCol>
                        <CCol xs="12" md="12" lg="12">
                            <label><b>Psychotic Disoder</b></label>
                            <RadioButtonList
                                name="psychotic_disoder"
                                title=" "
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                options={appConstants.EMOTIONAL_BEHAVIOUR}
                                content={emotionalBehaviour.psychotic_disoder}
                                manadatorySymbol={false} />
                        </CCol>
                        <CCol xs="12" md="12" lg="12">
                            <label><b>Addictive Behavior</b></label>
                            <RadioButtonList
                                name="addictive_behavior"
                                title=" "
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                options={appConstants.EMOTIONAL_BEHAVIOUR}
                                content={emotionalBehaviour.addictive_behavior}
                                manadatorySymbol={false} />
                        </CCol>
                        <CCol xs="12" md="12" lg="12">
                            <label><b>Anxiety Spec</b></label>
                            <RadioButtonList
                                name="anxiety_spec"
                                title=" "
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                options={appConstants.EMOTIONAL_BEHAVIOUR}
                                content={emotionalBehaviour.anxiety_spec}
                                manadatorySymbol={false} />
                        </CCol>
                        <CCol xs="12" md="12" lg="12">
                            <label><b>Other Spec</b></label>
                            <RadioButtonList
                                name="other_spec"
                                title=" "
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                options={appConstants.EMOTIONAL_BEHAVIOUR}
                                content={emotionalBehaviour.other_spec}
                                manadatorySymbol={false} />
                        </CCol>
                        <CCol xs="12" md="8" lg="8">
                            <label><b>Notes</b></label>
                            <SingleTextInput name="notes"
                                title="Notes"
                                inputType="text"
                                content={(emotionalBehaviour.notes) ? (emotionalBehaviour.notes) : ''}
                                placeholder="Notes"
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                multiline={true}
                                rows={4}
                                variant="outlined" />
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
        </>
    )

}

export default EmotionalBehaviour;
