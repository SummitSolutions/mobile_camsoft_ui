import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import {
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,

} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SelectBoxNew from '../../form-components/SelectBoxNew';
import SimpleReactValidator from 'simple-react-validator';
import SubstanceListService from 'src/api/SubstanceListService';
import GlobalState from '../../../shared/GlobalState';
import { Modal, Button } from "react-bootstrap";
import { makeStyles } from '@material-ui/core/styles';
import * as appConstants from './.././../../AppConstants';
import { } from "react-lodash";
import Alcohol from '../../substance-details/Alcohol';
import Caffeine from '../../substance-details/Caffeine';
import Opioids from '../../substance-details/Opioids';
import Cannabinioids from '../../substance-details/Cannabinioids';
import SedativeHypnotics from '../../substance-details/SedativeHypnotics';
import Cocaine from '../../substance-details/Cocaine';
import OtherStimulants from '../../substance-details/OtherStimulants';
import Hallucinogens from '../../substance-details/Hallucinogens';
import Inhalants from '../../substance-details/Inhalants';
import Tobacco from '../../substance-details/Tobacco';
import AnyOtherSubstance from '../../substance-details/AnyOtherSubstance';
import BehavioralAddiction from '../../substance-details/BehavioralAddiction';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import CurrentFunctioningAlcohol from '../current-function/CurrentFunctioningAlcohol.js';
import CurrentFunctioningAnyOtherSubstance from '../current-function/CurrentFunctioningAnyOtherSubstance.js';
import CurrentFunctioningCaffeine from '../current-function/CurrentFunctioningCaffeine';
import CurrentFunctioningOpioids from '../current-function/CurrentFunctioningOpioids';
import CurrentFunctioningCannabinioids from '../current-function/CurrentFunctioningCannabinioids';
import CurrentFunctioningSedativeHypnotics from '../current-function/CurrentFunctioningSedativeHypnotics';
import CurrentFunctioningCocaine from '../current-function/CurrentFunctioningCocaine';
import CurrentFunctioningOtherStimulants from '../current-function/CurrentFunctioningOtherStimulants';
import CurrentFunctioningInhalants from '../current-function/CurrentFunctioningInhalants';
import CurrentFunctioningHallucinogens from '../current-function/CurrentFunctioningHallucinogens';
import CurrentFunctioningTobaco from '../current-function/CurrentFunctioningTobaco';
import CurrentFunctioningBehavioralAddiction from '../current-function/CurrentFunctioningBehavioralAddiction';
import SubstanceUseService from 'src/api/SubstanceUseService';
import SubstanceDetailService from 'src/api/SubstanceDetailService';
import CurrentFunctioningService from 'src/api/CurrentFunctioningService';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
}));

const SubstanceCurrentFunctioning = () => {

    const [state, setState] = useContext(GlobalState);
    const history = useHistory();
    const params = useParams();
    const validator = useRef(new SimpleReactValidator());
    const [substance, setSubstance] = useState({});
    const [currentSubstance, setCurrentSubstance] = useState({ current_functioning: '' });
    const [page, setPage] = useState({ currentPage: 'Alcohol' });
    const [selectedOptionList, setSelectedOptionList] = useState([]);
    const [selectedOption, setSelectedOption] = useState({})
    const [listParams, setListParams] = useState({ substanceList: [] });
    const [substanceUseList, setSubstanceUseList] = useState([]);
    const [showModal, setShow] = useState(false);
    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    useEffect(() => {
        console.log("Substance use page loaded. Msg. from UseEffect")

        getSubstanceList();
        getAllSubstanceUse();
        // getSubstanceDetail(); 
        getAllCurrentFunctionings();

        return () => {
            console.log('Clearing Substance use from State')
        }
    }, []);

    //Get  list of substances
    const getSubstanceList = () => {
        SubstanceListService.getSubstanceList(state.authHeader).then((resp) => {
            console.log(resp)
            setListParams({ ...listParams, substanceList: resp.data });
        })
    }



    const handleChangeSubstance = (event) => {
        // selectedOptionList.push(event.target.value);
        // var index = listParams.substanceList.indexOf(event.target.value);
        // console.log(index)
        // listParams.substanceList.splice(index,1);
        // console.log( listParams.substanceList.splice(index,1))
        // setSelectedOption(selectedOption)
        // setListParams(listParams)
        // console.log(selectedOption, substanceList)     
        //console.log(listParams.substanceList)
        //let item = listParams.substanceList.find(element => element.substance_name === event.target.value);
        //console.log(item)
        //setSelectedOption(item);
    };

    const addSubstanceToSelectedOptionList = () => {
        //console.log('calling')
        //console.log(selectedOption)
        //setSelectedOptionList(selectedOption => [selectedOptionList, ...selectedOption]);
        //console.log(selectedOption => [selectedOptionList, ...selectedOption])
        //setSelectedOption({ selectedOptionList });
        //console.log(selectedOptionList)
    }

    const gotoPage = (substance) => {
        setPage({ ...page, currentPage: substance });
        handleShow();
    }
    // patient substance use api
    const getAllSubstanceUse = () => {
        SubstanceUseService.getAllSubstanceUse(params.patient_id, params.visit_id, state.authHeader).then((resp) => {
            setSubstanceUseList(resp.data);
        })
    }
    // fetches Substance Details for Selected Substance (Alcohol, Opiods ...)
    // const getSubstanceDetail = () => {
    //     SubstanceDetailService.getSubstanceDetailForSubstance(params.patient_id, params.visit_id, params.patient_substance_id, state.authHeader).then((resp) => {
    //         setSubstance(resp.data)
    //     });
    // }

    // handle change function for substance use page(modalpopup)
    const handleBlurSubstanceUse = (event) => {

        saveOrUpdateAlcohol(substance, event);
    }
    const handleChangeSubstanceUse = (event, date) => {
        let substanceDetailObj = {};
        const { name, value } = event.target;
        console.log('auto saving function')
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setSubstance({ ...substance, [event.target.name]: (event.target.checked) ? (value) : '' });
            substance[event.target.name] = (event.target.checked) ? (value) : '';
        }
        if ("last_use") {
            setSubstance({ ...substance, 'last_use': date });
        }

        else {
            console.log(event.target.name)
            setSubstance({ ...substance, [event.target.name]: value });
            substance[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdateAlcohol(substance, event);
        console.log("testing")
    }

    //AutoSave patient substance 
    const saveOrUpdateAlcohol = (data, event) => {
        console.log(data)
        if (data.id) {
            console.log('Alcohol data updating..: ', data)
            let id = (params.patient_id) ? params.patient_id : data.patient_id
            SubstanceDetailService.updateSubstanceDetail(data.id, data, state.authHeader).then(resp => {
                console.log(resp)
                setSubstance(resp.data);
            });
        }
        else {
            console.log("Alcohol data saving")
            data.patient_id = parseInt(params.patient_id);
            data.visit_id = parseInt(params.visit_id);
            //data.patient_substance_id = page.currentPage.substance_use_list.id;
            data.patient_substance_id = parseInt(params.visit_id);
            console.log(data)
            SubstanceDetailService.createSubstanceDetail(data, state.authHeader).then(resp => {
                setSubstance(resp.data);
            });
        }
    }

    // get current functioning by patientId visitId
    const getAllCurrentFunctionings = () => {
        CurrentFunctioningService.getAllCurrentFunctionings(params.patient_id, params.visit_id, state.authHeader).then((resp) => {
            setCurrentSubstance(resp.data);
        })
    }
    //handle change for current functioning page
    const handleBlur = (event) => {
        saveOrUpdateCurrentFunctioning(currentSubstance, event);
    };


    const handleChange = (event) => {
        const { name, value } = event.target;
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setCurrentSubstance({ ...currentSubstance, [event.target.name]: (event.target.checked) ? (value) : '' });
            currentSubstance[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setCurrentSubstance({ ...currentSubstance, [event.target.name]: value });
            currentSubstance[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdateCurrentFunctioning(currentSubstance, event);
    };

    //saveorupdate current functioning

    const saveOrUpdateCurrentFunctioning = (data, event) => {
        if (data.id) {
            console.log('Current functioning  data updating..: ',)
            let id = (params.patient_id) ? params.id : data.id
            CurrentFunctioningService.updateCurrentFunctioning(data.id, data, state.authHeader).then(res => {
                console.log(res)
                setCurrentSubstance(res.data);
            });
        }
        else {
            console.log('Current functioning saving')
            data.patient_id = parseInt(params.patient_id);
            data.visit_id = parseInt(params.visit_id);
            CurrentFunctioningService.createCurrentFunctioning(data, state.authHeader).then(res => {
                console.log("here")
                console.log(res.data)
                setCurrentSubstance(res.data);
            });
        }
    }


    return (
        <>
            <CCard accentColor="primary">
                <CCardHeader>Substance </CCardHeader>
                <CCardBody>
                    <CRow className="formMargins">
                        <CCol xs="8" md="6" lg="6">
                            <SelectBoxNew
                                name="substance_name"
                                title="Add substance"
                                placeholder='Add new substance'
                                controlFunc={handleChangeSubstance}
                                options={listParams.substanceList}
                                autosize={true}
                                manadatorySymbol={true}
                                label={'substance_name'}
                                value={'value'}
                            />
                        </CCol>
                        <CCol xs="4" md="6" lg="6">
                            <CButton
                                color={appConstants.SAVE_BTN_COLOR}
                                size="sm"
                                title="Add"
                                onClick={addSubstanceToSelectedOptionList}
                            >Add
                            </CButton>
                        </CCol>
                        <CCol>
                            {selectedOptionList}
                        </CCol>
                    </CRow>
                    <CRow>
                        <CDropdown className="c-header-nav-items toolBarCss" direction="down" >
                            <CRow>
                                <CCol xs="2" sm="2" md="1" lg="1" xl="1">
                                    <CButton type="button" color="light" ><FontAwesomeIcon icon={faAngleLeft} style={{ fontSize: "1.5rem" }} /></CButton>
                                </CCol>
                                <CCol xs="8" sm="8" md="10" lg="10" xl="10" align="center">
                                    <CDropdownToggle className="c-header-nav-link" caret={false}>
                                        <input type="button"
                                            className="header"
                                            value="Substance"
                                            className="btn-primary "
                                            style={{ width: '100%' }}
                                        />
                                    </CDropdownToggle>
                                    <CDropdownMenu className="pt-0" placement="bottom-end">
                                        <CDropdownItem
                                            header
                                            tag="div"
                                            color="light"
                                            className="text-center" >
                                            <strong></strong>
                                        </CDropdownItem>
                                        {
                                            listParams.substanceList?.map(element => {
                                                return (
                                                    <CDropdownItem key={element.substance_name} onClick={() => { gotoPage(element) }}>
                                                        &nbsp; {element.substance_name}
                                                    </CDropdownItem>
                                                )
                                            })
                                        }
                                    </CDropdownMenu>
                                </CCol>
                                <CCol xs="2" sm="2" md="1" lg="1" xl="1" align="right">
                                    <CButton type="button" color="light" style={{ marginLeft: '-12px' }}><FontAwesomeIcon icon={faAngleRight} style={{ fontSize: "1.5rem" }} /></CButton>
                                </CCol>
                            </CRow>
                        </CDropdown>
                    </CRow>
                </CCardBody>
            </CCard>
            <CCard accentColor="primary">
                <CCardHeader></CCardHeader>
                <CCardBody>
                    {
                        page.currentPage.substance_name == 'Alcohol' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Alcohol obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningAlcohol obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>
                    }
                    {
                        page.currentPage.substance_name == 'Caffeine' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Caffeine obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningCaffeine obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>
                    }
                    {
                        page.currentPage.substance_name == 'Opioids' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Opioids obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningOpioids obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>

                    }
                    {
                        page.currentPage.substance_name == 'Cannabinioids' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Cannabinioids obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningCannabinioids obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>
                    }
                    {
                        page.currentPage.substance_name == 'Sedative/Hypnotics' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <SedativeHypnotics obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningSedativeHypnotics obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>
                    }
                    {
                        page.currentPage.substance_name == 'Cocaine' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Cocaine obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningCocaine obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>
                    }
                    {
                        page.currentPage.substance_name == 'Other Stimulants' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <OtherStimulants obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningOtherStimulants obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>
                    }
                    {
                        page.currentPage.substance_name == 'Hallucinogens' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Hallucinogens obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningHallucinogens obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>
                    }
                    {
                        page.currentPage.substance_name == 'Inhalants' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Inhalants obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningInhalants obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>
                    }
                    {
                        page.currentPage.substance_name == 'Tobacco' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Tobacco obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningTobaco obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>
                    }
                    {
                        page.currentPage.substance_name == 'Any Other Substance' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <AnyOtherSubstance obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningAnyOtherSubstance obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>

                    }
                    {
                        page.currentPage.substance_name == 'Behavioral Addiction' &&
                        <div>
                            <Modal show={showModal} onHide={handleClose} backdrop="static">
                                <Modal.Header closeButton>
                                    <Modal.Title>Modal heading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <BehavioralAddiction obj={substance} controlBlurFunc={handleBlurSubstanceUse} controlChangeFunc={handleChangeSubstanceUse} />
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>Close </Button>
                                </Modal.Footer>
                            </Modal>
                            <div>
                                <CurrentFunctioningBehavioralAddiction obj={currentSubstance} controlBlurFunc={handleBlur} controlFunc={handleChange} />
                            </div>
                        </div>
                    }
                </CCardBody>
            </CCard>
        </>
    )
}

export default SubstanceCurrentFunctioning;
