import React, { useEffect, useState, useContext } from 'react'
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow, CButton } from '@coreui/react';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../../form-components/SingleTextInput';
import SingleCheckbox from '../../form-components/SingleCheckbox';
import GlobalState from '../../../shared/GlobalState';
import PhysicalMedicalFunctioningService from 'src/api/PhysicalMedicalFunctioningService';

const PhysicalMedicalFunctioning = () => {

    const [physicalMedicalFunctioning, setPhysicalMedicalFunctioning] = useState({});
    const params = useParams();
    const [state, setState] = useContext(GlobalState);

    useEffect(() => {
        getPhysicalMedicalFunctioningByAll();
    }, []);

    const getPhysicalMedicalFunctioningByAll = () => {
        PhysicalMedicalFunctioningService.getAllPhysicalMedicalFunctionings(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setPhysicalMedicalFunctioning(resp.data);
        })
    };

    const handleChange = (event) => {
        const { name, value } = event.target;
        console.log('auto saving function')
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setPhysicalMedicalFunctioning({ ...physicalMedicalFunctioning, [event.target.name]: (event.target.checked) ? (value) : '' });
            physicalMedicalFunctioning[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setPhysicalMedicalFunctioning({ ...physicalMedicalFunctioning, [event.target.name]: value });
            physicalMedicalFunctioning[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdatePhysicalMedicalFunctioning(physicalMedicalFunctioning, event);

    };

    const handleBlurChange = (event) => {
        console.log("Blur function is calling")
        saveOrUpdatePhysicalMedicalFunctioning(physicalMedicalFunctioning, event);

    };

    const saveOrUpdatePhysicalMedicalFunctioning = (data, event) => {
        if (data.id) {
            console.log('Physical medical functioning  data updating..: ')
            let id = (params.patient_id) ? params.id : data.id
            PhysicalMedicalFunctioningService.updatePhysicalMedicalFunctioning(data.id, data, state.authHeader).then(res => {
                console.log(res)
                setPhysicalMedicalFunctioning(res.data);
            });
        }
        else {
            console.log('psychotherapy saving')
            console.log(data)
            data.patient_id = parseInt(params.patient_id);
            data.visit_id = parseInt(params.visit_id);
            PhysicalMedicalFunctioningService.createPhysicalMedicalFunctioning(data, state.authHeader).then(res => {
                console.log("here")
                console.log(res.data)
                setPhysicalMedicalFunctioning(res.data);
            });
        }

    };


    return (
        <>
            <CCard borderColor="primary">
                <CCardBody>
                    <CRow className="formMargins">
                        <CCol xs="9" md="4" lg="4">
                            <label>Hypertension</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='hypertension'
                                title=''
                                type="checkbox"
                                value='hypertension'
                                content={(physicalMedicalFunctioning.hypertension) ? (physicalMedicalFunctioning.hypertension) : ''}
                            />
                        </CCol>
                        <CCol xs="9" md="4" lg="4">
                            <label>Infections</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='infections'
                                title=''
                                type="checkbox"
                                value='infections'
                                content={(physicalMedicalFunctioning.infections) ? (physicalMedicalFunctioning.infections) : ''}
                            />
                        </CCol>
                    </CRow>

                    <CRow className="formMargins">
                        <CCol xs="9" md="4" lg="4">
                            <label>Tuberculosis</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='tuberculosis'
                                title=''
                                type="checkbox"
                                value='tuberculosis'
                                content={(physicalMedicalFunctioning.tuberculosis) ? (physicalMedicalFunctioning.tuberculosis) : ''}
                            />
                        </CCol>
                        <CCol xs="9" md="4" lg="4">
                            <label>HIV</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='hiv'
                                title=''
                                type="checkbox"
                                value='hiv'
                                content={(physicalMedicalFunctioning.hiv) ? (physicalMedicalFunctioning.hiv) : ''}
                            />
                        </CCol>
                    </CRow>

                    <CRow className="formMargins">
                        <CCol xs="9" md="4" lg="4">
                            <label>Fungal</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='fungal'
                                title=''
                                type="checkbox"
                                value='fungal'
                                content={(physicalMedicalFunctioning.fungal) ? (physicalMedicalFunctioning.fungal) : ''}
                            />
                        </CCol>
                        <CCol xs="9" md="4" lg="4">
                            <label>Syphilis</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='syphilis'
                                title=''
                                type="checkbox"
                                value='syphilis'
                                content={(physicalMedicalFunctioning.syphilis) ? (physicalMedicalFunctioning.syphilis) : ''}
                            />
                        </CCol>
                    </CRow>

                    <CRow className="formMargins">
                        <CCol xs="9" md="4" lg="4">
                            <label>Gastritis</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='gastritis'
                                title=''
                                type="checkbox"
                                value='gastritis'
                                content={(physicalMedicalFunctioning.gastritis) ? (physicalMedicalFunctioning.gastritis) : ''}
                            />
                        </CCol>
                        <CCol xs="9" md="4" lg="4">
                            <label>Peripheral Neuritis</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='peripheral_neuritis'
                                title=''
                                type="checkbox"
                                value='peripheral neuritis'
                                content={(physicalMedicalFunctioning.peripheral_neuritis) ? (physicalMedicalFunctioning.peripheral_neuritis) : ''}
                            />
                        </CCol>
                    </CRow>

                    <CRow className="formMargins">
                        <CCol xs="9" md="4" lg="4">
                            <label>Pancreatitis</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='pancreatitis'
                                title=''
                                type="checkbox"
                                value='pancreatitis'
                                content={(physicalMedicalFunctioning.pancreatitis) ? (physicalMedicalFunctioning.pancreatitis) : ''}
                            />
                        </CCol>
                        <CCol xs="9" md="4" lg="4">
                            <label>Alcholic Liver Disease</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='alcholic_liver_disease'
                                title=''
                                type="checkbox"
                                value='alcholic liver disease'
                                content={(physicalMedicalFunctioning.alcholic_liver_disease) ? (physicalMedicalFunctioning.alcholic_liver_disease) : ''}
                            />
                        </CCol>
                    </CRow>

                    <CRow className="formMargins">
                        <CCol xs="9" md="4" lg="4">
                            <label>Diabetes</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='diabetes'
                                title=''
                                type="checkbox"
                                value='diabetes'
                                content={(physicalMedicalFunctioning.diabetes) ? (physicalMedicalFunctioning.diabetes) : ''}
                            />
                        </CCol>
                        <CCol xs="9" md="4" lg="4">
                            <label>Other</label>
                        </CCol>
                        <CCol xs="3" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='other'
                                title=''
                                type="checkbox"
                                value='other'
                                content={(physicalMedicalFunctioning.other) ? (physicalMedicalFunctioning.other) : ''}
                            />
                        </CCol>
                    </CRow>

                    <CRow className="formMargins">
                        <CCol xs="12" md="10" lg="10">
                            <label><b>Notes</b></label>
                            <SingleTextInput name="notes"
                                title="Notes"
                                inputType="text"
                                content={(physicalMedicalFunctioning.notes) ? (physicalMedicalFunctioning.notes) : ''}
                                placeholder="Notes"
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                multiline={true}
                                rows={4}
                                variant="outlined" />
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
        </>

    )

}

export default PhysicalMedicalFunctioning;
