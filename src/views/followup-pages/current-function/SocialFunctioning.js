import React, { useEffect, useState, useRef, Fragment, useContext } from 'react'
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CFormText, CLabel, CRow, CFormGroup, CButton } from '@coreui/react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../../form-components/SingleTextInput';
import SingleCheckbox from '../../form-components/SingleCheckbox';
import RadioButtonList from '../../form-components/RadioButtonList';
import * as appConstants from './.././../../AppConstants';
import moment from 'moment';
import GlobalState from '../../../shared/GlobalState';
import SocialFunctioningService from 'src/api/SocialFunctioningService';


const SocialFunctioning = () => {

    const [socialFunctioning, setSocialFunctioning] = useState({});
    const params = useParams();
    const [state, setState] = useContext(GlobalState);

    useEffect(() => {
        getSocialFunctioningByPatientIdVisitId();
    }, []);

    const getSocialFunctioningByPatientIdVisitId = () => {
        SocialFunctioningService.getAllSocialFunctionings(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setSocialFunctioning(resp.data);
        });
    }

    const handleChange = (event) => {
        const { name, value } = event.target;
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setSocialFunctioning({ ...socialFunctioning, [event.target.name]: (event.target.checked) ? (value) : '' });
            socialFunctioning[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setSocialFunctioning({ ...socialFunctioning, [event.target.name]: value });
            socialFunctioning[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdateSocialFunctioning(socialFunctioning, event);
    };

    const handleBlurChange = (event) => {
        console.log("Blur function is calling")
        saveOrUpdateSocialFunctioning(socialFunctioning, event);
    };

    const saveOrUpdateSocialFunctioning = (data, event) => {
        if (data.id) {
            console.log('social functioning  data updating..: ',)
            let id = (params.patient_id) ? params.id : data.id
            SocialFunctioningService.updateSocialFunctioning(data.id, data, state.authHeader).then(res => {
                console.log(res)
                setSocialFunctioning(res.data);
            });
        }
        else {
            console.log('Social functioning saving')
            console.log(data)
            data.patient_id = parseInt(params.patient_id);
            data.visit_id = parseInt(params.visit_id);
            SocialFunctioningService.createSocialFunctioning(data, state.authHeader).then(res => {
                console.log("here")
                console.log(res.data)
                setSocialFunctioning(res.data);
            });
        }
    }

    return (
        <>
            <CCard borderColor="primary">
                <CCardBody>
                    <CRow className="formMargins">
                        <CCol xs="12" md="12" lg="12">
                            <label><b>Work</b></label>
                            <RadioButtonList
                                name="work"
                                title=" "
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                options={appConstants.EMOTIONAL_BEHAVIOUR}
                                content={socialFunctioning.work}
                                manadatorySymbol={false} />
                        </CCol>
                    </CRow>
                    &nbsp;
                    <CRow className="formMargins">
                        <CCol xs="12" md="12" lg="12">
                            <label><b>Interpersonal</b></label>
                        </CCol>
                    </CRow>
                    <CRow className="formMargins">
                        <CCol xs="6" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='marital'
                                title='Marital '
                                type="checkbox"
                                value='marital'
                                content={(socialFunctioning.marital) ? (socialFunctioning.marital) : ''}
                            />
                        </CCol>
                        <CCol xs="6" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='family'
                                title='Family '
                                type="checkbox"
                                value='family'
                                content={(socialFunctioning.family) ? (socialFunctioning.family) : ''}
                            />
                        </CCol>
                        <CCol xs="6" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='friends'
                                title='Friends '
                                type="checkbox"
                                value='friends'
                                content={(socialFunctioning.friends) ? (socialFunctioning.friends) : ''}
                            />
                        </CCol>
                        <CCol xs="6" md="3" lg="3">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='neighbour'
                                title='Neighbour'
                                type="checkbox"
                                value='neighbour'
                                content={(socialFunctioning.neighbour) ? (socialFunctioning.neighbour) : ''}
                            />
                        </CCol>
                        <CCol xs="6" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='na_interpersonal'
                                title='NA'
                                type="checkbox"
                                value='Na'
                                content={(socialFunctioning.na) ? (socialFunctioning.na) : ''}
                            />
                        </CCol>
                    </CRow>
                    &nbsp;
                    <CRow className="formMargins">
                        <CCol xs="12" md="12" lg="12">
                            <label><b>Legal Issues</b></label>
                        </CCol>
                    </CRow>
                    <CRow className="formMargins">
                        <CCol xs="12" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='divorce'
                                title='Divorce'
                                type="checkbox"
                                value='divorce'
                                content={(socialFunctioning.divorce) ? (socialFunctioning.divorce) : ''}
                            />
                        </CCol>
                        <CCol xs="12" md="3" lg="3">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='criminal_case'
                                title='Criminal Case'
                                type="checkbox"
                                value='criminal case'
                                content={(socialFunctioning.criminal_case) ? (socialFunctioning.criminal_case) : ''}
                            />
                        </CCol>
                        <CCol xs="12" md="2" lg="2">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='na_legal_issues'
                                title='NA'
                                type="checkbox"
                                value='NA'
                                content={(socialFunctioning.na_legal_issues) ? (socialFunctioning.na_legal_issues) : ''}
                            />
                        </CCol>
                    </CRow>
                    &nbsp;
                    <CRow className="formMargins">
                        <CCol xs="12" md="8" lg="8">
                            <label><b>Notes</b></label>
                            <SingleTextInput name="notes"
                                title="Notes"
                                inputType="text"
                                content={(socialFunctioning.notes) ? (socialFunctioning.notes) : ''}
                                placeholder="Identification Marks"
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                multiline={true}
                                rows={4}
                                variant="outlined" />
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
        </>
    )

}

export default SocialFunctioning;