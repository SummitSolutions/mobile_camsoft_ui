import React, { useEffect, useState, useRef, useContext } from 'react'
import {
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CButton
} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
// import GlobalState from '../../shared/GlobalState';
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import SingleTextInput from '../../form-components/SingleTextInput';
import SelectBox from '../../form-components/SelectBox';
import * as appConstants from '../../../AppConstants';
import SingleCheckbox from '../../form-components/SingleCheckbox';
import RadioButtonList from '../../form-components/RadioButtonList';
import * as momentServices from './../../../shared/MomentService';
import * as DateConfigs from './../../../shared/configs/DateConfigs';
import KeyboardDatePickers from '../../form-components/KeyboardDatePicker';


const CurrentFunctioningAlcohol = (props) => (
    <>

        <div><h1>Current Functioning</h1></div>
        <CRow >
            <CCol xs="12" md="12" lg="12">
                <RadioButtonList
                    name='current_functioning'
                    title='Spirit'
                    controlBlurFunc={props.controlBlurFunc}
                    controlFunc={props.controlFunc}
                    options={appConstants.CURRENT_FUNCTIONING_OPTIONS}
                    content={props.obj.current_functioning}
                />
            </CCol>
        </CRow>
        {(props.obj.current_functioning == 'Abstinent') &&
            <CRow>
                <CCol xs="12" md="6" lg="6">
                    <SingleTextInput
                        name='abstinent_duration'
                        title='Duration'
                        inputType='number'
                        placeholder='Duration'
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}
                        content={(props.obj.abstinent_duration) ? (props.obj.abstinent_duration) : ''}
                    />
                </CCol>
                <CCol xs="12" md="6" lg="6">
                    <SelectBox
                        name="abstinent_duration_uom"
                        title=" "
                        options={appConstants.DURATION}
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}
                    />
                </CCol>
            </CRow>}
        {(props.obj.current_functioning == 'Abstinent with Lapses') &&
            <CRow>
                <CCol xs="12" md="6" lg="6">
                    <SingleTextInput
                        name='abstinent_laps'
                        title='No of lapses'
                        inputType='number'
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}
                        content={(props.obj.abstinent_laps) ? (props.obj.abstinent_laps) : ''}
                    />
                </CCol>
                <CCol xs="12" md="6" lg="6">
                    <SingleTextInput
                        name='abstinent_lap_mgmt'
                        title='Lap Mgmt'
                        inputType='text'
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}
                        content={(props.obj.abstinent_lap_mgmt) ? (props.obj.abstinent_lap_mgmt) : ''}
                    />
                </CCol>
            </CRow>}
        {(props.obj.current_functioning == 'Relapsed') &&
            <CRow>
                <CCol xs="12" md="5" lg="5">
                    <SelectBox
                        name="relapsed_options"
                        title=""
                        options={appConstants.RELAPSED_OPTIONS}
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}
                    />
                </CCol>
                <CCol xs="12" md="4" lg="4">
                    <SingleTextInput
                        name='relapsed_duration'
                        title='Duration'
                        inputType='number'
                        placeholder='Duration'
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}
                        content={(props.obj.relapsed_duration) ? (props.obj.relapsed_duration) : ''}
                    />
                </CCol>
                <CCol xs="12" md="3" lg="3">
                    <SelectBox
                        name="duration_uom"
                        title=""
                        options={appConstants.RELAPSED_DURATION}
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}
                    />
                </CCol>
            </CRow>}
        <CRow className="formMargins">
            <CCol xs='12' md='12' lg='12'>
                <SingleTextInput
                    name='notes'
                    title='Notes'
                    inputType='text'
                    multiline={true}
                    controlBlurFunc={props.controlBlurFunc}
                    controlFunc={props.controlFunc}
                    content={(props.obj.notes) ? (props.obj.notes) : ''}
                    rows={3}
                />
            </CCol>
        </CRow>
        <h1>Current Status</h1>
        <CRow>
            <CCol xs="12" md="4" lg="4">
                <SingleTextInput
                    name='current_quantity'
                    title="Current Quantity"
                    inputType='text'
                    placeholder='Current Quantity'
                    controlBlurFunc={props.controlBlurFunc}
                    controlFunc={props.controlFunc}
                    content={(props.obj.current_quantity) ? (props.obj.current_quantity) : ''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">
                <SelectBox
                    name="current_quantity_duration"
                    title=""
                    options={appConstants.CURRENT_USE_FREQUENCY}
                    controlBlurFunc={props.controlBlurFunc}
                    controlFunc={props.controlFunc}
                />
            </CCol>
            <CCol xs="8" md="4" lg="4">
                {/* <SingleTextInput name="last_use"
                    title="Last Use"
                    inputType="date"
                    content={(props.obj.last_use) ? (props.obj.last_use) : appConstants.FOLLOWUP_DATE}
                    placeholder="Last Use"
                    manadatorySymbol={false}
                    controlBlurFunc={props.controlBlurFunc}
                    controlFunc={props.controlFunc} /> */}
                <KeyboardDatePickers
                    name='last_use'
                    disableToolbar
                    format="DD/MM/yyyy"
                    content={(props.obj.last_use) ? (props.obj.last_use) : momentServices.currentDate(DateConfigs.DEFAULT_DATE_FORMAT_BACKEND)}
                    manadatorySymbol={false}
                    controlFunc={props.controlFunc}
                />
            </CCol>
            <CCol xs='12' md='12' lg='12'>
                <SingleTextInput
                    name='current_status_notes'
                    title='Notes'
                    inputType='text'
                    multiline={true}
                    controlBlurFunc={props.controlBlurFunc}
                    controlFunc={props.controlFunc}
                    content={(props.obj.current_status_notes) ? (props.obj.current_status_notes) : ''}
                    rows={3}
                />
            </CCol>

        </CRow>
    </>
)
export default CurrentFunctioningAlcohol;