import React, { useContext, useEffect, useState } from 'react'
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Tooltip from '@material-ui/core/Tooltip';
import LocalBarTwoToneIcon from '@material-ui/icons/LocalBarTwoTone';
import LocalHospitalTwoToneIcon from '@material-ui/icons/LocalHospitalTwoTone';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import HotelTwoToneIcon from '@material-ui/icons/HotelTwoTone';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Diagnosis from '../diagnosis/Diagnosis';
import Advice from './advice/Advice';
import CreateFollowupComponent from '../followup/Create';
import CurrentFunctioning from '../followup-pages/current-function/CurrentFunctioning';

const FollowupPages = (props) => {
  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-prevent-tabpanel-${index}`}
        aria-labelledby={`scrollable-prevent-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={4}>
            <Typography component={'div'}>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

  function a11yProps(index) {
    return {
      id: `scrollable-prevent-tab-${index}`,
      'aria-controls': `scrollable-prevent-tabpanel-${index}`,
    };
  }

  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      width: '100%',
    },
    fab: {
      margin: theme.spacing(2),
    },
    absolute: {
      position: 'absolute',
      bottom: theme.spacing(1),
      right: theme.spacing(3),
    },
  }));
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <Paper square className={classes.root}>
        <AppBar position="static">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="fullWidth"
            indicatorColor="primary"
            textColor="primary"
            aria-label="icon tabs example"
          >
            <Tooltip title="Current Functioning">
              <Tab icon={<LocalBarTwoToneIcon />} aria-label="favorite" {...a11yProps(0)} />
            </Tooltip>
            <Tooltip title="Followup Summary">
              <Tab icon={<AssignmentIndIcon />} aria-label="person" {...a11yProps(1)} />
            </Tooltip>
            <Tooltip title="Diagnosis">
              <Tab icon={<LocalHospitalTwoToneIcon />} aria-label="help" {...a11yProps(2)} />
            </Tooltip>
            <Tooltip title="Advice">
              <Tab icon={<HotelTwoToneIcon />} aria-label="shopping" {...a11yProps(3)} />
            </Tooltip>
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <CurrentFunctioning />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <CreateFollowupComponent />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Diagnosis />
        </TabPanel>
        <TabPanel value={value} index={3}>
          <Advice />
        </TabPanel>
      </Paper>
    </>
  )
}

export default FollowupPages;