import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../form-components/SingleTextInput';
import NameSection from '../form-components/section-components/NameSection';
//import CheckboxList from '../form-components/CheckboxList';
//import RadioButtonList from '../form-components/RadioButtonList';
import SelectBox from '../form-components/SelectBox';
import Divider from '../form-components/Divider';
import SimpleReactValidator from 'simple-react-validator';
import LocationService from '../../api/LocationService';
import * as appConstants from './../../AppConstants';
import moment from 'moment';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import GlobalState from './../../shared/GlobalState';

export default function CreateLocationComponent() {

    const { handleSubmit, errors, getValues, watch, formState } = useForm();
    const history = useHistory();
    const params = useParams();
    const [state, setState] = useContext(GlobalState);
    const [location, setLocation] = useState({
    });
    const validator = useRef(new SimpleReactValidator());
    useEffect(() => {
        console.log("Location Create page loaded. Msg. from UseEffect")
        getLocationById();

    }, []);

    const getLocationById = () => {
        if (params.location_id > 0) {
            LocationService.getLocationById(params.location_id, state.authHeader).then(resp => {
                if (resp.data.id) {
                    console.log("Setting Location Id to state")
                    setState(state => ({ ...state, location: resp.data }))
                    setLocation(resp.data)
                    console.log("Location Data retrieved Successfully")
                }
            });
        }
    }

    const saveOrUpdateLocation = (data, e) => {
        if (data.id) {
            console.log('Location data updating..: ',)
            //let id=(params.location_id) ? params.id : data.id
            LocationService.updateLocation(data, data.id, state.authHeader).then(res => {
                console.log(res)
                setLocation(res.data);
            });
        }
        else {
            console.log('Location data  saving')
            console.log(data)
            LocationService.createLocation(data, state.authHeader).then(res => {
                console.log("here")
                console.log(res.data)
                setLocation(res.data);
            });
        }

    }

    const handleChange = (event) => {
        const { name, value } = event.target;
        console.log('auto saving function')
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setLocation({ ...location, [event.target.name]: (event.target.checked) ? (value) : '' });
            location[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setLocation({ ...location, [event.target.name]: value });
            location[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdateLocation(location, event);

    };

    const handleBlurChange = (event) => {
        console.log("Blur is calling")
        saveOrUpdateLocation(location, event);
    }

    const resetLocation = () => {
        setLocation({});
    }

    const gotoLocationList = () => {
        resetLocation();
        history.replace('/location/list');

    }

    return (
        <>
            <CCard accentColor="primary">
                <CCardHeader>
                    <h3> Add Center</h3>
                </CCardHeader>
                <CCardBody>
                    <CRow className="formMargins">
                        <CCol xs="12" md="3" lg="6">
                            <SingleTextInput name="location_name"
                                title="Location Name"
                                inputType="text"
                                content={(location.location_name) ? (location.location_name) : ''}
                                placeholder="Location name"
                                manadatorySymbol={true}
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange} />
                            {validator.current.message('location', location.location_name, 'required')}
                        </CCol>
                        <CCol xs="12" md="4" lg="6">
                            <SelectBox
                                name="type_of_centre"
                                title="Type of centre"
                                placeholder='Type of centre'
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                options={appConstants.TYPEOFCENTRE}
                                selectedOption={location.type_of_centre ? location.type_of_centre : ''}
                                manadatorySymbol={true} />
                        </CCol>
                    </CRow>
                    &nbsp;&nbsp;
                    <CRow className="formMargins">
                        <CCol xs="12" md="4" lg="7">
                            <SingleTextInput name="address"
                                title="Address"
                                inputType="text"
                                content={(location.address) ? (location.address) : ''}
                                placeholder="Address"
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                multiline={true}
                                rows={5}
                                variant="outlined" />
                        </CCol>
                    </CRow>
                    <br></br>
                    <CRow className="formMargins">
                        <CCol xs="12" md="6" lg="6">
                            <SingleTextInput name="city_or_village"
                                title="City / Village"
                                inputType="text"
                                content={(location.city_or_village) ? (location.city_or_village) : ''}
                                placeholder="City / Village"
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange} />
                        </CCol>
                        <CCol xs="12" md="6" lg="6">
                            <SingleTextInput name="landmark"
                                title="Landmark"
                                inputType="text"
                                content={(location.landmark) ? (location.landmark) : ''}
                                placeholder="Landmark"
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange} />
                        </CCol>
                    </CRow>
                    <br></br>
                    <CRow className="formMargins">
                        <CCol xs="12" md="6" lg="6">
                            <SelectBox
                                name="country"
                                title="Country"
                                placeholder='Country'
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                options={appConstants.LOCATIONCOUNTRIES}
                                selectedOption={location.country ? location.country : ''}
                                manadatorySymbol={false} />
                        </CCol>
                        <CCol xs="12" md="6" lg="6">
                            {location.country == 'India' && <SelectBox
                                name="state"
                                title="State"
                                placeholder='State'
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange}
                                options={(location.country) ? appConstants.LOCATIONSTATES[location.country] : appConstants.STATES['India']}
                                selectedOption={location.state ? location.state : ''}
                                manadatorySymbol={false} />}
                            {location.country != 'India' &&
                                <SingleTextInput name="state"
                                    title="State"
                                    inputType="text"
                                    content={(location.state) ? (location.state) : ''}
                                    placeholder="State"
                                    manadatorySymbol={false}
                                    controlBlurFunc={handleBlurChange}
                                    controlFunc={handleChange} />}
                        </CCol>
                    </CRow>
                    <CRow className="formMargins">
                        <CCol xs="12" md="4" lg="4">
                            <SingleTextInput name="contact_number"
                                title="Contact Number"
                                inputType="text"
                                content={(location.contact_number) ? (location.contact_number) : ''}
                                placeholder="Contact Number"
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange} />
                            {validator.current.message('contact_number', location.contact_number, 'numeric|phone')}
                        </CCol>
                        <CCol xs="12" md="4" lg="4">
                            <SingleTextInput name="email"
                                title="Email"
                                inputType="email"
                                content={(location.email) ? (location.email) : ''}
                                placeholder="Email"
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange} />
                            {validator.current.message('email', location.email, 'email')}
                        </CCol>
                        <CCol xs="12" md="4" lg="4">
                            <SingleTextInput name="site_url"
                                title="Site URL"
                                inputType="text"
                                content={(location.site_url) ? (location.site_url) : ''}
                                placeholder="Site URL"
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange}
                                controlFunc={handleChange} />
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
        </>
    )
}