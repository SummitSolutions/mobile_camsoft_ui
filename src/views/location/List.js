import React, { useContext, useEffect, useState } from 'react';
import LocationService from '../../api/LocationService';
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CButtonGroup,
    CLink,
    CButton
} from '@coreui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus } from '@fortawesome/free-solid-svg-icons';
import * as momentService from './../../shared/MomentService';
import * as appConstants from './../../AppConstants';
import GlobalState from './../../shared/GlobalState';
import { useHistory } from 'react-router-dom';

const fields = [
    "location_name",
    "type_of_centre", 
    "site_url", 
    "contact_number",
    {
        key: 'edit_location',
        label: 'Action',
        _style: { width: '18%' },
        sorter: false,
        filter: false
    }
];

const LocationList = () => {

    const [state, setState] = useContext(GlobalState);
    const [locationList, setLocationList] = useState({data : null});
    const history = useHistory();
    
    useEffect(() => {
        console.log("Location List page loading")
        LocationService.getLocations(state.authHeader).then((res) => {
            setLocationList({ data: res.data});
        });
    }, []);

    const editLocation = (location) => {
        history.push('/location/update/'+location.id); 
    }

    const loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard accentColor="primary">
                    <CCardHeader>
                        Location List
                        <CLink 
                            className="c-subheader-nav-link" 
                            aria-current="page" 
                            to="/location/create"
                            style={{float:'right'}}>
                            <FontAwesomeIcon icon={faPlus} />&nbsp;New Location
                        </CLink>
                    </CCardHeader>
                    <CCardBody>
                    <CDataTable
                        items={locationList.data}
                        fields={fields}
                        itemsPerPage={appConstants.ITEMS_PER_PAGE}
                        pagination
                        tableFilter
                        itemsPerPageSelect
                        hover
                        sorter                    
                        scopedSlots = {{
                            'edit_location':
                                (item, index)=>{
                                return (
                                    <td className="py-2">
                                        <CButtonGroup>
                                            <CButton
                                                color={appConstants.SAVE_BTN_COLOR}
                                                size="sm"
                                                title="Edit Location"
                                                onClick={()=>{editLocation(item)}}
                                            >
                                                <FontAwesomeIcon icon={faPencilAlt} />
                                            </CButton>
                                        </CButtonGroup>
                                    </td>
                                    )
                                },
                            }
                        }
                    />
                    </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
}

export default LocationList;

/*
                            'last_visit_date' : 
                            (item, index)=>{
                                return (
                                    <td className="py-2">
                                        {momentService.formatDate(item.last_visit_date, 'DD-MMM-YYYY') }
                                    </td>
                                    )
                                },
                                */