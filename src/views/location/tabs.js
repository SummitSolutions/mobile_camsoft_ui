import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../form-components/SingleTextInput';
import SelectBox from '../form-components/SelectBox';
import SimpleReactValidator from 'simple-react-validator';
import PatientService from '../../api/PatientService';
import * as appConstants from './../../AppConstants';
import moment from 'moment';
import GlobalState from './../../shared/GlobalState';
import { Modal, Button } from "react-bootstrap";
import CreateSubstanceuseComponent from '../../views/accordian/Substanceone';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PhoneIcon from '@material-ui/icons/Phone';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import HelpIcon from '@material-ui/icons/Help';
import ShoppingBasket from '@material-ui/icons/ShoppingBasket';
import ThumbDown from '@material-ui/icons/ThumbDown';
import ThumbUp from '@material-ui/icons/ThumbUp';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));


export default function CreateSubstanceComponent() {
  const [state, setState] = useContext(GlobalState);
  const history = useHistory();
  const params = useParams();
  const [substance, setSubstance] = useState({
    add_substance: ''
  });
  const validator = useRef(new SimpleReactValidator());
  const [substanceList, setSubstanceList] = useState(appConstants.ADDSUBSTANCE);
  const [selectedItem, setSelectedItem] = useState([]);
  const [showModal, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleChangeSubstance = (event) => {
    console.log(event)
    selectedItem.push(event.target.value);
    var index = substanceList.indexOf(event.target.value);
    substanceList.splice(index, 1);
    setSelectedItem(selectedItem)
    setSubstanceList(substanceList)
    console.log(selectedItem, substanceList)
    setSubstanceList(substanceList)
    console.log(selectedItem, substanceList)

  };
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <form>
      <CCard accentColor="primary">
        <CCardHeader>Substance </CCardHeader>
        <CCardBody>
          <CRow className="formMargins">
            <CCol xs="12" md="4" lg="6">
              <SelectBox
                name="add_substance"
                title="Add substance"
                placeholder='Add new substance'
                controlFunc={handleChangeSubstance}
                options={substanceList}
                autosize={true}
                manadatorySymbol={true} />
              {validator.current.message('add_substance', substance.add_substance, 'required')}
            </CCol>
          </CRow>
          <CRow>
            <CCol xs="4" md="4" lg="6">
              <input float="center" type="button" className="btn btn-sm btn-success float-right" onClick={handleShow} value="Add substance" />
            </CCol>
            <CCol>
              {selectedItem}
            </CCol>
          </CRow>
          <CRow> {selectedItem} </CRow>
          <Modal show={showModal} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className={classes.root}>
                <AppBar position="static" color="default">
                  <Tabs
                    value={value}
                    onChange={handleChange}
                    variant="scrollable"
                    scrollButtons="on"
                    indicatorColor="primary"
                    textColor="primary"
                    aria-label="scrollable force tabs example"
                  >
                    <Tab label="Substance use"  {...a11yProps(0)} />
                    <Tab label="Life time pattern"  {...a11yProps(1)} />
                    <Tab label="Withdrawal symtoms" {...a11yProps(2)} />
                    <Tab label="Substance specific complcation"  {...a11yProps(3)} />
                    <Tab label="Previous attempts of abstinence"  {...a11yProps(4)} />
                    <Tab label="Item Six"   {...a11yProps(5)} />
                    <Tab label="Item Seven" {...a11yProps(6)} />
                  </Tabs>
                </AppBar>
                <TabPanel value={value} index={0}>
                  <CreateSubstanceuseComponent />
                </TabPanel>
                <TabPanel value={value} index={1}>
                  <CreateSubstanceuseComponent />
                </TabPanel>
                <TabPanel value={value} index={2}>
                  Item Three
                </TabPanel>
                <TabPanel value={value} index={3}>
                  Item Four
                </TabPanel>
                <TabPanel value={value} index={4}>
                  Item Five
                </TabPanel>
                <TabPanel value={value} index={5}>
                  Item Six
                </TabPanel>
                <TabPanel value={value} index={6}>
                  Item Seven
                </TabPanel>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={handleClose}>
                Save Changes
              </Button>
            </Modal.Footer>
          </Modal>
        </CCardBody>
      </CCard>
    </form>

  )
}
