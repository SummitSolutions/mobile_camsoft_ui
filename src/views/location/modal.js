import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import {
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,

} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SimpleReactValidator from 'simple-react-validator';
import SubstanceListService from 'src/api/SubstanceListService';
import moment from 'moment';
import GlobalState from '../../shared/GlobalState';
import { Modal, Button } from "react-bootstrap";
import { } from "react-lodash";
import Opioids from '../substance-details/Opioids';

export default function Model() {
    const [state, setState] = useContext(GlobalState);
    const history = useHistory();
    const params = useParams();
    const [modal, setModal] = useState({
    });
    const validator = useRef(new SimpleReactValidator());
    const [showModal, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleChange = () => {

    }
    const handleBlur = () => {

    }

    return (
        <>
            <CCard>
                <Opioids
                    obj={modal}
                // controlBlurFunc={handleBlur}
                // controlChangeFunc={handleChange}
                />
            </CCard>
        </>
    )

}

