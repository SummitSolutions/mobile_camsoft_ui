import React, { useEffect, useState, useRef, useContext } from 'react'
import {
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CButton
} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
//import Select from 'react-select';
//import makeAnimated from 'react-select/animated';
import SingleTextInput from '../form-components/SingleTextInput';
import SelectBox from '../form-components/SelectBox';
import * as appConstants from '../../AppConstants';
import Button from 'react-bootstrap/Button';
import DiagnosisDataService from '../../api/DiagnosisDataService';
import DiagnosisService from '../../api/DiagnosisService';
import GlobalState from './../../shared/GlobalState';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import { useParams } from "react-router";
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus, faEye, faTrash } from '@fortawesome/free-solid-svg-icons';


const Diagnosis = () => {
    const [state, setState] = useContext(GlobalState);
    const [diagnosis, setDiagnosis] = useState({});
    const [diaData, setDiaData] = useState({});
    const [diagnosisList, setDiagnosisList] = useState([]);
    const [diaDataList, setDiaDataList] = useState([]);
    const params = useParams();


    useEffect(() => {
        console.log("Diagnosis page loaded. Msg. from UseEffect")
        getDiagnosisData();
        getDiagnosisDataByPatientIdVisitId();

    }, []);



    const handleOnChange = (newValue) => {
        console.log(newValue);
        if (newValue?.id)
            setDiaData({ ...diaData, diagnosis_name: newValue.diagnosis_name, icd_code: newValue.icd_code });
        else
            setDiaData({});
    }

    const getDiagnosisData = () => {

        DiagnosisDataService.getDiagnosisData(state.authHeader).then(resp => {
            setDiagnosisList(resp.data);
            console.log(resp.data)
        });
    }

    const saveOrUpdateDiagnosisData = (diaData, e) => {
        console.log(diaData.patient_id)
        diaData.patient_id = parseInt(params.patient_id);
        diaData.visit_id = parseInt(params.visit_id);
        diaData.types = diagnosis.types ? diagnosis.types : '';
        console.log(diaData.types)
        DiagnosisService.createDiagnosis(diaData, state.authHeader).then((res) => {
            setDiaDataList(res.data);
            getDiagnosisDataByPatientIdVisitId();

        });

        console.log("save")
    }
    const getDiagnosisDataByPatientIdVisitId = () => {
        console.log(params);
        DiagnosisService.getDiagnosisDataById(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            console.log(params.patient_id)
            setDiaDataList(resp.data);
            // setDiagnosis(resp.data);
            console.log(resp.data)

        });
    }


    const deleteDiaData = (id) => {
        DiagnosisService.deleteDiagnosis(id, state.authHeader).then(resp => {
            setDiaDataList(resp.data);

        });
    }
    const test = (e) => {
        setDiagnosis({ ...diagnosis, [e.target.name]: e.target.value });

    }

    return (
        <>
            <h1>Diagnosis</h1>
            <CCard borderColor="primary">
                <CCardBody>
                    <CRow>
                        <CCol xs="12" md="6" lg="6" xl="6">
                            <Autocomplete
                                onChange={(event, newValue) => {
                                    handleOnChange(newValue)
                                }}
                                options={diagnosisList}
                                autoHighlight
                                getOptionLabel={(option) => option.diagnosis_name + ' ' + option.icd_code}
                                renderOption={(option) => (
                                    <React.Fragment>
                                        {option.diagnosis_name} {option.icd_code}
                                    </React.Fragment>
                                )}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label={'Diagnosis Name'}
                                        variant={appConstants.INPUT_FLOAT_LABEL.variant}
                                        inputProps={{
                                            ...params.inputProps
                                        }}
                                    />
                                )}
                            />
                        </CCol>
                        <CCol xs="12" md="4" lg="5">
                            <SelectBox
                                name="types"
                                title="Types"
                                placeholder='Types'
                                controlFunc={test}
                                //controlFunc = {handleBlurChange}
                                options={appConstants.DEFINITIVE}
                                selectedOption={diagnosis.types ? diagnosis.types : ''}
                                manadatorySymbol={true} />
                        </CCol>
                    </CRow>
                    &nbsp;
                    <CRow>
                        <CCol xs="12" md="4" lg="6">
                            <SingleTextInput name="notes"
                                title="Notes"
                                inputType="text"
                                content={(diagnosis.notes) ? (diagnosis.notes) : ''}
                                placeholder="Notes"
                                manadatorySymbol={false}
                                controlFunc={test}
                                multiline={true}
                                rows={5}
                                variant="outlined" />
                        </CCol>
                        <CCol xs="12" md="4" lg="6">
                            <SingleTextInput name="other_notes"
                                title="Other diagnosis Notes"
                                inputType="text"
                                content={(diagnosis.other_notes) ? (diagnosis.other_notes) : ''}
                                placeholder="Other diagnosis Notes"
                                manadatorySymbol={false}
                                controlFunc={test}
                                multiline={true}
                                rows={5}
                                variant="outlined" />
                        </CCol>
                    </CRow>
                    <CButton
                        className="add-button"
                        color={appConstants.SAVE_BTN_COLOR}
                        onClick={() => { saveOrUpdateDiagnosisData(diaData) }}
                        style={{ float: "right" }}
                    >
                        ADD
                    </CButton>
                    <br></br>
                    <CRow>
                        <fieldset className="scheduler-border">
                            <legend className="scheduler-border"><h3 className="subHeading">Notes:</h3></legend><br /><br />
                            <CRow>
                                <CCol xs="12" md="12" lg="12" xl="12">
                                    <h4>{(diagnosis.notes) ? (diagnosis.notes) : ''}</h4>
                                </CCol>
                            </CRow>
                        </fieldset>
                    </CRow>
                    <CRow>
                        <fieldset className="scheduler-border">
                            <legend className="scheduler-border"><h3 className="subHeading">Other Diagnosis Notes:</h3></legend><br /><br />

                            <CCol xs="12" md="12" lg="12" xl="12">
                                <h4>{(diagnosis.other_notes) ? (diagnosis.other_notes) : ''}</h4>
                            </CCol>
                        </fieldset>
                    </CRow>
                </CCardBody>
            </CCard>
            {diaDataList.length > 0 &&
                <TableContainer style={{ backgroundColor: 'lemonchiffon' }}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>
                                    <b>Diagnosis Name</b>
                                </TableCell>
                                <TableCell>
                                    <b>ICD Code</b>
                                </TableCell>
                                <TableCell>
                                    <b>Types</b>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        {diaDataList.length > 0 &&
                            <TableBody>
                                {diaDataList.map((element, i) => (
                                    <TableRow key={i}>
                                        <TableCell>{element?.diagnosis_name}</TableCell>
                                        <TableCell>{element.icd_code}</TableCell>
                                        <TableCell>{element.types}</TableCell>
                                        <TableCell>
                                            <CButton
                                                color={appConstants.DELETE_BTN_COLOR}
                                                size="sm"
                                                title="Delete"
                                                onClick={() => { deleteDiaData(element.id) }}
                                            >
                                                <FontAwesomeIcon icon={faTrash} />
                                            </CButton>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        }
                    </Table>
                </TableContainer>}
        </>
    )
}
export default Diagnosis;