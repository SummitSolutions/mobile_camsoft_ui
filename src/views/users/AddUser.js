import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow, } from '@coreui/react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../form-components/SingleTextInput';
import SelectBox from '../form-components/SelectBox';
import SimpleReactValidator from 'simple-react-validator';
import AddUserService from '../../api/AddUserService';
import * as appConstants from './../../AppConstants';
import GlobalState from './../../shared/GlobalState';
import SingleCheckbox from '../form-components/SingleCheckbox';
import KeyboardDatePickers from '../form-components/KeyboardDatePicker';
import * as momentServices from './../../shared/MomentService';
import * as DateConfigs from './../../shared/configs/DateConfigs';
import RadioButtonList from '../form-components/RadioButtonList';

export default function AddUser() {

    const { handleSubmit, errors, getValues, watch, formState } = useForm();
    const history = useHistory();
    const params = useParams();
    const [state, setState] = useContext(GlobalState);
    const [user, setUser] = useState({
    });
    const validator = useRef(new SimpleReactValidator());
    useEffect(() => {
        getUsersByCenterId();
        console.log("Center Create page loaded. Msg. from UseEffect")
    }, []);

    const handleChange = (event) => {
        const { name, value } = event.target;
        console.log('auto saving function')
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setUser({ ...user, [event.target.name]: (event.target.checked) ? (value) : '' });
            user[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setUser({ ...user, [event.target.name]: value });
            user[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdateUser(user, event);

    };

    const handleBlurChange = (event) => {
        console.log("Blur is calling")
        saveOrUpdateUser(user, event);
    }

    // const gotoCenterList = () => {
    //     resetCenter();
    //     history.replace('/center/center-list');

    // }
    const saveOrUpdateUser = (data, event) => {
        console.log(data)
        if (data.id) {
            console.log('center data updating..: ',)
            //let id = (params.patient_id) ? params.id : data.id
            AddUserService.updateUser(data.id, data, state.authHeader).then(res => {
                console.log(res)
                setUser(res.data);
            });
        }
        else {
            console.log('center saving')
            data.center_id = parseInt(params.center_id);
            console.log(data)
            AddUserService.createUser(data, state.authHeader).then(res => {
                console.log("here")
                console.log(res.data)
                setUser(res.data);
            });
        }
    }
    const handleKeyboardPickerChange = (date) => {
        if (date) {
            let age = momentServices.getAgeFromDOB(date, 'years');
            user.age = age;
            setUser({ ...user, 'age': age });
        }
        setUser({ ...user, 'dob': date });
    }

    const getUsersByCenterId = () => {
        AddUserService.getUserByCenterId(params.center_id, state.authHeader).then(resp => {
            setUser(resp.data);
        });
    }


    return (
        <>
            <div className="body-container">
                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="4">
                        <h3>{params.user_id && 'Update'} {!params.user_id && 'Add'} User</h3>
                    </CCol>
                </CRow>
                &nbsp;
                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="4">
                        <SingleTextInput name="first_name"
                            title="First Name"
                            inputType="text"
                            content={(user.first_name) ? (user.first_name) : ''}
                            placeholder="First name"
                            manadatorySymbol={true}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                        {validator.current.message('user', user.first_name, 'required')}
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SingleTextInput name="last_name"
                            title="Last Name"
                            inputType="text"
                            content={(user.last_name) ? (user.last_name) : ''}
                            placeholder="Last name"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <KeyboardDatePickers
                            name="dob"
                            disableToolbar
                            format="DD/MM/yyyy"
                            content={(user.dob) ? (user.dob) : momentServices.currentDate(DateConfigs.DEFAULT_DATE_FORMAT_BACKEND)}
                            manadatorySymbol={false}
                            controlFunc={handleKeyboardPickerChange}
                        />
                    </CCol>
                </CRow>
                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="4">
                        <RadioButtonList
                            name="gender"
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange}
                            options={appConstants.GENDER_OPTIONS}
                            content={user.gender}
                            manadatorySymbol={false} />
                    </CCol>
                    <CCol xs="12" md="1" lg="2">
                        <SingleTextInput name="age"
                            title="Age"
                            inputType="text"
                            content={(user.age) ? (user.age) : ''}
                            placeholder="Age"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                    <CCol xs="12" md="3" lg="3">
                        <SingleTextInput name="contact_number"
                            title="Contact Number"
                            inputType="text"
                            content={(user.contact_number) ? (user.contact_number) : ''}
                            placeholder="Contact Number"
                            manadatorySymbol={true}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                        {validator.current.message('contact_number', user.contact_number, 'numeric|phone')}
                    </CCol>
                    <CCol xs="12" md="3" lg="3">
                        <SingleTextInput name="alternate_number"
                            title="Alternate Number"
                            inputType="text"
                            content={(user.alternate_number) ? (user.alternate_number) : ''}
                            placeholder="Contact Number"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                </CRow>
                <CRow className="formMargins">
                    <CCol xs="12" md="3" lg="3">
                        <SingleTextInput name="email"
                            title="Email id"
                            inputType="email"
                            content={(user.email) ? (user.email) : ''}
                            placeholder="Email id"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                    <CCol xs="12" md="3" lg="3">
                        <SingleTextInput name="role"
                            title="Role"
                            inputType="text"
                            content={(user.role) ? (user.role) : ''}
                            placeholder="Role"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange}
                        />
                    </CCol>
                    <CCol xs="12" md="3" lg="3">
                        <SingleTextInput name="center_name"
                            title="Center Name"
                            inputType="text"
                            content={(user.center_name) ? (user.center_name) : ''}
                            placeholder="Center name"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                    <CCol xs="12" md="3" lg="3">
                        <SingleTextInput name="medical_reg_no"
                            title="Medical Reg no"
                            inputType="text"
                            content={(user.medical_reg_no) ? (user.medical_reg_no) : ''}
                            placeholder="Medical Reg no(Optional)"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                </CRow>
                <CRow className="formMargins">
                    <CCol xs="12" md="2" lg="4">
                        <SingleTextInput name="login_name"
                            title="Login name"
                            inputType="text"
                            content={(user.login_name) ? (user.login_name) : ''}
                            placeholder="Login name"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SingleTextInput
                            name="password"
                            title="Password"
                            inputType="password"
                            content={(user.password) ? (user.password) : ''}
                            placeholder="Password"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SingleTextInput
                            name="confirm_password"
                            title="Confirm Password"
                            inputType="password"
                            content={(user.confirm_password) ? (user.confirm_password) : ''}
                            placeholder="Confirm Password"
                            manadatorySymbol={false}
                            controlBlurFunc={handleBlurChange}
                            controlFunc={handleChange} />
                    </CCol>
                </CRow>
                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="4">
                        <SingleCheckbox
                            controlFunc={handleChange}
                            name='is_active'
                            title='Is Active'
                            type="checkbox"
                            value='is_active'
                            content={user.is_active}
                        />
                    </CCol>
                </CRow>
            </div>
        </>
    )
}