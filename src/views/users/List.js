import React, { useContext, useEffect, useState } from 'react';
import CreateCenterService from '../../api/CreateCenterService';
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CButtonGroup,
    CLink,
    CButton
} from '@coreui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus } from '@fortawesome/free-solid-svg-icons';
import * as momentService from './../../shared/MomentService';
import * as appConstants from './../../AppConstants';
import GlobalState from './../../shared/GlobalState';
import { useHistory } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import AddUserService from '../../api/AddUserService';
import { AddButtonWithTitle } from './../form-components/AddButtonWithTitle';

const fields = [
    "first_name",
    "last_name",
    "role",
    "contact_number",
    {
        key: 'edit_user',
        label: 'Action',
        _style: { width: '18%' },
        sorter: false,
        filter: false
    }
];

const UsersList = () => {

    const [state, setState] = useContext(GlobalState);
    const [userList, setUserList] = useState([]);
    const history = useHistory();
    const params = useParams();

    useEffect(() => {
        getUsers();
        console.log("User List page loading")

    }, []);

    const editUser = (user) => {
        history.push('/users/update/' + params.center_id + '/' + user.id);
    }
    const getUsers = (id) => {
        console.log()
        AddUserService.getUserByCenterId(params.center_id, state.authHeader).then((res) => {
            console.log(res)
            setUserList(res.data);

        });
    }

    const addUser = () => {
        history.push('/users/add-user/' + params.center_id);
    }



    const loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

    return (
        <>
            <AddButtonWithTitle
                title='ADD NEW USER'
                addNewEntityOnClick={addUser}
            />
            <div className="body-container">
                <CDataTable
                    items={userList}
                    fields={fields}
                    //itemsPerPage={appConstants.ITEMS_PER_PAGE}
                    pagination
                    tableFilter
                    //itemsPerPageSelect
                    hover
                    sorter
                    scopedSlots={{
                        'edit_user':
                            (item, index) => {
                                return (
                                    <td className="py-2">
                                        <CButtonGroup>
                                            <CButton
                                                color={appConstants.SAVE_BTN_COLOR}
                                                size="sm"
                                                title="Edit User"
                                                onClick={() => { editUser(item) }}
                                            >
                                                <FontAwesomeIcon icon={faPencilAlt} />
                                            </CButton>

                                        </CButtonGroup>
                                    </td>
                                )
                            },
                    }
                    }
                />
            </div>
        </>
    );
}

export default UsersList;
