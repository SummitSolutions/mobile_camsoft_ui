import React, { useEffect, useState, useRef, useContext } from 'react'
import PatientService from '../../api/PatientService';
import { useParams } from "react-router";
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CButtonGroup,
    CLink,
    CButton
} from '@coreui/react';
import { AddButtonWithTitle } from './../form-components/AddButtonWithTitle';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus } from '@fortawesome/free-solid-svg-icons';
import * as MomentService from './../../shared/MomentService';
import * as DateConfigs from './../../shared/configs/DateConfigs';
import * as appConstants from '../../AppConstants'
import GlobalState from '../../shared/GlobalState';
import PatientVisitService from './../../api/PatientVisitService';
import FollowupService from 'src/api/FollowupService';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';
import PatientDetails from './../form-components/PatientDetails';

const fields = [
    "visit_date", "mode",
    {
        key: "diagnosis",
        label: "Diagnosis"
    },
    {
        key: 'actions',
        label: 'Action',
        sorter: false,
        filter: false
    }
];

function PatientVisitList() {

    const params = useParams();
    const [state, setState] = useContext(GlobalState);
    const [patientVisitList, setPatientVisitList] = useState([]);
    const history = useHistory()
    const [patientVisit, setPatientVisit] = useState([])
    const [FollowupList, setFollowupList] = useState({ data: [] });
    const [double, setDouble] = useState(false);
    useEffect(() => {
        console.log("Patient Visit List page loaded. Msg. from UseEffect")
        console.log(state)
        getAllVisits();
        getPatientVisitData();
        if (params.patient_id) {
            return () => {
                console.log('Clearing Patient Id from State')
            }
        }
    }, []);



    const getPatientVisitData = () => {
        if (params.patient_id > 0) {
            PatientService.getPateintAndVisits(params.patient_id, state.authHeader).then(resp => {
                console.log(resp.data)

                let pvList = _.orderBy(resp.data.patientvisits, ['visit_date'], ['desc']);
                setPatientVisitList(pvList);
                delete resp.data.patientvisits;

                console.log("Setting Patient Visit Data to state")
                setState(state => ({ ...state, patient: resp.data }))
                console.log("Patient Visit Data retrieved Successfully")


            });
        }
    }

    const addNewVisit = (e, data) => {
        let patientVisit = {
            patient_id: parseInt(params.patient_id),
            mode: appConstants.VISIT_MODES.op
        };
        PatientVisitService.createPatientVisit(patientVisit, state.authHeader).then(resp => {
            if (resp?.data?.result === false) {
                alert("Patient Visit already created for today's date.");
            }
            else {
                setPatientVisitList(resp.data)
            }
        });
    }

    const gotoSubstance = (item) => {
        //history.replace('/substance-pages/patient-substance-use/'+params.patient_id+'/'+item.id);
        console.log(state)
        setState(state => ({ ...state, patient: state.patient, visitId: item.id, visitDate: item?.visit_date, proforma_type: item?.proforma_type }));
        history.replace('/substance-pages/mainpage/' + params.patient_id + '/' + item.id);
    }

    const gotoFollowup = (item) => {
        history.replace('/followup-pages/followup-page/' + params.patient_id + '/' + item.id);
    }
    const gotoSummary = (item) => {
        history.replace('/followup/summary/' + params.patient_id + '/' + item.id);
    }


    const getAllVisits = (item) => {
        console.log('calling')
        PatientVisitService.getPatientVisits(state.authHeader).then(resp => {
            setPatientVisitList(resp.data);
        });
    }



    return (
        <>
            <PatientDetails />
            <AddButtonWithTitle
                title='ADD NEW PATIENT VISIT'
                addNewEntityOnClick={addNewVisit}
            />
            <div className="body-container">
                <CDataTable
                    items={patientVisitList}
                    fields={fields}
                    itemsPerPage={appConstants.ITEMS_PER_PAGE}
                    pagination
                    // tableFilter
                    // itemsPerPageSelect
                    hover
                    sorter
                    //clickableRows
                    //onRowClick={() => { gotoProformaPages() }}
                    scopedSlots={{
                        'visit_date':
                            (item, index) => {
                                return (
                                    <td className="py-2">
                                        {MomentService.formatDate(item.visit_date, DateConfigs.DEFAULT_DATE_FORMAT_UI)}
                                    </td>
                                )
                            },
                        'diagnosis':
                            (item, index) => {
                                return (
                                    <td className="py-2">
                                        Diagnosis text here
                                    </td>
                                )
                            },
                        'actions':
                            (item, index) => {
                                return (
                                    <td className="py-2">
                                        <CButtonGroup>
                                            {(index === patientVisitList.length - 1) &&
                                                <CButton
                                                    color={appConstants.CANCEL_BTN_COLOR}
                                                    size="sm"
                                                    title="Go to Substance use"
                                                    onClick={() => { gotoSubstance(item) }}
                                                >
                                                    Substance use
                                                </CButton>}

                                            {((index === 0) && (patientVisitList.length > 1)) &&
                                                <CButtonGroup>
                                                    <CButton
                                                        color={appConstants.SAVE_BTN_COLOR}
                                                        size="sm"
                                                        title="Go to Followup pages"
                                                        onClick={() => { gotoFollowup(item) }}
                                                    >
                                                        Followup
                                                    </CButton>
                                                    <CButton
                                                        color={appConstants.EDIT_BTN_COLOR}
                                                        size="sm"
                                                        title="Go to Followup pages"
                                                        onClick={() => { gotoSummary(item) }}
                                                    >
                                                        Summary
                                                    </CButton>
                                                </CButtonGroup>
                                            }
                                        </CButtonGroup>
                                    </td>
                                )
                            }
                    }
                    }
                />
            </div>
        </>
    );
}

export default PatientVisitList;