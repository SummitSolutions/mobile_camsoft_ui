import React,{useContext, useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import LifeTimePatternInhalants from '../substance-pages/inhalants/LifeTimePatternInhalants';
import SubstanceUseInhalants from '../substance-pages/inhalants/SubstanceUseInhalants';
import WithdrawalSymptomsInhalants from '../substance-pages/inhalants/WithdrawalSymptomsInhalants';
import SubstanceSpecificInhalants from '../substance-pages/inhalants/SubstanceSpecificInhalants';
import PreviousAttemptsInhalants from '../substance-pages/inhalants/PreviousAttemptsInhalants';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

export default function Inhalants(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
        <h1 component={'div'}>Inhalants</h1>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
            >
            <Typography className={classes.heading} component={'div'}>Substance Use</Typography>
            </AccordionSummary>
            <AccordionDetails>
            <Typography>
                <SubstanceUseInhalants obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
            </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header">
            <Typography className={classes.heading} component={'div'}>Lifetime Pattern</Typography>
            </AccordionSummary>
            <AccordionDetails>
            <Typography component={'div'}>
                <LifeTimePatternInhalants obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
            </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header">
            <Typography className={classes.heading} component={'div'}>Withdrawal Symptoms</Typography>
            </AccordionSummary>
            <AccordionDetails>
            <Typography component={'div'}>
                <WithdrawalSymptomsInhalants obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
            </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header">
            <Typography className={classes.heading} component={'div'}>Substance Specific Complication</Typography>
            </AccordionSummary>
            <AccordionDetails>
            <Typography component={'div'}>
                <SubstanceSpecificInhalants obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
            </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header">
            <Typography className={classes.heading} component={'div'}>Previous Attempts of Abstinence</Typography>
            </AccordionSummary>
            <AccordionDetails>
            <Typography component={'div'}>
                <PreviousAttemptsInhalants obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
            </Typography>
            </AccordionDetails>
        </Accordion>
    </div>
  );
}