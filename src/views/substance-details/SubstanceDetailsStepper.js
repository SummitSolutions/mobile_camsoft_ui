import React, { useState, useEffect, useContext } from 'react';
import { useParams } from "react-router";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AddNewSubstanceModal from './../substance-pages/AddNewSubstanceModal';
import './index.css';
import { CCol, CRow } from '@coreui/react';
import GlobalState from '../../shared/GlobalState';
import { Modal } from "react-bootstrap";
import { AddButtonWithTitle } from './../form-components/AddButtonWithTitle';
import SubstanceUseService from 'src/api/SubstanceUseService';
import SubstanceDetailService from 'src/api/SubstanceDetailService';

import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
import PropTypes from 'prop-types';
import Alcohol from './../substance-details/Alcohol';
import Caffeine from './../substance-details/Caffeine';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Tooltip from '@material-ui/core/Tooltip';
import Box from '@mui/material/Box';

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
	},
	backButton: {
		marginRight: theme.spacing(1),
	},
	instructions: {
		marginTop: theme.spacing(1),
		marginBottom: theme.spacing(1),
	},
}));

function TabPanel(props) {
	const { children, value, index, ...other } = props;

	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`scrollable-auto-tabspanel-${index}`}
			aria-labelledby={`scrollable-auto-tab-${index}`}
			{...other}
		>
			{value === index && (
				<Box p={4}>
					<Typography component={'div'}>{children}</Typography>
				</Box>
			)}
		</div>
	);
}

TabPanel.propTypes = {
	children: PropTypes.node,
	index: PropTypes.any.isRequired,
	value: PropTypes.any.isRequired,
};

function a11yProps(index, obj) {
	console.log(index, obj)
	return {
		id: `scrollable-auto-tab-${index}`,
		'aria-controls': `scrollable-auto-tabpanel-${index}`,
	};
}

export default function SubstanceDetailsStepper(props) {
	const classes = useStyles();
	const theme = useTheme();
	const params = useParams();
	const [state, setState] = useContext(GlobalState);
	const [showModal, setShowModal] = useState(false);
	const [substanceUseList, setSubstanceUseList] = useState([]);
	const [patientSubstanceDetails, setPatientSubstanceDetails] = useState({});
	const [value, setValue] = useState(0);
	const [tabList, setTabList] = useState([]);

	useEffect(() => {
		getAllSubstanceUse();
	}, []);

	const handleShow = () => setShowModal(true);
	const handleClose = () => {
		setShowModal(false);
		getAllSubstanceUse();
	}

	// patient substance use api
	const getAllSubstanceUse = () => {
		SubstanceUseService.getAllSubstanceUse(params.patient_id, params.visit_id, state.authHeader).then((resp) => {
			setSubstanceUseList(resp.data);
			checkSubstanceExistOrNot(resp.data);
		})
	}

	//AutoSaveAlcohol
	const saveOrUpdateAlcohol = (data, event) => {
		if (data.id) {
			console.log('Alcohol data updating..: ', data)
			// let id = (params.patient_id) ? params.patient_id : data.patient_id
			SubstanceDetailService.updateSubstanceDetail(data.id, data, state.authHeader).then(resp => {
				console.log(resp)
				setPatientSubstanceDetails(resp.data);
			});
		}
		else {
			console.log("Alcohol data saving")
			data.patient_id = parseInt(params.patient_id);
			data.visit_id = parseInt(params.visit_id);
			data.patient_substance_id = tabList[value].substance_use_list.id;
			SubstanceDetailService.createSubstanceDetail(data, state.authHeader).then(resp => {
				setPatientSubstanceDetails(resp.data);
			});
		}
	}

	const handleBlur = (event) => {
		saveOrUpdateAlcohol(patientSubstanceDetails, event);
	};

	const handleChange = (event) => {
		const { name, value, checked, type } = event.target;
		if (type == 'checkbox') {
			setPatientSubstanceDetails({ ...patientSubstanceDetails, [name]: (checked) ? (value) : '' });
			patientSubstanceDetails[name] = (checked) ? (value) : '';
		}
		else {
			setPatientSubstanceDetails({ ...patientSubstanceDetails, [name]: value });
			patientSubstanceDetails[name] = value;
		}
		if (type != 'text')
			saveOrUpdateAlcohol(patientSubstanceDetails, event);
	};

	//Display in stepper when substance is checked 
	const checkSubstanceExistOrNot = (dataList) => {
		let listItems = [];
		dataList.forEach(substance => {
			if (substance.substance_use_list?.current_use || substance.substance_use_list?.ever_use) {
				listItems.push(substance);
			}
		});
		setTabList(listItems);
		if (listItems.length > 0) {
			// activeStep.currentPage = listItems[0];
			// setActiveStep({ ...activeStep, currentPage: listItems[0], currentPageIndex: 0 });
			getSubstanceDetail(listItems[0].substance_use_list.id);
		}
	}

	// fetches Substance Details for Selected Substance (Alcohol, Opiods ...)
	const getSubstanceDetail = (id) => {
		SubstanceDetailService.getSubstanceDetailForSubstance(params.patient_id, params.visit_id, id, state.authHeader).then((resp) => {
			setPatientSubstanceDetails(resp.data)
		});
	}

	const handleTabChange = (event, newValue) => {
		setValue(newValue);
	};

	const getTabs = () => {
		let tabs = []
		tabList.map((stepheader, index) => {
			tabs.push(<Tooltip title={stepheader.substance_name} key={stepheader.substance_name + '_' + stepheader.id}>
				<Tab label={stepheader.substance_name} aria-label={stepheader.substance_name} {...a11yProps(index)} />
			</Tooltip>)
		});
		return tabs;
	}

	return (
		<>
			<Modal show={showModal} onHide={handleClose} backdrop="static">
				<Modal.Header closeButton>
					<Modal.Title>Add New Substance</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<AddNewSubstanceModal />
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={handleClose}>Close </Button>
				</Modal.Footer>
			</Modal>
			<CRow>
				{(tabList?.length > 0) && <CCol xs="2" sm="1" md="1" lg="1" xl="1">
					<AddButtonWithTitle
						title={''}
						addNewEntityOnClick={handleShow}
					/>
				</CCol>
				}
				{(tabList?.length <= 0) &&
					<CCol xs="12" sm="12" md="12" lg="12" xl="12">
						<AddButtonWithTitle
							title={'Add New Substance'}
							addNewEntityOnClick={handleShow}
						/>
					</CCol>
				}
				<CCol>
					{(tabList?.length > 0) && <div position="static" style={{ marginLeft: '-15px' }}>
						<Tabs
							value={value}
							onChange={handleTabChange}
							variant="scrollable"
							scrollButtons="auto"
							indicatorColor="primary"
							textColor="primary"
							aria-label="scrollable auto tabs example"
							className={'patientVisitTopTabs'}
						>
							{getTabs()}
						</Tabs>
					</div>}
				</CCol>
			</CRow>
			<CRow>
				<CCol>
					{(tabList?.length > 0) &&
						<div className='patientVisitTopTabPanels'>
							<TabPanel value={value} index={0} dir={theme.direction}>
								<Alcohol obj={patientSubstanceDetails} controlBlurFunc={handleBlur} controlChangeFunc={handleChange} />
							</TabPanel>
							<TabPanel value={value} index={1} dir={theme.direction}>
								<Caffeine obj={patientSubstanceDetails} controlBlurFunc={handleBlur} controlChangeFunc={handleChange} />
							</TabPanel>
						</div>
					}
				</CCol>
			</CRow>
		</>
	);
}