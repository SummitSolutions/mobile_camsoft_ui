import React,{useContext, useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import LifeTimePatternAnyOtherSubstance from '../substance-pages/any-other-substance/LifeTimePatternAnyOtherSubstance';
import SubstanceUseAnyOtherSubstance from '../substance-pages/any-other-substance/SubstanceUseAnyOtherSubstance';
import WithdrawalAnyOtherSubstance from '../substance-pages/any-other-substance/WithdrawalAnyOtherSubstance';
import SubstanceSpecificAnyOtherSubstance from '../substance-pages/any-other-substance/SubstanceSpecificAnyOtherSubstance';
import PreviousAttemptsAnyOtherSubstance from '../substance-pages/any-other-substance/PreviousAttemptsAnyOtherSubstance';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

export default function AnyOtherSubstance(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
        <h1>Any Other Substance</h1>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                >
                <Typography className={classes.heading} component={'div'}>Substance Use</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <SubstanceUseAnyOtherSubstance obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc} />
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Lifetime Pattern</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <LifeTimePatternAnyOtherSubstance obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Withdrawal Symptoms</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <WithdrawalAnyOtherSubstance obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Substance Specific Complication</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <SubstanceSpecificAnyOtherSubstance obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc} />
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Previous Attempts of Abstinence</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <PreviousAttemptsAnyOtherSubstance obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
                </Typography>
            </AccordionDetails>
        </Accordion>
    </div>
  );
}