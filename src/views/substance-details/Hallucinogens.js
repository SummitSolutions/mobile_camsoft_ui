import React,{useContext, useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import LifeTimePatternHallucinogens from '../substance-pages/hallucinogens/LifeTimePatternHallucinogens';
import SubstanceUseHallucinogens from '../substance-pages/hallucinogens/SubstanceUseHallucinogens';
import WithdrawalSymptomsHallucinogens from '../substance-pages/hallucinogens/WithdrawalSymptomsHallucinogens';
import SubstanceSpecificHallucinogens from '../substance-pages/hallucinogens/SubstanceSpecificHallucinogens';
import PreviousAttemptsHallucinogens from '../substance-pages/hallucinogens/PreviousAttemptsHallucinogens';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

export default function Opioids(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
        <h1>Hallucinogens</h1>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                >
                <Typography className={classes.heading} component={'div'}>Substance Use</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <SubstanceUseHallucinogens obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc} />
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Lifetime Pattern</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <LifeTimePatternHallucinogens obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Withdrawal Symptoms</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <WithdrawalSymptomsHallucinogens obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Substance Specific Complication</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <SubstanceSpecificHallucinogens obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc} />
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Previous Attempts of Abstinence</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <PreviousAttemptsHallucinogens obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
                </Typography>
            </AccordionDetails>
        </Accordion>
    </div>
  );
}