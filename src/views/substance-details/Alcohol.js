import React, { useContext, useEffect, useState } from 'react';
import { useParams } from "react-router";
import * as appConstants from './../../AppConstants';
import './index.css';
import { CCol, CRow } from '@coreui/react';
import { useTheme } from '@mui/material/styles';
import GlobalState from '../../shared/GlobalState';

import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import StepContent from '@mui/material/StepContent';

import Button from '@mui/material/Button';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';
import Typography from '@mui/material/Typography';

import LifeTimePatternAlcohol from '../substance-pages/alcoholuse/LifeTimePatternAlcohol';
import SubstanceUseAlcohol from '../substance-pages/alcoholuse/SubstanceUseAlcohol';
import WithdrawalSymptomsAlcohol from '../substance-pages/alcoholuse/WithdrawalSymptomsAlcohol';
import SubstanceSpecificAlcohol from '../substance-pages/alcoholuse/SubstanceSpecificAlcohol';
import PreviousAttemptsAlcohol from '../substance-pages/alcoholuse/PreviousAttemptsAlcohol';
import CreatePatientComponent from '../patient/Create';



export default function Alcohol(props) {
    ;
    const theme = useTheme();
    const params = useParams();
    const [state, setState] = useContext(GlobalState);
    const [steps, setSteps] = useState(appConstants.SUBSTANCE_USE_TABS);
    const [activeStep, setActiveStep] = useState(0);

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };
    return (
        <>
            <CRow>
                <CCol>
                    <Stepper activeStep={activeStep} orientation="vertical">
                        {steps.map((substance, index) => (
                            <Step key={substance}>
                                <CRow>
                                    <CCol>
                                        <StepLabel>{substance}</StepLabel>
                                    </CCol>
                                    {(index === activeStep) && <CCol xs="5" sm="1" md="1" lg="1" xl="1">

                                        {(activeStep > 0) &&
                                            <Button aria-label="Previous"
                                                variant="contained"
                                                onClick={handleBack}
                                                color="primary"
                                                size="small"
                                                styel={{ minWidth: '0px', float: 'right' }}>
                                                <NavigateBeforeIcon />
                                            </Button>
                                        }
                                        {(activeStep < (steps.length - 1)) &&
                                            <Button aria-label="Next"
                                                variant="contained"
                                                onClick={handleNext}
                                                color="primary"
                                                size="small"
                                                styel={{ minWidth: '0px', float: 'right' }}>
                                                <NavigateNextIcon />
                                            </Button>
                                        }
                                    </CCol>}
                                </CRow>
                                <StepContent>
                                    <SubstanceUseAlcohol obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc} />
                                    <LifeTimePatternAlcohol obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc} />

                                </StepContent>
                            </Step>
                        ))}
                    </Stepper>
                </CCol>
            </CRow>
            <CRow>
                <CCol>
                </CCol>
            </CRow >
        </>
        // <div className={classes.root}>
        //     <Accordion>
        //         <AccordionSummary
        //         expandIcon={<ExpandMoreIcon />}
        //         aria-controls="panel1a-content"
        //         id="panel1a-header"
        //         >
        //         <Typography className={classes.heading} component={'div'}>Substance Use</Typography>
        //         </AccordionSummary>
        //         <AccordionDetails>
        //         <Typography>
        //             <SubstanceUseAlcohol obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
        //         </Typography>
        //         </AccordionDetails>
        //     </Accordion>
        //     <Accordion>
        //         <AccordionSummary
        //         expandIcon={<ExpandMoreIcon />}
        //         aria-controls="panel2a-content"
        //         id="panel2a-header">
        //         <Typography className={classes.heading} component={'div'}>Lifetime Pattern</Typography>
        //         </AccordionSummary>
        //         <AccordionDetails>
        //         <Typography component={'div'}>
        //             <LifeTimePatternAlcohol obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
        //         </Typography>
        //         </AccordionDetails>
        //     </Accordion>
        //     <Accordion>
        //         <AccordionSummary
        //         expandIcon={<ExpandMoreIcon />}
        //         aria-controls="panel2a-content"
        //         id="panel2a-header">
        //         <Typography className={classes.heading} component={'div'}>Withdrawal Symptoms</Typography>
        //         </AccordionSummary>
        //         <AccordionDetails>
        //         <Typography component={'div'}>
        //             <WithdrawalSymptomsAlcohol obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
        //         </Typography>
        //         </AccordionDetails>
        //     </Accordion>
        //     <Accordion>
        //         <AccordionSummary
        //         expandIcon={<ExpandMoreIcon />}
        //         aria-controls="panel2a-content"
        //         id="panel2a-header">
        //         <Typography className={classes.heading} component={'div'}>Substance Specific Complication</Typography>
        //         </AccordionSummary>
        //         <AccordionDetails>
        //         <Typography component={'div'}>
        //             <SubstanceSpecificAlcohol obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
        //         </Typography>
        //         </AccordionDetails>
        //     </Accordion>
        //     <Accordion>
        //         <AccordionSummary
        //         expandIcon={<ExpandMoreIcon />}
        //         aria-controls="panel2a-content"
        //         id="panel2a-header">
        //         <Typography className={classes.heading} component={'div'}>Previous Attempts of Abstinence</Typography>
        //         </AccordionSummary>
        //         <AccordionDetails>
        //         <Typography component={'div'}>
        //             <PreviousAttemptsAlcohol obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
        //         </Typography>
        //         </AccordionDetails>
        //     </Accordion>
        // </div>
    );
}