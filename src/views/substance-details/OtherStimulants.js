import React,{useContext, useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import LifeTimePatternStimulants from '../substance-pages/other-stimulants/LifeTimePatternStimulants';
import SubstanceUseStimulants from '../substance-pages/other-stimulants/SubstanceUseStimulants';
import WithdrawalSymptomsStimulants from '../substance-pages/other-stimulants/WithdrawalSymptomsStimulants';
import SubstanceSpecificStimulants from '../substance-pages/other-stimulants/SubstanceSpecificStimulants';
import PreviousAttemptsStimulants from '../substance-pages/other-stimulants/PreviousAttemptsStimulants';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

export default function OtherStimulants(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
        <h1>Other Stimulants</h1>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                >
                <Typography className={classes.heading} component={'div'}>Substance Use</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <SubstanceUseStimulants obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc} />
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Lifetime Pattern</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <LifeTimePatternStimulants obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Withdrawal Symptoms</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <WithdrawalSymptomsStimulants obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Substance Specific Complication</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <SubstanceSpecificStimulants obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc} />
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header">
                <Typography className={classes.heading} component={'div'}>Previous Attempts of Abstinence</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    <PreviousAttemptsStimulants obj={props.obj} controlFunc={props.controlChangeFunc} controlBlurFunc={props.controlBlurFunc}/>
                </Typography>
            </AccordionDetails>
        </Accordion>
    </div>
  );
}