import React, { useContext, useEffect, useState, useRef } from 'react';
import { useParams } from "react-router";
import GlobalState from '../../shared/GlobalState';
import SubstanceUseService from 'src/api/SubstanceUseService';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SubstanceHeader from '../substance-pages/SubstanceHeaderAlcohol';
import { useHistory } from 'react-router-dom';
import { faAngleDoubleUp } from '@fortawesome/free-solid-svg-icons';
import { CButton } from '@coreui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Button from 'react-bootstrap/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

export default function AlcoholSubstanceDetails() {
  const classes = useStyles();
  const params = useParams();
  const [substanceUseList, setSubstanceUseList] = useState([]);
  const [state, setState] = useContext(GlobalState);
  const [isVisible, setIsVisible] = useState(false);
  const history = useHistory();


  useEffect(() => {
    getAllSubstanceUse();
  }, []);

  useEffect(() => {
    window.addEventListener("scroll", toggleVisibility);
  }, []);



  const getAllSubstanceUse = () => {
    SubstanceUseService.getAllSubstanceUse(params.patient_id, params.visit_id, state.authHeader).then((resp) => {
      console.log(resp)
      setSubstanceUseList(resp.data)
    })
    console.log(substanceUseList.find(substance => substance.substance_id == 1))
  }
  const checkSubstanceExistOrNot = (substanceName) => {
    let substance = substanceUseList.find(substance => substance.substance_name == substanceName)
    if (substance.substance_use_list.current_use || substance.substance_use_list.ever_use)
      return true;
    return false;
  }

  const toggleVisibility = () => {
    if (window.pageYOffset > 300) {
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };

  // Set the top cordinate to 0
  // make scrolling smooth
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  };
  const gotoPatientSubstanceUse = () => {
    history.replace('/substance-pages/patient-substance-use/' + params.patient_id + '/' + params.visit_id);

  }
  return (
    <>
      {substanceUseList.length > 0 && <div className={classes.root} >
        { /*Alcohol */}
        {checkSubstanceExistOrNot('Alcohol') &&
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading} component={'div'}>Alcohol</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <SubstanceHeader />
              </Typography>
            </AccordionDetails>
          </Accordion>}
        {/* caffeine*/}
        {checkSubstanceExistOrNot('Caffeine') &&
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography className={classes.heading} component={'div'}>Caffeine</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>

              </Typography>
            </AccordionDetails>
          </Accordion>}
        {checkSubstanceExistOrNot('Opioids') &&
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography className={classes.heading} component={'div'}>Opioids</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>

              </Typography>
            </AccordionDetails>
          </Accordion>}
        <Accordion >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel3a-content"
            id="panel3a-header"
          >
            <Typography className={classes.heading} component={'div'}>Cannabinioids</Typography>
          </AccordionSummary>
        </Accordion>
      </div>}
      <div>
        <Button variant="success"
          className="btn btn-sm btn-success float-right"
          onClick={gotoPatientSubstanceUse}>Back</Button>
      </div>
      <div className="scroll-to-top">
        {isVisible &&
          <div onClick={scrollToTop}>
            <CButton type="button" color="dark"><FontAwesomeIcon icon={faAngleDoubleUp} alt='Go to top' /></CButton>
          </div>}
      </div>
    </>
  );
}