import React, { useEffect, useState, useRef, useContext } from 'react'
import { CCol, CRow } from '@coreui/react';
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../form-components/SingleTextInput';
import RadioButtonList from '../form-components/RadioButtonList';
import * as appConstants from '../../AppConstants';

const StepOne = (props) => {

    return (
        <>
            <form className="form-horizontal">
                <div className="body-container" >
                    <CRow className="formMargins">
                        <CCol xs="12" md="4" lg="4">
                            <SingleTextInput
                                name="first_name"
                                title="First Name"
                                inputType="text"
                                content={(props.obj.first_name) ? (props.obj.first_name) : ''}
                                placeholder="First Name"
                                manadatorySymbol={true}
                                // controlBlurFunc={handleBlurChange}
                                // controlFunc={handleChange}
                                controlFunc={props.controlFunc}
                                variant="outlined" />
                            {props.validators.current.message('first_name', props.obj.first_name, 'required|alpha_space')}
                        </CCol>

                        <CCol xs="12" md="2" lg="2">
                            <SingleTextInput name="age"
                                title="Age"
                                inputType="text"
                                content={(props.obj.age) ? (props.obj.age) : ''}
                                placeholder="Age"
                                variant="outlined"
                                manadatorySymbol={true}
                                // controlBlurFunc={handleBlurChange}
                                // controlFunc={handleChange} 
                                controlFunc={props.controlFunc}
                            />
                            {props.validators.current.message('age', props.obj.age, 'required')}
                        </CCol>
                        <CCol xs="12" md="6" lg="6">
                            <label className="labelstyle">Gender<span className="required">*</span></label>
                            <RadioButtonList
                                name="gender"
                                // controlBlurFunc={handleBlurChange}
                                // controlFunc={handleChange}
                                controlFunc={props.controlFunc}
                                options={appConstants.GENDER_OPTIONS}
                                content={props.obj.gender}
                            // manadatorySymbol={true}
                            />
                            {props.validators.current.message('gender', props.obj.gender, 'required')}
                        </CCol>
                        <br></br>
                    </CRow>
                    {/* 
                        <Button label="Cancel" color={appConstants.DELETE_BTN_COLOR} handleClick={gotoPatientList}></Button>
                        <Button label="Next" color={appConstants.SUBMIT_BTN_COLOR} type="submit" handleClick={props.handleClick}></Button>
                   */}
                </div>
            </form>
        </>
    )
}

export default StepOne;