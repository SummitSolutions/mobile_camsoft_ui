import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCol, CRow } from '@coreui/react';
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../form-components/SingleTextInput';
import NameSection from '../form-components/section-components/NameSection';
import SingleCheckbox from '../form-components/SingleCheckbox';
import RadioButtonList from '../form-components/RadioButtonList';
import SelectBox from '../form-components/SelectBox';
import SimpleReactValidator from 'simple-react-validator';
import PatientService from '../../api/PatientService';
import * as appConstants from './../../AppConstants';
import Button from '../form-components/Button';
import moment from 'moment';
import GlobalState from './../../shared/GlobalState';
import * as momentServices from './../../shared/MomentService';
import * as DateConfigs from './../../shared/configs/DateConfigs';
import KeyboardDatePickers from '../form-components/KeyboardDatePicker';

const CreatePatientComponent = (props) => {

    const [state, setState] = useContext(GlobalState);
    const history = useHistory();
    const params = useParams();
    const [patient, setPatient] = useState({

    });
    const validator = useRef(new SimpleReactValidator());
    const [, forceUpdate] = useState();

    useEffect(() => {
        console.log("Patient Create page loaded. Msg. from UseEffect")

        getPatientById();
        if (params.patient_id) {
            return () => {
                console.log('Clearing Patient Id from State')
                setState(state => ({ ...state, patient: {} }))
            }
        }
    }, []);

    console.log(state);
    const getPatientById = () => {
        if (params.patient_id > 0) {
            PatientService.getPatientById(params.patient_id, state.authHeader).then(resp => {
                if (resp.data.id) {
                    if (resp.data.dob) {
                        resp.data.dob = moment(resp.data.dob).locale('en').format('YYYY-MM-DD');
                    }

                    console.log("Setting Patient Id to state")
                    setState(state => ({ ...state, patient: resp.data }))
                    setPatient(resp.data)
                    console.log("Patient Data retrieved Successfully")
                }
            });
        }
    }

    {/*const saveOrUpdatePatient = (data, event) => {
        if(params.id){
            console.log('Patient data updating..: ', data)
            e.preventDefault();
            PatientService.updatePatient(data, params.id,state.authHeader).then(res =>{
                console.log(res)
                setPatient({});
                history.replace('/patient/list');
            });
        }
        else{                
            console.log('Patient data saving')
            e.preventDefault();
            PatientService.createPatient(data,state.authHeader).then(res =>{
                console.log(res)
                setPatient({});
                history.replace('/patient/list');
            });
        }
    }*/}

    const saveOrUpdatePatient = (data, event) => {
        console.log(data.id)
        if (data.id) {
            console.log('Patient data updating..: ', data)
            PatientService.updatePatient(data, data.id, state.authHeader).then(res => {
                console.log(res)
                setPatient(res.data);
                //history.replace('/patient/list');
            });
        }
        else {
            console.log('Patient data saving')
            PatientService.createPatient(data, state.authHeader).then(res => {
                console.log(res)
                setPatient(res.data);
                //history.replace('/patient/list');
            });
        }
    }

    const handleChange = (event) => {
        const { name, value } = event.target;
        console.log('auto saving function')
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setPatient({ ...patient, [event.target.name]: (event.target.checked) ? (value) : '' });
            patient[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setPatient({ ...patient, [event.target.name]: value });
            patient[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdatePatient(patient, event);
    };

    const handleBlurChange = (event) => {
        console.log("blur is calling")
        saveOrUpdatePatient(patient, event);
    }

    const resetPatient = () => {
        setPatient({});
    }

    const gotoPatientList = () => {
        resetPatient();
        history.replace('/patient/list');

    }
    const handleKeyboardPickerChange = (date) => {
        if (date) {
            let age = momentServices.getAgeFromDOB(date, 'years');
            patient.age = age;
            setPatient({ ...patient, 'age': age });
        }
        setPatient({ ...patient, 'dob': date });
    }


    return (
        <>
            <div className="body-container" >
                <CRow className="formMargins">
                    <CCol xs="12" md="6" lg="6">
                        <SingleTextInput name="centre_id"
                            title="Centre ID"
                            inputType="text"
                            content={(props.obj?.centre_id) ? (props.obj?.centre_id) : ''}
                            placeholder="Centre ID"
                            manadatorySymbol={true}
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            variant="outlined" />
                        {validator.current.message('centre_id', props.obj?.centre_id, 'required')}
                    </CCol>
                    <CCol xs="12" md="6" lg="6">
                        <SingleTextInput name="patient_no"
                            title="Patient No"
                            inputType="text"
                            content={(props.obj?.patient_no) ? (props.obj?.patient_no) : ''}
                            placeholder="Patient No"
                            manadatorySymbol={false}
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            variant="outlined" />
                    </CCol>
                </CRow>
                &nbsp;
                <NameSection obj={patient} validators={validator} controlFunc={props.controlFunc} controlBlurFunc={props.controlBlurFunc} />
                &nbsp;
                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="4">
                        {/* <SingleTextInput name="dob" 
                                    title="DOB" 
                                    inputType="date" 
                                    content={(patient.dob)?(patient.dob):appConstants.PICKER_DEFAULT_DATE} 
                                    placeholder="DOB" 
                                    manadatorySymbol={true} 
                                    controlFunc={props.controlFunc}/>
                                {validator.current.message('dob', patient.dob, 'required')} */}
                        <KeyboardDatePickers
                            name="dob"
                            disableToolbar
                            format="DD/MM/yyyy"
                            content={(props.obj?.dob) ? (props.obj?.dob) : momentServices.currentDate(DateConfigs.DEFAULT_DATE_FORMAT_BACKEND)}
                            manadatorySymbol={false}
                            controlFunc={handleKeyboardPickerChange}
                        />
                    </CCol>

                    {/* <CCol xs="12" md="2" lg="2">
                        <label className="labelstyle">Age</label>
                        <SingleTextInput name="age"
                            title="Age"
                            inputType="text"
                            content={(props.obj?.age) ? (props.obj?.age) : ''}
                            placeholder="Age"
                            variant="outlined"
                            manadatorySymbol={false}
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />
                    </CCol> */}
                    <CCol xs="12" md="6" lg="6">
                        <RadioButtonList
                            name="gender"
                            title="Gender"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            options={appConstants.GENDER_OPTIONS}
                            content={props.obj?.gender}
                        //manadatorySymbol={true} 
                        />
                        {validator.current.message('gender', props.obj?.gender, 'required')}
                    </CCol>
                    <br></br>
                </CRow>
                <CRow className="formMargins" >
                    <CCol xs="12]" md="4" lg="12">
                        <label className="labelstyle">Languages known <span className="required">*</span></label>
                    </CCol>
                </CRow>
                <CRow className="formMargins" >
                    <CCol xs="6" md="2" lg="2">
                        <SingleCheckbox
                            controlFunc={props.controlFunc}
                            name='kannada'
                            title='Kannada '
                            type="checkbox"
                            value='kannada'
                            content={props.obj?.kannada}
                        />
                    </CCol>
                    <CCol xs="6" md="2" lg="2">
                        <SingleCheckbox
                            controlFunc={props.controlFunc}
                            name='english'
                            title='English '
                            type="checkbox"
                            value='english'
                            content={props.obj?.english}
                        />
                    </CCol>
                    <CCol xs="6" md="2" lg="2">
                        <SingleCheckbox
                            controlFunc={props.controlFunc}
                            name='hindi'
                            title='Hindi '
                            type="checkbox"
                            value='hindi'
                            content={props.obj?.hindi}
                        />
                    </CCol>
                    <CCol xs="6" md="2" lg="2">
                        <SingleCheckbox
                            controlFunc={props.controlFunc}
                            name='telugu'
                            title='Telugu'
                            type="checkbox"
                            value='telugu'
                            content={props.obj?.telugu}
                        />
                    </CCol>
                    <CCol xs="12" md="2" lg="2">
                        <SingleCheckbox
                            controlFunc={props.controlFunc}
                            name='others'
                            title='Others'
                            type="checkbox"
                            value='others'
                            content={props.obj?.others}
                        />
                    </CCol>
                </CRow>
                &nbsp;
                {props.obj?.others &&
                    <CRow className="formMargins" >
                        <CCol xs="12" md="5" lg="5">
                            <SingleTextInput name="other_language"
                                title="Other language"
                                inputType="text"
                                content={(props.obj?.other_language) ? (props.obj?.other_language) : ''}
                                placeholder="Other language"
                                variant="outlined"
                                manadatorySymbol={false}
                                controlBlurFunc={props.controlBlurFunc}
                                controlFunc={props.controlFunc}
                            />
                        </CCol>
                    </CRow>}
                {/*(patientParms.selectedLanguages.includes('English')) && <CCol xs="12" md="4" lg="4">                                
                                <CheckboxList 
                                    name="marital_status"
                                    title="Marital Status"
                                    controlFunc={checkboxChangeHandler}
                                    options={ appConstants.MARITAL_STATUS }
                                    selectedOptions={ patientParms.selectedMaritalStatus }
                                    manadatorySymbol={true} />
                                {validator.current.message('marital_status', props.obj?.marital_status, 'required')}
                            </CCol>
                            <CCol xs="12" md="4" lg="4"> 
                                <SelectBox
                                    name="marital_status"
                                    title="Marital Status"
                                    placeholder='Marital Status'
                                    controlFunc={props.controlFunc}
                                    options={appConstants.MARITAL_STATUS}
                                    selectedOption={props.obj?.marital_status}
                                    manadatorySymbol={false} />
                            </CCol>*/}

                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="4">
                        <SingleTextInput name="aadhar_number"
                            title="Aadhar Number"
                            inputType="text"
                            content={(props.obj?.aadhar_number) ? (props.obj?.aadhar_number) : ''}
                            placeholder="Aadhar Number"
                            variant="outlined"
                            manadatorySymbol={true}
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />
                        {validator.current.message('aadhar_number', props.obj?.aadhar_number, 'required|numeric|max:12')}
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SelectBox
                            name="economic_status"
                            title=" Economic Status"
                            placeholder='Economic Status'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            options={appConstants.ECONOMIC_STATUS}
                            selectedOption={props.obj?.economic_status ? props.obj?.economic_status : ''}
                            manadatorySymbol={false}
                            variant="outlined" />
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SingleTextInput name="monthly_income"
                            title="Monthly Personal Income"
                            inputType="text"
                            content={(props.obj?.monthly_income) ? (props.obj?.monthly_income) : ''}
                            placeholder="Monthly Personal Income"
                            variant="outlined"
                            manadatorySymbol={false}
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />

                    </CCol>
                </CRow>
                &nbsp;
                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="4">
                        <SelectBox
                            name="educational_status"
                            title=" Educational Status"
                            placeholder='Educational Status'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            options={appConstants.EDUCATIONAL_STATUS}
                            selectedOption={props.obj?.educational_status ? props.obj?.educational_status : ''}
                            manadatorySymbol={false} />
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SingleTextInput name="years"
                            title="Years"
                            inputType="text"
                            content={(props.obj?.years) ? (props.obj?.years) : ''}
                            placeholder="Years"
                            variant="outlined"
                            manadatorySymbol={false}
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />

                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SelectBox
                            name="referral"
                            title="Referral "
                            placeholder='Referral'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            options={appConstants.REFERRAL}
                            selectedOption={props.obj?.referral ? props.obj?.referral : ''}
                            manadatorySymbol={false} />
                    </CCol>
                </CRow>
                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="4">
                        <SelectBox
                            name="occupation"
                            title="Occupation"
                            placeholder='Occupation'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            options={appConstants.OCCUPATION}
                            selectedOption={props.obj?.occupation ? props.obj?.occupation : ''}
                            manadatorySymbol={false} />
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SelectBox
                            name="marital_status"
                            title=" Marital Status"
                            placeholder=' Marital Status'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            options={appConstants.MARITAL_STATUS}
                            selectedOption={props.obj?.marital_status ? props.obj?.marital_status : ''}
                            manadatorySymbol={false} />
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SelectBox
                            name="employment_status"
                            title="Employment Status"
                            placeholder=' Employment Status'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            options={appConstants.EMPLOYMENT_STATUS}
                            selectedOption={props.obj?.employment_status ? props.obj?.employment_status : ''}
                            manadatorySymbol={false} />
                    </CCol>
                </CRow>
                &nbsp;
                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="4">
                        <SelectBox
                            name="living_arrangement"
                            title="Living Arrangement"
                            placeholder='Living Arrangement'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            options={appConstants.LIVING_ARRANGEMENT}
                            selectedOption={props.obj?.living_arrangement ? props.obj?.living_arrangement : ''}
                            manadatorySymbol={false} />
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <SelectBox
                            name="religion"
                            title="Religion"
                            placeholder='Religion'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            options={appConstants.RELIGION}
                            selectedOption={props.obj?.religion ? props.obj?.religion : ''}
                            manadatorySymbol={false} />
                    </CCol>
                    <CCol xs="12" md="3" lg="4">
                        <SingleTextInput name="subgroup"
                            title="Subgroup"
                            inputType="text"
                            content={(props.obj?.subgroup) ? (props.obj?.subgroup) : ''}
                            placeholder="Subgroup"
                            manadatorySymbol={false}
                            variant="outlined"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />
                    </CCol>
                    {/*<CCol xs="12" md="3" lg="3">                                        
                                    <SingleTextInput name="fdr" 
                                        title="FDR#" 
                                        inputType="text" 
                                        content={(props.obj?.fdr)?(props.obj?.fdr):''} 
                                        placeholder="FDR#" 
                                        manadatorySymbol={false} 
                                        controlFunc={props.controlFunc}/>           
                                </CCol>  
                                <CCol xs="8" md="3" lg="3">
                                    <SingleTextInput name="fdr_valid_through" 
                                    title="FDR Valid Through" 
                                    inputType="date" 
                                    content={(props.obj?.dob)?(props.obj?.dob):appConstants.VALID_DATE} 
                                    placeholder="FDR Valid Through" 
                                    manadatorySymbol={false} 
                                    controlFunc={props.controlFunc}/>
                                 </CCol> */}
                </CRow>

                <CRow className="formMargins">
                    <CCol xs="12" md="6" lg="6">
                        <SingleTextInput name="address"
                            title="Address"
                            inputType="text"
                            content={(props.obj?.address) ? (props.obj?.address) : ''}
                            placeholder="Address"
                            manadatorySymbol={false}
                            variant="outlined"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            variant="outlined" />
                    </CCol>
                    <CCol xs="12" md="6" lg="6">
                        <SingleTextInput name="city_or_village"
                            title="City / Village"
                            inputType="text"
                            content={(props.obj?.city_or_village) ? (props.obj?.city_or_village) : ''}
                            placeholder="City / Village"
                            manadatorySymbol={false}
                            variant="outlined"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />
                    </CCol>
                    <CCol xs="12" md="6" lg="6">
                        <SingleTextInput name="landmark"
                            title="Landmark"
                            inputType="text"
                            content={(props.obj?.landmark) ? (props.obj?.landmark) : ''}
                            placeholder="Landmark"
                            manadatorySymbol={false}
                            variant="outlined"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />
                    </CCol>
                    <CCol xs="12" md="6" lg="6">
                        <SelectBox
                            name="country"
                            title="Country"
                            placeholder='Country'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            options={appConstants.COUNTRIES}
                            selectedOption={props.obj?.country ? props.obj?.country : ''}
                            manadatorySymbol={false} />
                    </CCol>
                    <CCol xs="12" md="6" lg="6">
                        {props.obj.country == 'India' &&
                            <SelectBox
                                name="state"
                                title="State"
                                placeholder='State'
                                controlBlurFunc={props.controlBlurFunc}
                                controlFunc={props.controlFunc}
                                options={(props.obj?.country) ? appConstants.STATES[props.obj?.country] : appConstants.STATES['India']}
                                selectedOption={props.obj?.state ? props.obj?.state : ''}
                                manadatorySymbol={false} />}
                        {props.obj?.country != 'India' &&
                            <SingleTextInput name="state"
                                title="State"
                                inputType="text"
                                content={(props.obj?.state) ? (props.obj?.state) : ''}
                                placeholder="State"
                                manadatorySymbol={false}
                                variant="outlined"
                                controlBlurFunc={props.controlBlurFunc}
                                controlFunc={props.controlFunc} />}
                    </CCol>
                    <CCol xs="12" md="6" lg="6">
                        <SingleTextInput name="email"
                            title="Email"
                            inputType="email"
                            content={(props.obj?.email) ? (props.obj?.email) : ''}
                            placeholder="Email"
                            manadatorySymbol={false}
                            variant="outlined"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />
                        {validator.current.message('email', props.obj?.email, 'email')}
                    </CCol>
                    <CCol xs="12" md="6" lg="6">
                        <SingleTextInput name="primary_contact_number"
                            title="Primary Contact Number"
                            inputType="text"
                            content={(props.obj?.primary_contact_number) ? (props.obj?.primary_contact_number) : ''}
                            placeholder="Primary Contact Number"
                            manadatorySymbol={true}
                            variant="outlined"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />
                        {validator.current.message('primary_contact_number', props.obj?.primary_contact_number, 'required|numeric|phone')}
                    </CCol>
                    <CCol xs="12" md="6" lg="6">
                        <SingleTextInput name="secondary_contact_number"
                            title="Secondary Contact Number"
                            inputType="text"
                            content={(props.obj?.secondary_contact_number) ? (props.obj?.secondary_contact_number) : ''}
                            placeholder="Secondary Contact Number"
                            manadatorySymbol={false}
                            variant="outlined"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />
                        {validator.current.message('primary_contact_number', props.obj?.primary_contact_number, 'numeric|phone')}
                    </CCol>

                </CRow>


                {/* <CCardBody>   
                        <CRow className="formMargins">
                            <CCol xs="10" md="4" lg="4">
                                <SingleTextInput name="alternate_address" 
                                    title="Alternate Address" 
                                    inputType="text" 
                                    content={(props.obj?.alternate_address)?(props.obj?.alternate_address):''} 
                                    placeholder="Alternate Address" 
                                    manadatorySymbol={false} 
                                    controlFunc={props.controlFunc}
                                    multiline={true}
                                    rows={6}
                                    variant = "outlined"/>
                            </CCol>
                            <CCol xs="12" md="8" lg="8">
                                <CRow>
                                    <CCol xs="10" md="6" lg="6">
                                        <SingleTextInput name="alternate_city_or_village" 
                                            title="City / Village" 
                                            inputType="text" 
                                            content={(props.obj?.alternate_city_or_village)?(props.obj?.alternate_city_or_village):''} 
                                            placeholder="City / Village" 
                                            manadatorySymbol={false} 
                                            controlFunc={props.controlFunc}/>
                                    </CCol>
                                    <CCol xs="10" md="6" lg="6">
                                        <SingleTextInput name="alternate_landmark" 
                                            title="Landmark" 
                                            inputType="text" 
                                            content={(props.obj?.alternate_landmark)?(props.obj?.alternate_landmark):''} 
                                            placeholder="Landmark" 
                                            manadatorySymbol={false} 
                                            controlFunc={props.controlFunc}/>
                                    </CCol>
                                    <CCol xs="10" md="6" lg="6">           
                                        <SelectBox
                                            name="alternate_country"
                                            title="Country"
                                            placeholder='Country'
                                            controlFunc={props.controlFunc}
                                            options={appConstants.ALTERNATECOUNTRIES}
                                            selectedOption={props.obj?.alternate_country?props.obj?.alternate_country:''}
                                            manadatorySymbol={false} />
                                    </CCol>
                                    <CCol xs="10" md="6" lg="6">           
                                        {props.obj?.alternate_country == 'India' && <SelectBox
                                            name="alternate_state"
                                            title="State"
                                            placeholder='State'
                                            controlFunc={props.controlFunc}
                                            options={(props.obj?.alternate_country) ? appConstants.ALTERNATESTATES[props.obj?.alternate_country] : appConstants.STATES['India']}
                                            selectedOption={props.obj?.alternate_state?props.obj?.alternate_state:''}
                                            manadatorySymbol={false} />}
                                        {props.obj?.alternate_country != 'India' && 
                                            <SingleTextInput name="alternate_state" 
                                                title="State" 
                                                inputType="text" 
                                                content={(props.obj?.alternate_state)?(props.obj?.alternate_state):''} 
                                                placeholder="State" 
                                                manadatorySymbol={false} 
                                                controlFunc={props.controlFunc}/>}
                                    </CCol>
                                </CRow>
							</CCol>
									<CCol xs="10" md="4" lg="4">                                        
										<SingleTextInput name="alternate_contact_number" 
											title="Alternate Contact Number" 
											inputType="text" 
											content={(props.obj?.alternate_contact_number)?(props.obj?.alternate_contact_number):''} 
											placeholder="Alternate Contact Number" 
											manadatorySymbol={false} 
											controlFunc={props.controlFunc}/>		
									</CCol>
								    <CCol xs="10" md="4" lg="4">                                        
                                        <SingleTextInput name="email" 
                                            title="Alternate Email" 
                                            inputType="email" 
                                            content={(props.obj?.email)?(props.obj?.email):''} 
                                            placeholder="Alternate Email" 
                                            manadatorySymbol={false} 
                                            controlFunc={props.controlFunc}/>
									</CCol>
							</CRow>
                   </CCardBody> */}


                <CRow className="formMargins">
                    <CCol xs="12" md="4" lg="4">
                        <label className="labelstyle">Informant Name</label>
                        <SingleTextInput name="informant_name"
                            title="Informant Name"
                            inputType="text"
                            content={(props.obj?.informant_name) ? (props.obj?.informant_name) : ''}
                            placeholder="Informant Name"
                            manadatorySymbol={false}
                            variant="outlined"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <label className="labelstyle">Relation</label>
                        <SingleTextInput name="relation"
                            title="Relation"
                            inputType="text"
                            content={(props.obj?.relation) ? (props.obj?.relation) : ''}
                            placeholder="Relation"
                            manadatorySymbol={false}
                            variant="outlined"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />
                    </CCol>
                    <CCol xs="12" md="4" lg="4">
                        <label className="labelstyle">Contact Number</label>
                        <SingleTextInput name="contact_number"
                            title="Contact Number"
                            inputType="text"
                            content={(props.obj?.contact_number) ? (props.obj?.contact_number) : ''}
                            placeholder="Contact Number"
                            manadatorySymbol={false}
                            variant="outlined"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc} />
                    </CCol>
                </CRow>
                &nbsp;
                <CRow className="formMargins">
                    <CCol xs="12" md="6" lg="6">
                        <label className="labelstyle">Informant Address</label>
                        <SingleTextInput name="informant_address"
                            title="Informant Address"
                            inputType="text"
                            content={(props.obj?.informant_address) ? (props.obj?.informant_address) : ''}
                            placeholder="Informant Address"
                            manadatorySymbol={false}
                            variant="outlined"
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            variant="outlined"
                            multiline={true}
                            rows={5} />
                    </CCol>
                </CRow>

                <CRow className="formMargins">
                    {/* <CCol xs="10" md="3" lg="3">                                        
                                    <SingleTextInput name="emergency_contact_name" 
                                        title="Emergency Contact Name" 
                                        inputType="text" 
                                        content={(props.obj?.emergency_contact_name)?(props.obj?.emergency_contact_name):''} 
                                        placeholder="Emergency Contact Name" 
                                        manadatorySymbol={false} 
                                        controlFunc={props.controlFunc}/>
                                </CCol>
                                <CCol xs="10" md="3" lg="3">                                        
                                    <SingleTextInput name="relation" 
                                        title="Relation" 
                                        inputType="text" 
                                        content={(props.obj?.relation)?(props.obj?.relation):''} 
                                        placeholder="Relation" 
                                        manadatorySymbol={false} 
                                        controlFunc={props.controlFunc}/>
                                </CCol>
                                <CCol xs="10" md="3" lg="3">                                        
                                    <SingleTextInput name="emergency_contact_no" 
                                        title="Emergency Contact No" 
                                        inputType="text" 
                                        content={(props.obj?.emergency_contact_no)?(props.obj?.emergency_contact_no):''} 
                                        placeholder="Emergency Contact No" 
                                        manadatorySymbol={false} 
                                        controlFunc={props.controlFunc}/>
                                </CCol> 
                                <CCol xs="10" md="4" lg="3">
                                        <SingleTextInput name="emergency_address" 
                                        title="Emergency Address" 
                                        inputType="text" 
                                        content={(props.obj?.emergency_address)?(props.obj?.emergency_address):''} 
                                        placeholder="Emergency Address" 
                                        manadatorySymbol={false} 
                                        controlFunc={props.controlFunc}
                                        multiline={true}
                                rows={6}/> 
                                </CCol>*/}
                </CRow>
                &nbsp;
                <CRow className="formMargins">
                    <CCol xs="12" md="12" lg="12">

                        <label className="labelstyle">Consent for reminders</label>
                    </CCol>
                </CRow>
                <CRow className="formMargins">
                    <CCol xs="12" md="2" lg="2">
                        <SingleCheckbox
                            controlFunc={props.controlFunc}
                            name='sms'
                            title='SMS'
                            type="checkbox"
                            value='sms'
                            content={props.obj?.sms}
                        />
                    </CCol>
                    <CCol xs="12" md="2" lg="2">
                        <SingleCheckbox
                            controlFunc={props.controlFunc}
                            name='email'
                            title='E-mail'
                            type="checkbox"
                            value='email'
                            content={props.obj?.email}
                        />
                    </CCol>
                    <CCol xs="12" md="3" lg="3">
                        <SingleCheckbox
                            controlFunc={props.controlFunc}
                            name='phone_call'
                            title='Phone call'
                            type="checkbox"
                            value='phone_call'
                            content={props.obj?.phone_call}
                        />
                    </CCol>
                </CRow >
                <br></br>

            </div>
        </>
    )
}
export default CreatePatientComponent;