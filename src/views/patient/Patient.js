import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../form-components/SingleTextInput';
import RadioButtonList from '../form-components/RadioButtonList';
import SimpleReactValidator from 'simple-react-validator';
import PatientService from '../../api/PatientService';
import * as appConstants from './../../AppConstants';
import Button from '../form-components/Button';
import moment from 'moment';
import GlobalState from './../../shared/GlobalState';
import * as momentServices from './../../shared/MomentService';
import * as DateConfigs from './../../shared/configs/DateConfigs';
import KeyboardDatePickers from '../form-components/KeyboardDatePicker';
import StepOne from './StepOne';
import CreatePatientComponent from './Create';


const NewPatientComponent = (props) => {
	const [state, setState] = useContext(GlobalState);
	const history = useHistory();
	const params = useParams();
	const [patient, setPatient] = useState({

	});
	const [currentPage, setCurrentPage] = useState('FirstPage');
	const validator = useRef(new SimpleReactValidator());
	const [, forceUpdate] = useState();
	const [flag, setFlag] = useState(false);

	useEffect(() => {
		console.log("Patient Create page loaded. Msg. from UseEffect")

		getPatientById();
		if (params.patient_id) {
			return () => {
				console.log('Clearing Patient Id from State')
				setState(state => ({ ...state, patient: {} }))
			}
		}
	}, []);

	console.log(state);
	const getPatientById = () => {
		if (params.patient_id > 0) {
			PatientService.getPatientById(params.patient_id, state.authHeader).then(resp => {
				if (resp.data.id) {
					if (resp.data.dob) {
						resp.data.dob = moment(resp.data.dob).locale('en').format('YYYY-MM-DD');
					}

					console.log("Setting Patient Id to state")
					setState(state => ({ ...state, patient: resp.data }))
					setPatient(resp.data)
					console.log("Patient Data retrieved Successfully")
				}
			});
		}
	}

	{/*const saveOrUpdatePatient = (data, event) => {
        if(params.id){
            console.log('Patient data updating..: ', data)
            e.preventDefault();
            PatientService.updatePatient(data, params.id,state.authHeader).then(res =>{
                console.log(res)
                setPatient({});
                history.replace('/patient/list');
            });
        }
        else{                
            console.log('Patient data saving')
            e.preventDefault();
            PatientService.createPatient(data,state.authHeader).then(res =>{
                console.log(res)
                setPatient({});
                history.replace('/patient/list');
            });
        }
    }*/}

	const saveOrUpdatePatient = (data, page = null) => {
		console.log(data.id)
		if (data.id) {
			console.log('Patient data updating..: ', data)
			PatientService.updatePatient(data, data.id, state.authHeader).then(res => {
				console.log(res)
				setPatient(res.data);
				//history.replace('/patient/list');
			});
		}
		else {
			console.log('Patient data saving')
			PatientService.createPatient(data, state.authHeader).then(res => {
				console.log(res)
				setPatient(res.data);
				if (page) {
					setCurrentPage(page);
				}
			});
		}
	}

	const handleChange = (event) => {
		const { name, value } = event.target;
		console.log('auto saving function')
		if (event.target.type == 'checkbox') {
			console.log('checkbox')
			setPatient({ ...patient, [event.target.name]: (event.target.checked) ? (value) : '' });
			patient[event.target.name] = (event.target.checked) ? (value) : '';
		}
		else {
			console.log(event.target.name)
			setPatient({ ...patient, [event.target.name]: value });
			patient[event.target.name] = value;
		}
		if (event.target.type != 'text')
			saveOrUpdatePatient(patient, event);
	};

	const handleBlurChange = (event) => {
		console.log("blur is calling")
		saveOrUpdatePatient(patient, event);
	}

	const resetPatient = () => {
		setPatient({});
	}

	const gotoPatientList = () => {
		resetPatient();
		history.replace('/patient/list');

	}
	const handleKeyboardPickerChange = (date) => {
		if (date) {
			let age = momentServices.getAgeFromDOB(date, 'years');
			patient.age = age;
			setPatient({ ...patient, 'age': age });
		}
		setPatient({ ...patient, 'dob': date });
	}

	const handleSaveAndNext = (event) => {
		validateForm(event, 'SecondPage')
	}

	const validateForm = (event, page = null) => {
		console.log(patient)
		event.preventDefault();
		if (validator.current.allValid()) {
			console.log('Valid')
			if (page)
				saveOrUpdatePatient(patient, page);
			else
				saveOrUpdatePatient(patient);
		} else {
			console.log('Not Valid')
			validator.current.showMessages();
			forceUpdate(1)
		}
	}

	const handleChangeMandatory = (event) => {
		if (event.target.name == 'age') {
			let dob = momentServices.getDOBFromAge(event.target.value, 'years');
			patient.dob = dob;
			setPatient({ ...patient, 'dob': dob });
		}
		if (event.target.name == 'first_name' || event.target.name == 'age' || event.target.name == 'gender') {
			if (event.target.name == 'first_name') {
				if (validator.current.check(event.target.value, 'alpha_space')) {
					setPatient({ ...patient, [event.target.name]: event.target.value });
				}
			}
			if (event.target.name == 'age') {
				if (validator.current.check(event.target.value, 'numeric')) {
					setPatient({ ...patient, [event.target.name]: event.target.value });
				}
			}
			else {
				setPatient({ ...patient, [event.target.name]: event.target.value });
			}
		}
	}

	return (
		<>

			{(currentPage === 'FirstPage') && <StepOne obj={patient} controlFunc={handleChangeMandatory} validators={validator} />}
			{(currentPage === 'SecondPage') && <CreatePatientComponent obj={patient} controlFunc={handleChange} controlBlurFunc={handleBlurChange} validators={validator} />}
			<br></br>
			<Button label="Next" type="submit" color={appConstants.SUBMIT_BTN_COLOR} className="nextbtn" type="submit" handleClick={handleSaveAndNext} style={{ float: 'right' }}></Button>
			<Button label="Cancel" color={appConstants.DELETE_BTN_COLOR} handleClick={gotoPatientList}></Button>
		</>
	)


}
export default NewPatientComponent;