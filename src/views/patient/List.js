import React, { useContext, useEffect, useState } from 'react';
import PatientService from '../../api/PatientService';
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CButtonGroup,
    CLink,
    CButton
} from '@coreui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus, faAd } from '@fortawesome/free-solid-svg-icons';
import * as appConstants from './../../AppConstants';
import GlobalState from './../../shared/GlobalState';
import { useHistory } from 'react-router-dom';
import { Modal, Button } from "react-bootstrap";
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import CreatePatientComponent from '../patient/Create.js';
import NewPatientComponent from '../patient/Patient';
import { AddButtonWithTitle } from './../form-components/AddButtonWithTitle';

const fields = [
    "first_name",
    "primary_contact_number",
    "age",
    {
        key: 'edit_patient',
        label: 'Action',
        _style: { width: '18%' },
        sorter: false,
        filter: false
    }
];

const PatientList = () => {

    const [state, setState] = useContext(GlobalState);
    const [patientList, setPatientList] = useState({ data: null });
    const history = useHistory();
    const [showModal, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    useEffect(() => {
        console.log("Patient List page loading")
        console.log(state)
        PatientService.getPatients(state.authHeader).then((res) => {
            setPatientList({ data: res.data });
        });
    }, []);

    const editPatient = (patient) => {
        history.push('/patient/update/' + patient.id);
    }

    const gotoPatientVisit = (patient) => {
        console.log(patient)
        setState(state => ({ ...state, patient: patient }));
        history.push('/patient-visit/list/' + patient.id);
    }

    const gotoCreatePatient = () => {
        history.push('/patient/newpatient');
    }

    const loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

    return (
        <>

            <AddButtonWithTitle
                title='ADD NEW PATIENTS'
                addNewEntityOnClick={gotoCreatePatient}
            />
            {/* <Modal show={showModal} onHide={handleClose}>

                <Modal.Header closeButton><h2>New Patient</h2>
                </Modal.Header>
                <Modal.Body> <CreatePatientComponent /> </Modal.Body>
            </Modal> */}
            <div className="body-container">
                <CDataTable
                    items={patientList.data}
                    fields={fields}
                    //itemsPerPage={appConstants.ITEMS_PER_PAGE}
                    pagination
                    // tableFilter
                    // itemsPerPageSelect
                    hover
                    sorter
                    scopedSlots={{
                        'edit_patient':
                            (item, index) => {
                                return (
                                    <td className="py-2">
                                        <CButtonGroup>
                                            <CButton
                                                color={appConstants.SAVE_BTN_COLOR}
                                                size="sm"
                                                title="Edit Patient"
                                                onClick={() => { editPatient(item) }}
                                            >
                                                <FontAwesomeIcon icon={faPencilAlt} />
                                            </CButton>
                                            <CButton
                                                color={appConstants.CANCEL_BTN_COLOR}
                                                size="sm"
                                                title="Go to Patient Visit List"
                                                onClick={() => { gotoPatientVisit(item) }}
                                            >
                                                Visit List
                                            </CButton>
                                        </CButtonGroup>
                                    </td>
                                )
                            },
                    }
                    }
                />
            </div>
        </>
    );
}

export default PatientList;

/*
                            'last_visit_date' :
                            (item, index)=>{
                                return (
                                    <td className="py-2">
                                        {momentService.formatDate(item.last_visit_date, 'DD-MMM-YYYY') }
                                    </td>
                                    )
                                },
                                */