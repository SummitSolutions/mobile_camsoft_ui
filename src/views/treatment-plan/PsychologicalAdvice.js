import React, { useEffect, useState, useRef, useContext } from 'react'
import {
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CButton
} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import SingleTextInput from '../form-components/SingleTextInput';
import SelectBox from '../form-components/SelectBox';
import * as appConstants from '../../AppConstants';
import Button from 'react-bootstrap/Button';
import SingleCheckbox from '../form-components/SingleCheckbox';
import { useParams } from "react-router";
import GlobalState from '../../shared/GlobalState';
import PsychotherapyService from 'src/api/PsychotherapyService';

const PschologicalAdvice = () => {
    const params = useParams();
    const [state, setState] = useContext(GlobalState);
    const [psychotherapy, setPsychotherapy] = useState({

    });

    useEffect(() => {
        getpyschologicalAdvice();
    }, []);

    const handleChange = (event) => {
        const { name, value } = event.target;
        console.log('auto saving function')
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setPsychotherapy({ ...psychotherapy, [event.target.name]: (event.target.checked) ? (value) : '' });
            psychotherapy[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setPsychotherapy({ ...psychotherapy, [event.target.name]: value });
            psychotherapy[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdatepyschologicalAdvice(psychotherapy, event);
    };

    const handleBlurChange = (event) => {
        console.log("blur is calling")
        saveOrUpdatepyschologicalAdvice(psychotherapy, event);
    }

    const getpyschologicalAdvice = () => {
        PsychotherapyService.getAllPsychotherapys(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            console.log("get is working")
            setPsychotherapy(resp.data);
        });
    }

    const saveOrUpdatepyschologicalAdvice = (data, event) => {
        console.log(data)
        if (data.id) {
            console.log('psychotherapy data updating..: ',)
            let id = (params.patient_id) ? params.id : data.id
            PsychotherapyService.updatePsychotherapy(data.id, data, state.authHeader).then(res => {
                console.log(res)
                setPsychotherapy(res.data);
            });
        }
        else {
            console.log('psychotherapy saving')
            console.log(data)
            data.patient_id = parseInt(params.patient_id);
            data.visit_id = parseInt(params.visit_id);
            PsychotherapyService.createPsychotherapy(data, state.authHeader).then(res => {
                console.log("here")
                console.log(res.data)
                setPsychotherapy(res.data);
            });
        }
    }

    return (
        <>
            <CCard borderColor="primary">
                <CCardBody>
                    <CRow >
                        <CCol xs="8" md="8" lg="8">
                            <label>Counselling</label>
                        </CCol>
                        <CCol xs="4" md="4" lg="4">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='counselling'
                                title=' '
                                type="checkbox"
                                value='counselling'
                                content={psychotherapy.counselling}
                            />
                        </CCol>
                    </CRow>
                    <CRow>
                        <CCol xs="8" md="8" lg="8">
                            <label>Psychoeducation</label>
                        </CCol>
                        <CCol xs="4" md="4" lg="4">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='psychoeducation'
                                title=' '
                                type="checkbox"
                                value='psychoeducation'
                                content={psychotherapy.psychoeducation}
                            />
                        </CCol>
                    </CRow>
                    <CRow>
                        <CCol xs="8" md="8" lg="8">
                            <label>Family Therapy</label>
                        </CCol>
                        <CCol xs="4" md="4" lg="4">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='family_therapy'
                                title=' '
                                type="checkbox"
                                value='family therapy'
                                content={psychotherapy.family_therapy}
                            />
                        </CCol>
                    </CRow>
                    <CRow>
                        <CCol xs="8" md="8" lg="8">
                            <label>Individual Psychotherapy</label>
                        </CCol>
                        <CCol xs="4" md="4" lg="4">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='individual_psychotherapy'
                                title=' '
                                type="checkbox"
                                value='individual psychotherapy'
                                content={psychotherapy.individual_psychotherapy}
                            />
                        </CCol>
                    </CRow>
                    <CRow>
                        <CCol xs="8" md="8" lg="8">
                            <label>Tobacco Cessation Counselling</label>
                        </CCol>
                        <CCol xs="4" md="4" lg="4">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='tobacco_cessation_counselling'
                                title=' '
                                type="checkbox"
                                value='Tobacco cessation counselling'
                                content={psychotherapy.tobacco_cessation_counselling}
                            />
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
        </>
    )
}
export default PschologicalAdvice;