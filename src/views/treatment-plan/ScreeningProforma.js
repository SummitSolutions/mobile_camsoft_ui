import React, { useEffect, useState, useRef, useContext } from 'react'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CButton
} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import { useParams} from "react-router";
import GlobalState from '../../shared/GlobalState';
import {  CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
//import Select from 'react-select';
//import makeAnimated from 'react-select/animated';
import SingleTextInput from '../form-components/SingleTextInput';
import SubstanceUseService from 'src/api/SubstanceUseService';
import SelectBox from '../form-components/SelectBox'; 
import * as appConstants from '../../AppConstants';
import Button from 'react-bootstrap/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import SingleCheckbox from '../form-components/SingleCheckbox';
import RadioButtonList from '../form-components/RadioButtonList';
import ScreeningProformaService from 'src/api/ScreeningProformaService';

const ScreeningProforma = () => {

    const [screeningProforma, setScreeningProforma] = useState({ 
       
  });
    const [substanceUseList, setSubstanceUseList] = useState([]);
    const params = useParams();
    const [state, setState] = useContext(GlobalState);
    const [menuItemList, setMenuItemList] = useState([]); 

    useEffect(() => {
     getAllSubstanceUse();
     getScreeningProforma();
    console.log("use effect")
    }, []); 

    

const getAllSubstanceUse = () => {
    SubstanceUseService.getAllSubstanceUse(params.patient_id, params.visit_id, state.authHeader).then((resp) =>{
        setSubstanceUseList(resp.data)
        checkSubstanceExistOrNot(resp.data);
    })
    console.log(substanceUseList.find(substance => substance.substance_id == 1))
}
{/*const checkSubstanceExistOrNot = (substanceName) => {
    let substance = substanceUseList.find(substance => substance.substance_name == substanceName)
    if(substance.substance_use_list.current_use || substance.substance_use_list.ever_use)
      return true;
    return false;
}*/}
const checkSubstanceExistOrNot = (dataList) => {
    let listItems = [];
    dataList.forEach(substance => {
        if(substance.substance_use_list?.current_use || substance.substance_use_list?.ever_use)
        {
            listItems.push(substance);
        }    
    });
    setMenuItemList(listItems);
}
const handleChange = (event) => {
    const {name, value} = event.target;
    console.log('auto saving function')
    if(event.target.type == 'checkbox')
    {
        console.log('checkbox')
        setScreeningProforma({ ...screeningProforma, [event.target.name]: (event.target.checked)?(value) :''});
        screeningProforma[event.target.name] = (event.target.checked) ? (value) :'';
    }
    else
    {
        console.log(event.target.name)
        setScreeningProforma({...screeningProforma, [event.target.name]:value});
        screeningProforma[event.target.name] = value;
    }
    if(event.target.type != 'text')
    saveOrUpdateScreeningProforma(screeningProforma, event);
  };

const handleBlurChange = (event) => {
    console.log("blur is calling")
    saveOrUpdateScreeningProforma(screeningProforma, event);
}
const setFormValues = (data) => {
    console.log(data)
    if(data.id)
    {
        setScreeningProforma(data)  
        console.log("screening retrieved Successfully")
    }
}

const getScreeningProforma = () => {
    ScreeningProformaService.getAllScreeningProformas(params.patient_id, params.visit_id, state.authHeader).then(resp => {
        setScreeningProforma(resp.data);
    });
}

const saveOrUpdateScreeningProforma = (data, event) => {
    console.log(data.id)
    if(data.id){
        console.log('screening data updating..: ', )
        //let id=(params.id) ? params.id : data.id
        ScreeningProformaService.updateScreeningProforma(data.id, data, state.authHeader).then(res =>{ 
            console.log(res)
            setFormValues(res.data);
        });
    }
    else{                
        console.log('screening data saving')
        data.patient_id =  parseInt(params.patient_id);
        data.visit_id =  parseInt(params.visit_id);
        ScreeningProformaService.createScreeningProforma(data,state.authHeader).then(res =>{
            console.log(res.data)
            setFormValues(res.data);
        });
    }
}
return(
        <>
        <CCard accentColor="primary">
        <CCardBody> 
        <label><b><u>Identification marks</u></b></label>
        <CRow>
            <CCol xs="12" md="4" lg="6"> 
                <SingleTextInput name="identification_marks1" 
                    title="Identification Marks" 
                    inputType="text" 
                    content={(screeningProforma.identification_marks1)?(screeningProforma.identification_marks1):''} 
                    placeholder="Identification Marks" 
                    manadatorySymbol={false}
                    controlBlurFunc={handleBlurChange}  
                    controlFunc={handleChange}
                    multiline={true}
                    rows={1}
                    variant = "outlined"/>
            </CCol>
            <CCol xs="12" md="4" lg="6"> 
                <SingleTextInput name="identification_marks2" 
                    title="Identification Marks" 
                    inputType="text" 
                    content={(screeningProforma.identification_marks2)?(screeningProforma.identification_marks2):''} 
                    placeholder="Identification Marks" 
                    manadatorySymbol={false} 
                    controlBlurFunc={handleBlurChange} 
                    controlFunc={handleChange}
                    multiline={true}
                    rows={1}
                    variant = "outlined"/>
            </CCol> 
       </CRow>
       &nbsp;
       <label><b><u>Age at</u></b></label>
       <CRow>
       <TableContainer style = {{width : '500px',overflow:'scroll'}}>
           <Table>
                <TableHead>
                    <TableRow>
                        <TableCell >Substance</TableCell>
                        <TableCell >Age at first Use</TableCell>
                        <TableCell >Age of Dependence</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                { menuItemList?.map( (element) => (
                    <TableRow hover role="checkbox" tabIndex={-1} key={element.substance_name}>
                    <TableCell>{element.substance_name}</TableCell>
                    <TableCell>
                            <SingleTextInput 
                                name="age_firstuse2" 
                                title="Age at first use" 
                                inputType="text" 
                                content={(screeningProforma.age_firstuse2)?(screeningProforma.age_firstuse2):''} 
                                placeholder="Age at first use" 
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange}  
                                controlFunc={handleChange}
                                style ={{width:"50px"}}
                                variant = "outlined"/>
                    </TableCell>
                    <TableCell>
                            <SingleTextInput  
                                   name="age_dependence_caffeine" 
                                title="Age of Dependence " 
                                inputType="text" 
                                content={(screeningProforma.age_dependence_caffeine)?(screeningProforma.age_dependence_caffeine):''} 
                                placeholder="Age of Dependence" 
                                manadatorySymbol={false} 
                                controlBlurFunc={(event) => {handleBlurChange(event, element)}} 
                                controlFunc={(event) => {handleChange(event, element)}}
                                variant = "outlined"/>
                     </TableCell>
                 </TableRow>
                ))}
                </TableBody>
                    </Table>
        </TableContainer>
        </CRow>
        &nbsp;
        <CRow>
            <CCol xs="4" md="1" lg="1">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='physical_complication'
                    title='NA '
                    type="checkbox" 
                    value='physical_complication'
                    content={(screeningProforma.physical_complication)?(screeningProforma.physical_complication):''}
                /> 
            </CCol>
            <CCol xs="6" md="2" lg="2">
                <label>Onset of physical complication  </label>
                <SingleTextInput  
                    name="age_physical_complication" 
                    title="Age in years " 
                    inputType="text" 
                    content={(screeningProforma.age_physical_complication)?(screeningProforma.age_physical_complication):''} 
                    placeholder="Age in years" 
                    manadatorySymbol={false} 
                    controlBlurFunc={handleBlurChange} 
                    controlFunc={handleChange}
                    disabled = {(screeningProforma.physical_complication === 'physical_complication') ? true : false}
                />
            </CCol>
             <CCol xs="4" md="1" lg="1">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='psychiatric_complication'
                    title='NA '
                    type="checkbox" 
                    value='psychiatric_complication'
                    content={(screeningProforma.psychiatric_complication)?(screeningProforma.psychiatric_complication):''}
                /> 
            </CCol>
            <CCol xs="6" md="2" lg="2">
                <label>Onset of Psychiatric complication  </label>
                <SingleTextInput  
                    name="age_psychiatric_complication" 
                    title="Age in years " 
                    inputType="text" 
                    content={(screeningProforma.age_psychiatric_complication)?(screeningProforma.age_psychiatric_complication):''} 
                    placeholder="Age in years" 
                    manadatorySymbol={false}
                    controlBlurFunc={handleBlurChange}  
                    controlFunc={handleChange}
                    disabled = {(screeningProforma.psychiatric_complication === 'psychiatric_complication') ? true : false}
                />
            </CCol>
            <CCol xs="4" md="1" lg="1">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='withdrawl_complication'
                    title='NA '
                    type="checkbox" 
                    value='withdrawl_complication'
                    content={(screeningProforma.withdrawl_complication)?(screeningProforma.withdrawl_complication):''}
                /> 
            </CCol>
            <CCol xs="6" md="2" lg="2">
                <label>Onset of Withdrawl  complication  </label>
                <SingleTextInput  
                    name="age_withdrawl_complication" 
                    title="Age in years " 
                    inputType="text" 
                    content={(screeningProforma.age_withdrawl_complication)?(screeningProforma.age_withdrawl_complication):''} 
                    placeholder="Age in years" 
                    manadatorySymbol={false}
                    controlBlurFunc={handleBlurChange}  
                    controlFunc={handleChange}
                    disabled = {(screeningProforma.withdrawl_complication === 'withdrawl_complication') ? true : false}
                />
            </CCol>
            <CCol xs="7" md="3" lg="3">
                <label>First Treatment Sought</label>
                &nbsp;
                &nbsp;
                <SingleTextInput  
                    name="first_treatment_sought" 
                    title="Age in years" 
                    inputType="text" 
                    content={(screeningProforma.first_treatment_sought)?(screeningProforma.first_treatment_sought):''}
                    placeholder="Age in years" 
                    manadatorySymbol={false}
                    controlBlurFunc={handleBlurChange}  
                    controlFunc={handleChange}
                />
            </CCol>
        </CRow>
        &nbsp;
        <CRow>
        <CCol xs="8" md="5" lg="5">
            <label><b><u>Physical medical illness</u></b></label>
            &nbsp;&nbsp;
            <SingleCheckbox 
                controlFunc={handleChange} 
                name='physical_medical_illness'
                title=''
                type="checkbox" 
                value='physical_medical_illness'
                content={(screeningProforma.physical_medical_illness)?(screeningProforma.physical_medical_illness):''}
                />  
        </CCol>
        </CRow>
        {!(screeningProforma.physical_medical_illness) &&
        <CCard>
        <CRow>
            <CCol xs="12" md="3" lg="3"> 
            <label><b>Hypertension -</b></label>
                <RadioButtonList 
                    name="hypertension"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.NA}
                    content={screeningProforma.hypertension}
                    manadatorySymbol={false}/>
            </CCol> 
            <CCol xs="12" md="3" lg="3"> 
            <label><b>Diabetes -</b></label>
                <RadioButtonList 
                    name="diabities"
                    title=" "
                    controlFunc= {handleChange}
                    options={appConstants.NA}
                    content={screeningProforma.diabities}
                    manadatorySymbol={false}/>
            </CCol> 
            <CCol xs="12" md="3" lg="3"> 
            <label><b>HIV -</b></label>
                <RadioButtonList 
                    name="hiv"
                    title=" "
                    controlBlurFunc={handleBlurChange} 
                    controlFunc= {handleChange}
                    options={appConstants.NA}
                    content={screeningProforma.hiv}
                    manadatorySymbol={false}/>
            </CCol>
            <CCol xs="12" md="3" lg="3"> 
            <label><b>Jaundice -</b></label>
                <RadioButtonList 
                    name="jaundice"
                    title=" "
                    controlBlurFunc={handleBlurChange} 
                    controlFunc= {handleChange}
                    options={appConstants.NA}
                    content={screeningProforma.jaundice}
                    manadatorySymbol={false}/>
            </CCol>
            <CCol xs="6" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='syphilis'
                    title='Syphilis '
                    type="checkbox" 
                    value='syphilis'
                    content={(screeningProforma.syphilis)?(screeningProforma.syphilis):''}
                /> 
             </CCol>
            <CCol xs="6" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='fungal'
                    title='Fungal '
                    type="checkbox" 
                    value='fungal'
                    content={(screeningProforma.fungal)?(screeningProforma.fungal):''}
                /> 
             </CCol>
            <CCol xs="6" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='physical_injuries'
                    title='Physical Injuries '
                    type="checkbox" 
                    value='physical_injuries'
                    content={(screeningProforma.physical_injuries)?(screeningProforma.physical_injuries):''}
                /> 
            </CCol>
            <CCol xs="6" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='head_injury'
                    title='Head Injury'
                    type="checkbox" 
                    value='head_injury'
                    content={(screeningProforma.head_injury)?(screeningProforma.head_injury):''}
                /> 
            </CCol>
           
            <CCol xs="6" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='peripheral_neuritis'
                    title='Peripheral Neuritis '
                    type="checkbox" 
                    value='peripheral_neuritis'
                    content={(screeningProforma.peripheral_neuritis)?(screeningProforma.peripheral_neuritis):''}
                /> 
            </CCol>
            <CCol xs="6" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='hgs_ag'
                    title='HgS Ag'
                    type="checkbox" 
                    value='hgs_ag'
                    content={(screeningProforma.hgs_ag)?(screeningProforma.hgs_ag):''}
                /> 
            </CCol>
           
            <CCol xs="6" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='gastritis'
                    title='Gastritis'
                    type="checkbox" 
                    value='gastritis'
                    content={(screeningProforma.gastritis)?(screeningProforma.gastritis):''}
                /> 
            </CCol>
            <CCol xs="6" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='hepatitis'
                    title='Hepatitis'
                    type="checkbox" 
                    value='hepatitis'
                    content={(screeningProforma.hepatitis)?(screeningProforma.hepatitis):''}
                /> 
            </CCol>
            <CCol xs="8" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='thrompophlebitis'
                    title='Thrompophlebitis'
                    type="checkbox" 
                    value='thrompophlebitis'
                    content={(screeningProforma.thrompophlebitis)?(screeningProforma.thrompophlebitis):''}
                /> 
            </CCol>
            
        </CRow>
        </CCard>}
        &nbsp;
        <CRow>
            <CCol xs="10" md="5" lg="5">
                <label><b><u>Contributing/Suspectibility Factors</u></b></label>
                &nbsp;&nbsp;
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='suspectibility_factors'
                    title=''
                    type="checkbox" 
                    value='suspectibility_factors'
                    content={(screeningProforma.suspectibility_factors)?(screeningProforma.suspectibility_factors):''}
                    />  
            </CCol>
        </CRow>
        {!(screeningProforma.suspectibility_factors) &&
        <CCard>
        <CRow>
            <CCol xs="12" md="3" lg="3">
            <label><b>Impulsivity</b></label>
            </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="impulsivity"
                    title=" "
                    controlBlurFunc={handleBlurChange} 
                    controlFunc= {handleChange}
                    options={appConstants.USUAL}
                    content={screeningProforma.impulsivity}
                    manadatorySymbol={false}/>
                </CCol>
        </CRow>
        <CRow>
            <CCol xs="12" md="3" lg="3">
                <label><b>Inattention</b></label>
            </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="inattention"
                    title=" "
                    controlBlurFunc={handleBlurChange} 
                    controlFunc= {handleChange}
                    options={appConstants.USUAL}
                    content={screeningProforma.inattention}
                    manadatorySymbol={false}/>
                </CCol>
        </CRow>
        <CRow>
            <CCol xs="12" md="3" lg="3">
                <label><b>Hyperactivity</b></label>
            </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="hyperactivity"
                    title=" "
                    controlBlurFunc={handleBlurChange} 
                    controlFunc= {handleChange}
                    options={appConstants.USUAL}
                    content={screeningProforma.hyperactivity}
                    manadatorySymbol={false}/>
                </CCol>
        </CRow>
        <CRow>
            <CCol xs="12" md="3" lg="3">    
                <label><b>Conduct Problems</b></label>
            </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="conduct_problems"
                    title=" "
                    controlBlurFunc={handleBlurChange} 
                    controlFunc= {handleChange}
                    options={appConstants.USUAL}
                    content={screeningProforma.conduct_problems}
                    manadatorySymbol={false}/>
                </CCol>
            
        </CRow>
        <CRow>
            <CCol xs="12" md="3" lg="3">
            < label><b>Defiant Behavior</b></label>
            </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="defiant"
                    title=" "
                    controlBlurFunc={handleBlurChange} 
                    controlFunc= {handleChange}
                    options={appConstants.USUAL}
                    content={screeningProforma.defiant}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
        <CRow>
            <CCol xs="12" md="3" lg="3">
                <label><b>Dissocial Behavior</b></label>
            </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                name="dissocial_behavior"
                title=" "
                controlBlurFunc={handleBlurChange} 
                controlFunc= {handleChange}
                options={appConstants.USUAL}
                content={screeningProforma.dissocial_behavior}
                manadatorySymbol={false}/>
            </CCol>
        </CRow>
        <CRow>
            <CCol xs="12" md="3" lg="3">
                <label><b>Generalized Anxiety</b></label>
            </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="generalized_anxiety"
                    title=" "
                    controlBlurFunc={handleBlurChange} 
                    controlFunc= {handleChange}
                    options={appConstants.USUAL}
                    content={screeningProforma.generalized_anxiety}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
        <CRow>
            <CCol xs="12" md="3" lg="3">
                <label><b>Social Anxiety</b></label>
            </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="social_anxiety"
                    title=" "
                    controlBlurFunc={handleBlurChange} 
                    controlFunc= {handleChange}
                    options={appConstants.USUAL}
                    content={screeningProforma.social_anxiety}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
    </CCard>}
    <CRow>
        <CCol xs="10" md="5" lg="5">
            <label><b><u>Psychiatric History</u></b></label>
            &nbsp;&nbsp;
            <SingleCheckbox 
                controlFunc={handleChange} 
                name='psychiatric_history'
                title=''
                type="checkbox" 
                value='psychiatric_history'
                content={(screeningProforma.psychiatric_history)?(screeningProforma.psychiatric_history):''}
                />  
        </CCol>
    </CRow>
    {!(screeningProforma.psychiatric_history) &&
    <CCard>
        <CRow>
            <CCol xs="8" md="4" lg="4">
                <SelectBox
                    name="bipolar_disorder"
                    title="Bipolar Disorder"
                    placeholder='Bipolar Disorder'
                    controlBlurFunc={handleBlurChange} 
                    controlFunc={handleChange}
                    options={appConstants.PREVIOUS_TREATMENT}
                    selectedOption={screeningProforma.bipolar_disorder?screeningProforma.bipolar_disorder:''}
                    manadatorySymbol={false} />
            </CCol>
            <CCol xs="8" md="4" lg="4">
                <SelectBox
                    name="depressive_disorder"
                    title="Depressive Disorder"
                    placeholder='Depressive Disorder'
                    controlFunc={handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.PREVIOUS_TREATMENT}
                    selectedOption={screeningProforma.depressive_disorder?screeningProforma.depressive_disorder:''}
                    manadatorySymbol={false} />
            </CCol>
            <CCol xs="8" md="4" lg="4">
                <SelectBox
                    name="schizophrenia"
                    title="Schizophrenia"
                    placeholder='Schizophrenia'
                    controlFunc={handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.PREVIOUS_TREATMENT}
                    selectedOption={screeningProforma.schizophrenia?screeningProforma.schizophrenia:''}
                    manadatorySymbol={false} />
            </CCol>
        </CRow>
        &nbsp;
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>Cognitive Deficits</b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="cognitive_deficits"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.DEFICTS}
                    content={screeningProforma.cognitive_deficits}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
    </CCard>}
    <CRow>
        <CCol xs="8" md="5" lg="5">
            <label><b><u>Consequences Personal </u></b></label>
            &nbsp;&nbsp;
            <SingleCheckbox 
                controlFunc={handleChange} 
                name='consequences_personal'
                title=''
                type="checkbox" 
                value='consequences_personal'
                content={(screeningProforma.consequences_personal)?(screeningProforma.consequences_personal):''}
                />  
        </CCol>
    </CRow> 
    {!(screeningProforma.consequences_personal) &&
    <CCard>
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>Cognitive Emotional</b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="cognitive_emotional"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.COGNITIVE_EMOTIONAL}
                    content={screeningProforma.cognitive_emotional}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>Economic</b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="economic"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.ECONOMIC}
                    content={screeningProforma.economic}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>Occupational</b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="occupational"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.OCCUPATIONAL}
                    content={screeningProforma.occupational}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
    </CCard>}
    <CRow>
        <CCol xs="10" md="4" lg="5">
            <label><b><u>Consequences interpersonal </u></b></label>
            &nbsp;&nbsp;
            <SingleCheckbox 
                controlFunc={handleChange} 
                name='consequences__interpersonal'
                title=''
                type="checkbox" 
                value='consequences__interpersonal'
                content={(screeningProforma.consequences__interpersonal)?(screeningProforma.consequences__interpersonal):''}
                />
                  
        </CCol>
    </CRow>
    {!(screeningProforma.consequences__interpersonal) &&
    <CCard>
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>Tension </b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="tension"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.SIGNIFICANT}
                    content={screeningProforma.tension}
                    manadatorySymbol={false}/>
               
            </CCol>
        </CRow>
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>Physical Violence</b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="physical_violence"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.FREQUENT}
                    content={screeningProforma.physical_violence}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>Marital Disharmony</b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="marital_disharmony"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.SIGNIFICANT}
                    content={screeningProforma.marital_disharmony}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>Co-Dependence</b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="co_dependence"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.SIGNIFICANT}
                    content={screeningProforma.co_dependence}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
    </CCard>}
    <CRow>
        <CCol xs="10" md="5" lg="5">
            <label><b><u>Mental Status Examination</u></b></label>
            &nbsp;&nbsp;
            <SingleCheckbox 
                controlFunc={handleChange}
                controlBlurFunc={handleBlurChange}  
                name='mental_state_examination'
                title=''
                type="checkbox" 
                value='mental_state_examination'
                content={(screeningProforma.mental_state_examination)?(screeningProforma.mental_state_examination):''}
                />  
        </CCol>
    </CRow>
    {!(screeningProforma.mental_state_examination) &&
    <CCard>
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>General Appearance And Behavior</b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="general_appearance_and_behavior"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.APPEARANCE}
                    content={screeningProforma.general_appearance_and_behavior}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>Psychomotor Activity</b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="psychomotor_activity"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.PSYCOMOTOR}
                    content={screeningProforma.psychomotor_activity}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>Speech</b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="speech"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.SPEECH}
                    content={screeningProforma.speech}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
        <CRow> 
            <CCol xs="12" md="3" lg="3">
                    <label><b>Mood</b></label>
                    </CCol>
            <CCol xs="12" md="9" lg="9">
                <RadioButtonList 
                    name="mood"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.MOOD}
                    content={screeningProforma.mood}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
    </CCard>}
    {!(screeningProforma.mental_state_examination) &&
    <CCard>
        &nbsp;
        <CRow>
            <CCol xs="12" md="3" lg="2">
                <label><b><u>Thought</u></b></label>
                &nbsp;&nbsp;
            </CCol>
            <CCol xs="12" md="3" lg="2">
               <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='derailment'
                    title='Derailment'
                    type="checkbox" 
                    value='derailment'
                    content={(screeningProforma.derailment)?(screeningProforma.derailment):''}
                    /> 
            </CCol>
            <CCol xs="12" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='flight_of_ideas'
                    title='Flight of ideas'
                    type="checkbox" 
                    value='flight_of_ideas'
                    content={(screeningProforma.flight_of_ideas)?(screeningProforma.flight_of_ideas):''}
                    /> 
            </CCol>
            <CCol xs="12" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='circumstantiality'
                    title=' Circumstantiality'
                    type="checkbox" 
                    value='circumstantiality'
                    content={(screeningProforma.circumstantiality)?(screeningProforma.circumstantiality):''}
                    /> 
            </CCol>
            <CCol xs="12" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='neologism'
                    title='Neologism'
                    type="checkbox" 
                    value='neologism'
                    content={(screeningProforma.neologism)?(screeningProforma.neologism):''}
                    /> 
            </CCol>
            <CCol xs="12" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='poverty_of_content'
                    title='Poverty of content'
                    type="checkbox" 
                    value='poverty_of_content'
                    content={(screeningProforma.poverty_of_content)?(screeningProforma.poverty_of_content):''}
                    /> 
            </CCol>
            <CCol xs="12" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='no_abnormality_noted'
                    title='No Abnormality noted'
                    type="checkbox" 
                    value='no_abnormality_noted'
                    content={(screeningProforma.no_abnormality_noted)?(screeningProforma.no_abnormality_noted):''}
                    /> 
            </CCol>
        </CRow>
        &nbsp;
        <CRow>
            <CCol xs="12" md="3" lg="2">
                <label><b><u>Perception</u></b></label>
                &nbsp;&nbsp;
            </CCol>
                <CCol xs="12" md="3" lg="3">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='auditory_hallucination'
                    title='Auditory hallucination'
                    type="checkbox" 
                    value='auditory_hallucination'
                    content={(screeningProforma.auditory_hallucination)?(screeningProforma.auditory_hallucination):''}
                    /> 
            </CCol>
            <CCol  xs="12" md="2" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='visual_hallucination'
                    title='Visual hallucination'
                    type="checkbox" 
                    value='flight_of_ideas'
                    content={(screeningProforma.visual_hallucination)?(screeningProforma.visual_hallucination):''}
                    /> 
            </CCol>
            <CCol  xs="12" md="3" lg="3">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='olfactory_hallucination'
                    title='Olfactory hallucination'
                    type="checkbox" 
                    value='olfactory_hallucination'
                    content={(screeningProforma.olfactory_hallucination)?(screeningProforma.olfactory_hallucination):''}
                    /> 
            </CCol>
            <CCol  xs="12" md="3" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='tactile'
                    title='Tactile'
                    type="checkbox" 
                    value='tactile'
                    content={(screeningProforma.tactile)?(screeningProforma.tactile):''}
                    /> 
            </CCol>
            
            <CCol  xs="12" md="3" lg="2">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='imagery'
                    title='Imagery'
                    type="checkbox" 
                    value='imagery'
                    content={(screeningProforma.imagery)?(screeningProforma.imagery):''}
                    /> 
            </CCol>
            <CCol xs="12" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={handleChange} 
                    name='no_abnormality_noted_perception'
                    title='No Abnormality noted'
                    type="checkbox" 
                    value='no_abnormality_noted_perception'
                    content={(screeningProforma.no_abnormality_noted_perception)?(screeningProforma.no_abnormality_noted_perception):''}
                    /> 
            </CCol>
        </CRow>
        &nbsp;
        <CRow> 
            <CCol xs="12" md="2" lg="2">
                    <label><b><u>Level Of Motivation</u></b></label>
                    </CCol>
            <CCol xs="12" md="10" lg="10">
                <RadioButtonList 
                    name="level_of_motivation"
                    title=" "
                    controlFunc= {handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.MOTIVATION}
                    content={screeningProforma.level_of_motivation}
                    manadatorySymbol={false}/>
            </CCol>
        </CRow>
    </CCard>}
    <CRow>
        <CCol xs="12" md="12" lg="12">
            <label><b><u>Patient Visit Details</u></b></label>
        </CCol>
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="pulse" 
            title="Pulse" 
            inputType="text" 
            content={(screeningProforma.pulse)?(screeningProforma.pulse):''} 
            placeholder="Pulse" 
            manadatorySymbol={false} 
            controlFunc={handleChange}
            controlBlurFunc={handleBlurChange} />
        </CCol>
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="bp" 
            title="BP" 
            inputType="text" 
            content={(screeningProforma.bp)?(screeningProforma.bp):''} 
            placeholder="BP" 
            manadatorySymbol={false}
            controlBlurFunc={handleBlurChange}  
            controlFunc={handleChange}/>
        </CCol> 
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="height" 
            title="Height" 
            inputType="text" 
            content={(screeningProforma.height)?(screeningProforma.height):''} 
            placeholder="Height" 
            manadatorySymbol={false} 
            controlBlurFunc={handleBlurChange} 
            controlFunc={handleChange}/>
        </CCol> 
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="weight" 
            title="Weight" 
            inputType="text" 
            content={(screeningProforma.weight)?(screeningProforma.weight):''} 
            placeholder="Weight" 
            manadatorySymbol={false} 
            controlBlurFunc={handleBlurChange} 
            controlFunc={handleChange}/>
        </CCol>
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="bmi" 
            title="BMI" 
            inputType="text" 
            content={(screeningProforma.bmi)?(screeningProforma.bmi):''} 
            placeholder="BMI" 
            manadatorySymbol={false}
            controlBlurFunc={handleBlurChange}  
            controlFunc={handleChange}/>
        </CCol> 
        <CCol xs="4" md="2" lg="2">
            <SelectBox
                    name="cgi_score"
                    title="CGI-Score"
                    placeholder='CGI-Score'
                    controlFunc={handleChange}
                    controlBlurFunc={handleBlurChange} 
                    options={appConstants.CGISCORE}
                    selectedOption={screeningProforma.cgi_score?screeningProforma.cgi_score:''}
                    manadatorySymbol={false} />
        </CCol> 
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="cvs" 
            title="CVS" 
            inputType="text" 
            content={(screeningProforma.cvs)?(screeningProforma.cvs):''} 
            placeholder="CVS" 
            manadatorySymbol={false}
            controlBlurFunc={handleBlurChange}  
            controlFunc={handleChange}/>
        </CCol>
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="cns" 
            title="CNS" 
            inputType="text" 
            content={(screeningProforma.cns)?(screeningProforma.cns):''} 
            placeholder="CNS" 
            manadatorySymbol={false}
            controlBlurFunc={handleBlurChange}  
            controlFunc={handleChange}/>
        </CCol>
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="rr" 
            title="RR" 
            inputType="text" 
            content={(screeningProforma.rr)?(screeningProforma.rr):''} 
            placeholder="RR" 
            manadatorySymbol={false}
            controlBlurFunc={handleBlurChange}  
            controlFunc={handleChange}/>
        </CCol> 
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="abdomen" 
            title="Abdomen" 
            inputType="text" 
            content={(screeningProforma.abdomen)?(screeningProforma.abdomen):''} 
            placeholder="Abdomen" 
            manadatorySymbol={false}
            controlBlurFunc={handleBlurChange}  
            controlFunc={handleChange}/>
        </CCol>  
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="pallor" 
            title="Pallor" 
            inputType="text" 
            content={(screeningProforma.pallor)?(screeningProforma.pallor):''} 
            placeholder="Pallor" 
            manadatorySymbol={false}
            controlBlurFunc={handleBlurChange}  
            controlFunc={handleChange}/>
        </CCol>
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="icterus" 
            title="Icterus" 
            inputType="text" 
            content={(screeningProforma.icterus)?(screeningProforma.icterus):''} 
            placeholder="Icterus" 
            manadatorySymbol={false}
            controlBlurFunc={handleBlurChange}  
            controlFunc={handleChange}/>
        </CCol>  
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="cyanosis" 
            title="Cyanosis" 
            inputType="text" 
            content={(screeningProforma.cyanosis)?(screeningProforma.cyanosis):''} 
            placeholder="Cyanosis" 
            manadatorySymbol={false}
            controlBlurFunc={handleBlurChange}  
            controlFunc={handleChange}/>
        </CCol>   
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="clubbing" 
            title="Clubbing" 
            inputType="text" 
            content={(screeningProforma.clubbing)?(screeningProforma.clubbing):''} 
            placeholder="Clubbing" 
            manadatorySymbol={false} 
            controlBlurFunc={handleBlurChange} 
            controlFunc={handleChange}/>
        </CCol> 
        <CCol xs="4" md="2" lg="2">
            <SingleTextInput name="edema" 
            title="Edema" 
            inputType="text" 
            content={(screeningProforma.edema)?(screeningProforma.edema):''} 
            placeholder="Edema" 
            manadatorySymbol={false}
            controlBlurFunc={handleBlurChange}  
            controlFunc={handleChange}/>
        </CCol> 
        <CCol xs="5" md="2" lg="2">
            <SingleTextInput name="lymphadenopathy" 
            title="Lymphadenopathy" 
            inputType="text" 
            content={(screeningProforma.lymphadenopathy)?(screeningProforma.lymphadenopathy):''} 
            placeholder="Lymphadenopathy" 
            manadatorySymbol={false}
            controlBlurFunc={handleBlurChange}  
            controlFunc={handleChange}/>
        </CCol>  
        
    </CRow>
    </CCardBody>
    </CCard>
    </>
)
}
export default ScreeningProforma;