import React, { useEffect, useState, useRef, useContext } from 'react'
import {
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CButton,
    CButtonGroup,
} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
//import Select from 'react-select';
//import makeAnimated from 'react-select/animated';
import SingleTextInput from '../form-components/SingleTextInput';
import SelectBox from '../form-components/SelectBox';
import * as appConstants from '../../AppConstants';
import Button from 'react-bootstrap/Button';
import SingleCheckbox from '../form-components/SingleCheckbox';
import GlobalState from './../../shared/GlobalState';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import MedicationDataService from '../../api/MedicationDataService';
import MedicationService from '../../api/MedicationService';
import { useParams } from "react-router";
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus, faEye, faTrash } from '@fortawesome/free-solid-svg-icons';


const Medication = () => {


    const [medication, setMedication] = useState({});
    const [state, setState] = useContext(GlobalState);
    const [medicationDataList, setMedicationDataList] = useState([]);
    const [medicationList, setMedicationList] = useState([]);
    const [medicationData, setMedicationData] = useState([]);
    const params = useParams();


    useEffect(() => {
        console.log("Medication page loaded. Msg. from UseEffect")
        getMedicationData();
        getMedicationDataByPatientIdVisitId();
    }, []);

    const getMedicationData = () => {
        MedicationDataService.getMedicationData(state.authHeader).then(resp => {
            setMedicationList(resp.data);
            console.log(resp.data)
        });
    }

    const getMedicationDataByPatientIdVisitId = () => {
        MedicationService.getMedicationDataById(params.patient_id, params.visit_id, state.authHeader).then(resp => {

            setMedicationDataList(resp.data);
            setMedication(resp.data);
            console.log(resp.data)
        });
    }
    const handleOnChange = (newValue) => {
        console.log(newValue)
        if (newValue?.id)
            setMedicationData({ ...medicationData, drug_name: newValue.drug_name });
        else
            setMedicationData({});

    }
    const saveOrUpdateMedicationData = (medicationData, e) => {
        console.log(medicationData.patient_id)
        medicationData.patient_id = parseInt(params.patient_id);
        medicationData.visit_id = parseInt(params.visit_id);
        medicationData.daily_dose = medication.daily_dose ? medication.daily_dose : '';
        medicationData.duration = medication.duration ? medication.duration : '';
        medicationData.uom = medication.uom ? medication.uom : '';

        MedicationService.createMedication(medicationData, state.authHeader).then((res) => {
            setMedicationDataList(res.data);
            getMedicationDataByPatientIdVisitId();
        });

        console.log("save")
    }
    const handleChange = (e) => {

        setMedication({ ...medication, [e.target.name]: e.target.value });

    }
    const test = (event) => {
        const value = event.target;
        //setMedication({ ...medication, [e.target.name]: e.target.value });
        setMedication({ ...medication, [event.target.name]: (event.target.checked) ? (value) : '' });

    }
    const deleteMedicationData = (id) => {
        MedicationService.deleteMedication(id, state.authHeader).then(resp => {
            setMedicationDataList(resp.data);

        });
    }

    const addMedication = () => {
        console.log(medication)
        MedicationService.createMedication(medication, state.authHeader).then(resp => {
            setMedicationDataList(resp.data);
        });

    }

    const handleChangeCheckbox = (event) => {
        const { name, value } = event.target;
        console.log('auto saving function')
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setMedication({ ...medication, [event.target.name]: (event.target.checked) ? (value) : '' });
            medication[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setMedication({ ...medication, [event.target.name]: value });
            medication[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdateCheckbox(medication, event);
    };

    const saveOrUpdateCheckbox = (data, event) => {
        console.log(data)
        if (data.id) {
            console.log('medication data updating..: ',)
            //let id=(params.patient_id) ? params.id : data.id
            MedicationService.updateMedication(data.id, data, state.authHeader).then(res => {
                console.log(res)
                setMedication(res.data);
            });
        }
        else {
            console.log('psychotherapy saving')
            console.log(data)
            data.patient_id = parseInt(params.patient_id);
            data.visit_id = parseInt(params.visit_id);
            MedicationService.createMedication(data, state.authHeader).then(res => {
                console.log("here")
                console.log(res.data)
                setMedication(res.data);
            });
        }
    }



    return (
        <>
            <CCard borderColor="primary">
                <CCardBody>
                    <CRow>
                        <CCol xs="12" md="6" lg="6" xl="6">
                            <Autocomplete
                                onChange={(event, newValue) => {
                                    handleOnChange(newValue)
                                }}
                                options={medicationList}
                                autoHighlight
                                getOptionLabel={(option) => option.drug_name}

                                renderOption={(option) => (
                                    <React.Fragment>
                                        {option.drug_name}
                                    </React.Fragment>
                                )}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label={'Chemical Name'}
                                        variant={appConstants.INPUT_FLOAT_LABEL.variant}
                                        inputProps={{
                                            ...params.inputProps
                                        }}
                                    />
                                )}
                            />
                        </CCol>
                        <CCol xs="12" md="4" lg="4">
                            <SingleTextInput name="daily_dose"
                                title="Daily Dose"
                                inputType="text"
                                content={(medication.daily_dose) ? (medication.daily_dose) : ''}
                                placeholder="Daily Dose"
                                manadatorySymbol={false}
                                controlFunc={handleChange}
                            />
                        </CCol>
                        <CCol xs="12" md="4" lg="4">
                            <SingleTextInput name="duration"
                                title="Duration"
                                inputType="text"
                                content={(medication.duration) ? (medication.duration) : ''}
                                placeholder="Duration"
                                manadatorySymbol={false}
                                controlFunc={handleChange}
                                multiline={false}
                            />
                        </CCol>
                        <CCol xs="12" md="4" lg="4">
                            <SelectBox
                                name="uom"
                                title="Uom"
                                placeholder='Uom'
                                controlFunc={handleChange}
                                options={appConstants.UOM}
                                selectedOption={medication.uom ? medication.uom : ''}
                                manadatorySymbol={true} />
                        </CCol>
                    </CRow>
                    &nbsp;
                    <CRow>
                        <CCol xs="12" md="4" lg="4">
                            <SingleTextInput name="notes"
                                title="Notes"
                                inputType="text"
                                content={(medication.notes) ? (medication.notes) : ''}
                                placeholder="Notes"
                                manadatorySymbol={false}
                                controlFunc={handleChange}
                                multiline={true}
                                rows={5}
                                variant="outlined" />
                        </CCol>
                        <CCol xs="12" md="4" lg="4">
                            <SingleTextInput name="allergies"
                                title="Allergies"
                                inputType="text"
                                content={(medication.allergies) ? (medication.allergies) : ''}
                                placeholder="Allergies"
                                manadatorySymbol={false}
                                controlFunc={handleChange}
                                multiline={true}
                                rows={5}
                                variant="outlined" />
                        </CCol>
                        <CCol xs="12" md="4" lg="4">
                            <SingleTextInput name="notes_next_visit"
                                title="Notes to next visit"
                                inputType="text"
                                content={(medication.notes_next_visit) ? (medication.notes_next_visit) : ''}
                                placeholder="Notes to next visit"
                                manadatorySymbol={false}
                                controlFunc={handleChange}
                                multiline={true}
                                rows={5}
                                variant="outlined" />
                        </CCol>
                    </CRow>
                    <CRow>
                        <CCol xs="12" md="12" lg="12">
                            <label>Allergies and Symptoms Notes:</label>
                        </CCol>
                    </CRow>
                    &nbsp;
                    <CRow>
                        <CCol xs="12" md="4" lg="6">
                            <SingleCheckbox
                                controlFunc={handleChange}
                                name='ost'
                                title='OST'
                                type="checkbox"
                                value='ost'
                                checked={medication.ost}
                                content={(medication.ost) ? (medication.ost) : ''}
                            />
                        </CCol>
                    </CRow>
                    <CButton
                        className="add-button"
                        color={appConstants.SAVE_BTN_COLOR}
                        onClick={() => { saveOrUpdateMedicationData(medicationData) }}
                        style={{ float: "right" }}
                    >
                        ADD
                    </CButton>
                </CCardBody>
            </CCard>
            {medicationDataList.length > 0 &&
                <TableContainer style={{ backgroundColor: 'lemonchiffon' }}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>
                                    <b>Chemical Name</b>
                                </TableCell >
                                <TableCell>Dosage</TableCell>
                                <TableCell colspan="2">Duration</TableCell>
                            </TableRow>
                        </TableHead>
                        {medicationDataList.length > 0 &&
                            <TableBody>
                                {medicationDataList.map((element, i) => (
                                    <TableRow key={i}>
                                        <TableCell>{element?.drug_name}</TableCell>
                                        <TableCell>{element?.daily_dose}</TableCell>
                                        <TableCell>{element?.duration}</TableCell>
                                        <TableCell>{element?.uom}</TableCell>


                                        <TableCell>
                                            <CButtonGroup>
                                                <CButton
                                                    color={appConstants.DELETE_BTN_COLOR}
                                                    size="sm"
                                                    title="Delete"
                                                    onClick={() => { deleteMedicationData(element.id) }}
                                                >
                                                    <FontAwesomeIcon icon={faTrash} />
                                                </CButton>
                                                {/* <CButton
                                                    color={appConstants.SAVE_BTN_COLOR}
                                                    onClick={() => { addMedication(medicationData) }}
                                                    style={{ float: 'right' }}>
                                                    <FontAwesomeIcon icon={faPlus} />&nbsp;
                                                </CButton> */}
                                            </CButtonGroup>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        }
                    </Table>
                </TableContainer>}
        </>
    )
}
export default Medication;