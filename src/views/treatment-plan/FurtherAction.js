import React, { useEffect, useState, useRef, useContext } from 'react'
import {
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CButton,
    CButtonGroup
} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import SingleTextInput from '../form-components/SingleTextInput';
import SelectBox from '../form-components/SelectBox';
import * as appConstants from '../../AppConstants';
import Button from 'react-bootstrap/Button';
import RadioButtonList from '../form-components/RadioButtonList';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus, faPrint, faCertificate } from '@fortawesome/free-solid-svg-icons';
import { useParams } from "react-router";
import GlobalState from '../../shared/GlobalState';
import FurtherDetailService from 'src/api/FurtherDetailService';
import * as momentServices from './../../shared/MomentService';
import * as DateConfigs from './../../shared/configs/DateConfigs';
import KeyboardDatePickers from '../form-components/KeyboardDatePicker';

const FurtherAction = () => {

    const params = useParams();
    const [state, setState] = useContext(GlobalState);
    const [furtherAction, setFurtherAction] = useState({

    });

    useEffect(() => {
        getAllFurtherDetails();
        console.log("use effect")
    }, []);

    const handleChange = (event) => {
        const { name, value } = event.target;
        console.log('auto saving function')
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setFurtherAction({ ...furtherAction, [event.target.name]: (event.target.checked) ? (value) : '' });
            furtherAction[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setFurtherAction({ ...furtherAction, [event.target.name]: value });
            furtherAction[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdateFurtherDetails(furtherAction, event);
    };

    const handleBlurChange = (event) => {
        console.log("blur is calling")
        saveOrUpdateFurtherDetails(furtherAction, event);
    }

    const getAllFurtherDetails = () => {
        FurtherDetailService.getAllFurtherDetails(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setFurtherAction(resp.data);
        });
    }

    const saveOrUpdateFurtherDetails = (data, event) => {
        console.log(data.id)
        if (data.id) {
            console.log('screening data updating..: ',)
            let id = (params.id) ? params.id : data.id
            FurtherDetailService.updateFurtherDetail(data.id, data, state.authHeader).then(res => {
                console.log(res)
                setFurtherAction(res.data);
            });
        }
        else {
            console.log('further details data saving')
            data.patient_id = parseInt(params.patient_id);
            data.visit_id = parseInt(params.visit_id);
            FurtherDetailService.createFurtherDetail(data, state.authHeader).then(res => {
                console.log(res.data)
                setFurtherAction(res.data);
            });
        }
    }

    const handleKeyboardPickerChangeAdmission = (date) => {
        setFurtherAction({ ...furtherAction, 'admission': date });
    }
    const handleKeyboardPickerChangeFollowup = (date) => {
        setFurtherAction({ ...furtherAction, 'followup_date': date });
    }

    const handleKeyboardPickerChangeTele = (date) => {
        setFurtherAction({ ...furtherAction, 'tele_followup_date': date });

    }

    return (
        <>
            <CCard>
                <CCardBody>
                    <CRow>
                        <CCol xs="12" md="12" lg="12">
                            <RadioButtonList
                                name="further_action"
                                title=" "
                                controlFunc={handleChange}
                                options={appConstants.FURTHER_ACTION}
                                content={furtherAction.further_action}
                                manadatorySymbol={false} />
                        </CCol>
                    </CRow>
                    {furtherAction.further_action == 'Admission' &&
                        <CRow>
                            <CCol xs="12" md="12" lg="12">
                                <CButtonGroup style={{ float: 'right' }}>
                                    <CButton

                                        color={appConstants.SAVE_BTN_COLOR}
                                        size="sm"
                                        title="Admission"
                                    //onClick={()=>{gotoPatientVisit(item)}}
                                    >
                                        Admission
                                    </CButton>
                                    <CButton
                                        color={appConstants.CANCEL_BTN_COLOR}
                                        size="sm"
                                        title="Edit Patient"
                                    //onClick={()=>{editPatient(item)}}
                                    >
                                        <FontAwesomeIcon icon={faPrint} />
                                    </CButton>
                                    <CButton
                                        color={appConstants.CANCEL_BTN_COLOR}
                                        size="sm"
                                        title="Edit Patient"
                                    //onClick={()=>{editPatient(item)}}
                                    >
                                        <FontAwesomeIcon icon={faCertificate} />
                                    </CButton>
                                </CButtonGroup>
                            </CCol>
                        </CRow>}
                    {furtherAction.further_action == 'Admission' &&
                        <CRow>
                            <CCol xs="8" md="6" lg="6">
                                <KeyboardDatePickers
                                    name="admission"
                                    disableToolbar
                                    format="DD/MM/yyyy"
                                    content={(furtherAction.admission) ? (furtherAction.admission) : momentServices.currentDate(DateConfigs.DEFAULT_DATE_FORMAT_BACKEND)}
                                    manadatorySymbol={false}
                                    controlFunc={handleKeyboardPickerChangeAdmission}
                                />
                            </CCol>
                        </CRow>}
                    {/*}
                    {furtherAction.further_action == 'Admission' &&
                    <CRow>
                        <CCol xs="12" md="12" lg="12"> 
                            <RadioButtonList 
                            name="ward"
                            title=" "
                            controlFunc= {handleChange}
                            options={appConstants.WARD}
                            content={furtherAction.ward}
                            manadatorySymbol={false}/>
                        </CCol> 
      </CRow>}*/}
                    {furtherAction.further_action == 'Admission' &&
                        furtherAction.ward == 'Ward' &&
                        <CRow>
                            <CCol xs="12" md="12" lg="12">
                                <CButtonGroup>
                                    <CButton
                                        color={appConstants.SAVE_BTN_COLOR}
                                        size="sm"
                                        title="Admission"
                                    //onClick={()=>{gotoPatientVisit(item)}}
                                    >
                                        Check Availabilty
                                    </CButton>
                                    <CButton
                                        color={appConstants.CANCEL_BTN_COLOR}
                                        size="sm"
                                        title="Admission"
                                    //onClick={()=>{gotoPatientVisit(item)}}
                                    >
                                        Assign ward
                                    </CButton>
                                </CButtonGroup>
                            </CCol>
                        </CRow>}
                    {furtherAction.further_action == 'Follow Up' &&
                        <CRow>
                            <CCol xs="8" md="6" lg="6">
                                <KeyboardDatePickers
                                    name="followup_date"
                                    disableToolbar
                                    format="DD/MM/yyyy"
                                    content={(furtherAction.followup_date) ? (furtherAction.followup_date) : momentServices.currentDate(DateConfigs.DEFAULT_DATE_FORMAT_BACKEND)}
                                    manadatorySymbol={false}
                                    controlFunc={handleKeyboardPickerChangeFollowup}
                                />
                            </CCol>
                        </CRow>}
                    {furtherAction.further_action == 'Tele Follow Up' &&
                        <CRow>
                            <CCol xs="8" md="6" lg="6">
                                <KeyboardDatePickers
                                    name="tele_followup_date"
                                    disableToolbar
                                    format="DD/MM/yyyy"
                                    content={(furtherAction.tele_followup_date) ? (furtherAction.tele_followup_date) : momentServices.currentDate(DateConfigs.DEFAULT_DATE_FORMAT_BACKEND)}
                                    manadatorySymbol={false}
                                    controlFunc={handleKeyboardPickerChangeTele}
                                />
                            </CCol>
                        </CRow>}
                </CCardBody>
            </CCard>
            <CCard>
                <CCardBody>
                    <CRow>
                        <CCol xs="12" md="12" lg="12">
                            <RadioButtonList
                                name="referral"
                                title=" "
                                controlFunc={handleChange}
                                options={appConstants.FURTHER_REFERRAL}
                                content={furtherAction.referral}
                                manadatorySymbol={false} />
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
        </>
    )
}
export default FurtherAction;