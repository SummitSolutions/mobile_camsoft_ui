import React, { useEffect, useState, useRef, useContext } from 'react'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CButton
} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import {  CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import SingleTextInput from '../form-components/SingleTextInput';
import SelectBox from '../form-components/SelectBox'; 
import * as appConstants from '../../AppConstants';
import Button from 'react-bootstrap/Button';
import RadioButtonList from '../form-components/RadioButtonList';
import { useParams} from "react-router";
import InvestigationService from 'src/api/InvestigationService';
import GlobalState from '../../shared/GlobalState';


const Investigation = () => {
    const params = useParams();
    const [state, setState] = useContext(GlobalState);
    const [investigation, setInvestigation] = useState({ 
      
  });
useEffect(() => {
    getInvestigation(); 
}, []);
 

const handleChange = (e) => {
    const { name, value } = e.target;
    console.log('auto saving function')
    if(e.target.type == 'checkbox')
    {
        console.log('checkbox')
        setInvestigation({ ...investigation, [e.target.name] : (e.target.checked)?(value) : ''});
        investigation[e.target.name] = (e.target.checked) ? (value) : '';
    } 
    else
    {  
        console.log('not checkbox')
        setInvestigation({ ...investigation, [e.target.name]: value});
        investigation[e.target.name] = value;
    }
    if(e.target.type != 'text')
    saveOrUpdateInvestigation(investigation, e);
}

const handleBlurChange = (e) => {
    console.log("blur is calling")
    saveOrUpdateInvestigation(investigation, e);
}

const setFormValues = (data) => {
    console.log(data)
    if(data.id)
    {
        setInvestigation(data)
        console.log("Investigation retrieved Successfully")
    }
}

const saveOrUpdateInvestigation = (data, e) => {
    console.log(data.id)
    if(params.id || data.id){
        console.log('investigation data updating..: ', data)
        let id=(params.id) ? params.id : data.id
        InvestigationService.updateInvestigation(id,data, state.authHeader).then(res =>{ 
            console.log(res)
            setFormValues(res);
        });
    }
    else{                
        console.log('investigation data saving')
        data.patient_id =  parseInt(params.patient_id);
        data.visit_id =  parseInt(params.visit_id);
        InvestigationService.createInvestigation(data,state.authHeader).then(res =>{
            console.log(res.data)
            setFormValues(res.data);
        });
    }
}

const getInvestigation = () => {
        InvestigationService.getAllInvestigations(params.patient_id, params.visit_id, state.authHeader).then(resp => {
            setFormValues(resp.data);
        });
   
}
  
    return (
        <>
            <CCard>
                <CCardBody>
                    <CRow>
                        <CCol xs="12" md="10" lg="10"> 
                            <SingleTextInput name="notes" 
                                title="Notes" 
                                inputType="text" 
                                content={(investigation.notes)?(investigation.notes):''} 
                                placeholder="Notes" 
                                manadatorySymbol={false}
                                controlBlurFunc={handleBlurChange} 
                                controlFunc={handleChange}
                                multiline={true}
                                rows={5}
                                variant = "outlined"/>
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
         </>
      )
  }
  export default Investigation;