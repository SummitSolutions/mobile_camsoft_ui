import React,{useContext, useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Investigation from '../treatment-plan/Investigation';
import Medication from '../treatment-plan/Medication';
import PschologicalAdvice from '../treatment-plan/PsychologicalAdvice';
import FurtherAction from '../treatment-plan/FurtherAction';
import ScreeningProforma from '../treatment-plan/ScreeningProforma';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

export default function Mainpage(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <h1>Treatment Plan</h1>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
            >
            <Typography className={classes.heading}>Investigation</Typography>
            </AccordionSummary>
            <AccordionDetails>
            <Typography>
            <Investigation/>
            </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header">
            <Typography className={classes.heading}>Medication</Typography>
            </AccordionSummary>
            <AccordionDetails>
            <Typography>
                <Medication/>
            </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header">
            <Typography className={classes.heading}>Psychotherapy</Typography>
            </AccordionSummary>
            <AccordionDetails>
            <Typography>
                <PschologicalAdvice/>
            </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header">
            <Typography className={classes.heading}>Screening Proforma</Typography>
            </AccordionSummary>
            <AccordionDetails>
            <Typography>
            <ScreeningProforma/>
            </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header">
            <Typography className={classes.heading}>Further Action</Typography>
            </AccordionSummary>
            <AccordionDetails>
            <Typography>
                <FurtherAction/>
            </Typography>
            </AccordionDetails>
        </Accordion>
    </div>
  );
}