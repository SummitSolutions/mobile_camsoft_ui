import React, { useEffect, useState } from 'react';
import { CCard, CCardBody, CCardHeader, CCardFooter, CRow, CCol, CButton } from '@coreui/react';
import StepOne from './StepOne';
import StepTwo from './StepTwo';
import StepThree from './StepThree';
import 'semantic-ui-css/semantic.min.css';
import { Button } from "semantic-ui-react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import StepperComponent from './../../shared/stepper/Stepper';
import * as appointmentConfig from './../../shared/AppointmentConfig';
import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import ConfirmComponent from './../form-components/ConfirmComponent';
import * as momentServices from './../../shared/MomentService';
import * as appConstants from './../../AppConstants';

const AppointmentComponent = () => {

    const [state, setState] = useState({
        steps : appointmentConfig.APPOINTMENT_STEPS,
        currentStep : 1
    });

    const [windowState, setWindowState] = useState({ open: false });

    const [appointment, setAppointment] = useState({
        first_name: '',
        age: '',
        gender: '',
        address: '',
        primary_contact_number: '',
        uhid: '',
        last_name: '',
        department: '',
        unit: '',
        doctor: '',
        proforma_type: '',
        appointment_date: momentServices.currentDateTime()
    });
    
    const onClickNext = () => {
        setState({
            ...state, currentStep: state.currentStep + 1
        });
    }

    const onClickPrevious = () => {
        setState({
            ...state, currentStep: state.currentStep - 1
        });
    }

    const handleChange = (e) => {
        console.log(e.target.value)
        setAppointment({ ...appointment, [e.target.name]: e.target.value });
    }

    const show = () => {
        setConfirmBoxProps({...confirmBoxProps, windowState: true});
    }

    const handleConfirm = () => {
        console.log("Confirmed")
        setConfirmBoxProps({...confirmBoxProps, windowState: false});
    }

    const handleCancel = () => {
        console.log("Cancelled")
        setConfirmBoxProps({...confirmBoxProps, windowState: false});
    }

    const [confirmBoxProps, setConfirmBoxProps] = useState({
        'confirmHeader': 'Appointment Confirmation', 
        'confirmMsg' : 'Please Confirm your appointment', 
        'cancelButtonText' : 'Cancel',
        'confirmButtonText' : 'Ok',
        'windowState' : windowState.open,
        'handleCancel' : handleCancel,
        'handleConfirm' : handleConfirm,
        'windowSize' : 'mini',
        'handleButtonClick' : show
    });
    
    const setNextDate = () => {
        setAppointment({...appointment, appointment_date: momentServices.add({"date":appointment.appointment_date, "count":1, "term":'d'})});
    }

    const setPrevDate = () => {
        setAppointment({...appointment, appointment_date: momentServices.subtract({"date":appointment.appointment_date, "count":1, "term":'d'})});
    }

    return (
        <>
            <CCard accentColor="primary">
                <CCardHeader align="center">
                    <StepperComponent steps={ state.steps } 
                        activeStep={ state.currentStep } disabledSteps={ [] } 
                        height={40}/>
                </CCardHeader>
                <CCardBody>
                    {state.currentStep==1 &&
                        <StepOne obj={appointment} controlFunc = {handleChange} />
            		}
        			{state.currentStep==2 &&
        				<StepTwo obj={appointment} controlFunc={handleChange} setNextDate={setNextDate} setPrevDate={setPrevDate}/>
            		}
            		{state.currentStep==3 &&
        				<StepThree obj={appointment} />
            		}
                </CCardBody>
                <CCardFooter>
                    {(state.currentStep > 1) && 
                        <CButton color={appConstants.SAVE_BTN_COLOR} style={{ float: 'left' }} onClick={ onClickPrevious }>
                            <FontAwesomeIcon icon={faAngleLeft}/> Prev
                        </CButton>
                    }
                    { state.currentStep < state.steps.length && 
                        <CButton color={appConstants.SAVE_BTN_COLOR} style={{ float: 'right' }} onClick={ onClickNext }>
                            Next <FontAwesomeIcon icon={faAngleRight}/> 
                        </CButton>
                    }
                    { state.currentStep === state.steps.length && 
                        <ConfirmComponent confirmBoxProps={confirmBoxProps} />                        
                    }
                </CCardFooter>
            </CCard>
        </>
    );
}
export default AppointmentComponent;