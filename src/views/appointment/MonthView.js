import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import {Scheduler, MonthView, Appointments, AppointmentTooltip} from '@devexpress/dx-react-scheduler-material-ui';
import { appointments } from './../demo-data/appointments';

class MonthViewer extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            data: appointments
        };
    }

    render() {
        const {data} = this.state;
        return (
            <Paper>
                <Scheduler
                    data={data}
                    height={660}
                >
                    <MonthView />
                    <Appointments />
                    <AppointmentTooltip
                        //showOpenButton
                        showCloseButton
                        //showDeleteButton
                    />
                </Scheduler>
            </Paper>
        );
    }
}

export default MonthViewer;