import React from 'react';
import SingleTextInput from '../form-components/SingleTextInput';
import { CCol, CRow} from  '@coreui/react';
import RadioButtonList from '../form-components/RadioButtonList';
import * as appConstants from './../../AppConstants';
import Divider from '../form-components/Divider';

const PatientInfo = (props) =>
{
    return (
        <>
        <Divider label="Patient Info"/>    
        <CRow className="formMargins">
            <CCol xs="12" md="6" lg="3">                                        
                <SingleTextInput name="first_name" 
                    title="Name" 
                    inputType="text" 
                    content={(props.obj.first_name)?(props.obj.first_name):''} 
                    placeholder="Name" 
                    controlFunc={props.controlFunc}
                />
            </CCol>
            <CCol xs="12" md="6" lg="2">
                <SingleTextInput name="age" 
                    title="Age" 
                    inputType="number" 
                    content={(props.obj.age)?(props.obj.age):''} 
                    placeholder="Age" 
                    controlFunc={props.controlFunc}
                />
            </CCol>
            <CCol xs="12" md="6" lg="3">
                <SingleTextInput name="primary_contact_number" 
                    title="Phone Number" 
                    inputType="number" 
                    content={(props.obj.primary_contact_number)?(props.obj.primary_contact_number):''} 
                    placeholder="Phone Number" 
                    controlFunc={props.controlFunc}
                />
            </CCol>
            <CCol xs="12" md="6" lg="3">                                
                <RadioButtonList 
                    name="gender"
                    title="Gender"
                    controlFunc={ props.controlFunc }
                    options={ appConstants.GENDER_OPTIONS}
                    content={ props.obj.gender }
                />
            </CCol>
        </CRow>
        </>
    )
}
export default PatientInfo;