import * as fs from "fs";
import { Document, Packer, Paragraph, TextRun } from "docx";
import { saveAs } from 'file-saver';

// Create document
export const generate = (props) => {
    const doc = new Document();

    // Documents contain sections, you can have multiple sections per document, go here to learn more about sections
    // This simple example will only contain one section
    doc.addSection({
        properties: {},
        children: [
            new Paragraph({
                children: [
                    new TextRun(props[0].name),
                    new TextRun({
                        text: "Foo Bar",
                        bold: true,
                    }),
                    new TextRun({
                        text: "\tGithub is the best",
                        bold: true,
                    }),
                ],
            }),
        ],
    });

    // Used to export the file into a .docx file
    Packer.toBlob(doc).then((blob) => {
        console.log(blob);
        saveAs(blob, "example.docx");
        console.log("Document created successfully");
    });

    // Done! A file called 'My Document.docx' will be in your file system.
}