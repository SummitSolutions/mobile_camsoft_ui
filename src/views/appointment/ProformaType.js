import React from 'react';
import { CCol, CRow} from  '@coreui/react';
import RadioButtonList from '../form-components/RadioButtonList';
import * as appConstants from './../../AppConstants';
import Divider from '../form-components/Divider';

const ProformaType = (props) =>
{
    return (
        <>
            <Divider label="Proforma Type"/>    
            <CRow className="formMargins">
                <CCol xs="12" md="12" lg="12">                                        
                    <RadioButtonList 
                        name="proforma_type"
                        title=""
                        controlFunc={ props.controlFunc }
                        options={ appConstants.PROFORMA_TYPE }
                        content={ props.obj.proforma_type }
                    />
                </CCol>
            </CRow>
        </>
    )
}
export default ProformaType;