import React from 'react';
import { CCol, CRow, CLabel } from  '@coreui/react';

const StepThree = (props) => {
    return (
        <CRow>
            <CCol xs="6">
                <CLabel htmlFor="name">Patient Name:  <b>{props.obj.first_name}</b></CLabel>
            </CCol>
            <CCol xs="6">
                <CLabel htmlFor="age">Appointment Date: <b></b></CLabel>
            </CCol>
            <CCol xs="6">
                <CLabel htmlFor="phone_number">Phone Number:  882781891</CLabel>
            </CCol>
            <CCol xs="6">
                <CLabel htmlFor="age">Appointment Time:  12:00 PM</CLabel>
            </CCol>
            <CCol xs="6">
                <CLabel htmlFor="age">Age:  73</CLabel>
            </CCol>
            <CCol xs="6">
                <CLabel htmlFor="age">Doctor:  ABC</CLabel>
            </CCol>
            <CCol xs="6">
                <CLabel htmlFor="age">Proforma Type : Screening</CLabel>
            </CCol>
            <CCol xs="6">
                <CLabel htmlFor="age">Unit:  Unit 2</CLabel>
            </CCol>
        </CRow>
    )
}
export default StepThree;