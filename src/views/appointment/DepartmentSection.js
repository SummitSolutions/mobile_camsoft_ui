import React from 'react';
import { CCol, CRow} from  '@coreui/react';
import RadioButtonList from '../form-components/RadioButtonList';
import SelectBox from '../form-components/SelectBox';
import * as appConstants from './../../AppConstants';
import Divider from '../form-components/Divider';

const DepartmentSection = (props) =>
{
    return (
        <>
            <Divider label="Department/Unit"/>    
            <CRow className="formMargins">
                <CCol xs="12" md="4" lg="12">                                        
                    <RadioButtonList 
                        name="department"
                        title="Department"
                        controlFunc={ props.controlFunc }
                        options={ appConstants.DEPARTMENTS }
                        content={ props.obj.department }
                    />
                </CCol>
                <CCol xs="12" md="4" lg="4">                                        
                    <SelectBox 
                        name="unit"
                        title="Unit"
                        controlFunc={ props.controlFunc }
                        options={ appConstants.UNITS }
                        content={ props.obj.unit }
                    />
                </CCol>
                <CCol xs="12" md="4" lg="4">                                        
                    <SelectBox 
                        name="doctor"
                        title="Doctor"
                        controlFunc={ props.controlFunc }
                        options={ appConstants.DOCTORS }
                        content={ props.obj.doctor }
                    />
                </CCol>                
            </CRow>
        </>
    )
}
export default DepartmentSection;