import React from 'react';
import PatientInfo from './PatientInfo';
import ProformaType from './ProformaType';
import DepartmentSection from './DepartmentSection';

const StepOne = (props) => {
    return (
        <>
            <PatientInfo obj = {props.obj} controlFunc = {props.controlFunc} />
            <ProformaType obj = {props.obj} controlFunc = {props.controlFunc} />
            <DepartmentSection obj = {props.obj} controlFunc = {props.controlFunc} />
        </>
    );
}
export default StepOne;