import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import { ViewState } from '@devexpress/dx-react-scheduler';
import { Scheduler, WeekView, Appointments, AppointmentTooltip } from '@devexpress/dx-react-scheduler-material-ui';
import { withStyles } from '@material-ui/core/styles';
import { appointments } from './../demo-data/appointments';
import * as appointmentConfig from './../../shared/AppointmentConfig';
import * as momentServices from './../../shared/MomentService';

const styles = theme => ({
    addButton: {
        position: 'absolute',
        bottom: theme.spacing(1) * 3,
        right: theme.spacing(1) * 4,
    }
});

/* eslint-disable-next-line react/no-multi-comp */
class WeekViewer extends React.PureComponent {
  constructor(props) {
        super(props);
        this.state = {
            data: appointments,
            currentDate: momentServices.currentDate('Y-MM-DD'),
            startDayHour: appointmentConfig.START_DAY_HOUR,
            endDayHour: appointmentConfig.END_DAY_HOUR
        };
  }

  render() {
    const { currentDate, data, startDayHour, endDayHour } = this.state;

    return (
        <Paper>
            <Scheduler
            data={data}
            height={420}
            >
            <ViewState
                currentDate={currentDate}
            />
            <WeekView
                startDayHour={startDayHour}
                endDayHour={endDayHour}
            />
            <Appointments />
            <AppointmentTooltip
                //showOpenButton
                showCloseButton
                //showDeleteButton
            />
            </Scheduler>

        </Paper>
    );
  }
}

export default withStyles(styles)(WeekViewer);