import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import {
    CCard, CCardBody, CCardHeader, CCol, CRow,
    CBadge, CButtonGroup, CButton
} from '@coreui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import 'semantic-ui-css/semantic.min.css'
import RadioButtonList from '../form-components/RadioButtonList';
import SelectBox from '../form-components/SelectBox';
import KeyboardDatePickers from '../form-components/KeyboardDatePicker';
import * as appConstants from './../../AppConstants';
import * as appointmentConfig from './../../shared/AppointmentConfig';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import WeekViewer from './WeekView';
import MonthViewer from './MonthView'

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
}));

const StepTwo = (props) => {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [apptView, setApptView] = React.useState('Day');

    useEffect(() => {
        console.log("Appointment Step Two loaded. Msg. from UseEffect")
    }, []);

    const saveOrUpdatePatient = (e) => {

    }

    const handleChange = (e) => {
        console.log(e.target.value)
    }

    const handleAccordianChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const getComponentFor = (view) => {
        if (view == 'Day') {
            return (
                <CCard className="scrollable-body">
                    <CCardBody>
                        {
                            appointmentConfig.SLOTS.map((item, index) => {
                                if (index < (appointmentConfig.SLOTS.length) - 1) {
                                    return (
                                        <Accordion key={index} expanded={expanded === item} onChange={handleAccordianChange(item)}>
                                            <AccordionSummary
                                                expandIcon={<ExpandMoreIcon />}
                                                aria-controls="panel1bh-content"
                                                id="panel1bh-header"
                                            >
                                                <CRow style={{ width: '100%' }}>
                                                    <CCol md="5" lg="5">
                                                        <CButtonGroup>
                                                            <CButton color="light">{item.slot}</CButton>
                                                            <CButton color="dark">-</CButton>
                                                            <CButton color="light">{appointmentConfig.SLOTS[index + 1].slot}</CButton>
                                                        </CButtonGroup>
                                                    </CCol>
                                                    <CCol xs="7" md="2" lg="2">
                                                        <CButtonGroup>
                                                            <CButton color="light">{appointmentConfig.APPOINTMENTS[item.slot].length}</CButton>
                                                            <CButton color="dark">/</CButton>
                                                            <CButton color="light">{item.max_no_appts}</CButton>
                                                        </CButtonGroup>
                                                    </CCol>
                                                    <CCol xs="5" align="center">
                                                        {((appointmentConfig.APPOINTMENTS[item.slot].length < item.max_no_appts)) &&
                                                            <CButton color={appConstants.SAVE_BTN_COLOR}>Book now</CButton>
                                                        }
                                                    </CCol>
                                                </CRow>
                                            </AccordionSummary>
                                            <AccordionDetails>
                                                <CRow style={{ width: '100%' }}>
                                                    {appointmentConfig.APPOINTMENTS[item.slot].map((obj, j) => {
                                                        return (
                                                            <CCol md="6" lg="4" xl="3" key={j}>
                                                                <CCard>
                                                                    <CCardHeader>
                                                                        <CRow>
                                                                            <CCol>
                                                                                <b>{obj.first_name}</b>
                                                                            </CCol>
                                                                            <CCol xs="5" sm="5" md="4" lg="4" xl="4" align="right">
                                                                                <CBadge className={classes.heading} color="danger">{appConstants.RECORD_TYPE_SHORT_NAME[obj.record_type]}</CBadge>
                                                                            </CCol>
                                                                        </CRow>
                                                                        <CRow>
                                                                            <CCol>
                                                                                {obj.doctor_name}
                                                                            </CCol>
                                                                        </CRow>
                                                                    </CCardHeader>
                                                                </CCard>
                                                            </CCol>
                                                        )
                                                    })
                                                    }
                                                </CRow>
                                            </AccordionDetails>
                                        </Accordion>
                                    )
                                }
                            })
                        }
                    </CCardBody>
                </CCard>
            )
        }
        else if (apptView == 'Week') {
            return (
                <>
                    <WeekViewer />
                </>
            )
        }

        else if (apptView == 'Month') {
            return (
                <>
                    <MonthViewer />
                </>
            )
        }
    }

    return (
        <form onSubmit={saveOrUpdatePatient} className="form-horizontal">
            <CCard color="grey">
                <CCardHeader>
                    <CRow>
                        <CCol xs="6" sm="6" md="3" lg="2" className="setMarginBottom">
                            <CButtonGroup>
                                <CButton color="dark">Department </CButton>
                                <CButton color="secondary">{props.obj.department}</CButton>
                            </CButtonGroup>
                        </CCol>
                        <CCol xs="6" sm="6" md="3" lg="2" className="setMarginBottom">
                            <CButtonGroup>
                                <CButton color="dark">Unit </CButton>
                                <CButton color="secondary">{props.obj.unit}</CButton>
                            </CButtonGroup>
                        </CCol>
                        <CCol md="4" lg="3">
                            <CRow>
                                <CCol align="right" className="setMarginBottom">
                                    <CButton color={appConstants.SAVE_BTN_COLOR} onClick={props.setPrevDate}>
                                        <FontAwesomeIcon icon={faChevronLeft} />
                                    </CButton>
                                </CCol>
                                <CCol xs="8" sm="8" md="8" lg="8" align="center" className="setMarginBottom">
                                    <KeyboardDatePickers
                                        name="appointment_date"
                                        disableToolbar
                                        format="DD-MMM-yyyy"
                                        content={props.obj.appointment_date}
                                        manadatorySymbol={false}
                                        controlFunc={props.controlFunc}
                                    />
                                </CCol>
                                <CCol className="setMarginBottom">
                                    <CButton color={appConstants.SAVE_BTN_COLOR} onClick={props.setNextDate}>
                                        <FontAwesomeIcon icon={faChevronRight} />
                                    </CButton>
                                </CCol>
                            </CRow>
                        </CCol>
                        <CCol xs="12" sm="12" md="6" lg="2" className="setMarginBottom">
                            <RadioButtonList
                                name="appt_selector"
                                controlFunc={handleChange}
                                options={appointmentConfig.APPOINTMENT_SELECTORS}
                                manadatorySymbol={false} />
                        </CCol>
                        <CCol xs="12" sm="12" md="6" lg="2" className="setMarginBottom">
                            <RadioButtonList
                                name="view"
                                options={appointmentConfig.APPOINTMENT_VIEW_SELECTORS}
                                controlFunc={e => { setApptView(e.target.value) }}
                                content={apptView}
                                manadatorySymbol={false} />
                        </CCol>
                        <CCol xs="12" sm="12" md="2" lg="1" className="setMarginBottom">
                            <SelectBox
                                controlFunc={handleChange}
                                options={["Doctor 1", "Doctor 2", "Doctor 3"]}
                                selectedOption="Doctor 1" />
                        </CCol>
                    </CRow>
                </CCardHeader>
            </CCard>
            {getComponentFor(apptView)}
        </form>
    );
}
export default StepTwo;