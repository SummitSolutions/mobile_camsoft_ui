import React, { useEffect, useState, useRef, useContext } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';
import { useParams} from "react-router";
 import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../form-components/SingleTextInput';
import NameSection from '../form-components/section-components/NameSection';
//import CheckboxList from '../form-components/CheckboxList';
import SelectBox from '../form-components/SelectBox';
import SimpleReactValidator from 'simple-react-validator';
import LocationService from '../../api/LocationService';
import * as appConstants from './../../AppConstants';
import moment from 'moment';
import GlobalState from './../../shared/GlobalState';

export default function CreateSubstanceuseComponent() {

    const [state, setState] = useContext(GlobalState);
    const history = useHistory();
    const params = useParams();
    const [substanceuse, setSubstanceuse] = useState({
        type_of_solvent:'', age:'', initial_amount:'', avg_session:'',
    });  
    const validator = useRef(new SimpleReactValidator());
    const handleChange = (e) => {
        
    }
 
    return (
        <div>
            <h1>FamilyHistory</h1> 
            <form>
                <CCard accentColor="primary">
                    <CCardHeader></CCardHeader>
                            <CCardBody>
                                
                                <CRow className="formMargins">    
                                    <CCol xs="12" md="10" lg="10">                                        
                                        <SingleTextInput name="new" 
                                        title="New" 
                                        inputType="text" 
                                        content={(substanceuse.new)?(substanceuse.new):''} 
                                        placeholder="New" 
                                        manadatorySymbol={false} 
                                        controlFunc={handleChange}/>
                                    </CCol>
                                </CRow>  
                              
                            </CCardBody> 
                </CCard>
            </form>
        </div>
    )
}                          