import React from 'react';
import PropTypes from 'prop-types';

const Divider = (props) => (
    <>
        <legend className="legend">
            <span className="legendLabel">{props.label}</span>
        </legend>
    </>
);

Divider.propTypes = {
    label: PropTypes.string
};

export default Divider;
