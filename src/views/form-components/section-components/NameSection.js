import React from 'react';
import { CCol, CRow } from '@coreui/react';
import SingleTextInput from '../../form-components/SingleTextInput';

const NameSection = (props) => (
    <div>
        <CRow className="formMargins">
            {/* <CCol xs="12" md="4" lg="4">
                <label className="labelstyle">First Name</label>
                <SingleTextInput
                    name="first_name"
                    title="First Name"
                    inputType="text"
                    content={(props.obj.first_name) ? (props.obj.first_name) : ''}
                    placeholder="First Name"
                    manadatorySymbol={true}
                    controlFunc={props.controlFunc}
                    variant="outlined" />
                {props.validators.current.message('first_name', props.obj.first_name, 'required|alpha_space')}
            </CCol> */}
            <CCol xs="12" md="4" lg="4">
                <label className="labelstyle">Middle Name</label>
                <SingleTextInput
                    name="middle_name"
                    title="Middle Name"
                    inputType="text"
                    content={(props.obj.middle_name) ? (props.obj.middle_name) : ''}
                    placeholder="Middle Name"
                    manadatorySymbol={false}
                    variant="outlined"
                    controlFunc={props.controlFunc} />
                {props.validators.current.message('middle_name', props.obj.middle_name, 'alpha_space')}
            </CCol>
            <CCol xs="12" md="4" lg="4">
                <label className="labelstyle">Last Name</label>
                <SingleTextInput
                    name="last_name"
                    title="Last Name"
                    inputType="text"
                    content={(props.obj.last_name) ? (props.obj.last_name) : ''}
                    placeholder="Last Name"
                    manadatorySymbol={true}
                    variant="outlined"
                    controlFunc={props.controlFunc} />
                {props.validators.current.message('last_name', props.obj.last_name, 'required|alpha_space')}
            </CCol>
        </CRow>
    </div>
);

export default NameSection;