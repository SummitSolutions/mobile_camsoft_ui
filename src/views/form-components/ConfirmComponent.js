import React from "react";
import { Button, Confirm } from "semantic-ui-react";
import { faCalendarCheck, faThumbsUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ConfirmComponent = (props) => {
    return (
        <div>
            <Button style={{ float: 'right' }} onClick={props.confirmBoxProps.handleButtonClick}><FontAwesomeIcon icon={faThumbsUp}/> &nbsp;&nbsp; Confirm &nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faCalendarCheck}/> </Button>
            <Confirm
                header={props.confirmBoxProps.confirmHeader}
                content={props.confirmBoxProps.confirmMsg}
                cancelButton={(props.confirmBoxProps.cancelButtonText) ? (props.confirmBoxProps.cancelButtonText) : 'Cancel'}
                confirmButton={(props.confirmBoxProps.confirmButtonText) ? (props.confirmBoxProps.confirmButtonText) : 'Ok'}
                open={props.confirmBoxProps.windowState}
                onCancel={props.confirmBoxProps.handleCancel}
                onConfirm={props.confirmBoxProps.handleConfirm}
                size={props.confirmBoxProps.windowSize}
            />
        </div>
    )

}

export default ConfirmComponent;
//for Confirm props Please reffer => 'https://react.semantic-ui.com/addons/confirm/' 