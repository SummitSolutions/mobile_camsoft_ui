import React from 'react';
import { CFormText, CLabel } from '@coreui/react';
import { TextField } from '@material-ui/core';
import { OutlinedInput } from '@material-ui/core';
import MandatoryIndicator from './MandatoryIndicator';
import * as appConstants from './../../AppConstants';

const SingleTextInput = (props) => (
    <div>
        <TextField
            id={appConstants.INPUT_FLOAT_LABEL.id}
            variant={(props.variant) ? props.variant : appConstants.INPUT_FLOAT_LABEL.variant}
            label={(props.manadatorySymbol) ? (<MandatoryIndicator title={props.title} />) : props.title}
            name={props.name}
            type={props.inputType}
            id={props.name}
            value={props.content}
            onChange={props.controlFunc}
            onBlur={props.controlBlurFunc ? props.controlBlurFunc : null}
            placeholder={props.placeholder}
            multiline={(props.multiline) ? true : false}
            rows={(props.rows) ? props.rows : 0}
            disabled={(props.disabled) ? true : false} />
        <CFormText>{props.helpText}</CFormText>
    </div>
);

export default SingleTextInput;