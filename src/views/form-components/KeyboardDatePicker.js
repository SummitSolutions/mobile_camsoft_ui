import React from 'react';
import { CFormText, CLabel } from '@coreui/react';
import MandatoryIndicator from './MandatoryIndicator';
import * as appConstants from './../../AppConstants';
import { KeyboardDatePicker, KeyboardTimePicker, KeyboardDateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';

const KeyboardDatePickers = (props) => (
    <div>
        <MuiPickersUtilsProvider utils={MomentUtils}>
            <KeyboardDatePicker
                id={appConstants.INPUT_FLOAT_LABEL.id}
                variant={"inline" + appConstants.INPUT_FLOAT_LABEL.variant}
                //disableToolbar
                format={props.format}
                name={props.name}
                margin="normal"
                label={(props.manadatorySymbol) ? (<MandatoryIndicator title={props.title} />) : props.title}
                value={props.content}
                onChange={props.controlFunc}
            />
            &nbsp;
        </MuiPickersUtilsProvider>
    </div>
);

export default KeyboardDatePickers;