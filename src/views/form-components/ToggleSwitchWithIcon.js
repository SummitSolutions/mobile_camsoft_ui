import React, { Component } from 'react';
import Switch from "react-switch";

const ToggleSwitchWithIcon = (props) => {
 
    return (
        <Switch
            name ={props.name}
            onChange={props.controlFunc}
            checked={props.content}
        />   
    );
}

export default ToggleSwitchWithIcon;