import React from 'react'
import { CButton } from '@coreui/react';
import CIcon from '@coreui/icons-react'
import * as appConstant from '../../AppConstants'

var buttonStyle = {
  marginLeft: '20px',
  minWidth: '100px'
};

const Button = (props) => (
  <>
    <CButton
      variant={props.variant ? appConstant.buttonOutlined : ''}
      color={props.color}
      shape={(props.shape === false) ? '' : appConstant.buttonShape}
      onClick={props.handleClick}
      style={props.buttonStyle ? props.buttonStyle : {}}
      title={props.title}
      disabled={props.isDisabled ? props.isDisabled : false}
      readOnly={props.isReadonly ? props.isReadonly : false}
    >
      {props.iconLeft &&
        <CIcon size="sm" name={props.iconName} />
      }
      {' ' + props.label ? props.label : '' + ' '}
      {props.iconRight &&
        <CIcon size="sm" name={props.iconName} />
      }

    </CButton>
  </>
);

export default Button;