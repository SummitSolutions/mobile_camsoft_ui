import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MandatoryIndicator from './MandatoryIndicator';
import * as appConstants from './../../AppConstants';

const SelectBox = (props) => (
    <div>
        <FormControl variant={appConstants.INPUT_FLOAT_LABEL.variant}>
            <InputLabel htmlFor={props.name}>
                {props.title}
                <span className="text-danger">
                    {props.manadatorySymbol ? ' *' : ''}
                </span>
            </InputLabel>
            <Select
                native
                id={appConstants.INPUT_FLOAT_LABEL.id}
                variant={appConstants.INPUT_FLOAT_LABEL.variant}
                name={props.name}
                value={props.selectedOption}
                id={props.name}
                onChange={props.controlFunc}
                onBlur={props.controlBlurFunc}
            >
                <option aria-label="None" value=""></option>
                {props.options.map(opt => {
                    return (
                        <option
                            key={opt}
                            value={opt}
                        >{opt}</option>
                    );
                })};
            </Select>
        </FormControl>
    </div>
);
export default SelectBox;