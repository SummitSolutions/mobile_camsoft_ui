import React, { useEffect, useState, useRef, useContext } from 'react'
import 'semantic-ui-css/semantic.min.css'
import { CCol, CRow, CLabel } from '@coreui/react';
import GlobalState from './../../shared/GlobalState';
import PatientService from '../../api/PatientService';
import _ from 'lodash';
import { useParams } from "react-router";

const PatientDetails = () => {
    const [state, setState] = useContext(GlobalState);
    const [patientDetails, setPatientDetails] = useState([]);
    const params = useParams();
    useEffect(() => {
        console.log("Patient Visit List page loaded. Msg. from UseEffect")
        console.log(state)
        getPatientVisitData();
        if (params.patient_id) {
            return () => {
                console.log('Clearing Patient Id from State')
                setState(state => ({ ...state, patient: {} }))
            }
        }
    }, []);

    const getPatientVisitData = () => {
        if (params.patient_id > 0) {
            PatientService.getPateintAndVisits(params.patient_id, state.authHeader).then(resp => {
                console.log(resp.data)

                let pvList = _.orderBy(resp.data.patientvisits, ['visit_date'], ['desc']);
                setPatientDetails(pvList);
                delete resp.data.patientvisits;

                console.log("Setting Patient Visit Data to state")
                setState(state => ({ ...state, patient: resp.data }))
                console.log("Patient Visit Data retrieved Successfully")


            });
        }
    }
    return (
        <div className="body-container">
            <fieldset >
                <legend className="scheduler-border"><h3 className="subHeading">Patient Info</h3></legend>
                <CRow>
                    {/* <CCol xs="6">
                        <CLabel htmlFor="name">Patient Name : <b>&nbsp;{state.patient.first_name}</b></CLabel>
                    </CCol> */}
                    {/* <CCol xs="6">
                         <CLabel htmlFor="name">UHID :  <b>&nbsp;{state.patient.uhid}</b></CLabel>
                    </CCol> */}

                    {/* <CCol xs="6">
                        <CLabel htmlFor="age">Gender : <b>&nbsp;{state.patientDetails.gender}</b></CLabel>
                    </CCol> */}
                    {/* <CCol xs="6">
                        <CLabel htmlFor="age">Age : <b>&nbsp;{state.patient.age}</b></CLabel>
                    </CCol> */}

                </CRow>
            </fieldset>
        </div>
    )
}



export default PatientDetails;