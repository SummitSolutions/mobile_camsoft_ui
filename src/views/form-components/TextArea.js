import React from 'react';
import { CFormText, CLabel } from '@coreui/react'

const TextArea = (props) => (
    <div>
        <CLabel htmlFor={props.name}>
            {props.title}
            <span className="text-danger">
                { props.manadatorySymbol ? ' *' : '' }
            </span>
        </CLabel>
        <textarea 
            className="form-control" 
            name={props.name} 
            id={props.name} 
            value={props.content} 
            style={props.resize ? null : {resize: 'none'}}
            rows={props.rows}
            onChange={props.controlFunc} 
            placeholder={props.placeholder} 
        />
        <CFormText>{ props.help_text }</CFormText>
    </div>
);

export default TextArea;