import React from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col, Button, Spinner } from 'react-bootstrap';
import { Plus } from 'react-bootstrap-icons';

export const AddButtonWithTitle = ({
    title,
    addNewEntityOnClick,
    spinnerActive,
}) => {

    const addNewButtonOnClick = () => {
        addNewEntityOnClick();
    }

    const getButtonChildren = () => {
        if (spinnerActive) {
            return (
                <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                    style={{ marginLeft: '-8px' }}
                />
            )
        } else {
            return <Plus style={{ marginLeft: '-8px' }} />
        }
    }

    return (
        <div>
            <Container className='p-2' fluid>
                <Row>
                    <Col xs={2} md={1} style={{ width: '50px' }}>
                        <Button
                            type='submit'
                            style={{ backgroundColor: '#43D4FF', color: 'white', fontSize: 15, width: '10px' }}
                            variant="outline-success"
                            onClick={() => { addNewButtonOnClick() }}
                        >
                            {getButtonChildren()}
                        </Button>
                    </Col>
                    <Col xs={10} md={11} className='p-0'>
                        <p className="display-6" style={{ color: '#5E5E5E' }}>{title}</p>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

AddButtonWithTitle.propTypes = {
    title: PropTypes.string.isRequired,
    addNewEntityOnClick: PropTypes.func.isRequired,
};