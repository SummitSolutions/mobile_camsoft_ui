import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';

const CheckboxList = (props) => (  
    <div>
        <InputLabel htmlFor={props.name}>
            {props.title}
            <span className="text-danger">
                { props.manadatorySymbol ? ' *' : '' }
            </span>
        </InputLabel>
        {props.options.map(opt => {
            return (
                <label key={opt} variant="custom-checkbox" inline="false">
                    &nbsp;&nbsp;
                    <input
                        name={props.name}
                        onChange={props.controlFunc}
                        value={opt}
                        checked={ props.selectedOptions.indexOf(opt) > -1 }
                        type="checkbox" />&nbsp;&nbsp;{opt}
                </label>
            );
        })}
    </div>
);

export default CheckboxList;