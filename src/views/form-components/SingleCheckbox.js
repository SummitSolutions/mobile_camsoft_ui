import React from "react";

const SingleCheckbox = (props) => {
    return (
        <>       
            <label variant="custom-checkbox" inline="true">
                &nbsp;
                <input
                    name={props.name}
                    onChange={props.controlFunc}
                    value={props.value}
                    checked={ props.content }
                    type="checkbox" 
                /> &nbsp;{props.title} 
            </label>
        </>
    );
}

export default SingleCheckbox;