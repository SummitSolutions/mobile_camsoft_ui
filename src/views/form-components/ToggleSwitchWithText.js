import React, { Component } from 'react';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { withStyles } from '@material-ui/core/styles';

const AntSwitch = withStyles((theme) => ({
    root: {
      width: 28,
      height: 16,
      padding: 0,
      display: 'flex',
    },
    switchBase: {
      padding: 2,
      color: theme.palette.grey[500],
      '&$checked': {
        transform: 'translateX(12px)',
        color: theme.palette.common.white,
        '& + $track': {
          opacity: 1,
          backgroundColor: theme.palette.primary.main,
          borderColor: theme.palette.primary.main,
        },
      },
    },
    thumb: {
      width: 12,
      height: 12,
      boxShadow: 'none',
    },
    track: {
      border: `1px solid ${theme.palette.grey[500]}`,
      borderRadius: 16 / 2,
      opacity: 1,
      backgroundColor: theme.palette.common.white,
    },
    checked: {},
  }))(Switch);
  
const ToggleSwitchWithText = (props) => {
 
    return (
        <Typography component="div">
          <Grid component="label" container alignItems="center" spacing={1}>
            <Grid item>{props.trueText}</Grid>
            <Grid item>
              <AntSwitch checked={props.content} onChange={props.controlBlurFunc} onBlur = {props.controlFunc} name={props.name} />
            </Grid>
        <Grid item>{props.falseText}</Grid>
          </Grid>
        </Typography> 
    );
}

export default ToggleSwitchWithText;