import React, {useCallback} from 'react';
import Dropzone from 'react-dropzone';
import { Icon, Label } from 'semantic-ui-react'
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CButton,
    CButtonGroup,
    CLabel
} from '@coreui/react';

class DragAndDropFileUpload extends React.Component {
    constructor() {
        super();
        this.onDrop = (files) => {

            this.setState({...this.state, files: (this.state.files.concat(files))})
            console.log(this.state.files)
        };
        this.state = {
            files: []
        };
    }
  
    render() {
        {/*
            Label Colors ["red","orange","yellow","olive","green","teal","blue","violet","purple","pink","brown","grey","black"]
        */}
        const files = this.state.files.map(file => (
            <Label as='a' color='grey' key={file.name} style={{margin: '10px 0px 0px 10px'}}>
                {file.name} - {file.size} bytes &nbsp;
                <Label.Detail><Icon color="red" name='delete'/></Label.Detail>
            </Label>
        ));

        return (
            <CCard accentColor="primary">
                <CCardHeader>
                    Drag and Drop multiple file uploader
                </CCardHeader>
                <CCardBody>
                    <Dropzone onDrop={this.onDrop}>
                        {({getRootProps, getInputProps, isDragActive}) => (
                            <section className="container">
                                <div {...getRootProps({className: 'dropzone'})} style={{ height: '120px', paddingTop: '5px', borderRadius: '5px', backgroundColor: '#ebedef', textAlign: 'center' }}>
                                    <input {...getInputProps()} />
                                    {
                                        isDragActive ?
                                            <CLabel>Drop the files here ...</CLabel> : 
                                            <div>
                                                <p>Drag 'n' drop some files here</p>
                                                <p>or </p>
                                                <p>click to <button className="btn btn-sm btn-info">Select</button> files</p>
                                            </div>
                                    }
                                </div>
                                <aside>
                                    <h4>Files</h4>
                                    <ul>{files}</ul>
                                </aside>
                            </section>
                        )}
                    </Dropzone>
                </CCardBody>
            </CCard>
        );
    }
}
  
export default DragAndDropFileUpload;