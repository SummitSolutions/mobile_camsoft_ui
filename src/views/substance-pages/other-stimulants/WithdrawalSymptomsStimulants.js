import React from 'react'
import {
  CCol,CRow,
} from '@coreui/react'
import SingleCheckbox from '../../form-components/SingleCheckbox';


const WithdrawalSymptomsStimulants = (props) => (
    <>
        <h3>Simple withdrawal</h3> 
            <CRow>
                    <CCol xs="12" md="4" lg="8">
                        <label><b>1. Psychological Symptoms</b></label>
                        <label>(Anxiety,Nervousness,Low mood)</label>
                    </CCol>
            </CRow>
            <CRow>
                    <CCol xs="5" md="4" lg="4">
                        <SingleCheckbox
                            name = 'psychological_symptoms_one'
                            type="checkbox" 
                            content={(props.obj.psychological_symptoms_one)?(props.obj.psychological_symptoms_one):''}
                            controlFunc={props.controlFunc} 
                            value='psychological_symptoms_one'                                          
                        />
                    </CCol> 
                </CRow>
                &nbsp;
                <CRow>
                    <CCol xs="12" md="4" lg="8">
                        <label><b>2. Physiological Symptoms</b></label>
                        <label>(Palpitations,Sweating,Appetite change)</label>
                    </CCol>
                </CRow>
                <CRow>
                    <CCol xs="5" md="4" lg="4">
                        <SingleCheckbox
                            name = 'psychological_symptoms_two'
                            type="checkbox"
                            content={(props.obj.psychological_symptoms_two)?(props.obj.psychological_symptoms_two):''}
                            controlFunc={props.controlFunc} 
                            value='psychological_symptoms_two'                                             
                        />
                    </CCol>
                </CRow>
    </>
)
export default WithdrawalSymptomsStimulants;