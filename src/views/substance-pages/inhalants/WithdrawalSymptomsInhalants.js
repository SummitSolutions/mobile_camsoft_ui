import React from 'react'
import {
  CCol,CRow,
} from '@coreui/react'
import SingleCheckbox from '../../form-components/SingleCheckbox';


const WithdrawalSymptomsInhalants = (props) => (
    <>
        <CRow>
                <CCol xs="9" md="8" lg="8">
                    <label> Nervousness/Anxiety</label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox
                        name = 'nervous_anxiety'
                        type="checkbox" 
                        content={(props.obj.nervous_anxiety)?(props.obj.nervous_anxiety):''}
                        controlFunc={props.controlFunc} 
                        value='nervous_anxiety'                                          
                    />
                </CCol>
                <CCol xs="9" md="8" lg="8">
                    <label> Irritability</label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox
                        name = 'irritability'
                        type="checkbox" 
                        content={(props.obj.irritability)?(props.obj.irritability):''}
                        controlFunc={props.controlFunc} 
                        value='irritability'                                          
                    />
                </CCol>
                <CCol xs="9" md="8" lg="8">
                    <label> Sleep difficulty</label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox
                        name = 'sleep_difficulty'
                        type="checkbox" 
                        content={(props.obj.sleep_difficulty)?(props.obj.sleep_difficulty):''}
                        controlFunc={props.controlFunc} 
                        value='sleep_difficulty'                                          
                    />
                </CCol>  
                <CCol xs="9" md="8" lg="8">
                <label> Depressed Mood</label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox
                        name = 'depressed_mood'
                        type="checkbox" 
                        content={(props.obj.depressed_mood)?(props.obj.depressed_mood):''}
                        controlFunc={props.controlFunc} 
                        value='depressed_mood'                                          
                    />
                </CCol>
                <CCol xs="9" md="8" lg="8">
                <label> Change in appetite</label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox
                        name = 'change_in_appetite'
                        type="checkbox" 
                        content={(props.obj.change_in_appetite)?(props.obj.change_in_appetite):''}
                        controlFunc={props.controlFunc} 
                        value='change_in_appetite'                                          
                    />
                </CCol>  
        </CRow>
    </>
)
export default WithdrawalSymptomsInhalants;