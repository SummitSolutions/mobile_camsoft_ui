import React from 'react'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CButton,
  CCol,CRow,
} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import ToggleSwitchWithText from '../../form-components/ToggleSwitchWithText';
import SingleTextInput from '../../form-components/SingleTextInput';
import SelectBox from '../../form-components/SelectBox';
import * as appConstants from './.././../../AppConstants';
import RadioButtonList from '../../form-components/RadioButtonList';
import SingleCheckbox from '../../form-components/SingleCheckbox';


const WithdrawalSymptomsOpioids = (props) => (
    <> 
            <CRow>
                    <CCol xs="7" md="4" lg="8">
                        <label>Insomnia</label>
                        
                    </CCol>
                    <CCol xs="5" md="4" lg="4">
                        <SingleCheckbox
                            name = 'insomnia'
                            content = {(props.obj.insomnia)?(props.obj.insomnia):''}
                            controlFunc = {props.controlFunc}
                            value = 'insomnia'
                            type="checkbox"                                           
                        />
                    </CCol> 
                </CRow>
                &nbsp;
                <CRow>
                    <CCol xs="7" md="4" lg="8">
                        <label>Restlessness</label>
                    </CCol>
                    <CCol xs="5" md="4" lg="4">
                        <SingleCheckbox
                            name = 'restlessness'
                            content = {(props.obj.restlessness)?(props.obj.restlessness):''}
                            controlFunc = {props.controlFunc}
                            value = 'restlessness'
                            type="checkbox"                                           
                        />
                    </CCol>
                </CRow>
                &nbsp;
                <CRow>
                    <CCol xs="7" md="4" lg="8">
                        <label>Rhinorrhea</label>
                    </CCol>
                    <CCol xs="5" md="4" lg="4">
                        <SingleCheckbox
                            name = 'rhinorrhea'
                            content = {(props.obj.rhinorrhea)?(props.obj.rhinorrhea):''}
                            controlFunc = {props.controlFunc}
                            value = 'rhinorrhea'
                            type="checkbox"                                            
                        />
                    </CCol>
                </CRow>
                &nbsp;
                <CRow>
                    <CCol xs="7" md="4" lg="8">
                        <label>Lacrimation</label>
                    </CCol>
                    <CCol xs="5" md="4" lg="4">
                        <SingleCheckbox
                            name = 'lacrimation'
                            controlFunc = {props.controlFunc}
                            content = {(props.obj.lacrimation)?(props.obj.lacrimation):''}
                            value = 'lacrimation'
                            type="checkbox"                                         
                        />
                    </CCol>
                </CRow>
                &nbsp;
                <CRow>
                    <CCol xs="7" md="4" lg="8">
                        <label>Pilo erection</label>
                    </CCol>
                    <CCol xs="5" md="4" lg="4">
                        <SingleCheckbox
                            name = 'pilo_erection'
                            controlFunc = {props.controlFunc}
                            content = {(props.obj.pilo_erection)?(props.obj.pilo_erection):''}
                            value = 'pilo_erection'
                            type="checkbox" 
                                                                        
                        />
                    </CCol>
                </CRow>
                &nbsp;
                <CRow>
                    <CCol xs="7" md="4" lg="8">
                        <label>Anxiety</label>
                    </CCol>
                    <CCol xs="5" md="4" lg="4">
                        <SingleCheckbox
                            name = 'anxiety'
                            controlFunc = {props.controlFunc}
                            content = {(props.obj.anxiety)?(props.obj.anxiety):''}
                            value = 'anxiety'
                            type="checkbox" 
                                                                       
                        />
                    </CCol>
                </CRow>
                &nbsp;
                <CRow>
                    <CCol xs="7" md="4" lg="8">
                        <label>Aches/Pains</label>
                    </CCol>
                    <CCol xs="5" md="4" lg="4">
                        <SingleCheckbox
                            name = 'aches_pains'
                            controlFunc = {props.controlFunc}
                            content = {(props.obj.aches_pains)?(props.obj.aches_pains):''}
                            value = 'aches_pains'
                            type="checkbox" 
                                                                        
                        />
                    </CCol>
                </CRow>
                &nbsp;
                <CRow>
                    <CCol xs="7" md="4" lg="8">
                        <label>Vomitting/Loose Motion</label>
                    </CCol>
                    <CCol xs="5" md="4" lg="4">
                        <SingleCheckbox
                            name = 'vomitting'
                            controlFunc = {props.controlFunc}
                            content = {(props.obj.vomitting)?(props.obj.vomitting):''}
                            value = 'vomitting'
                            type="checkbox" 
                                                                       
                        />
                    </CCol>
                </CRow>
    </>
)
export default WithdrawalSymptomsOpioids;