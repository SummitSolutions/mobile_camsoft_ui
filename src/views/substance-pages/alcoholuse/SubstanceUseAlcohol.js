import React from 'react'
import {
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CButton
} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import SingleTextInput from '../../form-components/SingleTextInput';
import SelectBox from '../../form-components/SelectBox';
import MultiSelect from '../../form-components/SelectBox';
import * as appConstants from './.././../../AppConstants';

const alcoholOptions = [
    { value: 'beer', label: 'Beer' },
    { value: 'wine', label: 'Wine' },
    { value: 'spirit', label: 'spirit' },
    { value: 'arrack', label: 'Arrack' },
    { value: 'country_liquor', label: 'Country liquor' },
    { value: 'vodka', label: 'Vodka' },
    { value: 'brandy', label: 'Brandy' },
    { value: 'rum', label: 'Rum' },
    { value: 'gin', label: 'Gin' },
    { value: 'toddy', label: 'Toddy' },
];

const SubstanceUseAlcohol = (props) => {
    return (
        <>
            <CRow className="formMargins">
                <CCol xs="12" md="8" lg="8">
                    <label>Alcohol type</label>
                    <Select
                        name='alcohol_type'
                        //ref={register}
                        closeMenuOnSelect={false}
                        components={props.animatedComponents}
                        isMulti
                        options={alcoholOptions}
                    //onChange={handleChangeMulti}
                    />
                </CCol>
                <CCol xs="6" md="3" lg="3">
                    <SingleTextInput name="age"
                        title="Age"
                        inputType="number"
                        content={(props.obj.age) ? (props.obj.age) : ''}
                        placeholder="Age"
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}
                    />
                </CCol>
                <CCol xs="6" md="3" lg="3">
                    <SingleTextInput name="initial_amount"
                        title="Initial amount"
                        inputType="text"
                        content={(props.obj.initial_amount) ? (props.obj.initial_amount) : ''}
                        placeholder="Units"
                        manadatorySymbol={false}
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc} />
                </CCol>
                <CCol xs="8" md="4" lg="4">
                    <SingleTextInput name="avg_session"
                        title="Avg Qty/Session/Day"
                        inputType="text"
                        content={(props.obj.avg_session) ? (props.obj.avg_session) : ''}
                        placeholder="Avg Qty/Session/Day"
                        manadatorySymbol={false}
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc} />
                </CCol>
                <CCol xs="4" md="4" lg="4">
                    <SelectBox
                        name="average_uom"
                        title=" "
                        placeholder=' '
                        controlFunc={props.controlFunc}
                        options={appConstants.ALCOHOL_UOM}
                        autosize={true}
                    />
                </CCol>
            </CRow>
            <CRow className="formMargins">
                <CCol xs="12" md="4" lg="6">
                    <SelectBox
                        name="frequency"
                        title="Frequency"
                        placeholder='Frequency'
                        controlFunc={props.controlFunc}
                        options={appConstants.FREQUENCY}
                        autosize={true}
                        manadatorySymbol={false} />
                </CCol>
                <CCol xs="12" md="4" lg="6">
                    <SingleTextInput name="last_use_quantity"
                        title="Last use quantity"
                        inputType="number"
                        content={(props.obj.last_use_quantity) ? (props.obj.last_use_quantity) : ''}
                        placeholder="Last use quantity"
                        manadatorySymbol={false}
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc} />
                </CCol>
                <CCol xs="12" md="4" lg="6">
                    <SingleTextInput name="duration_last_use"
                        title="Duration since last use"
                        inputType="number"
                        content={(props.obj.duration_last_use) ? (props.obj.duration_last_use) : ''}
                        placeholder="Duration since last use"
                        manadatorySymbol={false}
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc} />
                </CCol>
                <CCol xs="12" md="4" lg="6">
                    <SelectBox
                        name="duration"
                        title="Duration"
                        placeholder='Duration'
                        controlFunc={props.controlFunc}
                        options={appConstants.DURATION}
                        autosize={true}
                        manadatorySymbol={false} />
                </CCol>
            </CRow>
            <CRow className="formMargins">
                <CCol xs="12" md="4" lg="6">
                    <SingleTextInput name="drinking_days"
                        title="No of drinking days"
                        inputType="number"
                        content={(props.obj.drinking_days) ? (props.obj.drinking_days) : ''}
                        placeholder="Last month"
                        manadatorySymbol={true}
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc} />
                </CCol>
                <CCol xs="12" md="4" lg="6">
                    <SingleTextInput name="heavy_drinking_days"
                        title="No of heavy drinking days"
                        inputType="number"
                        content={(props.obj.heavy_drinking_days) ? (props.obj.heavy_drinking_days) : ''}
                        placeholder="Last month"
                        manadatorySymbol={true}
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc} />
                </CCol>
            </CRow>
        </>
    )
    /*
      <CRow className="formMargins">    
          {/*<CCol xs="12" md="4" lg="8">           
              <Select
                  name='alcohol_type' 
                  //ref={register}
                  closeMenuOnSelect={false}
                  components={props.animatedComponents}
                  isMulti
                  options={alcoholOptions}
                  onChange={handleChangeMulti}
              />
            </CCol>
      </CRow> 
    )*/
}
export default SubstanceUseAlcohol;
