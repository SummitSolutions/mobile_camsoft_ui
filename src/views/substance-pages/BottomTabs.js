import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import TopTabs from './TopTabs';
import GlobalState from '../../shared/GlobalState';
import { appBarClasses } from '@mui/material';
import * as appConstants from './../../AppConstants';
import CreatePatientComponent from '../patient/Create';
import SubstanceDetailsStepper from '../substance-details/SubstanceDetailsStepper';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabspanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={4}>
                    <Typography component={'div'}>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
}

export default function BottomTabs(props) {
    const [value, setValue] = React.useState(0);
    const [state, setState] = useContext(GlobalState);


    useEffect(() => {
        console.log("Bottom Tabs page loaded. Msg. from UseEffect")

        return () => {
            console.log('Clearing Bottom Tabs from State')
        }
    }, []);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <>
            {state.proforma_type === appConstants.PROFORMA_TYPE.follow_up &&
                <div className={'body-container patientVisitBottomTabPanels'}>
                    <TabPanel value={value} index={0}>
                        Current Functioning
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        Current Status
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                        Diagnosis
                    </TabPanel>
                    <TabPanel value={value} index={3}>
                        Followup Summary
                    </TabPanel>
                    <TabPanel value={value} index={4}>
                        Advise
                    </TabPanel>
                </div>
            }
            {state.proforma_type === appConstants.PROFORMA_TYPE.first_visit &&
                <div className={'body-container patientVisitBottomTabPanels'}>
                    <TabPanel value={value} index={0}>
                        <CreatePatientComponent />
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <SubstanceDetailsStepper />
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                        Diagnosis
                    </TabPanel>
                    <TabPanel value={value} index={3}>
                        Treatment Plan
                    </TabPanel>
                </div>
            }
            <div position="static">
                {state.proforma_type === appConstants.PROFORMA_TYPE.first_visit &&
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        variant="scrollable"
                        scrollButtons="auto"
                        indicatorColor="primary"
                        textColor="primary"
                        aria-label="scrollable auto tabs example"
                        className={'patientVisitBottomTabs'}
                    >
                        <Tooltip title="Demographic">
                            <Tab label={'Demographic'} aria-label="phone" {...a11yProps(0)} />
                        </Tooltip>
                        <Tooltip title="Substance Use">
                            <Tab label={'Substance Use'} aria-label="phone" {...a11yProps(1)} />
                        </Tooltip>
                        <Tooltip title="Diagnosis">
                            <Tab label={'Diagnosis'} aria-label="phone" {...a11yProps(2)} />
                        </Tooltip>
                        <Tooltip title="Treatment Plan">
                            <Tab label={'Treatment Plan'} aria-label="phone" {...a11yProps(3)} />
                        </Tooltip>
                    </Tabs>
                }
                {state.proforma_type === appConstants.PROFORMA_TYPE.follow_up &&
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        variant="scrollable"
                        scrollButtons="auto"
                        indicatorColor="primary"
                        textColor="primary"
                        aria-label="scrollable auto tabs example"
                        className={'patientVisitBottomTabs'}
                    >
                        <Tooltip title="Current Functioning">
                            <Tab label={'Current Functioning'} aria-label="phone" {...a11yProps(0)} />
                        </Tooltip>
                        <Tooltip title="Current Status">
                            <Tab label={'Current Status'} aria-label="phone" {...a11yProps(1)} />
                        </Tooltip>
                        <Tooltip title="Diagnosis">
                            <Tab label={'Diagnosis'} aria-label="phone" {...a11yProps(2)} />
                        </Tooltip>
                        <Tooltip title="Followup Summary">
                            <Tab label={'Followup Summary'} aria-label="phone" {...a11yProps(3)} />
                        </Tooltip>
                        <Tooltip title="Advise">
                            <Tab label={'Advise'} aria-label="phone" {...a11yProps(4)} />
                        </Tooltip>
                    </Tabs>
                }
            </div>
        </>
    );
}