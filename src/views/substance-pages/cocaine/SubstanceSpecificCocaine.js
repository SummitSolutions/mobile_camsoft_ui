import React from 'react'
import {
  CCard,
  CCol,CRow,
} from '@coreui/react'
import SingleTextInput from '../../form-components/SingleTextInput';
import SingleCheckbox from '../../form-components/SingleCheckbox';
import * as appConstants from './.././../../AppConstants';
import RadioButtonList from '../../form-components/RadioButtonList';

const SubstanceSpecificCocaine = (props) => (
    <>
         <CRow> 
            <CCol xs="9" md="4" lg="8">
                <label>Suspiciousness</label>
            </CCol> 
            <CCol xs="3" md="4" lg="4"> 
            <SingleCheckbox 
                controlFunc={props.controlFunc} 
                name='suspiciousness'
                title=''
                type="checkbox" 
                value='suspiciousness'
                content={(props.obj.suspiciousness)?(props.obj.suspiciousness):''}
            />
            </CCol> 
        </CRow>
        <CRow>
            <CCol xs="9" md="4" lg="8">
                <label>Overdose</label>
            </CCol>
            <CCol xs="3" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='overdose'
                    type="checkbox" 
                    value='overdose'
                    content={(props.obj.overdose)?(props.obj.overdose):''}
                /> 
            </CCol>
        </CRow>
        <CRow>
            <CCol xs="9" md="4" lg="8">
                <label>High Risk Behaviors</label>
            </CCol>
            <CCol xs="3" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='high_risk_behaviours'
                    type="checkbox" 
                    value='high_risk_behaviours'
                    content={(props.obj.high_risk_behaviours)?(props.obj.high_risk_behaviours):''}
                />  
            </CCol>
        </CRow>
        <CRow>
            <CCol xs="9" md="4" lg="8">
                <label>Gambling</label>
            </CCol>
            <CCol xs="3" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='gambling'
                    type="checkbox" 
                    value='gambling'
                    content={(props.obj.gambling)?(props.obj.gambling):''}
                />  
            </CCol>
        </CRow>
        <CRow>
            <CCol xs="9" md="4" lg="8">
                <label>Ever had sex with sex workers?</label>
            </CCol>
            <CCol xs="3" md="4" lg="4"> 
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='sex_with_sex_workers'
                    type="checkbox" 
                    value='sex_with_sex_workers'
                    content={(props.obj.sex_with_sex_workers)?(props.obj.sex_with_sex_workers):''}
                />   
            </CCol>
        </CRow>
        <CRow>
            <CCol xs="9" md="4" lg="8">
                <label>Hr Sexual Behaviors</label>
            </CCol>
            <CCol xs="3" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='hr_sexual_behaviors'
                    type="checkbox" 
                    value='hr_sexual_behaviors'
                    content={(props.obj.hr_sexual_behaviors)?(props.obj.hr_sexual_behaviors):''}
                />  
            </CCol>
        </CRow>
        <CRow>
            <CCol xs="9" md="4" lg="8">
                <label>Physical Injuries</label>

            </CCol>
            <CCol xs="3" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='physical_injuries'
                    type="checkbox" 
                    value='physical_injuries'
                    content={(props.obj.physical_injuries)?(props.obj.physical_injuries):''}
                />  
            </CCol>
        </CRow>
        {props.obj.physical_injuries &&
        <CCard>
            <CRow>
                <CCol xs="12" md="4" lg="12">  
                    <label>Severity</label>  
                </CCol>
                <CCol xs="12" md="4" lg="12">                           
                    <RadioButtonList 
                    name="severity"
                    title=" "
                    controlBlurFunc={props.controlBlurFunc}
                    controlFunc={props.controlFunc}
                    options={appConstants.SEVERITY}
                    content={props.obj.severity}
                    manadatorySymbol={false} />
                </CCol>
            </CRow>
        </CCard>}
    {props.obj.physical_injuries &&
        <CCard>
            <CRow>
                <CCol xs="12" md="4" lg="12">  
                    <label>Frequency</label>  
                </CCol>
                <CCol xs="12" md="4" lg="12">                           
                    <RadioButtonList 
                    name="physical_frequency"
                    title=" "
                    controlBlurFunc={props.controlBlurFunc}
                    controlFunc={props.controlFunc}
                    options={appConstants.PHYSICAL_FREQUENCY}
                    content={props.obj.physical_frequency}
                    manadatorySymbol={false} />
                </CCol>
            </CRow>
        </CCard>}
        <CRow>
            <CCol xs="9" md="4" lg="8">
                <label>Head Injuries</label>
            </CCol>
            <CCol xs="3" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='head_injuries'
                    type="checkbox" 
                    value='head_injuries'
                    content={(props.obj.head_injuries)?(props.obj.head_injuries):''}
                />   
            </CCol>
        </CRow>
        {props.obj.head_injuries &&
        <CCard>
            <CRow>
                <CCol xs="12" md="4" lg="12">  
                    <label>Severity</label>  
                </CCol>
                <CCol xs="12" md="4" lg="12">                           
                    <RadioButtonList 
                    name="head_injury"
                    title=" "
                    controlBlurFunc={props.controlBlurFunc}
                    controlFunc={props.controlFunc}
                    options={appConstants.HEAD_INJURY}
                    content={props.obj.head_injury}
                    manadatorySymbol={false} />
                </CCol>
            </CRow>
        </CCard>}
        <h3>Medical</h3>
        <CRow>
            <CCol xs="9" md="4" lg="8">
                <label>Deliberate Self Harm</label>
            </CCol>
            <CCol xs="3" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='self_harm'
                    type="checkbox" 
                    value='self_harm'
                    content={(props.obj.self_harm)?(props.obj.self_harm):''}
                /> 
            </CCol>
        </CRow>
        {props.obj.self_harm &&
        <CRow>
            <CCol xs="12" md="4" lg="12">  
                <label>Attempts</label>  
            </CCol>
            <CCol xs="12" md="4" lg="12">                           
                <RadioButtonList 
                name="attempts"
                title=" "
                controlBlurFunc={props.controlBlurFunc}
                controlFunc={props.controlFunc}
                options={appConstants.ATTEMPTS}
                content={props.obj.attempts}
                manadatorySymbol={false} />
            </CCol>
        </CRow>}
        {props.obj.self_harm &&
        <CRow>
            <CCol xs="12" md="4" lg="12">  
                <label>Last Attempt Intentionality</label>  
            </CCol>
            <CCol xs="12" md="4" lg="12">                           
                <RadioButtonList 
                name="last_attempt"
                title=" "
                controlBlurFunc={props.controlBlurFunc}
                controlFunc={props.controlFunc}
                options={appConstants.LAST_ATTEMPT}
                content={props.obj.last_attempt}
                manadatorySymbol={false} />
            </CCol>
        </CRow>} 
        <CRow>
            <CCol xs="12" md="4" lg="8">
                <SingleTextInput name="medical_others" 
                title="Medical others " 
                inputType="text" 
                content={(props.obj.medical_others)?(props.obj.medical_others):''} 
                placeholder="Medical Others" 
                manadatorySymbol={false} 
                controlBlurFunc={props.controlBlurFunc}
                controlFunc={props.controlFunc}
                />
            </CCol> 
        </CRow> 
        <CRow>
            <CCol xs="12" md="4" lg="8">
                <SingleTextInput name="suicide" 
                title="Suicide " 
                inputType="text" 
                content={(props.obj.suicide)?(props.obj.suicide):''} 
                placeholder="Suicide" 
                manadatorySymbol={false}
                controlBlurFunc={props.controlBlurFunc} 
                controlFunc={props.controlFunc}
                />
            </CCol> 
        </CRow>
        &nbsp; 
        <CRow>
            <CCol xs="9" md="4" lg="8">
                <label>Gastritics</label>
            </CCol>
            <CCol xs="3" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='gastritics'
                    type="checkbox" 
                    value='gastritics'
                    content={(props.obj.gastritics)?(props.obj.gastritics):''}
                />  
            </CCol>
        </CRow>
        &nbsp;                    
        <CRow>
            <CCol xs="9" md="4" lg="8">
                <label>Psychosis</label>
            </CCol>
            <CCol xs="3" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='psychosis'
                    type="checkbox" 
                    value='psychosis'
                    content={(props.obj.psychosis)?(props.obj.psychosis):''}
                /> 
            </CCol>
        </CRow>
        {props.obj.psychosis &&
        <CRow>
            <CCol xs="12" md="7" lg="7">
                <SingleTextInput name="describe" 
                title="Describe" 
                inputType="text" 
                content={(props.obj.describe)?(props.obj.describe):''} 
                placeholder="Describe" 
                manadatorySymbol={false} 
                controlBlurFunc={props.controlBlurFunc} 
                controlFunc={props.controlFunc} 
                multiline={true}
                rows={5}
                variant = "outlined"/>
            </CCol>
        </CRow>}
        <CRow>
            <CCol xs="9" md="4" lg="8">
                <label>Mood disorder</label>
            </CCol>
            <CCol xs="3" md="4" lg="4">
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='mood_disorder'
                    type="checkbox" 
                    value='mood_disorder'
                    content={(props.obj.mood_disorder)?(props.obj.mood_disorder):''}
                /> 
            </CCol>
        </CRow>
        {props.obj.mood_disorder &&
        <CRow>
            <CCol xs="12" md="7" lg="7">
                <SingleTextInput name="mood_disorder_value" 
                title="Mood Disorder Value" 
                inputType="text" 
                content={(props.obj.mood_disorder_value)?(props.obj.mood_disorder_value):''} 
                placeholder="Mood Disorder Value" 
                manadatorySymbol={false} 
                controlBlurFunc={props.controlBlurFunc}  
                controlFunc={props.controlFunc} 
                multiline={true}
                rows={5}
                variant = "outlined"/>
            </CCol>
    </CRow>
        }
    </>
)
export default SubstanceSpecificCocaine;