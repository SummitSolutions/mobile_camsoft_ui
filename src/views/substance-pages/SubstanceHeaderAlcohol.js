import React, { useContext, useEffect, useState } from 'react'
import {
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CButton,
    CCol, CRow,
} from '@coreui/react'
import { CCard, CCardBody, CCardFooter, CCardHeader } from '@coreui/react';
import Alcohol from '../substance-details/Alcohol';
import Caffeine from '../substance-details/Caffeine';
import Opioids from '../substance-details/Opioids';
import Cannabinioids from '../substance-details/Cannabinioids';
import SedativeHypnotics from '../substance-details/SedativeHypnotics';
import Cocaine from '../substance-details/Cocaine';
import OtherStimulants from '../substance-details/OtherStimulants';
import Hallucinogens from '../substance-details/Hallucinogens';
import Inhalants from '../substance-details/Inhalants';
import Tobacco from '../substance-details/Tobacco';
import AnyOtherSubstance from '../substance-details/AnyOtherSubstance';
import BehavioralAddiction from '../substance-details/BehavioralAddiction';
import { makeStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useHistory } from 'react-router-dom';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import makeAnimated from 'react-select/animated';
import SubstanceDetailService from 'src/api/SubstanceDetailService';
import { useParams } from "react-router";
import GlobalState from '../../shared/GlobalState';
import SubstanceUseService from 'src/api/SubstanceUseService';
import Substance from './Mainpage';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(0.5),
        },
    },
}));

const SubstanceHeader = (props) => {
    const classes = useStyles();
    const history = useHistory();
    const params = useParams();
    const [state, setState] = useContext(GlobalState);
    const [substanceUseList, setSubstanceUseList] = useState([]);
    const [page, setPage] = useState({ currentPage: 'Alcohol' });
    const [patientSubstanceDetails, setPatientSubstanceDetails] = useState({});
    const [menuItemList, setMenuItemList] = useState([]);


    useEffect(() => {
        getAllSubstanceUse();
    }, []);
    // const menuItemList = [
    //     //{'name':'Substance', 'title':'Substance Use'}, 
    //     //{'name':'Lifetime', 'title':'Life Time Pattern'}, 
    //     //{'name':'Withdrawal', 'title':'Withdrawal Symptoms'}, 
    //     //{'name' :'Specific', 'title':'Substance Specific'},
    //     //{'name':'Previous', 'title':'Previous Attempts'},
    //     //{'name':'Accordian', 'title':'Alcohol Substance details'},
    //     {'name':'Alcohol', 'title':'Alcohol'},
    //     {'name': 'Opioids','title':'Opioids'}
    // ];

    const gotoPage = (substance) => {
        setPage({ ...page, currentPage: substance });
    }

    const animatedComponents = makeAnimated();
    const substanceUseAlcoholOptions = [
        { value: 'beer', label: 'Beer' },
        { value: 'wine', label: 'Wine' },
        { value: 'spirit', label: 'spirit' },
        { value: 'arrack', label: 'Arrack' },
        { value: 'country_liquor', label: 'Country liquor' },
        { value: 'vodka', label: 'Vodka' },
        { value: 'brandy', label: 'Brandy' },
        { value: 'rum', label: 'Rum' },
        { value: 'gin', label: 'Gin' },
        { value: 'toddy', label: 'Toddy' },
    ];


    const handleBlur = (event) => {
        saveOrUpdateAlcohol(patientSubstanceDetails, event);
        console.log(patientSubstanceDetails)
    };


    const handleChange = (event) => {
        let substanceDetailObj = {};
        const { name, value } = event.target;
        console.log('auto saving function')
        if (event.target.type == 'checkbox') {
            console.log('checkbox')
            setPatientSubstanceDetails({ ...patientSubstanceDetails, [event.target.name]: (event.target.checked) ? (value) : '' });
            patientSubstanceDetails[event.target.name] = (event.target.checked) ? (value) : '';
        }
        else {
            console.log(event.target.name)
            setPatientSubstanceDetails({ ...patientSubstanceDetails, [event.target.name]: value });
            patientSubstanceDetails[event.target.name] = value;
        }
        if (event.target.type != 'text')
            saveOrUpdateAlcohol(patientSubstanceDetails, event);
        console.log("testing")
    };
    //Left button 
    const handleBack = () => {
        console.log(page)
        let currPageIndex = menuItemList.findIndex(item => item.substance_name == page.currentPage);
        console.log(currPageIndex)
        gotoPage(menuItemList[currPageIndex - 1])
    };
    //Right button 
    const handleNext = () => {
        console.log(page)
        let currPageIndex = menuItemList.findIndex(item => item.name == page.currentPage);
        console.log(currPageIndex)
        gotoPage(menuItemList[currPageIndex + 1])
    };

    //AutoSaveAlcohol
    const saveOrUpdateAlcohol = (data, event) => {
        console.log(data)
        if (data.id) {
            console.log('Alcohol data updating..: ', data)
            // let id = (params.patient_id) ? params.patient_id : data.patient_id
            SubstanceDetailService.updateSubstanceDetail(data.id, data, state.authHeader).then(resp => {
                console.log(resp)
                setPatientSubstanceDetails(resp.data);
            });
        }
        else {
            console.log("Alcohol data saving")
            data.patient_id = parseInt(params.patient_id);
            data.visit_id = parseInt(params.visit_id);
            data.patient_substance_id = page.currentPage.substance_use_list.id;
            console.log(data)
            SubstanceDetailService.createSubstanceDetail(data, state.authHeader).then(resp => {
                console.log(resp.data)
                setPatientSubstanceDetails(resp.data);
            });
        }
    }
    // patient substance use api
    const getAllSubstanceUse = () => {
        SubstanceUseService.getAllSubstanceUse(params.patient_id, params.visit_id, state.authHeader).then((resp) => {
            setSubstanceUseList(resp.data);
            checkSubstanceExistOrNot(resp.data);
        })
    }
    //Display in dropdown when substance is checked 
    const checkSubstanceExistOrNot = (dataList) => {
        let listItems = [];
        dataList.forEach(substance => {
            if (substance.substance_use_list?.current_use || substance.substance_use_list?.ever_use) {
                listItems.push(substance);
            }
        });
        setMenuItemList(listItems);
        if (listItems.length > 0) {
            page.currentPage = listItems[0];
            setPage({ ...page, currentPage: listItems[0] });
            getSubstanceDetail();
        }
    }

    // fetches Substance Details for Selected Substance (Alcohol, Opiods ...)
    const getSubstanceDetail = () => {
        SubstanceDetailService.getSubstanceDetailForSubstance(params.patient_id, params.visit_id, page.currentPage.substance_use_list.id, state.authHeader).then((resp) => {
            setPatientSubstanceDetails(resp.data)
        });
    }

    return (
        <>
            <CDropdown className="c-header-nav-items toolBarCss" direction="down" >
                <CRow>
                    <CCol xs="2" sm="2" md="1" lg="1" xl="1">
                        <CButton type="button" disabled={page.currentPage.substance_name === 'Alcohol'} color="light" onClick={handleBack}><FontAwesomeIcon icon={faAngleLeft} style={{ fontSize: "1.5rem" }} /></CButton>
                    </CCol>
                    <CCol xs="8" sm="8" md="10" lg="10" xl="10" align="center">
                        <CDropdownToggle className="c-header-nav-link" caret={false}>
                            <input type="button" value="Go to" className="btn-primary " style={{ width: '100%' }} />
                        </CDropdownToggle>
                        <CDropdownMenu className="pt-0" placement="bottom-end" >
                            <CDropdownItem
                                header
                                tag="div"
                                color="light"
                                className="text-center">
                                <strong></strong>
                            </CDropdownItem>
                            {
                                menuItemList?.map(element => {
                                    return (
                                        <CDropdownItem key={element.substance_name} onClick={() => { gotoPage(element) }}>
                                            &nbsp; {element.substance_name}
                                        </CDropdownItem>
                                    )
                                })
                            }
                        </CDropdownMenu>
                    </CCol>
                    <CCol xs="2" sm="2" md="1" lg="1" xl="1" align="right">
                        <CButton type="button" /*disabled={page.currentPage === 'Previous'}*/ onClick={handleNext} color="light" style={{ marginLeft: '-12px' }}><FontAwesomeIcon icon={faAngleRight} style={{ fontSize: "1.5rem" }} /></CButton>
                    </CCol>
                </CRow>
            </CDropdown>
            <CCard accentColor="primary">
                <CCardHeader></CCardHeader>
                <CCardBody>

                    {/* {
                    page.currentPage == 'Substance' &&
                        <SubstanceUseAlcohol  obj={substanceUseAlcohol} controlBlurFunc = {handleChangeSubstance}controlFunc = {handleChangeCheckboxSubstance} />
                    }
                    
                    {
                    page.currentPage == 'Lifetime' &&
                        <LifeTimePatternAlcohol  obj = {lifeTimeAlcohol} controlBlurFunc = {handleChangeLifetime} controlFunc = {handleChangeCheckboxLifetime}/>
                        
                    }
                    {
                    page.currentPage == 'Withdrawal' &&
                        <WithdrawalSymptomsAlcohol obj = {withdrawalAlcohol} controlBlurFunc = {handleChangeWithdrawal} controlFunc = {handleChangeCheckboxWithdrawal} />
                    }
                    {
                    page.currentPage == 'Specific' &&
                        <SubstanceSpecificAlcohol obj = {substanceSpecific} controlBlurFunc = {handleChangeSpecific}  controlFunc= {handleChangeCheckboxSpecific} />
                    }
                     {
                    page.currentPage == 'Previous' &&
                        <PreviousAttemptsAlcohol obj = {previousAttempts} controlBlurFunc = {handleChangePrevious} controlFunc = {handleChangeCheckboxPrevious} />
                    }*/}

                    {
                        page.currentPage.substance_name == 'Alcohol' &&
                        <Alcohol
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }
                    {
                        page.currentPage.substance_name == 'Caffeine' &&
                        <Caffeine
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }
                    {
                        page.currentPage.substance_name == 'Opioids' &&
                        <Opioids
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }
                    {
                        page.currentPage.substance_name == 'Cannabinioids' &&
                        <Cannabinioids
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }
                    {
                        page.currentPage.substance_name == 'Sedative/Hypnotics' &&
                        <SedativeHypnotics
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }
                    {
                        page.currentPage.substance_name == 'Cocaine' &&
                        <Cocaine
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }
                    {
                        page.currentPage.substance_name == 'Other Stimulants' &&
                        <OtherStimulants
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }
                    {
                        page.currentPage.substance_name == 'Hallucinogens' &&
                        <Hallucinogens
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }
                    {
                        page.currentPage.substance_name == 'Inhalants' &&
                        <Inhalants
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }
                    {
                        page.currentPage.substance_name == 'Tobacco' &&
                        <Tobacco
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }
                    {
                        page.currentPage.substance_name == 'Any Other Substance' &&
                        <AnyOtherSubstance
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }
                    {
                        page.currentPage.substance_name == 'Behavioral Addiction' &&
                        <BehavioralAddiction
                            obj={patientSubstanceDetails}
                            controlBlurFunc={handleBlur}
                            controlChangeFunc={handleChange} />
                    }


                </CCardBody>
            </CCard>
        </>
    )
}

export default SubstanceHeader;
