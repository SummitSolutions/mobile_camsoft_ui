import React from 'react'
import {
  CCol,CRow,
} from '@coreui/react'
import SingleCheckbox from '../../form-components/SingleCheckbox';


const WithdrawalSymptomsCannabinioids = (props) => (
    <>
        <CRow>
                <CCol xs="12" md="12" lg="12">
                    <label><b> Psychological Symptoms</b></label>
                    <label>(Abdominal Pain,Tremors,Sweating,Head ache)</label>
                    <SingleCheckbox
                        name = 'psychological_symptoms_cannabinioids'
                        type="checkbox" 
                        content={(props.obj.psychological_symptoms_cannabinioids)?(props.obj.psychological_symptoms_cannabinioids):''}
                        controlFunc={props.controlFunc} 
                        value='psychological_symptoms_cannabinioids'                                          
                    />
                </CCol>
                <CCol xs="9" md="8" lg="8">
                    <label><b> Restlessness</b></label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox
                        name = 'restlessness'
                        type="checkbox" 
                        content={(props.obj.restlessness)?(props.obj.restlessness):''}
                        controlFunc={props.controlFunc} 
                        value='restlessness'                                          
                    />
                </CCol>
                <CCol xs="9" md="8" lg="8">
                    <label><b> Nervousness/Anxiety</b></label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox
                        name = 'nervous_anxiety'
                        type="checkbox" 
                        content={(props.obj.nervous_anxiety)?(props.obj.nervous_anxiety):''}
                        controlFunc={props.controlFunc} 
                        value='nervous_anxiety'                                          
                    />
                </CCol>
                <CCol xs="9" md="8" lg="8">
                    <label><b> Irritability</b></label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox
                        name = 'irritability'
                        type="checkbox" 
                        content={(props.obj.irritability)?(props.obj.irritability):''}
                        controlFunc={props.controlFunc} 
                        value='irritability'                                          
                    />
                </CCol>
                <CCol xs="9" md="8" lg="8">
                    <label><b> Sleep difficulty</b></label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox
                        name = 'sleep_difficulty'
                        type="checkbox" 
                        content={(props.obj.sleep_difficulty)?(props.obj.sleep_difficulty):''}
                        controlFunc={props.controlFunc} 
                        value='sleep_difficulty'                                          
                    />
                </CCol>  
                <CCol xs="9" md="8" lg="8">
                <label><b> Depressed Mood</b></label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox
                        name = 'depressed_mood'
                        type="checkbox" 
                        content={(props.obj.depressed_mood)?(props.obj.depressed_mood):''}
                        controlFunc={props.controlFunc} 
                        value='depressed_mood'                                          
                    />
                </CCol>
                <CCol xs="9" md="8" lg="8">
                <label><b> Change in appetite</b></label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox
                        name = 'change_in_appetite'
                        type="checkbox" 
                        content={(props.obj.change_in_appetite)?(props.obj.change_in_appetite):''}
                        controlFunc={props.controlFunc} 
                        value='change_in_appetite'                                          
                    />
                </CCol>  
        </CRow>
               
    </>
)
export default WithdrawalSymptomsCannabinioids;