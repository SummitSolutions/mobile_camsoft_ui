import React, { useEffect, useState, useRef, useContext, Component } from 'react'
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from '@coreui/react';
import { useHistory } from 'react-router-dom';
import { useParams} from "react-router";
 import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../form-components/SingleTextInput';
import SelectBox from '../form-components/SelectBox';
import SimpleReactValidator from 'simple-react-validator';
import * as appConstants from './../../AppConstants';
import GlobalState from './../../shared/GlobalState';
import { Modal, Button } from "react-bootstrap";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { withStyles } from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';
import Switch from '@material-ui/core/Switch';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import RadioButtonList from '../form-components/RadioButtonList';
import CheckboxList from '../form-components/CheckboxList';
import ToggleSwitchWithText from './../form-components/ToggleSwitchWithText';
import Divider from '../form-components/Divider';


function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
       
      >
        {value === index && (
          <Box m={1} x= {0}>
            <Typography component={'div'}>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  
function a11yProps(index) {
    return {
      id: `scrollable-force-tab-${index}`,
      'aria-controls': `scrollable-force-tabpanel-${index}`,
      
    };
  }
  
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
  }));
  

export default function CreateAlcoholComponent() {
    //const [state, setState] = useContext(GlobalState);
    const history = useHistory();
    const params = useParams();
    const validator = useRef(new SimpleReactValidator());
    const [substanceList, setSubstanceList] =  useState(appConstants.ADDSUBSTANCE);
    const [showModal, setShow] = useState(true);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true); 
    const [alcohol, setAlcohol] = React.useState({
        type_of_solvent:'', age:'', initial_amount:'', avg_session:'',symptoms_a:'',head_injury:'',
        psychological_harm: false, loss_of_control: false,early_morning : false,
        tolerance: false, irresistible_urge: false, salience : false, interpersonal_problem  : false,
        physical_hazardous : false, major_role_obligations : false, procuring_substance : false,
        current_pattern: false, currently_present : false, past_occurance  : false,regular_antipileptic : false,
        currently_present_delerium : false,past_occurance_delerium : false,altered_sensorium: false,
        hallucinations: false,automatic_arousal: false,suspiciousness: false,wernicke_korsakoff: false,
        myopathies: false,overdose: false,high_risk_behaviours: false,gambling: false,wernicke_encephalopathy: false
        ,sex_with_sex_workers: false,hr_sexual_behaviors: false,physical_injuries: false,head_injuries: false,gastritics: false
        ,peripheral_neuritis: false,pancreatitis: false,alcoholic_liver_disease: false,self_harm: false,psychosis: false
      });
    
    const [symptoms, setSymptoms] = useState({
		selectedSymptoms : [],   selectedSeizure :[], selectedMedication :[],
        selectedAttempts:[], selectedAbstinence : []

    });
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const animatedComponents = makeAnimated();
    const handleChange = (event) => {
      setAlcohol({ ...alcohol, [event.target.name]: event.target.value });
        console.log(alcohol)
    };
    const handleToggleChange = (event) => {
        setAlcohol({...alcohol, [event.target.name]:event.target.checked});
    }
    const handleChangeTab =  (event, newValue) => {
        setValue(newValue);}
    const alcoholOptions = [
        { value: 'beer', label: 'Beer' },
        { value: 'wine', label: 'Wine' },
        { value: 'spirit', label: 'spirit' },
        { value: 'arrack', label: 'Arrack' },
        { value: 'country_liquor', label: 'Country liquor' },
        { value: 'vodka', label: 'Vodka' },
        { value: 'brandy', label: 'Brandy' },
        { value: 'rum', label: 'Rum' },
        { value: 'gin', label: 'Gin' },
        { value: 'toddy', label: 'Toddy'},
    ];
    
    const handleChangeMulti = selectedOption => {
            console.log(`Option selected:`, selectedOption);
        };

    const checkboxChangeHandler = (e) => {
        const newSelection = e.target.value;
        let newSelectionArray;
        switch(e.target.name)
        {
                case 'symptoms_a' :         if(symptoms.selectedSymptoms.indexOf(newSelection) > -1) {
                                                newSelectionArray = symptoms.selectedSymptoms.filter(s => s !== newSelection)
                                            } else {
                                                newSelectionArray = [...symptoms.selectedSymptoms, newSelection];
                                            }
                                            symptoms.selectedSymptoms = newSelectionArray;
                                            setAlcohol({ ...alcohol, [e.target.name]: symptoms.selectedSymptoms.join(", ")});
                                            break;
                case 'seizure_type' :       if(symptoms.selectedSeizure.indexOf(newSelection) > -1) {
                                                newSelectionArray = symptoms.selectedSeizure.filter(s => s !== newSelection)
                                            } else {
                                                newSelectionArray = [...symptoms.selectedSeizure, newSelection];
                                            }
                                            symptoms.selectedSeizure = newSelectionArray;
                                            setAlcohol({ ...alcohol, [e.target.name]: symptoms.selectedSeizure.join(", ")});
                                            break;
            case 'previous_medication' :    if(symptoms.selectedMedication.indexOf(newSelection) > -1) {
                                                newSelectionArray = symptoms.selectedMedication.filter(s => s !== newSelection)
                                            } else {
                                                newSelectionArray = [...symptoms.selectedMedication, newSelection];
                                            }
                                            symptoms.selectedMedication = newSelectionArray;
                                            setAlcohol({ ...alcohol, [e.target.name]: symptoms.selectedMedication.join(", ")});
                                            break;
             case 'previous_attempts' :     if(symptoms.selectedAttempts.indexOf(newSelection) > -1) {
                                                newSelectionArray = symptoms.selectedAttempts.filter(s => s !== newSelection)
                                            } else {
                                                newSelectionArray = [...symptoms.selectedAttempts, newSelection];
                                            }
                                            symptoms.selectedAttempts = newSelectionArray;
                                            setAlcohol({ ...alcohol, [e.target.name]: symptoms.selectedAttempts.join(", ")});
                                            break;                                                         
            case 'medication_abstinence' :  if(symptoms.selectedAbstinence.indexOf(newSelection) > -1) {
                                                newSelectionArray = symptoms.selectedAbstinence.filter(s => s !== newSelection)
                                            } else {
                                                newSelectionArray = [...symptoms.selectedAbstinence, newSelection];
                                            }
                                            symptoms.selectedAbstinence = newSelectionArray;
                                            setAlcohol({ ...alcohol, [e.target.name]: symptoms.selectedAbstinence.join(", ")});
                                            break;   
                                            
        }
    }       
return (
            <Modal show={showModal} onHide={handleClose}>
                    <Modal.Header closeButton>   
                    </Modal.Header>
                    <Modal.Body>
                        <div className={classes.root}>
                            <AppBar position="static" color="default">
                                <Tabs
                                value={value}
                                onChange={handleChangeTab}
                                variant="scrollable"
                                scrollButtons="on"
                                indicatorColor="primary"
                                textColor="primary"
                                aria-label="scrollable force tabs example"
                                >
                                <Tab label="Substance use" {...a11yProps(0) }/>
                                <Tab label="Life time pattern"  {...a11yProps(1)}/>
                                <Tab label="Withdrawal symtoms" {...a11yProps(2)} />
                                <Tab label="Substance specific"  {...a11yProps(3)} />
                                <Tab label="Previous attempts"  {...a11yProps(4)} />
                                </Tabs>
                            </AppBar>
                            {/*1.SUBSTANCE USE TAB */}
                            <TabPanel value={value} index={0}>
                                    <div>
                                        <form>
                                            <CCard accentColor="primary">
                                                <CCardHeader>  Substance </CCardHeader>
                                                    <CCardBody>
                                                        <CRow className="formMargins">    
                                                            <CCol xs="12" md="4" lg="8">           
                                                                <Select
                                                                    name='colors' 
                                                                    //ref={register}
                                                                    closeMenuOnSelect={false}
                                                                    components={animatedComponents}
                                                                    isMulti
                                                                    options={alcoholOptions}
                                                                    onChange={handleChangeMulti}
                                                                />
                                                            </CCol>
                                                        </CRow> 
                                                        <CRow className="formMargins">   
                                                            <CCol xs="6" md="4" lg="8">                                        
                                                                <SingleTextInput name="age" 
                                                                title="Age at first use" 
                                                                inputType="text" 
                                                                content={(alcohol.age)?(alcohol.age):''} 
                                                                placeholder="Age" 
                                                                manadatorySymbol={false} 
                                                                controlFunc={handleChange}/>
                                                            </CCol>
                                                            <CCol xs="6" md="4" lg="8">                                        
                                                                <SingleTextInput name="initial_amount" 
                                                                title="Initial amount" 
                                                                inputType="text" 
                                                                content={(alcohol.initial_amount)?(alcohol.initial_amount):''} 
                                                                placeholder="Units" 
                                                                manadatorySymbol={false} 
                                                                controlFunc={handleChange}/>
                                                            </CCol>
                                                        </CRow>
                                                        <CRow className="formMargins">    
                                                            <CCol xs="12" md="4" lg="8">                                        
                                                                <SingleTextInput name="avg_session" 
                                                                title="Avg Qty/Session/Day" 
                                                                inputType="text" 
                                                                content={(alcohol.avg_session)?(alcohol.avg_session):''} 
                                                                placeholder="Avg Qty/Session/Day " 
                                                                manadatorySymbol={false} 
                                                                controlFunc={handleChange}/>
                                                            </CCol>
                                                        </CRow>  
                                                        <CRow className="formMargins">    
                                                            <CCol xs="12" md="4" lg="8">           
                                                                <SelectBox
                                                                name="frequency"
                                                                title="Frequency"
                                                                placeholder='Frequency'
                                                                controlFunc={handleChange}
                                                                options={appConstants.FREQUENCY}
                                                                autosize={true}
                                                                manadatorySymbol={true}/>
                                                            </CCol>
                                                        </CRow>
                                                        <CRow className="formMargins">    
                                                            <CCol xs="12" md="4" lg="8">                                        
                                                                <SingleTextInput name="last_use" 
                                                                title="Last use quantity" 
                                                                inputType="text" 
                                                                content={(alcohol.last_use)?(alcohol.last_use):''} 
                                                                placeholder="Last use quantity" 
                                                                manadatorySymbol={false} 
                                                                controlFunc={handleChange}/>
                                                            </CCol>
                                                        </CRow>
                                                        <CRow className="formMargins">    
                                                            <CCol xs="12" md="4" lg="8">                                        
                                                                <SingleTextInput name="duration_last_use" 
                                                                title="Duration since last use" 
                                                                inputType="text" 
                                                                content={(alcohol.duration_last_use)?(alcohol.duration_last_use):''} 
                                                                placeholder="Duration since last use" 
                                                                manadatorySymbol={false} 
                                                                controlFunc={handleChange}/>
                                                            </CCol>
                                                        </CRow>
                                                        <CRow className="formMargins">    
                                                            <CCol xs="12" md="4" lg="8">                                        
                                                                <SingleTextInput name="duration_last_use" 
                                                                title="Duration since last use" 
                                                                inputType="text" 
                                                                content={(alcohol.duration_last_use)?(alcohol.duration_last_use):''} 
                                                                placeholder="Duration since last use" 
                                                                manadatorySymbol={false} 
                                                                controlFunc={handleChange}/>
                                                            </CCol>
                                                        </CRow>   
                                                        <CRow className="formMargins">    
                                                            <CCol xs="12" md="4" lg="8">                                        
                                                                <SingleTextInput name="drinking_days" 
                                                                title="No of drinking days" 
                                                                inputType="text" 
                                                                content={(alcohol.drinking_days)?(alcohol.drinking_days):''} 
                                                                placeholder="Last month" 
                                                                manadatorySymbol={true} 
                                                                controlFunc={handleChange}/>
                                                            </CCol>
                                                        </CRow> 
                                                        <CRow className="formMargins">    
                                                            <CCol xs="12" md="4" lg="8">                                        
                                                                <SingleTextInput name="heavy_drinking_days" 
                                                                title="No of heavy drinking days" 
                                                                inputType="text" 
                                                                content={(alcohol.heavy_drinking_days)?(alcohol.heavy_drinking_days):''} 
                                                                placeholder="Last month" 
                                                                manadatorySymbol={true} 
                                                                controlFunc={handleChange}/>
                                                            </CCol>
                                                        </CRow>              
                                                    </CCardBody> 
                                            </CCard>
                                        </form>
                                    </div>
                            </TabPanel>
                            {/*2.LIFE TIME PATTERN TAB*/ }
                            <TabPanel value={value} index={1}>
                                <FormGroup>
                                    <CRow> 
                                        <CCol xs="7" md="4" lg="8">
                                            <label>1. Continued use despite Physical / Psychological harm</label>
                                        </CCol> 
                                        <CCol xs="5" md="4" lg="4"> 
                                            <ToggleSwitchWithText
                                                name = 'psychological_harm'
                                                content = {alcohol.psychological_harm?true:false}
                                                controlFunc = {handleToggleChange}
                                                trueText = "No"
                                                falseText = "Yes"                                            
                                            />
                                        </CCol> 
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="7" md="4" lg="8">
                                        <label>2.Loss of control:Use after deciding not to or use more than intended ?</label>
                                        </CCol>
                                        <CCol xs="5" md="4" lg="4">
                                            <ToggleSwitchWithText
                                                name = 'loss_of_control'
                                                content = {alcohol.loss_of_control?true:false}
                                                controlFunc = {handleToggleChange}
                                                trueText = "No"
                                                falseText = "Yes"                                            
                                            />
                                       </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="7" md="4" lg="8">
                                            <label>3.Early Morning: Ever needed/ taken just after getting up in the morning</label>
                                        </CCol>
                                        <CCol xs="5" md="4" lg="4">
                                            <ToggleSwitchWithText
                                                name = 'early_morning'
                                                content = {alcohol.early_morning?true:false}
                                                controlFunc = {handleToggleChange}
                                                trueText = "No"
                                                falseText = "Yes"                                            
                                            />
                                        </CCol> 
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="7" md="4" lg="8">
                                            <label>4.Tolerance:50% more to give same effect</label>
                                        </CCol>
                                        <CCol xs="5" md="4" lg="4">
                                            <ToggleSwitchWithText
                                                name = 'tolerance'
                                                content = {alcohol.tolerance?true:false}
                                                controlFunc = {handleToggleChange}
                                                trueText = "No"
                                                falseText = "Yes"                                            
                                            />
                                        </CCol> 
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="7" md="4" lg="8">
                                            <label>5.Craving :Irresistible urge /Strong desire for alcohol /drugs –could not manage without it – could not think of anything else</label>
                                        </CCol>
                                        <CCol xs="5" md="4" lg="8">
                                            <ToggleSwitchWithText
                                                name = 'irresistible_urge'
                                                content = {alcohol.irresistible_urge?true:false}
                                                controlFunc = {handleToggleChange}
                                                trueText = "No"
                                                falseText = "Yes"                                            
                                            />
                                        </CCol> 
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="7" md="4" lg="8">
                                            <label>6.Salience : Given up / greatly reduced important activities while using / recovering from effects for several days or more</label>
                                        </CCol>
                                        <CCol xs="5" md="4" lg="8">
                                            <ToggleSwitchWithText
                                                name = 'salience'
                                                content = {alcohol.salience?true:false}
                                                controlFunc = {handleToggleChange}
                                                trueText = "No"
                                                falseText = "Yes"                                            
                                            />
                                        </CCol> 
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="7" md="4" lg="8">
                                            <label>7.Continued Use Despite Social Interpersonal Problem:</label>
                                        </CCol>
                                        <CCol xs="5" md="4" lg="4">
                                            <ToggleSwitchWithText
                                                name = 'interpersonal_problem'
                                                content = {alcohol.interpersonal_problem?true:false}
                                                controlFunc = {handleToggleChange}
                                                trueText = "No"
                                                falseText = "Yes"                                            
                                            />
                                        </CCol> 
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="7" md="4" lg="8">
                                            <label>8.Use of Substance in Physical Hazardous Situation</label>
                                        </CCol>
                                        <CCol xs="5" md="4" lg="4">
                                            <ToggleSwitchWithText
                                                name = 'physical_hazardous'
                                                content = {alcohol.physical_hazardous?true:false}
                                                controlFunc = {handleToggleChange}
                                                trueText = "No"
                                                falseText = "Yes"                                            
                                            />
                                        </CCol> 
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="7" md="4" lg="8">
                                            <label>9.Failure to Fulfill Major Role Obligations</label> 
                                        </CCol>
                                        <CCol xs="5" md="4" lg="4">
                                            <ToggleSwitchWithText
                                                name = 'major_role_obligations'
                                                content = {alcohol.major_role_obligations?true:false}
                                                controlFunc = {handleToggleChange}
                                                trueText = "No"
                                                falseText = "Yes"                                            
                                            />
                                        </CCol> 
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="7" md="4" lg="8">
                                            <label>10. Majority of Time Spent in Procuring Substance</label>
                                        </CCol>
                                        <CCol xs="5" md="4" lg="4">
                                            <ToggleSwitchWithText
                                                name = 'procuring_substance'
                                                content = {alcohol.procuring_substance?true:false}
                                                controlFunc = {handleToggleChange}
                                                trueText = "No"
                                                falseText = "Yes"                                            
                                            />
                                        </CCol> 
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="7" md="4" lg="8">
                                            <label>11.Current Pattern of Dependent Use:</label>
                                        </CCol>
                                        <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                                name = 'current_pattern_dependent'
                                                content = {alcohol.current_pattern_dependent?true:false}
                                                controlFunc = {handleToggleChange}
                                                trueText = "No"
                                                falseText = "Yes"                                            
                                            />
                                        </CCol> 
                                    </CRow>
                                    {alcohol.current_pattern_dependent &&
                                    <CRow className="formMargins">    
                                        <CCol xs="12" md="4" lg="8">                                        
                                            <SingleTextInput name="age_of_dep" 
                                            title="Age of Dependence" 
                                            inputType="text" 
                                            content={(alcohol.age_of_dep)?(alcohol.age_of_dep):''} 
                                            placeholder="Age of Dependence" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}/>
                                        </CCol>
                                    </CRow> }
                                    <br></br>
                                    <CRow>
                                        <CCol xs="12" md="4" lg="12">  
                                            <label>12.CURRENT PATTERN(12 months)</label>  
                                        </CCol>
                                        <CCol xs="12" md="4" lg="12">                           
                                            <RadioButtonList 
                                            name="current_pattern"
                                            title=" "
                                            controlFunc={ handleChange }
                                            options={appConstants.CURRENT_PATTERN}
                                            content={alcohol.current_pattern}
                                            manadatorySymbol={false} />
                                        </CCol>
                                    </CRow>
                                </FormGroup>
                            </TabPanel>
                            {/*3. WITHDRAWAL SYMPTOMS TAB*/}
                            <TabPanel value={value} index={2}>
                                <h3>Simple withdrawal</h3> 
                                <CRow>
                                    <CCol xs="6" md="4" lg="8">
                                        <label>Currently Present</label>
                                    </CCol>
                                    <CCol xs="6" md="4" lg="8">
                                        <label>Past Occurence</label>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'currently_present'
                                            content = {alcohol.currently_present?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol> 
                                    <CCol xs="6" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'past_occurance'
                                            content = {alcohol.past_occurance?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol> 
                                    
                                </CRow> 
                                <CRow>
                                    <CCol xs="8" md="4" lg="8">
                                        <label><b>Symptoms A</b></label>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6" md="4" lg="12">        
                                        <CheckboxList 
                                        name="symptoms_a"
                                        title=" "
                                        controlFunc={checkboxChangeHandler}
                                        options={appConstants.SYMPTOMS}
                                        selectedOptions={symptoms.selectedSymptoms}
                                        manadatorySymbol={false}/>
                                    </CCol>
                                    <CCol xs="6" md="4" lg="12"> 
                                        <CheckboxList 
                                        name="symptoms_a"
                                        title=" "
                                        controlFunc={checkboxChangeHandler}
                                        options={appConstants.SYMPTOMS_ONE}
                                        selectedOptions={symptoms.selectedSymptoms}
                                        manadatorySymbol={false}/>
                                    </CCol>
                                </CRow>
                                <br></br>
                                <CRow>
                                    <CCol xs="8" md="4" lg="8">
                                        <label><b>Seizure Type</b></label>
                                    </CCol>
                                </CRow> 
                                <CRow>
                                    <CCol xs="12" md="4" lg="12">                        
                                        <CheckboxList 
                                        name="seizure_type"
                                        title=" "
                                        controlFunc={checkboxChangeHandler}
                                        options={ appConstants.SEIZURE_TYPE }
                                        selectedOptions={symptoms.selectedSeizure }
                                        manadatorySymbol={false} />
                                    </CCol>
                                    {symptoms.selectedSeizure.includes('Other') &&
                                    <CCol xs="6" md="4" lg="12">                                        
                                        <SingleTextInput name="other" 
                                        title="Other" 
                                        inputType="text" 
                                        content={(alcohol.other)?(alcohol.other):''} 
                                        placeholder="Other" 
                                        manadatorySymbol={false} 
                                        controlFunc={handleChange}/>
                                    </CCol>}
                                </CRow>
                                <br></br>
                                <CRow>
                                    <CCol xs="8" md="4" lg="8">
                                        <label>On Regular Antipileptic</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'regular_antipileptic'
                                            content = {alcohol.regular_antipileptic?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol> 
                                </CRow>
                                <br></br>
                                <CRow>
                                    <CCol xs="4" md="4" lg="8">
                                        <h3>DELIRIUM</h3>
                                    </CCol>     
                                    <CCol xs="8" md="4" lg="8">
                                        <SingleTextInput name="delirium" 
                                        inputType="text" 
                                        content={(alcohol.delirium)?(alcohol.delirium):''} 
                                        placeholder=" " 
                                        manadatorySymbol={false} 
                                        controlFunc={handleChange}
                                        variant = "outlined"/>
                                    </CCol> 
                                </CRow> 
                                <CRow>
                                    <CCol xs="6" md="4" lg="8">
                                        <label>Currently Present</label>
                                    </CCol>
                                    <CCol xs="6" md="4" lg="8">
                                        <label>Past Occurence</label>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'currently_present_delerium'
                                            content = {alcohol.currently_present_delerium?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol>
                                    <CCol xs="6" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'past_occurance_delerium'
                                            content = {alcohol.past_occurance_delerium?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol> 
                                </CRow>  
                                <CRow>
                                    <CCol xs="6" md="4" lg="8">
                                        <label>Altered Sensorium</label>
                                    </CCol>
                                    <CCol xs="6" md="4" lg="8">
                                        <label>Hallucinations</label>
                                    </CCol>
                                </CRow> 
                                <CRow>
                                    <CCol xs="6" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'altered_sensorium'
                                            content = {alcohol.altered_sensorium?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />  
                                    </CCol> 
                                    <CCol xs="6" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'hallucinations'
                                            content = {alcohol.hallucinations?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol> 
                                </CRow>  
                                <br></br>
                                <CRow>
                                    <CCol xs="6" md="4" lg="8">
                                        <SingleTextInput name="age_of_fo" 
                                        title="Age of FO" 
                                        inputType="text" 
                                        content={(alcohol.age_of_fo)?(alcohol.age_of_fo):''} 
                                        placeholder="Age of FO" 
                                        manadatorySymbol={false} 
                                        controlFunc={handleChange}
                                        />
                                    </CCol> 
                                    <CCol xs="6" md="4" lg="8">
                                        <SingleTextInput name="age_of_lo" 
                                        title="Age of LO" 
                                        inputType="text" 
                                        content={(alcohol.age_of_lo)?(alcohol.age_of_lo):''} 
                                        placeholder="Age of LO" 
                                        manadatorySymbol={false} 
                                        controlFunc={handleChange}
                                        />
                                    </CCol> 
                                </CRow>            
                                <CRow>
                                    <CCol xs="8" md="4" lg="8">
                                        <label>Autonomic Arousal</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'automatic_arousal'
                                            content = {alcohol.automatic_arousal?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol> 
                                </CRow> 
                            </TabPanel>
                            {/*4. SUBSTANCE SPECIFIC COMPLICATION*/}
                            <TabPanel value={value} index={3}>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Suspiciousness</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'suspiciousness'
                                            content = {alcohol.suspiciousness?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Wernicke Korsakoff</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'wernicke_korsakoff'
                                            content = {alcohol.wernicke_korsakoff?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Myopathies</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'myopathies'
                                            content = {alcohol.myopathies?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Overdose</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'overdose'
                                            content = {alcohol.overdose?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        /> 
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>High Risk Behaviors</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'high_risk_behaviours'
                                            content = {alcohol.high_risk_behaviours?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        /> 
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Gambling</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'gambling'
                                            content = {alcohol.gambling?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />  
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Wernicke Encephalopathy</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'wernicke_encephalopathy'
                                            content = {alcohol.wernicke_encephalopathy?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />  
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Ever had sex with sex workers?</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'sex_with_sex_workers'
                                            content = {alcohol.sex_with_sex_workers?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />   
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Hr Sexual Behaviors</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'hr_sexual_behaviors'
                                            content = {alcohol.hr_sexual_behaviors?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        /> 
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Physical Injuries</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'physical_injuries'
                                            content = {alcohol.physical_injuries?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        /> 
                                    </CCol>
                                </CRow>
                               
                                {alcohol.physical_injuries &&
                                 <CCard>
                                <CRow>
                                   
                                    <CCol xs="12" md="4" lg="12">  
                                        <label>Severity</label>  
                                    </CCol>
                                    <CCol xs="12" md="4" lg="12">                           
                                        <RadioButtonList 
                                        name="severity"
                                        title=" "
                                        controlFunc={handleChange}
                                        options={appConstants.SEVERITY}
                                        content={alcohol.severity}
                                        manadatorySymbol={false} />
                                    </CCol>
                                </CRow>
                                </CCard>}
                            {alcohol.physical_injuries &&
                            <CCard>
                                <CRow>
                                    <CCol xs="12" md="4" lg="12">  
                                        <label>Frequency</label>  
                                    </CCol>
                                    <CCol xs="12" md="4" lg="12">                           
                                        <RadioButtonList 
                                        name="physical_frequency"
                                        title=" "
                                        controlFunc={handleChange}
                                        options={appConstants.PHYSICAL_FREQUENCY}
                                        content={alcohol.physical_frequency}
                                        manadatorySymbol={false} />
                                    </CCol>
                                </CRow>
                                </CCard>}
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Head Injuries</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'head_injuries'
                                            content = {alcohol.head_injuries?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        /> 
                                    </CCol>
                                </CRow>
                                
                                {alcohol.head_injuries &&
                                <CCard>
                                <CRow>
                                    <CCol xs="12" md="4" lg="12">  
                                        <label>Severity</label>  
                                    </CCol>
                                    <CCol xs="12" md="4" lg="12">                           
                                        <RadioButtonList 
                                        name="head_injury"
                                        title=" "
                                        controlFunc={handleChange}
                                        options={appConstants.HEAD_INJURY}
                                        content={alcohol.head_injury}
                                        manadatorySymbol={false} />
                                    </CCol>
                                </CRow>
                                </CCard>}
                                <h3>Medical</h3>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Gastritics</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'gastritics'
                                            content = {alcohol.gastritics?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        /> 
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Peripheral Neuritis</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'peripheral_neuritis'
                                            content = {alcohol.peripheral_neuritis?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Pancreatitis</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'pancreatitis'
                                            content = {alcohol.pancreatitis?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Alcholic Liver Disease</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'alcoholic_liver_disease'
                                            content = {alcohol.alcoholic_liver_disease?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Deliberate Self Harm</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'self_harm'
                                            content = {alcohol.self_harm?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol>
                                </CRow>
                                {alcohol.self_harm &&
                                <CRow>
                                    <CCol xs="12" md="4" lg="12">  
                                        <label>Attempts</label>  
                                    </CCol>
                                    <CCol xs="12" md="4" lg="12">                           
                                        <RadioButtonList 
                                        name="attempts"
                                        title=" "
                                        controlFunc={handleChange}
                                        options={appConstants.ATTEMPTS}
                                        content={alcohol.attempts}
                                        manadatorySymbol={false} />
                                    </CCol>
                                </CRow>}
                                {alcohol.self_harm &&
                                <CRow>
                                    <CCol xs="12" md="4" lg="12">  
                                        <label>Last Attempt Intentionality</label>  
                                    </CCol>
                                    <CCol xs="12" md="4" lg="12">                           
                                        <RadioButtonList 
                                        name="last_attempt"
                                        title=" "
                                        controlFunc={handleChange}
                                        options={appConstants.LAST_ATTEMPT}
                                        content={alcohol.last_attempt}
                                        manadatorySymbol={false} />
                                    </CCol>
                                </CRow>} 
                                <CRow>
                                    <CCol xs="12" md="4" lg="8">
                                        <SingleTextInput name="medical_others" 
                                        title="Medical others " 
                                        inputType="text" 
                                        content={(alcohol.medical_others)?(alcohol.medical_others):''} 
                                        placeholder="Medical Others" 
                                        manadatorySymbol={false} 
                                        controlFunc={handleChange}
                                        />
                                    </CCol> 
                                </CRow> 
                                <CRow>
                                    <CCol xs="12" md="4" lg="8">
                                        <SingleTextInput name="suicide" 
                                        title="Suicide " 
                                        inputType="text" 
                                        content={(alcohol.suicide)?(alcohol.suicide):''} 
                                        placeholder="Suicide" 
                                        manadatorySymbol={false} 
                                        controlFunc={handleChange}
                                        />
                                    </CCol> 
                                </CRow>                     
                                <CRow>
                                    <CCol xs="7" md="4" lg="8">
                                        <label>Psychosis</label>
                                    </CCol>
                                    <CCol xs="5" md="4" lg="4">
                                        <ToggleSwitchWithText
                                            name = 'psychosis'
                                            content = {alcohol.psychosis?true:false}
                                            controlFunc = {handleToggleChange}
                                            trueText = "No"
                                            falseText = "Yes"                                            
                                        />
                                    </CCol>
                                </CRow>

                            </TabPanel>
                            {/* 5.PREVIOUS ATTEMPTS OF ABSTINENCE */}
                            <TabPanel value={value} index={4}>
                                <CRow className="formMargins"> 
                                    <label><b>Previous treatment of drug abuse</b></label>   
                                    <CCol xs="12" md="4" lg="8">           
                                        <SelectBox
                                        name="previous_treatment"
                                        title=" "
                                        placeholder='Frequency'
                                        controlFunc={handleChange}
                                        options={appConstants.PREVIOUS_TREATMENT}
                                        autosize={true}
                                        manadatorySymbol={false} />
                                    </CCol>
                                </CRow>
                                {alcohol.previous_treatment == 'Yes' &&
                                <CRow>
                                     <CCol xs="12" md="4" lg="12"> 
                                     <label>Ever hospitalized for treatment of drug abuse</label>
                                     </CCol>
                                    <CCol xs="12" md="4" lg="12">                           
                                        <RadioButtonList 
                                        name="drug_absuse"
                                        title=" "
                                        controlFunc={handleChange}
                                        options={appConstants.DRUG_ABSUSE}
                                        content={alcohol.drug_absuse}
                                        manadatorySymbol={false} />
                                    </CCol>
                                </CRow>}
                                <CRow>
                                    <CCol xs="10" md="4" lg="12">  
                                        <label><b>Previous Attempts</b></label>  
                                    </CCol> 
                                </CRow>
                                <CRow>
                                    <CCol xs="6" md="4" lg="12">  
                                        <CheckboxList 
                                        name="previous_attempts"
                                        title=" "
                                        controlFunc={checkboxChangeHandler}
                                        options={ appConstants.PREVIOUS_ATTEMPTS }
                                        selectedOptions={symptoms.selectedAttempts }
                                        manadatorySymbol={false} />
                                    </CCol>
                                    <CCol xs="6" md="4" lg="12">  
                                        <CheckboxList 
                                        name="previous_attempts"
                                        title=" "
                                        controlFunc={checkboxChangeHandler}
                                        options={ appConstants.PREVIOUS_ATTEMPTS_ONE }
                                        selectedOptions={symptoms.selectedAttempts }
                                        manadatorySymbol={false} />
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="10" md="4" lg="12"> 
                                        <label><b>Previous Medication</b></label> 
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6" md="4" lg="12">  
                                        <CheckboxList 
                                        name="previous_medication"
                                        title=" "
                                        controlFunc={checkboxChangeHandler}
                                        options={ appConstants.PREVIOUS_MEDICATION }
                                        selectedOptions={symptoms.selectedMedication}
                                        manadatorySymbol={false} />
                                    </CCol>
                                    <CCol xs="6" md="4" lg="12">  
                                        <CheckboxList 
                                        name="previous_medication"
                                        title=" "
                                        controlFunc={checkboxChangeHandler}
                                        options={ appConstants.PREVIOUS_MEDICATION_ONE }
                                        selectedOptions={symptoms.selectedMedication}
                                        manadatorySymbol={false} />
                                    </CCol>
                                </CRow> 
                                <CRow>
                                    <CCol xs="10" md="4" lg="12"> 
                                        <label><b>Medication while Abstinence</b></label>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6" md="4" lg="12">      
                                        <CheckboxList 
                                            name="medication_abstinence"
                                            title=" "
                                            controlFunc={checkboxChangeHandler}
                                            options={ appConstants.MEDICATION_ABSTINENCE }
                                            selectedOptions={symptoms.selectedAbstinence }
                                            manadatorySymbol={false} />
                                    </CCol>
                                    <CCol xs="6" md="4" lg="12">      
                                        <CheckboxList 
                                            name="medication_abstinence"
                                            title=" "
                                            controlFunc={checkboxChangeHandler}
                                            options={ appConstants.MEDICATION_ABSTINENCE_ONE }
                                            selectedOptions={symptoms.selectedAbstinence }
                                            manadatorySymbol={false} />
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol>
                                        <label><b>Maximum Abstinence</b></label>
                                    </CCol>
                                </CRow> 
                                <CRow>
                                        <CCol xs="6" md="4" lg="8">
                                            <SingleTextInput name="maximum_abstinence" 
                                            title=" " 
                                            inputType="text" 
                                            content={(alcohol.maximum_abstinence)?(alcohol.maximum_abstinence):''} 
                                            placeholder="Maximum Abstinence" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}
                                            />
                                        </CCol> 
                                        <CCol xs="6" md="4" lg="8">           
                                            <SelectBox
                                            name="abstinence_time"
                                            title=" "
                                            placeholder=' '
                                            controlFunc={handleChange}
                                            options={appConstants.ABSTINENCE_TIME}
                                            autosize={true}
                                            manadatorySymbol={false} />
                                        </CCol>
                                </CRow> 
                                <CRow>
                                        <CCol xs="12" md="4" lg="8">
                                            <SingleTextInput name="abstinence_reason" 
                                            title="Abstinence Reason " 
                                            inputType="text" 
                                            content={(alcohol.abstinence_reason)?(alcohol.abstinence_reason):''} 
                                            placeholder="Abstinence Reason" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}
                                            />
                                        </CCol> 
                                </CRow> 
                                <CRow>
                                        <CCol xs="12" md="4" lg="8">
                                            <SingleTextInput name="relapse_reason" 
                                            title="Relapse Reason " 
                                            inputType="text" 
                                            content={(alcohol.relapse_reason)?(alcohol.relapse_reason):''} 
                                            placeholder="Relapse Reason" 
                                            manadatorySymbol={false} 
                                            controlFunc={handleChange}
                                            />
                                        </CCol> 
                                </CRow>
                            </TabPanel>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={handleClose}>
                            Save Changes
                        </Button>
                    </Modal.Footer>
            </Modal> 
)
}
