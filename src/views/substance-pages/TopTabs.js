import React from 'react';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';
import SwipeableViews from 'react-swipeable-views';
import { useTheme } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import CreatePatientComponent from '../patient/Create';
import CreateSubstanceuseComponent from '../accordian/Substanceone';
import Diagnosis from '../diagnosis/Diagnosis';
import Mainpage from '../treatment-plan/Mainpage';
import SubstanceDetailsStepper from '../substance-details/SubstanceDetailsStepper';
import './index.css';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabspanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={4}>
                    <Typography component={'div'}>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
}

export default function TopTabs(props) {
    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    return (
        <Box>
            <div position="static">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    variant="scrollable"
                    scrollButtons="auto"
                    indicatorColor="primary"
                    textColor="primary"
                    aria-label="scrollable auto tabs example"
                    className={'patientVisitTopTabs'}
                >
                    <Tooltip title="Demographic">
                        {/* <Tab icon={<AccountBoxTwoToneIcon />} aria-label="phone" {...a11yProps(0)} /> */}
                        <Tab label={'Demographic'} aria-label="phone" {...a11yProps(0)} />
                    </Tooltip>
                    <Tooltip title="Substance Use">
                        {/* <Tab icon={<LocalBarTwoToneIcon />} aria-label="favorite" {...a11yProps(1)} /> */}
                        <Tab label={'Substance Use'} aria-label="phone" {...a11yProps(1)} />
                    </Tooltip>
                    <Tooltip title="Family History">
                        {/* <Tab icon={<GroupAddTwoToneIcon />} aria-label="person" {...a11yProps(2)} /> */}
                        <Tab label={'Family History'} aria-label="phone" {...a11yProps(2)} />
                    </Tooltip>
                    <Tooltip title="Diagnosis">
                        {/* <Tab icon={<LocalHospitalTwoToneIcon />} aria-label="help" {...a11yProps(3)} /> */}
                        <Tab label={'Diagnosis'} aria-label="phone" {...a11yProps(3)} />
                    </Tooltip>
                    <Tooltip title="Treatment Plan">
                        {/* <Tab icon={<HotelTwoToneIcon />} aria-label="shopping" {...a11yProps(4)} /> */}
                        <Tab label={'Treatment Plan'} aria-label="phone" {...a11yProps(4)} />
                    </Tooltip>
                </Tabs>
            </div>
            <div className='patientVisitTopTabPanels'>
                <SwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={value}
                    onChangeIndex={handleChangeIndex}
                >
                    <TabPanel value={value} index={0} dir={theme.direction}>
                        <CreatePatientComponent />
                    </TabPanel>
                    <TabPanel value={value} index={1} dir={theme.direction}>
                        <SubstanceDetailsStepper />
                    </TabPanel>
                    <TabPanel value={value} index={2} dir={theme.direction}>
                        <CreateSubstanceuseComponent />
                    </TabPanel>
                    <TabPanel value={value} index={3} dir={theme.direction}>
                        <Diagnosis />
                    </TabPanel>
                    <TabPanel value={value} index={4} dir={theme.direction}>
                        <Mainpage />
                    </TabPanel>
                </SwipeableViews>
            </div>
        </Box >
    );
}