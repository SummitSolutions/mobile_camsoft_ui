import React from 'react'
import {
  CButton
} from '@coreui/react'

import {  CCard, CCardBody, CCardHeader, CCol, CRow } from '@coreui/react';
import SingleTextInput from '../../form-components/SingleTextInput';
import SelectBox from '../../form-components/SelectBox';
import * as appConstants from './.././../../AppConstants';
import SingleCheckbox from '../../form-components/SingleCheckbox';

const SubstanceUseBehavioralAddiction = (props) => 
{
    return (
        <>
            <CRow>
                <CCol xs="6" md="4" lg="4">                                        
                    <SingleTextInput name="type" 
                        title="Type" 
                        inputType="text" 
                        content={(props.obj.type)?(props.obj.type):''} 
                        placeholder="Type" 
                        manadatorySymbol={false}
                        controlBlurFunc={props.controlBlurFunc} 
                        controlFunc={props.controlFunc}/>
                </CCol>
            </CRow>
            <CRow className="formMargins">    
                <CCol xs="6" md="3" lg="3">
                    <SingleTextInput name="age" 
                        title="Age" 
                        inputType="number" 
                        content={(props.obj.age)?(props.obj.age):''} 
                        placeholder="Age" 
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}
                    />             
                    
                </CCol>
                <CCol xs="6" md="3" lg="3">                                        
                    <SingleTextInput name="initial_amount" 
                        title="Initial amount" 
                        inputType="text" 
                        content={(props.obj.initial_amount)?(props.obj.initial_amount):''} 
                        placeholder="Units" 
                        manadatorySymbol={false}
                        controlBlurFunc={props.controlBlurFunc} 
                        controlFunc={props.controlFunc}/>
                </CCol>
                <CCol xs="8" md="3" lg="3">                                        
                    <SingleTextInput name="avg_session" 
                        title="Avg Qty/Session/Day" 
                        inputType="text" 
                        content={(props.obj.avg_session)?(props.obj.avg_session):''} 
                        placeholder="Avg Qty/Session/Day" 
                        manadatorySymbol={false}
                        controlBlurFunc={props.controlBlurFunc} 
                        controlFunc={props.controlFunc}/>
                </CCol>
                <CCol xs="4" md="3" lg="3">           
                    <SelectBox
                        name="average_uom"
                        title=" "
                        placeholder=' '
                        controlFunc={props.controlFunc}
                        options={appConstants.BEHAVIORAL_UOM}
                        autosize={true}
                    />
                </CCol>
            </CRow>  
            <CRow className="formMargins">    
                <CCol xs="12" md="4" lg="6">           
                    <SelectBox
                        name="frequency"
                        title="Frequency"
                        placeholder='Frequency'
                        controlFunc={props.controlFunc}
                        options={appConstants.FREQUENCY}
                        autosize={true}
                        manadatorySymbol={true}/>
                </CCol>
                <CCol xs="12" md="4" lg="6">                                        
                    <SingleTextInput name="last_use_quantity" 
                        title="Last use quantity" 
                        inputType="number" 
                        content={(props.obj.last_use_quantity)?(props.obj.last_use_quantity):''} 
                        placeholder="Last use quantity" 
                        manadatorySymbol={false} 
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}/>
                </CCol>
                <CCol xs="12" md="4" lg="6">                                        
                    <SingleTextInput name="duration_last_use" 
                        title="Duration since last use" 
                        inputType="number" 
                        content={(props.obj.duration_last_use)?(props.obj.duration_last_use):''} 
                        placeholder="Duration since last use" 
                        manadatorySymbol={false} 
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}/>
                </CCol>
                <CCol xs="12" md="4" lg="6">                                        
                    <SelectBox
                        name="duration"
                        title="Duration"
                        placeholder='Duration'
                        controlFunc={props.controlFunc}
                        options={appConstants.DURATION}
                        autosize={true}
                        manadatorySymbol={false}/>
                </CCol>
            </CRow>
        </>
    )              
 }
export default SubstanceUseBehavioralAddiction;
