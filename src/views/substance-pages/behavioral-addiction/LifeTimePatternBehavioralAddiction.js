import React from 'react'
import {
  CCol,CRow,
} from '@coreui/react'
import SingleTextInput from '../../form-components/SingleTextInput';
import SelectBox from '../../form-components/SelectBox';
import * as appConstants from './.././../../AppConstants';
import RadioButtonList from '../../form-components/RadioButtonList';
import SingleCheckbox from '../../form-components/SingleCheckbox';

const LifeTimePatternBehavioralAddiction = (props) => (
        <>
         <CRow> 
              <CCol xs="9" md="10" lg="10">
                  <label>1. Continued use despite Physical / Psychological harm</label>
              </CCol> 
              <CCol xs="3" md="2" lg="2"> 
                <SingleCheckbox 
                  controlFunc={props.controlFunc} 
                  name='psychological_harm'
                  title=''
                  type="checkbox" 
                  value='psychological_harm'
                  content={(props.obj.psychological_harm)?(props.obj.psychological_harm):''}
                />
              </CCol> 
            </CRow>
           &nbsp;
            <CRow>
              <CCol xs="9" md="10" lg="10">
                  <label>2. Loss of control: Use after deciding not to or use more than intended ?</label>
              </CCol>
              <CCol xs="3" md="2" lg="2">
                  <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='loss_of_control'
                    title=''
                    type="checkbox" 
                    value='loss_of_control'
                    content={(props.obj.loss_of_control)?(props.obj.loss_of_control):''}
                  />
              </CCol>
            </CRow>
            &nbsp;
            <CRow>
              <CCol xs="9" md="10" lg="10">
                  <label>3. Early Morning: Ever needed / taken just after getting up in the morning</label>
              </CCol>
              <CCol xs="3" md="2" lg="2">
                <SingleCheckbox 
                  controlFunc={props.controlFunc} 
                  name='early_morning'
                  title=''
                  type="checkbox" 
                  value='early_morning'
                  content={(props.obj.early_morning)?(props.obj.early_morning):''}
                />
              </CCol> 
            </CRow>
            &nbsp;
            <CRow>
              <CCol xs="9" md="10" lg="10">
                <label>4. Tolerance : 50% more to give same effect</label>
              </CCol>
              <CCol xs="3" md="2" lg="2">
                <SingleCheckbox 
                  controlFunc={props.controlFunc} 
                  name='tolerance'
                  title=''
                  type="checkbox" 
                  value='tolerance'
                  content={(props.obj.tolerance)?(props.obj.tolerance):''}
                />
              </CCol> 
            </CRow>
            &nbsp;
            <CRow>
              <CCol xs="9" md="10" lg="10">
                <label>5.Craving:Irresistible urge / Strong desire for alcohol / drugs – could not manage without it – could not think of anything else</label>
              </CCol>
              <CCol xs="3" md="2" lg="2">
                <SingleCheckbox 
                  controlFunc={props.controlFunc} 
                  name='irresistible_urge'
                  title=''
                  type="checkbox" 
                  value='irresistible_urge'
                  content={(props.obj.irresistible_urge)?(props.obj.irresistible_urge):''}
                />
              </CCol> 
            </CRow>
            &nbsp;
            <CRow>
              <CCol xs="9" md="10" lg="10">
                <label>6.Salience : Given up / greatly reduced important activities while using / recovering from effects for several days or more</label>
              </CCol>
              <CCol xs="3" md="2" lg="2">
                <SingleCheckbox 
                  controlFunc={props.controlFunc} 
                  name='salience'
                  title=''
                  type="checkbox" 
                  value='salience'
                  content={(props.obj.salience)?(props.obj.salience):''}
                />
              </CCol> 
            </CRow>
            &nbsp;
            <CRow>
              <CCol xs="9" md="10" lg="10">
                <label>7.Continued Use Despite Social Interpersonal Problem:</label>
              </CCol>
              <CCol xs="3" md="2" lg="2">
                <SingleCheckbox 
                  controlFunc={props.controlFunc} 
                  name='interpersonal_problem'
                  title=''
                  type="checkbox" 
                  value='interpersonal_problem'
                  content={(props.obj.interpersonal_problem)?(props.obj.interpersonal_problem):''}
                />
              </CCol> 
            </CRow>
            &nbsp;
            <CRow>
              <CCol xs="9" md="10" lg="10">
                <label>8.Use of Substance in Physical Hazardous Situation</label>
              </CCol>
              <CCol xs="3" md="2" lg="2">
                <SingleCheckbox 
                  controlFunc={props.controlFunc} 
                  name='physical_hazardous'
                  title=''
                  type="checkbox" 
                  value='physical_hazardous'
                  content={(props.obj.physical_hazardous)?(props.obj.physical_hazardous):''}
                />
              </CCol> 
            </CRow>
            &nbsp;
            <CRow>
              <CCol xs="9" md="10" lg="10">
                <label>9.Failure to Fulfill Major Role Obligations</label> 
              </CCol>
              <CCol xs="3" md="2" lg="2">
                <SingleCheckbox 
                  controlFunc={props.controlFunc} 
                  name='major_role_obligations'
                  title=''
                  type="checkbox" 
                  value='major_role_obligations'
                  content={(props.obj.major_role_obligations)?(props.obj.major_role_obligations):''}
                />
              </CCol> 
            </CRow>
            &nbsp;
            <CRow>
              <CCol xs="9" md="10" lg="10">
                <label>10. Majority of Time Spent in Procuring Substance</label>
              </CCol>
              <CCol xs="3" md="2" lg="2">
                <SingleCheckbox 
                  controlFunc={props.controlFunc} 
                  name='procuring_substance'
                  title=''
                  type="checkbox" 
                  value='procuring_substance'
                  content={(props.obj.procuring_substance)?(props.obj.procuring_substance):''}
                />
              </CCol> 
            </CRow>
            &nbsp;
            <CRow>
              <CCol xs="9" md="10" lg="10">
                <label>11.Current Pattern of Dependent Use:</label>
              </CCol>
              <CCol xs="3" md="2" lg="2">
                <SingleCheckbox 
                  controlFunc={props.controlFunc} 
                  name='current_pattern_dependent'
                  title=''
                  type="checkbox" 
                  value='current_pattern_dependent'
                  content={(props.obj.current_pattern_dependent)?(props.obj.current_pattern_dependent):''}
                />
              </CCol> 
            </CRow>
            {(props.obj.current_pattern_dependent &&
            <CRow className="formMargins">    
              <CCol xs="12" md="10" lg="10">                                        
                <SingleTextInput 
                  name="age_of_dependence" 
                  title="Age of Dependence" 
                  inputType="text" 
                  content={(props.obj.age_of_dependence)?(props.obj.age_of_dependence):''} 
                  placeholder="Age of Dependence" 
                  manadatorySymbol={false} 
                  controlBlurFunc={props.controlBlurFunc}
                  controlFunc={props.controlFunc}
                />
              </CCol>
            </CRow> )}
            &nbsp;
            <CRow>
              <CCol xs="12" md="10" lg="12">  
                <label>12.CURRENT PATTERN(12 months)</label>  
              </CCol>
              <CCol xs="12" md="4" lg="12">                           
                <RadioButtonList 
                name="current_pattern"
                title=" "
                controlBlurFunc={props.controlBlurFunc}
                controlFunc={props.controlFunc}
                options={appConstants.CURRENT_PATTERN}
                content={props.obj.current_pattern}
                manadatorySymbol={false} />
              </CCol>
            </CRow> 
            <CRow>
              <CCol xs="12" md="10" lg="12">  
                <label>13.Circumstance of use</label>  
              </CCol>
              <CCol xs="12" md="4" lg="12">                           
                <RadioButtonList 
                name="circumstance_of_use"
                title=" "
                controlBlurFunc={props.controlBlurFunc}
                controlFunc={props.controlFunc}
                options={appConstants.CIRCUMSTANCE_USE}
                content={props.obj.circumstance_of_use}
                manadatorySymbol={false}/>
              </CCol>
            </CRow>  
        </>
    
)

export default LifeTimePatternBehavioralAddiction;