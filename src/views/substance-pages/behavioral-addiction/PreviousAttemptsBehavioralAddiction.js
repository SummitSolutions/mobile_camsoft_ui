import React from 'react'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CButton,
  CCol,CRow,
} from '@coreui/react'
import { useHistory } from 'react-router-dom';
import ToggleSwitchWithText from '../../form-components/ToggleSwitchWithText';
import SingleTextInput from '../../form-components/SingleTextInput';
import SelectBox from '../../form-components/SelectBox';
import * as appConstants from './.././../../AppConstants';
import RadioButtonList from '../../form-components/RadioButtonList';
import SingleCheckbox from '../../form-components/SingleCheckbox';

const PreviousAttemptsBehavioralAddiction = (props) => (
    <>
        <CRow >
            <CCol  xs="12" md="10" lg="10">
                <label><b>Previous treatment of drug abuse</b></label>   
            </CCol>
            <CCol xs="12" md="6" lg="6">           
                <SelectBox
                name="previous_treatment"
                title="Previous Treatment "
                placeholder='Frequency'
                controlBlurFunc={props.controlBlurFunc}
                controlFunc={props.controlFunc}
                options={appConstants.PREVIOUS_TREATMENT}
                autosize={true}
                manadatorySymbol={false} />
            </CCol>
        </CRow>
        {props.obj.previous_treatment == 'Yes' &&
        <CRow>
            <CCol xs="12" md="6" lg="6"> 
                <label>Ever hospitalized for treatment of drug abuse</label>
            </CCol>
            <CCol xs="12" md="12" lg="12">                           
                <RadioButtonList 
                name="drug_absuse"
                title=""
                controlBlurFunc={props.controlBlurFunc}
                controlFunc={props.controlFunc}
                options={appConstants.DRUG_ABSUSE}
                content={props.obj.drug_absuse}
                manadatorySymbol={false} />
            </CCol>
        </CRow>}
        &nbsp;
        <CRow>
            <CCol xs="10" md="12" lg="12">  
                <label><b>Previous Attempts</b></label>  
            </CCol> 
        </CRow>
        <CRow>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='general_medical'
                    title='General Medical'
                    type="checkbox" 
                    value='general_medical'
                    content={(props.obj.general_medical)?(props.obj.general_medical):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='self_abstinence'
                    title='Self Abstinence'
                    type="checkbox" 
                    value='self_abstinence'
                    content={(props.obj.self_abstinence)?(props.obj.self_abstinence):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='general_psychiatric'
                    title='General Psychiatric'
                    type="checkbox" 
                    value='general_psychiatric'
                    content={(props.obj.general_psychiatric)?(props.obj.general_psychiatric):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='traditional_healers'
                    title='Traditional healers'
                    type="checkbox" 
                    value='Traditional healers'
                    content={(props.obj.traditional_healers)?(props.obj.traditional_healers):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='deaddiction_facilities'
                    title='Deaddiction Facilities'
                    type="checkbox" 
                    value='Deaddiction Facilities'
                    content={(props.obj.deaddiction_facilities)?(props.obj.deaddiction_facilities):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='self_help_groups'
                    title='Self Help Groups'
                    type="checkbox" 
                    value='self_help_groups'
                    content={(props.obj.self_help_groups)?(props.obj.self_help_groups):''}
                />
            </CCol>    
        </CRow>
        &nbsp;
        <CRow>
            <CCol xs="10" md="4" lg="12"> 
                <label><b>Previous Medication</b></label> 
            </CCol>
        </CRow>
        <CRow>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='previous_disulfiram'
                    title='Disulfiram'
                    type="checkbox" 
                    value='previous_disulfiram'
                    content={(props.obj.previous_disulfiram)?(props.obj.previous_disulfiram):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='previous_baclofen'
                    title='Baclofen'
                    type="checkbox" 
                    value='previous_baclofen'
                    content={(props.obj.previous_baclofen)?(props.obj.previous_baclofen):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='previous_naltraxone'
                    title='Naltraxone'
                    type="checkbox" 
                    value='previous_naltraxone'
                    content={(props.obj.previous_naltraxone)?(props.obj.previous_naltraxone):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='previous_acamprosaate'
                    title='Acamprosate'
                    type="checkbox" 
                    value='previous_acamprosaate'
                    content={(props.obj.previous_acamprosaate)?(props.obj.previous_acamprosaate):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='previous_ondensetron'
                    title='Ondensetron'
                    type="checkbox" 
                    value='previous_ondensetron'
                    content={(props.obj.previous_ondensetron)?(props.obj.previous_ondensetron):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='previous_ssri'
                    title='SSRI'
                    type="checkbox" 
                    value='previous_ssri'
                    content={(props.obj.previous_ssri)?(props.obj.previous_ssri):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='previous_methadone'
                    title='Methadone (Opioid)'
                    type="checkbox" 
                    value='previous_methadone'
                    content={(props.obj.previous_methadone)?(props.obj.previous_methadone):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='previous_buprenorphine '
                    title='Buprenorphine (Opioid)'
                    type="checkbox" 
                    value='previous_buprenorphine'
                    checked={props.obj.previous_buprenorphine}
                    content={(props.obj.previous_buprenorphine)?(props.obj.previous_buprenorphine):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='previous_nac'
                    title='NAC (Cannabis)'
                    type="checkbox" 
                    value='previous_nac'
                    content={(props.obj.previous_nac)?(props.obj.previous_nac):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='previous_nomedication'
                    title=' No Medication'
                    type="checkbox" 
                    value='previous_nomedication'
                    content={(props.obj.previous_nomedication)?(props.obj.previous_nomedication):''}
                />
            </CCol>
        </CRow>
        &nbsp;
        <CRow>
            <CCol xs="10" md="12" lg="12"> 
                <label><b>Medication while Abstinence</b></label>
            </CCol>
        </CRow>
        <CRow>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='abstinence_disulfiram'
                    title='Disulfiram'
                    type="checkbox" 
                    value='Disulfiram'
                    checked={props.obj.abstinence_disulfiram}
                    content={(props.obj.abstinence_disulfiram)?(props.obj.abstinence_disulfiram):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='abstinence_baclofen'
                    title='Baclofen'
                    type="checkbox" 
                    value='abstinence_baclofen'
                    content={(props.obj.abstinence_baclofen)?(props.obj.abstinence_baclofen):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='abstinence_naltraxone'
                    title='Naltraxone'
                    type="checkbox" 
                    value='abstinence_naltraxone'
                    content={(props.obj.abstinence_naltraxone)?(props.obj.abstinence_naltraxone):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='abstinence_acamprosaate'
                    title='Acamprosate'
                    type="checkbox" 
                    value='abstinence_acamprosaate'
                    checked={props.obj.abstinence_acamprosaate}
                    content={(props.obj.abstinence_acamprosaate)?(props.obj.abstinence_acamprosaate):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='abstinence_ondensetron'
                    title='Ondensetron'
                    type="checkbox" 
                    value='abstinence_ondensetron'
                    content={(props.obj.abstinence_ondensetron)?(props.obj.abstinence_ondensetron):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='abstinence_ssri'
                    title='SSRI'
                    type="checkbox" 
                    value='abstinence_ssri'
                    content={(props.obj.abstinence_ssri)?(props.obj.abstinence_ssri):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='abstinence_methadone'
                    title='Methadone (Opioid)'
                    type="checkbox" 
                    value='abstinence_methadone'
                    content={(props.obj.abstinence_methadone)?(props.obj.abstinence_methadone):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='abstinence_buprenorphine'
                    title='Buprenorphine (Opioid) '
                    type="checkbox" 
                    value='Buprenorphine (Opioid)'
                    checked={props.obj.abstinence_buprenorphine}
                    content={(props.obj.abstinence_buprenorphine)?(props.obj.abstinence_buprenorphine):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='abstinence_nac'
                    title='NAC (Cannabis)'
                    type="checkbox" 
                    value='abstinence_nac'
                    content={(props.obj.abstinence_nac)?(props.obj.abstinence_nac):''}
                />
            </CCol>
            <CCol xs="12" md="3" lg="3">        
                <SingleCheckbox 
                    controlFunc={props.controlFunc} 
                    name='abstinence_nomedication'
                    title='No Medication'
                    type="checkbox" 
                    value='abstinence_nomedication'
                    content={(props.obj.abstinence_nomedication)?(props.obj.abstinence_nomedication):''}
                />
            </CCol>
        </CRow>
        &nbsp;
        <CRow>
            <CCol>
                <label><b>Maximum Abstinence</b></label>
            </CCol>
        </CRow> 
        <CRow>
            <CCol xs="7" md="6" lg="6">
                <SingleTextInput name="maximum_abstinence" 
                title="Maximum Abstinence " 
                inputType="number" 
                content={(props.obj.maximum_abstinence)?(props.obj.maximum_abstinence):''} 
                placeholder="Maximum Abstinence" 
                manadatorySymbol={false} 
                controlBlurFunc={props.controlBlurFunc}
                controlFunc={props.controlFunc}
                />
            </CCol> 
            <CCol xs="4" md="6" lg="6">           
                <SelectBox
                name="abstinence_time"
                title=" "
                placeholder=' '
                controlBlurFunc={props.controlBlurFunc}
                controlFunc={props.controlFunc}
                options={appConstants.ABSTINENCE_TIME}
                autosize={true}
                manadatorySymbol={false} />
            </CCol>
        </CRow> 
        <CRow>
            <CCol xs="12" md="12" lg="12">
                <SingleTextInput 
                name="abstinence_reason" 
                title="Abstinence Reason " 
                inputType="text" 
                content={(props.obj.abstinence_reason)?(props.obj.abstinence_reason):''} 
                placeholder="Abstinence Reason" 
                manadatorySymbol={false}
                controlBlurFunc={props.controlBlurFunc} 
                controlFunc={props.controlFunc}
                />
            </CCol> 
        </CRow> 
        <CRow>
            <CCol xs="12" md="12" lg="12">
                <SingleTextInput name="relapse_reason" 
                title="Relapse Reason " 
                inputType="text" 
                content={(props.obj.relapse_reason)?(props.obj.relapse_reason):''} 
                placeholder="Relapse Reason" 
                manadatorySymbol={false}
                controlBlurFunc={props.controlBlurFunc} 
                controlFunc={props.controlFunc}
                />
            </CCol> 
        </CRow>
    </>
)

export default PreviousAttemptsBehavioralAddiction;