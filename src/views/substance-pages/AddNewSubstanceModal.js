import React, { useContext, useEffect, useState, useRef } from 'react';
import { useParams } from "react-router";
import GlobalState from '../../shared/GlobalState';
import SimpleReactValidator from 'simple-react-validator';
import SubstanceUseService from 'src/api/SubstanceUseService';
import SubstanceListService from 'src/api/SubstanceListService';
import SingleCheckbox from './../form-components/SingleCheckbox';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from 'react-bootstrap/Button';
import { useHistory } from 'react-router-dom';

export default function AddNewSubstanceModal(props) {

    const params = useParams();
    const [state, setState] = useContext(GlobalState);
    const validator = useRef(new SimpleReactValidator());
    const [substanceUseList, setSubstanceUseList] = useState([]);
    const [substanceList, setSubstanceList] = useState([]);
    const [substanceUse, setSubstanceUse] = useState({});
    const history = useHistory();

    useEffect(() => {
        console.log("Substance use page loaded. Msg. from UseEffect")

        //getSubstanceList();
        getAllSubstanceUse();


        return () => {
            console.log('Clearing Substance use from State')
        }
    }, []);

    const getSubstanceList = () => {
        SubstanceListService.getSubstanceList(state.authHeader).then((resp) => {
            console.log(resp)
            setSubstanceList(resp.data)
        })
    }
    const getAllSubstanceUse = () => {
        SubstanceUseService.getAllSubstanceUse(params.patient_id, params.visit_id, state.authHeader).then((resp) => {
            console.log(resp)
            setSubstanceUseList(resp.data)
        })
        console.log(substanceUseList.find(substance => substance.substance_id == 1))
    }

    const handleToggleChange = (event, obj) => {
        let substanceUseObj = {};
        console.log(obj?.substance_use_list?.id)
        if (obj?.substance_use_list?.id) {
            console.log(true)
            substanceUseObj = obj?.substance_use_list;
            // substanceUseObj.substance_id = obj.id;
            if (event.target.name == 'current_use') {
                substanceUseObj.current_use = event.target.checked;
                if (event.target.checked)
                    substanceUseObj.ever_use = true;
            }
            if (event.target.name == 'ever_use')
                substanceUseObj.ever_use = event.target.checked;
            updateSubstanceUse(substanceUseObj);
        }
        else {
            console.log(false)
            console.log((event.target.name == 'current_use'), (event.target.checked))
            substanceUseObj = {
                patient_id: parseInt(params.patient_id),
                visit_id: parseInt(params.visit_id),
                substance_id: obj.id,
                current_use: (event.target.name == 'current_use') ? event.target.checked : false,
                ever_use: ((event.target.name == 'current_use') && (event.target.checked)) ? true : (event.target.name == 'ever_use') ? (event.target.checked) : false
            };
            saveSubstanceUse(substanceUseObj);
        }
        //setSubstanceUse({...substanceUse, [event.target.name]:event.target.checked});
    }

    const updateSubstanceUse = (data) => {
        console.log(data)
        SubstanceUseService.updateSubstanceUse(data.id, data, state.authHeader).then(resp => {
            console.log(resp);
            setSubstanceUseList(resp.data);
        });
    }

    const saveSubstanceUse = (data) => {
        console.log(data)
        SubstanceUseService.createSubstanceUse(data, state.authHeader).then(resp => {
            console.log(resp);
            setSubstanceUseList(resp.data);
        });
    }
    const gotoSubstanceDetails = () => {
        //history.replace('/substance-pages/substance-header/'+params.patient_id+'/'+params.visit_id);

    }
    return (
        <Paper>
            <TableContainer style={{ height: '400px', overflowY: 'auto' }}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Substance Name</TableCell>
                            <TableCell>Current Use</TableCell>
                            <TableCell>Ever Use</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {substanceUseList.map((row) => (
                            <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                                <TableCell>{row.substance_name}</TableCell>
                                <TableCell>
                                    <SingleCheckbox
                                        name='current_use'
                                        content={(row?.substance_use_list?.current_use) ? true : false}
                                        controlFunc={(e) => { handleToggleChange(e, row) }}
                                        type="checkbox"
                                        value='current_use'
                                    />
                                </TableCell>
                                <TableCell>
                                    <SingleCheckbox
                                        name='ever_use'
                                        content={(row?.substance_use_list?.ever_use) ? true : false}
                                        controlFunc={(e) => { handleToggleChange(e, row) }}
                                        type="checkbox"
                                        value='ever_use'
                                    />
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

        </Paper>
    );
}