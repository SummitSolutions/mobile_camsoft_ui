import React from 'react'
import {
  CCol,CRow,
} from '@coreui/react'
import SingleTextInput from '../../form-components/SingleTextInput';


const WithdrawalSymptomsHallucinogens = (props) => (
    <>
        <CRow>    
            <CCol xs="12" md="8" lg="8">
                <SingleTextInput name="symptoms" 
                inputType="text" 
                content={(props.obj.symptoms)?(props.obj.symptoms):''} 
                placeholder=" " 
                manadatorySymbol={false} 
                controlBlurFunc={props.controlBlurFunc}
                controlFunc = {props.controlFunc}
                multiline= {true}
                rows = {4}
                variant = "outlined"/>
            </CCol> 
        </CRow> 
    </>
)
export default WithdrawalSymptomsHallucinogens;