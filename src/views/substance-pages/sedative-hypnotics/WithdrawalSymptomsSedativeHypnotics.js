import React from 'react'
import {
  CCol,CRow, CCard,
} from '@coreui/react'
import SingleTextInput from '../../form-components/SingleTextInput';
import SelectBox from '../../form-components/SelectBox';
import * as appConstants from './.././../../AppConstants';
import SingleCheckbox from '../../form-components/SingleCheckbox';


const WithdrawalSymptomsSedativeHypnotics = (props) => (
    <> 
        <div>
            <h4>Simple withdrawal</h4> 
            <CRow>
                <CCol xs="9" md="4" lg="6">
                    <label>Currently Present</label>
                </CCol>
                <CCol xs="3" md="4" lg="6">
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='currently_present1'
                        type="checkbox" 
                        value='currently_present1'
                        content={(props.obj.currently_present1)?(props.obj.currently_present1):''}
                    /> 
                </CCol>
                </CRow>
                <CRow>
                    <CCol xs="9" md="4" lg="6">
                        <label>Past Occurence</label>
                    </CCol>
                    <CCol xs="3" md="4" lg="6">
                        <SingleCheckbox 
                            controlFunc={props.controlFunc} 
                            name='past_occurance_delerium1'
                            type="checkbox" 
                            value='past_occurance_delerium1'
                            content={(props.obj.past_occurance_delerium1)?(props.obj.past_occurance_delerium1):''}
                        /> 
                    </CCol> 
                </CRow>  
            <CRow>
                <CCol xs="8" md="4" lg="8">
                    <label><b>Symptoms A</b></label>
                </CCol>
            </CRow>
            <CRow>
                <CCol xs="12" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='feeling_anxious'
                        title='Feeling Anxious'
                        type="checkbox" 
                        value='Feeling Anxious'
                        //checked={props.obj.feeling_anxious}
                        content={(props.obj.feeling_anxious)?(props.obj.feeling_anxious):''}
                    />
                </CCol>         
                <CCol xs="12" md="4" lg="3">             
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='unable_to_sleep'
                        title='Unable to sleep'
                        type="checkbox" 
                        value='unable_to_sleep'
                        //checked={props.obj.unable_to_sleep}
                        content={(props.obj.unable_to_sleep)?(props.obj.unable_to_sleep):''}
                    />
                </CCol>
                <CCol xs="12" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='irritable'
                        title='Irritable'
                        type="checkbox" 
                        value='irritable'
                        //checked={props.obj.irritable}
                        content={(props.obj.irritable)?(props.obj.irritable):''}
                        />
                </CCol>         
                <CCol xs="12" md="4" lg="3">             
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='feel_depressed'
                        title='Feel depressed'
                        type="checkbox" 
                        value='feel_depressed'
                        //checked={props.obj.feel_depressed}
                        content={(props.obj.feel_depressed)?(props.obj.feel_depressed):''}
                    />
                </CCol> 
                <CCol xs="12" md="4" lg="3">             
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='sweating'
                        title='Sweating'
                        type="checkbox" 
                        value= 'sweating'
                        //checked={props.obj.sweating}
                        content={(props.obj.sweating)?(props.obj.sweating):''}
                    />
                </CCol>
                <CCol xs="12" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='fast_heart_beat'
                        title='Fast heart beat'
                        type="checkbox" 
                        value='fast_heart_beat'
                        //checked={props.obj.fast_heart_beat}
                        content={(props.obj.fast_heart_beat)?(props.obj.fast_heart_beat):''}
                    />
                </CCol>  
                <CCol xs="12" md="4" lg="3">             
                <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='hands_trembling'
                        title='Hands trembling'
                        type="checkbox" 
                        value='hands_trembling'
                        //checked={props.obj.hands_trembling}
                        content={(props.obj.hands_trembling)?(props.obj.hands_trembling):''}
                    /> 
                </CCol>
                <CCol xs="12" md="4" lg="3">  
                <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='head_aches'
                        title='Head Aches'
                        type="checkbox" 
                        value= 'head_aches'
                        //checked={props.obj.head_aches}
                        content={(props.obj.head_aches)?(props.obj.head_aches):''}
                    /> 
                </CCol>       
            </CRow>
            <CRow>
                <CCol xs="12" md="4" lg="3"> 
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='physically_weak'
                        title='Feel Physically Weak'
                        type="checkbox" 
                        value='physically_weak'
                        //checked={props.obj.physically_weak}
                        content={(props.obj.physically_weak)?(props.obj.physically_weak):''}
                    />
                </CCol> 
                <CCol xs="12" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='nausea_vomitting'
                        title='Nausea Vomitting'
                        type="checkbox" 
                        value='nausea_vomitting'
                        //checked={props.obj.nausea_vomitting}
                        content={(props.obj.nausea_vomitting)?(props.obj.nausea_vomitting):''}
                    />
                </CCol>   
                <CCol xs="12" md="4" lg="3">             
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='fidgety'
                        title='Fidgety/Restless'
                        type="checkbox" 
                        value= 'fidgety'
                        //checked={props.obj.fidgety}
                        content={(props.obj.fidgety)?(props.obj.fidgety):''}
                        />
                </CCol>   
            </CRow>
            <h4>Complicated Withdrawal</h4>
            <CRow>
                <CCol xs="9" md="4" lg="8">
                    <label>Currently Present</label>
                </CCol>
                <CCol xs="3" md="4" lg="4">
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='currently_present2'
                        type="checkbox" 
                        value='currently_present2'
                        content={(props.obj.currently_present2)?(props.obj.currently_present2):''}
                    /> 
                </CCol>
                </CRow>
                <CRow>
                    <CCol xs="9" md="4" lg="8">
                        <label>Past Occurence</label>
                    </CCol>
                    <CCol xs="3" md="4" lg="4">
                        <SingleCheckbox 
                            controlFunc={props.controlFunc} 
                            name='past_occurance_delerium2'
                            type="checkbox" 
                            value='past_occurance_delerium2'
                            content={(props.obj.past_occurance_delerium2)?(props.obj.past_occurance_delerium2):''}
                        /> 
                    </CCol> 
                </CRow>
                <CRow>
                    <CCol xs="9" md="4" lg="8">
                        <h4><u>SEIZURES:-</u></h4>
                    </CCol>
                </CRow>
                <CCard>
                <CRow>
                    <CCol xs="9" md="6" lg="6">
                        <SingleCheckbox 
                            controlFunc={props.controlFunc} 
                            name='check1_seizures'
                            type="checkbox" 
                            value='check1_seizures'
                            content={(props.obj.check1_seizures)?(props.obj.check1_seizures):''}
                        />
                    </CCol> 
                </CRow>
                </CCard> 
                {props.obj.check1_seizures &&
                <CRow>
                    <CCol xs="12" md="12" lg="12">
                        <label><b>Duration since first occurance</b></label>
                    </CCol>
                    <CCol xs="7" md="4" lg="4">                                        
                        <SingleTextInput 
                        name="seizures_withdrawal_first_episode_duration" 
                        title=" " 
                        inputType="number" 
                        content={(props.obj.seizures_withdrawal_first_episode_duration)?(props.obj.seizures_withdrawal_first_episode_duration):''} 
                        placeholder=" " 
                        manadatorySymbol={false}
                        controlBlurFunc={props.controlBlurFunc} 
                        controlFunc={props.controlFunc}/>
                    </CCol>
                    <CCol xs="5" md="4" lg="4"> 
                        <SelectBox
                            name="seizures_withdrawal_first_episode_duration_uom"
                            title="Duration"
                            placeholder='Duration'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.handleChange}
                            options={appConstants.DURATION}
                            autosize={true}
                            manadatorySymbol={false}/>
                    </CCol>
                    <CCol xs="7" md="4" lg="4">                                        
                        <SingleTextInput 
                        name="seizures_withdrawal_num_episodes" 
                        title="Num Of Episodes" 
                        inputType="number" 
                        content={(props.obj.seizures_withdrawal_num_episodes)?(props.obj.seizures_withdrawal_num_episodes):''} 
                        placeholder=" " 
                        manadatorySymbol={false} 
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}/>
                    </CCol>
                    <CCol xs="12" md="4" lg="12">
                        <label><b>Duration since Recent occurance</b></label>
                    </CCol>
                    <CCol xs="7" md="4" lg="4">                                        
                        <SingleTextInput 
                        name="seizures_withdrawal_recent_episode_duration" 
                        title=" " 
                        inputType="number" 
                        content={(props.obj.seizures_withdrawal_recent_episode_duration)?(props.obj.seizures_withdrawal_recent_episode_duration):''} 
                        placeholder=" " 
                        manadatorySymbol={false} 
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}/>
                    </CCol>
                    <CCol xs="5" md="4" lg="4"> 
                        <SelectBox
                            name="seizures_withdrawal_recent_episode_duration_uom"
                            title="Duration"
                            placeholder='Duration'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.handleChange}
                            options={appConstants.DURATION}
                            autosize={true}
                            manadatorySymbol={false}/>
                    </CCol>
                </CRow>}
                <CCard>
                <CRow>
                    <CCol xs="9" md="4" lg="8">
                        <SingleCheckbox 
                            controlFunc={props.controlFunc} 
                            name='check2_seizures'
                            type="checkbox" 
                            value='check2_seizures'
                            content={(props.obj.check2_seizures)?(props.obj.check2_seizures):''}
                        />
                    </CCol> 
                </CRow>
                </CCard>
                {props.obj.check2_seizures &&
                <CRow>
                    <CCol xs="12" md="4" lg="12">
                        <label><b>Duration since first occurance</b></label>
                    </CCol>
                    <CCol xs="7" md="4" lg="4">                                        
                        <SingleTextInput 
                        name="seizures_independent_first_episode_duration" 
                        title=" " 
                        inputType="number" 
                        content={(props.obj.seizures_independent_first_episode_duration)?(props.obj.seizures_independent_first_episode_duration):''} 
                        placeholder=" " 
                        manadatorySymbol={false} 
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}/>
                    </CCol>
                    <CCol xs="5" md="4" lg="4"> 
                        <SelectBox
                            name="seizures_independent_first_episode_duration_uom"
                            title="Duration"
                            placeholder='Duration'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc={props.controlFunc}
                            options={appConstants.DURATION}
                            autosize={true}
                            manadatorySymbol={false}/>
                    </CCol>
                    <CCol xs="7" md="4" lg="4">                                        
                        <SingleTextInput 
                        name="seizures_independent_num_episodes" 
                        title="Num Of Episodes" 
                        inputType="number" 
                        content={(props.obj.seizures_independent_num_episodes)?(props.obj.seizures_independent_num_episodes):''} 
                        placeholder=" " 
                        manadatorySymbol={false}
                        controlBlurFunc={props.controlBlurFunc} 
                        controlFunc={props.controlFunc}/>
                    </CCol>
                    <CCol xs="12" md="4" lg="12">
                        <label><b>Duration since Recent occurance</b></label>
                    </CCol>
                    <CCol xs="7" md="4" lg="4">                                        
                        <SingleTextInput 
                        name="seizures_independent_recent_episode_duration" 
                        title=" " 
                        inputType="number" 
                        content={(props.obj.seizures_independent_recent_episode_duration)?(props.obj.seizures_independent_recent_episode_duration):''} 
                        placeholder=" " 
                        manadatorySymbol={false}
                        controlBlurFunc={props.controlBlurFunc} 
                        controlFunc={props.controlFunc}/>
                    </CCol>
                    <CCol xs="5" md="4" lg="4"> 
                        <SelectBox
                            name="seizures_independent_recent_episode_duration_uom"
                            title="Duration"
                            placeholder='Duration'
                            controlBlurFunc={props.controlBlurFunc}
                            controlFunc = {props.controlFunc}
                            options={appConstants.DURATION}
                            autosize={true}
                            manadatorySymbol={false}/>
                    </CCol>
                </CRow>}
            <CRow>
                <CCol xs="8" md="4" lg="8">
                    <label><b>Seizure Type</b></label>
                </CCol>
            </CRow>
            <CRow>
                <CCol xs="6" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='gtcs'
                        title='Gtcs'
                        type="checkbox" 
                        value='gtcs'
                        //checked={props.obj.gtcs}
                        content={(props.obj.gtcs)?(props.obj.gtcs):''}
                    />
                </CCol>
                <CCol xs="6" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='cps'
                        title='Cps'
                        type="checkbox" 
                        value='cps'
                        //checked={props.obj.cps}
                        content={(props.obj.cps)?(props.obj.cps):''}
                    />
                </CCol>
                <CCol xs="6" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='cps_sec'
                        title='Cps Sec'
                        type="checkbox" 
                        value='cps_sec'
                        //checked={props.obj.cps_sec}
                        content={(props.obj.cps_sec)?(props.obj.cps_sec):''}
                    />
                </CCol>
                <CCol xs="6" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='other'
                        title='Other'
                        type="checkbox" 
                        value='Other'
                        //checked={props.obj.other}
                        content={(props.obj.other)?(props.obj.other):''}
                    />
                </CCol>
            </CRow>
            {(props.obj.other &&
            <CRow>
                <CCol xs="12" md="4" lg="8">
                    <SingleTextInput name="seizure_other"
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc={props.controlFunc}
                        title="Other" 
                        inputType="text" 
                        content={(props.obj.seizure_other)?(props.obj.seizure_other):''} 
                        placeholder="Other" 
                       
                    />  
                </CCol>
            </CRow>)}
            &nbsp;
            <CRow>
                    <CCol xs="9" md="4" lg="8">
                        <label>On Regular Antipileptic</label>
                    </CCol>
                    <CCol xs="3" md="4" lg="4">
                        {/*<ToggleSwitchWithText
                            name = 'regular_antipileptic'
                            content = {props.obj.regular_antipileptic?true:false}
                            controlBlurFunc = {props.controlBlurFunc}
                            trueText = "No"
                            falseText = "Yes"                                            
                        />*/}
                        <SingleCheckbox 
                            controlFunc={props.controlFunc} 
                            name='regular_antipileptic'
                            type="checkbox" 
                            value='regular_antipileptic'
                            content={(props.obj.regular_antipileptic)?(props.obj.regular_antipileptic):''}
                        /> 
                    </CCol> 
                </CRow>
                &nbsp;
                <CRow>
                    <CCol xs="6" md="8" lg="8">
                        <h3>DELIRIUM</h3>
                    </CCol> 
                </CRow>
                <CRow>    
                    <CCol xs="12" md="8" lg="8">
                        <SingleTextInput name="delirium" 
                        inputType="text" 
                        content={(props.obj.delirium)?(props.obj.delirium):''} 
                        placeholder=" " 
                        manadatorySymbol={false} 
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc = {props.controlFunc}
                        variant = "outlined"/>
                    </CCol> 
                </CRow> 
                &nbsp;
                <CRow>
                    <CCol xs="9" md="6" lg="6">
                        <label>Currently Present</label>
                    </CCol>
                    <CCol xs="3" md="6" lg="6">
                        <SingleCheckbox 
                            controlFunc={props.controlFunc} 
                            name='currently_present_delerium3'
                            type="checkbox" 
                            value='currently_present_delerium3'
                            content={(props.obj.currently_present_delerium3)?(props.obj.currently_present_delerium3):''}
                        /> 
                    </CCol>
                </CRow>
                <CRow>
                    <CCol xs="9" md="6" lg="6">
                        <label>Past Occurence</label>
                    </CCol>
                    <CCol xs="3" md="6" lg="6">
                        <SingleCheckbox 
                            controlFunc={props.controlFunc} 
                            name='past_occurance_delerium3'
                            type="checkbox" 
                            value='past_occurance_delerium3'
                            content={(props.obj.past_occurance_delerium3)?(props.obj.past_occurance_delerium3):''}
                        /> 
                    </CCol> 
                </CRow>  
                <CRow>
                    <CCol xs="9" md="6" lg="6">
                        <label>Altered Sensorium</label>
                    </CCol>
                    <CCol xs="3" md="6" lg="6">
                        <SingleCheckbox 
                            controlFunc={props.controlFunc} 
                            name='altered_sensorium'
                            type="checkbox" 
                            value='altered_sensorium'
                            content={(props.obj.altered_sensorium)?(props.obj.altered_sensorium):''}
                        />  
                    </CCol>
                </CRow> 
                <CRow>
                    <CCol xs="9" md="6" lg="6">
                        <label>Hallucinations</label>
                    </CCol> 
                    <CCol xs="3" md="6" lg="6">
                        <SingleCheckbox 
                            controlFunc={props.controlFunc} 
                            name='hallucinations'
                            type="checkbox" 
                            value='hallucinations'
                            content={(props.obj.hallucinations)?(props.obj.hallucinations):''}
                        /> 
                    </CCol> 
                </CRow> 
                { props.obj.hallucinations && 
                <CRow>
                    <CCol xs="12" md="6" lg="6">                                        
                        <SelectBox
                            name="hallucination_type"
                            title="Hallucinations Type"
                            placeholder='Hallucinations Type'
                            controlFunc={props.controlFunc}
                            options={appConstants.HALLUCINATION_TYPE}
                            autosize={true}
                            manadatorySymbol={false}/>
                    </CCol>
                    <CCol xs="12" md="6" lg="6">                                        
                    <SingleTextInput name="hallucinations_description" 
                        title="Hallucinations Description" 
                        inputType="text" 
                        content={(props.obj.hallucinations_description)?(props.obj.hallucinations_description):''} 
                        placeholder="Hallucinations Description" 
                        multiline={true}
                        rows={5}
                        variant = "outlined" 
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc = {props.controlFunc}
                        />
                    </CCol>
                </CRow>
                } 
                <CRow>
                    <CCol xs="6" md="6" lg="6">
                        <SingleTextInput name="age_of_fo" 
                        title="Age of FO" 
                        inputType="text" 
                        content={(props.obj.age_of_fo)?(props.obj.age_of_fo):''} 
                        placeholder="Age of FO" 
                        manadatorySymbol={false} 
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc = {props.controlFunc}
                        />
                    </CCol> 
                    <CCol xs="6" md="6" lg="6">
                        <SingleTextInput name="age_of_lo" 
                        title="Age of LO" 
                        inputType="text" 
                        content={(props.obj.age_of_lo)?(props.obj.age_of_lo):''} 
                        placeholder="Age of LO" 
                        manadatorySymbol={false} 
                        controlBlurFunc={props.controlBlurFunc}
                        controlFunc = {props.controlFunc}
                        />
                    </CCol> 
                </CRow> 
                &nbsp;           
                <CRow>
                    <CCol xs="9" md="6" lg="6">
                        <label>Autonomic Arousal</label>
                    </CCol>
                    <CCol xs="3" md="6" lg="6">
                        <SingleCheckbox 
                            controlFunc={props.controlFunc} 
                            name='automatic_arousal'
                            type="checkbox" 
                            value='automatic_arousal'
                            content={(props.obj.automatic_arousal)?(props.obj.automatic_arousal):''}
                        /> 
                    </CCol> 
                </CRow> 
        </div>
    </>
)
export default WithdrawalSymptomsSedativeHypnotics;