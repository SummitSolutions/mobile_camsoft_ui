import React from 'react'
import {
  CCol,CRow, CCard,
} from '@coreui/react'
import SingleTextInput from '../../form-components/SingleTextInput';
import SelectBox from '../../form-components/SelectBox';
import * as appConstants from './.././../../AppConstants';
import SingleCheckbox from '../../form-components/SingleCheckbox';


const WithdrawalSymptomsTobacco = (props) => (
    <> 
        <div> 
            <CRow>
                <CCol xs="8" md="4" lg="8">
                    <label><b>Symptoms A</b></label>
                </CCol>
            </CRow>
            <CRow>
            <CCol xs="12" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='irritablity'
                        title='Irritablity'
                        type="checkbox" 
                        value='irritablity'
                        content={(props.obj.irritablity)?(props.obj.irritablity):''}
                        />
                </CCol>  
                <CCol xs="12" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='frustration'
                        title='Frustration'
                        type="checkbox" 
                        value='frustration'
                        content={(props.obj.frustration)?(props.obj.frustration):''}
                    />
                </CCol>         
                <CCol xs="12" md="4" lg="3">             
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='anger'
                        title='Anger'
                        type="checkbox" 
                        value='anger'
                        content={(props.obj.anger)?(props.obj.anger):''}
                    />
                </CCol>
                <CCol xs="12" md="4" lg="3">             
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='anxiety'
                        title='Anxiety'
                        type="checkbox" 
                        value='anxiety'
                        content={(props.obj.anxiety)?(props.obj.anxiety):''}
                    />
                </CCol> 
                <CCol xs="12" md="4" lg="3">             
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='difficulty_concentrating'
                        title='Difficulty concentrating'
                        type="checkbox" 
                        value= 'difficulty_concentrating'
                        content={(props.obj.difficulty_concentrating)?(props.obj.difficulty_concentrating):''}
                    />
                </CCol>
                <CCol xs="12" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='increased_appetite'
                        title='Increased appetitet'
                        type="checkbox" 
                        value='increased_appetite'
                        content={(props.obj.increased_appetite )?(props.obj.increased_appetite):''}
                    />
                </CCol>  
                <CCol xs="12" md="4" lg="3">             
                <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='restlessness'
                        title='Restlessness'
                        type="checkbox" 
                        value='restlessness'
                        content={(props.obj.restlessness)?(props.obj.restlessness):''}
                    /> 
                </CCol>
                <CCol xs="12" md="4" lg="3">  
                <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='depressed_mood'
                        title='Depressed mood'
                        type="checkbox" 
                        value= 'depressed_mood'
                        //checked={props.obj.head_aches}
                        content={(props.obj.depressed_mood)?(props.obj.depressed_mood):''}
                    /> 
                </CCol> 
                <CCol xs="12" md="4" lg="3"> 
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='insomnia'
                        title='Insomnia'
                        type="checkbox" 
                        value='insomnia'
                        content={(props.obj.insomnia)?(props.obj.insomnia):''}
                    />
                </CCol> 
                <CCol xs="12" md="4" lg="3">        
                    <SingleCheckbox 
                        controlFunc={props.controlFunc} 
                        name='constipation'
                        title='Constipation'
                        type="checkbox" 
                        value='nausea_vomitting'
                        content={(props.obj.constipation)?(props.obj.constipation):''}
                    />
                </CCol>   
            </CRow>
        </div>
    </>
)
export default WithdrawalSymptomsTobacco;