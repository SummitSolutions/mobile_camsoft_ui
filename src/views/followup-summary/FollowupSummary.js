import React, { useEffect, useState, useRef, Fragment } from 'react'
import { CCard, CCardBody, CCardFooter, CCardHeader, CCol, CFormText, CLabel, CRow, CFormGroup, CButton } from '@coreui/react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router";
import 'semantic-ui-css/semantic.min.css'
import SingleTextInput from '../form-components/SingleTextInput';
import CheckboxList from '../form-components/CheckboxList';
import RadioButtonList from '../form-components/RadioButtonList';
import SelectBox from '../form-components/SelectBox';
import Divider from '../form-components/Divider';
import SimpleReactValidator from 'simple-react-validator';
import FollowupService from '../../api/FollowupService';
import Icon from '@material-ui/core/Icon';
import * as appConstants from './../../AppConstants';
import DragAndDropFileUpload from '../form-components/DragAndDropFileUpload';
import TextArea from '../form-components/TextArea';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus } from '@fortawesome/free-solid-svg-icons';

export default function CreateFollowupComponent() {
    const { register, handleSubmit, errors, getValues, watch, formState } = useForm();
    const history = useHistory();
    const params = useParams();
    const [followup, setFollowup] = useState({
        pulse: '', mode: '', cgi_improvement: '', bp: '', weight: '', compliance: '', notes: '', informant_name: '',
        informant_relation: '', followup_mode: '', follow_up_date: '', reason: '', description: '',
        agency: '', expiry_date: '', expiry_reason: '',
    });
    const [followupParms, setfollowupParms] = useState({
        selectedHold: [],
    });

    const validator = useRef(new SimpleReactValidator());
    const [, forceUpdate] = useState();
    useEffect(() => {
        console.log("Patient Create page loaded. Msg. from UseEffect")

        getFollowupById();
    }, []);
    let formData = new FormData();


    const getFollowupById = () => {
        if (params.id > 0) {
            FollowupService.getFollowupById(params.id).then(resp => {
                if (resp.data.data.id) {
                    if (resp.data.data.issue_date) {
                        resp.data.data.issue_date = moment(resp.data.data.issue_date).locale('en').format('YYYY-MM-DD');
                    }
                    setFollowup(resp.data.data)
                    console.log("Patient Data retrieved Successfully")
                }
            });
        }
    }

    const saveOrUpdateReport = (data, e) => {
        if (params.id) {
            //console.log('Patient data updating..: ', data)
            e.preventDefault();
            FollowupService.updateFollowup(data, params.id).then(res => {
                //console.log(res)
                setFollowup({});
                history.replace('/patient/List');
            });
        }
        else {
            console.log('Patient data saving')
            e.preventDefault();
            //console.log(data);
            FollowupService.createFollowup(data).then(res => {
                console.log(res)
                //setFollowup({});
                history.replace('/patient/List');
            });
        }
    }

    const validateForm = (e) => {
        console.log(followup)
        e.preventDefault();
        if (validator.current.allValid()) {
            console.log('Update is calling')
            saveOrUpdateReport(followup, e)
        } else {
            console.log('Create is calling')
            validator.current.showMessages();
            forceUpdate(1)
        }
    }

    const handleChange = (e) => {
        console.log(e)
        setFollowup({ ...followup, [e.target.name]: e.target.value });
        console.log(followup)
    }


    const checkboxChangeHandler = (e) => {
        const newSelection = e.target.value;
        let newSelectionArray;
        switch (e.target.name) {
            case 'followup_hold': if (followupParms.selectedHold.indexOf(newSelection) > -1) {
                newSelectionArray = followupParms.selectedHold.filter(s => s !== newSelection)
            } else {
                newSelectionArray = [...followupParms.selectedHold, newSelection];
            }
                followupParms.selectedHold = newSelectionArray;
                setFollowup({ ...followup, [e.target.name]: followupParms.selectedHold.join(", ") });
                break;
        }
    }
    return (
        <form onSubmit={validateForm} className="form-horizontal">
            <CCard accentColor="primary">
                <CCardHeader>
                    <h3>Followup Summary</h3>
                </CCardHeader>
                <CCardBody>
                    <CRow>
                        <CCol xs="12" md="3" lg="6">
                            <CCard borderColor="primary">
                                <CCardBody>
                                    <CRow>
                                        <CCol xs="12" md="4" lg="4">
                                            <SelectBox
                                                name="mode"
                                                title="Mode"
                                                placeholder='Mode'
                                                controlFunc={handleChange}
                                                options={appConstants.MODE}
                                                selectedOption={followup.mode ? followup.mode : 'OP'}
                                            />
                                        </CCol>
                                        <CCol xs="12" md="4" lg="4">
                                            {/* <CButton color="info">
                                                                <FontAwesomeIcon icon={faPencilAlt}/>
                                                         </CButton> */}
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="12" md="4" lg="4">
                                            <SelectBox
                                                name="cgi_improvement"
                                                title="CGI improvement"
                                                placeholder='CGI improvement'
                                                controlFunc={handleChange}
                                                options={appConstants.CGI}
                                                selectedOption={followup.cgi_improvement ? followup.cgi_improvement : ''}
                                            />
                                        </CCol>

                                        <CCol xs="12" md="3" lg="3">
                                            <SingleTextInput name="pulse"
                                                title="Pulse"
                                                inputType="text"
                                                content={(followup.pulse) ? (followup.pulse) : ''}
                                                placeholder="Pulse"
                                                manadatorySymbol={false}
                                                controlFunc={handleChange} />
                                        </CCol>
                                        <CCol xs="12" md="4" lg="2">
                                            <SingleTextInput name="bp"
                                                title="BP"
                                                inputType="text"
                                                content={(followup.bp) ? (followup.bp) : ''}
                                                placeholder="BP"
                                                manadatorySymbol={false}
                                                controlFunc={handleChange} />
                                        </CCol>
                                        <CCol xs="12" md="4" lg="3">
                                            <SingleTextInput name="weight"
                                                title="Weight"
                                                inputType="text"
                                                content={(followup.weight) ? (followup.weight) : ''}
                                                placeholder="Weight"
                                                manadatorySymbol={false}
                                                controlFunc={handleChange} />
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="12" md="4" lg="10">
                                            <RadioButtonList
                                                name="compliance"
                                                title="Compliance"
                                                controlFunc={handleChange}
                                                options={appConstants.COMPLIANCE}
                                                content={followup.compliance}
                                                manadatorySymbol={false} />
                                            {/* {validator.current.message('compliance', patient.compliance, 'required')} */}
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="12" md="4" lg="10">
                                            <SingleTextInput name="complication"
                                                title="Complication"
                                                inputType="text"
                                                content={(followup.complication) ? (followup.complication) : ''}
                                                placeholder="Complication"
                                                manadatorySymbol={false}
                                                controlFunc={handleChange}
                                                multiline={true}
                                                rows={3}
                                                variant="outlined" />
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="12" md="4" lg="10">
                                            <SingleTextInput name="notes"
                                                title="Notes"
                                                inputType="text"
                                                content={(followup.notes) ? (followup.notes) : ''}
                                                placeholder="Notes"
                                                manadatorySymbol={false}
                                                controlFunc={handleChange}
                                                multiline={true}
                                                rows={3}
                                                variant="outlined" />
                                        </CCol>
                                    </CRow>
                                </CCardBody>
                            </CCard>
                        </CCol>
                        <CCol xs="12" md="3" lg="6">
                            <CCard borderColor="primary">
                                <CCardBody>
                                    <CRow>
                                        <CCol xs="12" md="4" lg="6">
                                            <SelectBox
                                                name="followup_mode"
                                                title="Next followup Mode"
                                                placeholder='Next followup Mode'
                                                controlFunc={handleChange}
                                                options={appConstants.FOLLOWUPMODE}
                                                selectedOption={followup.followup_mode ? followup.followup_mode : ''}
                                                manadatorySymbol={true}
                                            />
                                            {validator.current.message('followup_mode', followup.followup_mode, 'required')}
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="8" md="3" lg="4">
                                            <SingleTextInput name="follow_up_date"
                                                title="Next followup date"
                                                inputType="date"
                                                content={(followup.follow_up_date) ? (followup.follow_up_date) : appConstants.FOLLOWUP_DATE}
                                                placeholder="Next followup date"
                                                manadatorySymbol={true}
                                                controlFunc={handleChange} />
                                            {validator.current.message('follow_up_date', followup.follow_up_date, 'required')}

                                            <CheckboxList
                                                name="followup_hold"
                                                // title="Followup on hold"
                                                controlFunc={checkboxChangeHandler}
                                                options={appConstants.FOLLOWUP_HOLD}
                                                selectedOptions={followupParms.selectedHold}
                                                manadatorySymbol={false} />
                                        </CCol>
                                        {(followupParms.selectedHold.includes('Followup on hold')) &&
                                            <CCol xs="12" md="4" lg="3">
                                                <SelectBox
                                                    name="reason"
                                                    title="Reason"
                                                    placeholder='Reason'
                                                    controlFunc={handleChange}
                                                    options={appConstants.REASON}
                                                    selectedOption={followup.reason ? followup.reason : ''}
                                                />
                                            </CCol>}
                                        {followup.reason == 'Expired' &&
                                            <SingleTextInput name="expiry_date"
                                                title="Expiry date"
                                                inputType="date"
                                                content={(followup.expiry_date) ? (followup.expiry_date) : appConstants.EXPIRY_DATE}
                                                placeholder="Expiry date"
                                                manadatorySymbol={false}
                                                controlFunc={handleChange} />}
                                        <CCol xs="12" md="4" lg="10">
                                            {followup.reason == 'Expired' &&
                                                <SingleTextInput name="expiry_reason"
                                                    title="Expiry Reason"
                                                    inputType="text"
                                                    content={(followup.expiry_reason) ? (followup.expiry_reason) : ''}
                                                    placeholder="Expiry Reason"
                                                    manadatorySymbol={false}
                                                    controlFunc={handleChange}
                                                    multiline={true}
                                                    rows={3}
                                                    variant="outlined" />}
                                        </CCol>
                                        <CCol xs="12" md="4" lg="10">
                                            {followup.reason == 'Under treatment from other agency' &&
                                                <SingleTextInput name="agency"
                                                    title="Agency"
                                                    inputType="text"
                                                    content={(followup.agency) ? (followup.agency) : ''}
                                                    placeholder="Agency"
                                                    manadatorySymbol={false}
                                                    controlFunc={handleChange}
                                                    multiline={true}
                                                    rows={6}
                                                    variant="outlined" />}
                                        </CCol>
                                        <CCol xs="12" md="4" lg="10">
                                            {followup.reason == 'Others' &&
                                                <SingleTextInput name="description"
                                                    title="Description"
                                                    inputType="text"
                                                    content={(followup.description) ? (followup.description) : ''}
                                                    placeholder="Description"
                                                    manadatorySymbol={false}
                                                    controlFunc={handleChange}
                                                    multiline={true}
                                                    rows={6}
                                                    variant="outlined" />}
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="12" md="4" lg="12">
                                            <SingleTextInput name="informant_name"
                                                title="Informant Name"
                                                inputType="text"
                                                content={(followup.informant_name) ? (followup.informant_name) : ''}
                                                placeholder="Informant Name"
                                                manadatorySymbol={false}
                                                controlFunc={handleChange} />
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    <CRow>
                                        <CCol xs="12" md="4" lg="12">
                                            <SingleTextInput name="informant_relation"
                                                title="Informant Relation"
                                                inputType="text"
                                                content={(followup.informant_relation) ? (followup.informant_relation) : ''}
                                                placeholder="Informant Relation"
                                                manadatorySymbol={false}
                                                controlFunc={handleChange} />
                                        </CCol>
                                    </CRow>
                                    <br></br>
                                    {/*<CRow>
                                                <CCol xs="12" md="4" lg="6">           
                                                    <SelectBox
                                                    name="research_thesis"
                                                    title="Consider for research or thesis"
                                                    placeholder='Consider for research or thesis'
                                                    controlFunc={handleChange}
                                                    options={appConstants.RESEARCH}
                                                    selectedOption={followup.research_thesis?followup.research_thesis:''}
                                                    />
                                                </CCol>
                                             </CRow> */}
                                    <br></br>
                                </CCardBody>
                            </CCard>
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
            <input type="submit" className="btn btn-sm btn-primary float-right" value={(params.id) ? 'Update' : 'Submit'} />
        </form>
    )
}




