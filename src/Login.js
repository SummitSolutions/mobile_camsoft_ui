import React, { useEffect, useState, useRef, useContext } from 'react';
import { Link } from 'react-router-dom';
import { CButton, CCard, CCardBody, CCardGroup, CCol, CContainer, CForm, CInput,
    CInputGroup, CInputGroupPrepend, CInputGroupText, CRow } from '@coreui/react';
import { useHistory } from 'react-router-dom';
import CIcon from '@coreui/icons-react';
import SimpleReactValidator from 'simple-react-validator';
import LoginService from './api/LoginService';
import GlobalState from './shared/GlobalState';
import * as SharedService from './shared/SharedService';
import navigation from './containers/_nav';
import * as permissions from './Permissions';

const Login = () => {

    const history = useHistory();
    const [loginObj, setLoginObj] = useState({});
    const validator = useRef(new SimpleReactValidator());
    const [, forceUpdate] = useState();
    const [state, setState] = useContext(GlobalState);

    useEffect(() => {
        //setState(state => ({...state, authHeader: {'headers' : {'Authentication-Token': ''}}}))
        console.log(state)
    }, []);
    
    const login = () => {
        console.log("login is called")
        LoginService.login(loginObj).then(res =>{
            state.authHeader = SharedService.getAuthHeaders(res.data.fs_uniquifier);
            state.loggedUser = res.data;
            state.leftNavMenuItems = permissions?.getRoutesByRole(navigation, res.data.roles);
            setState(state => ({ ...state, authHeader: SharedService.getAuthHeaders(res.data.fs_uniquifier) }));
            setState(state => ({ ...state, loggedUser :  res.data} ));
            setState(state => ({ ...state, leftNavMenuItems: permissions?.getRoutesByRole(navigation, res.data.roles) }));
            setState(state => ({ ...state, isAuthenticated :  true} ));
            let leaveModule = state.leftNavMenuItems.find(item => item.route == '/leave');
            if(leaveModule)
            {
                if(leaveModule._children.find(element => element.value == "ManageLeaves"))
                {
                    history.replace('/leave/manageleaves');
                }
                else{
                    history.replace('/leave/myleaves');
                }
            }
            else
            {
                history.replace('/patient/list');
            }
            console.log("logged in")
        }).catch(error => { 
            console.log(error)
            // your error handling goes here
        })
    }
    
    const handleChange = (e) => {
        setLoginObj({...loginObj, [e.target.name] : e.target.value});
    }

    const validateForm = (e) => {
        e.preventDefault();
        if (validator.current.allValid()) {
            console.log('valid ')
            login()
        } else {
            console.log('Not valid')
            validator.current.showMessages();
            forceUpdate(1)
        }
    }

    return (
        <div className="c-app c-default-layout flex-row align-items-center">
            <CContainer>
                <CRow className="justify-content-center">
                    <CCol md="8">
                        <CCardGroup>
                            <CCard className="p-4">
                                <CCardBody>
                                    <form onSubmit={validateForm}>
                                        <h1>Login</h1>
                                        <p className="text-muted">Sign In to your account</p>
                                        <CInputGroup className="mb-3">
                                            <CInputGroupPrepend>
                                                <CInputGroupText>
                                                    <CIcon name="cil-user" />
                                                </CInputGroupText>
                                            </CInputGroupPrepend>
                                            <CInput type="text" placeholder="Employee ID" name="username" onChange={handleChange} />
                                        </CInputGroup>
                                            {validator.current.message('username', loginObj.username, 'required')}
                                        <CInputGroup className="mb-4">
                                            <CInputGroupPrepend>
                                                <CInputGroupText>
                                                    <CIcon name="cil-lock-locked" />
                                                </CInputGroupText>
                                            </CInputGroupPrepend>
                                            <CInput type="password" placeholder="Password" autoComplete="current-password" name="password" onChange={handleChange}  />
                                        </CInputGroup>
                                            {validator.current.message('password', loginObj.password, 'required')}
                                        <CRow>
                                            <CCol xs="6" className="text-right">
                                                <CButton color="link" className="px-0">Forgot password?</CButton>
                                            </CCol>
                                            <CCol xs="6" sm="6" md="5" lg="4" xl="4">
                                                <CButton type="submit" color="primary" className="px-4">Login</CButton>
                                            </CCol>
                                        </CRow>
                                    </form>
                                </CCardBody>
                            </CCard>
                            <CCard className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                                <CCardBody className="text-center">
                                    <div>
                                        <h2>Welcome to Camsoft!!</h2>
                                        {/*<p></p>
                                        <Link to="/register">
                                            <CButton color="primary" className="mt-3" active tabIndex={-1}>
                                                Register Now!
                                            </CButton>
                                        </Link>*/}
                                    </div>
                                </CCardBody>
                          </CCard>
                        </CCardGroup>
                    </CCol>
                </CRow>
            </CContainer>
        </div>
        )
    }

export default Login
