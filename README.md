# Installations Steps follows here:

# Step 1 => download and Install node.js if not installed.
https://nodejs.org/en/download/

# Step 2 => open Command prompt
$node -v
$v12.19.0
$npm -v
6.14.8
$npm install -g npm

# Step 3 => Install Create-React-App
# In order to install your app, first go to your workspace (D:/react-apps or a folder) and run the following command:
# this will install and create react project
$npx create-react-app app_name
# The installation process may take a few minutes
# this will generate node_modules by default

# to runn already existing project just run
$npx create-react-app

# step 4 => How to Run the App You Created with Create-React-App
$cd app_name

# to install all necessary node package run the below command 
$npm install

# finally run npm start to see your app live on localhost
$npm start
# opens in brower http://localhost:3000